#!/bin/bash

source ../.env.dev

export K6_INFLUXDB_USERNAME=$INFLUXDB_WRITE_USER
export K6_INFLUXDB_PASSWORD=$INFLUXDB_WRITE_USER_PASSWORD
export K6_INFLUXDB_INSECURE=true

k6 run --out influxdb=http://localhost:8086/$INFLUXDB_DB performance_testing.js