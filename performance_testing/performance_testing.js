import http from 'k6/http';
import { check, sleep, group } from 'k6';
import { b64encode } from 'k6/encoding';

// Uncomment to sent all the requests per VU at one batch, otherwise it sends request for one sensor type after the other
const PERFORM_BATCH_REQUESTS = false

// Time each VU will wait before the next iteration (before sending the next (batch) of requests)
const SLEEP_TIME_S = 0.5

function getTimestamp() {
  return Date.now() / 1000
}

// init code
export let options = {
  vus: 50,
  duration: '60s',
};

const TestDefinition = {
  ShellyRed: {
    apiEndpoint: '/sensors/shelly-red',
    dto_list: JSON.parse(open('./dtos/shelly-red-dto-list.json'))
  },
  ShellyEM: {
    apiEndpoint: '/sensors/shelly-em',
    dto_list: JSON.parse(open('./dtos/shelly-em-dto-list.json'))
  },
  Door: {
    apiEndpoint: '/sensors/door',
    dto_list: JSON.parse(open('./dtos/door-dto-list.json'))
  },
  Environment: {
    apiEndpoint: '/sensors/environment',
    dto_list: JSON.parse(open('./dtos/environment-dto-list.json'))
  },
  Camera: {
    apiEndpoint: '/sensors/camera',
    dto_list: JSON.parse(open('./dtos/camera-dto-list.json'))
  },
  /*
  Emfit: {
    apiEndpoint: '/sensors/emfit-data',
    dto_list: JSON.parse(open('./dtos/emfit-dto-list.json'))
  },
  */
  Lidar: {
    apiEndpoint: '/sensors/lidar',
    dto_list: JSON.parse(open('./dtos/lidar-dto-list.json'))
  },
  Luminance: {
    apiEndpoint: '/sensors/luminance',
    dto_list: JSON.parse(open('./dtos/luminance-dto-list.json'))
  },
  WaterFlow:{
    apiEndpoint: '/sensors/waterflow',
    dto_list: JSON.parse(open('./dtos/waterflow-dto-list.json'))
  },
}

const SENSOR_TYPES = Object.keys(TestDefinition)

const HOST = 'http://localhost:4000'

const image = open('./test-data/kitchen-1.jpg', 'b')

var params = {
  headers: {
    'Content-Type': 'application/json'
  }
}

export default function () {
  // vu code (run over and over for as long as test is running)

  group('post sensor data', function() {

    const requests = []

    for(let i=0; i<SENSOR_TYPES.length; i++) {
      const endpoint = TestDefinition[SENSOR_TYPES[i]].apiEndpoint
      const dto_list = TestDefinition[SENSOR_TYPES[i]].dto_list

      // Update timestamp with current timestamp
      dto_list.forEach(dto => {
        dto.timestamp = getTimestamp()
      })

      // Add image data for camera
      if(SENSOR_TYPES[i] == 'Camera') {
        dto_list.forEach(dto => {
          dto.data = b64encode(image)
        })
      }

      if(PERFORM_BATCH_REQUESTS) {
        requests.push({
          method: 'POST',
          url: `${HOST}${endpoint}`,
          body: JSON.stringify(dto_list),
          params: params
        })
      } else {
        let res = http.post(`${HOST}${endpoint}`, JSON.stringify(dto_list), params);
        check(res, { 'status was 201': (r) => r.status == 201 });
      }
    }

    if(PERFORM_BATCH_REQUESTS) {
      let responses = http.batch(requests);
      for(let i=0; i<responses.length; i++) {
        check(responses[i], { 'status was 201': (r) => r.status == 201 });
      }
    }
  })

  // VU without any sleep() is akin to a user who constantly presses F5 to refresh the page.
  sleep(SLEEP_TIME_S);
}