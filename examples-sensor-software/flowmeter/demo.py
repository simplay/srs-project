import RPi.GPIO as GPIO

import os
import time
from datetime import datetime
import sys

GPIO.setmode(GPIO.BOARD)
INPUT_PIN = 13
GPIO.setup(INPUT_PIN, GPIO.IN)

# minuates passed since starting the program
uptime = 0

# the YF-S201 creates 6 interrupts per revolution
PULSES_TO_LITERS = 0.006
next_time_measurement = 0.0
report_interval_in_seconds = 1

global rate_count, total_count
rate_count = 0
total_count = 0

def count_pulses(inpt_pin):
    global rate_count, total_count
    rate_count += 1
    total_count += 1

GPIO.add_event_detect(INPUT_PIN, GPIO.FALLING, callback=count_pulses, bouncetime=10)

print("Starting flow-sensor measurements at ", time.asctime(time.localtime(time.time())))

while True:
    dt = time.time() + report_interval_in_seconds
    rate_count = 0
    while time.time() <= dt:
        try:
            input_msg = "Input on pin " + str(INPUT_PIN) + ": "  + str(GPIO.input(INPUT_PIN))
        except KeyboardInterrupt:
            GPIO.cleanup()
            sys.exit()

    now = datetime.now()
    timestamp = datetime.timestamp(now)

    frequency = str(rate_count / report_interval_in_seconds)
    pulses = str(rate_count)
    uptime += 1

    print(f"{now} - pulses: {frequency} Freq: {frequency}")
