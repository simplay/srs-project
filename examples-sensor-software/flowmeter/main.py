import RPi.GPIO as GPIO

from dotenv import load_dotenv
import os
import time
from datetime import datetime
import sys
import socket
import requests
import json

load_dotenv()

# E.g. 192.168.178:4000 # without the http://
BACKEND_URL = os.getenv("BACKEND_URL")
LOCATION = os.getenv("SENSOR_LOCATION")
SAMPLING_FREQUENCY = float(os.getenv("SAMPLING_FREQUENCY"))
HOST_IP = os.getenv("HOST_IP")


if HOST_IP:
  ip_address = HOST_IP
else:
    socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        socket.connect(("8.8.8.8", 80))
        ip_address = socket.getsockname()[0]
    except Exception:
        ip_address = '127.0.0.1'
    finally:
        socket.close()

GPIO.setmode(GPIO.BOARD)
INPUT_PIN = 13
GPIO.setup(INPUT_PIN, GPIO.IN)

# in seconds
SLEEP_TIME = 1.0 / SAMPLING_FREQUENCY

headers = {"Content-Type": "application/json"}
url = f"http://{BACKEND_URL}/sensors/waterflow"

print(f"Sending data to {url}")

# minuates passed since starting the program
uptime = 0

# the YF-S201 creates 6 interrupts per revolution
PULSES_TO_LITERS = 0.006
next_time_measurement = 0.0
report_interval_in_seconds = 1

global rate_count, total_count
rate_count = 0
total_count = 0

def count_pulses(inpt_pin):
    global rate_count, total_count
    rate_count += 1
    total_count += 1

GPIO.add_event_detect(INPUT_PIN, GPIO.FALLING, callback=count_pulses, bouncetime=10)

print("Starting flow-sensor measurements at ", time.asctime(time.localtime(time.time())))

while True:
    dt = time.time() + report_interval_in_seconds
    rate_count = 0
    while time.time() <= dt:
        try:
            input_msg = "Input on pin " + str(INPUT_PIN) + ": "  + str(GPIO.input(INPUT_PIN))
        except KeyboardInterrupt:
            GPIO.cleanup()
            sys.exit()

    now = datetime.now()
    timestamp = datetime.timestamp(now)

    frequency = float(rate_count / report_interval_in_seconds)
    pulses = float(rate_count)
    params = [
        {
            'frequency': frequency,
            'pulses': pulses,
            'timestamp': float(timestamp),
            'location': LOCATION,
            'ip': ip_address
        }
    ]
    uptime += 1

    print(f"{now} - pulses: {frequency} Freq: {frequency}")

    try:
        response = requests.post(
            url, 
            data=json.dumps(params), 
            headers=headers
        )

        if response.status_code == 201:
            print(response)
        else:
            print(response.text)

    except Exception as e:
        print(e)
        print("Connection error - reconnecting to backend server...")

        time.sleep(SLEEP_TIME)
