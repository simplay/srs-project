# Flowmeter-Sensor

![Wiring of Raspberry Pi 3B+](schema.jpg)

Make sure that you have enabled the `I2C` interface (e.g., via `raspi-config`)

## Wiring

+ Pin 2 5V Power
+ Pin 6 GND
+ Pin 13: Input

Input goes through a voltage divider circuit (otherwise 5V might kill the PI). Input 4.7k ohm resistor -> PIN 13 -> 10k ohm resistor -> GND

## Run via Docker

```
Execute sudo ./run.sh
```

## Run Locally

### Setup

```
sudo apt-get install libgpiod2
pip3 install -r requirements.txt
cp .env.example .env # fill in the env variables
```

### Usage

Execute

```
python3 main.py
```

to measure and send samples to the specified backend server.

Execute

```
python3 demo.py
```

to test the sensor.
