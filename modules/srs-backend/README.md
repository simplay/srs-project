# SRS-Backend

The `SRS-Backend` is the entry point for the SRS project. The service is built upon `Nest`.
Nest (NestJS) is a framework for building efficient, scalable Node.js server-side applications.

[Access NestJS Documentation](https://docs.nestjs.com/)

## Initial setup

### Software prerequisites

* [Node](https://nodejs.org/)
* [npm](https://www.npmjs.com/)
* [mongodump](https://github.com/mongodb/homebrew-brew)

### Sensors Config File

The available sensor files can be found in the module `neurotec-sensors` (files that are named like `*.sensors.json`)
. The path to a target sensor-file has to be specified via the environment variable `PIPELINE_SENSORS_FILEPATH`. Details
regarding that variable can be found below.

The `SRS-Backend` expects a default config defining the sensors available for the experiment.

A sample is provided in `sensors.json.example`. Adapt this file to your need.

```shell script
cp sensors.json.example sensors.json
```

### Certificates for JWT

This project makes use of authentication using JWT. A key pair is required to manage the JWT (generation / validation).
Use the following script to generate a new key/pair.

```shell script
cd certs
./generate_jwt_certificates.sh
```

### Environment variables

The following environment variables are expected to be defined by the service and are checked at application start. You
can define those through your IDE (if running in DEV mode) or passing them to the docker container (in PROD).

- SMS_JWT_PRIVATE_KEY_FILEPATH: path to JWT private key (e.g.: `/certs/jwt_private_key_dev.pem`)
- SMS_JWT_PUBLIC_KEY_FILEPATH:  path to JWT public key (e.g.: `/certs/jwt_public_key_dev.pem`)
- SMS_JWT_VALIDITY_LENGTH: validity period for the JWT (e.g.: "1m", "2 days", "10h", "7d")
- MONGODB_USER: user to connect to MongoDB,
- MONGODB_PASSWORD: password to connect to MongoDB,
- MONGODB_HOST: host for MongoDB (`localhost` if running locally, `mp-mongo` if running from inside docker),
- MONGODB_HOST: host for MongoDB (`localhost` if running locally, `mp-mongo` if running from inside docker),
- PIPELINE_SENSORS_FILEPATH: relative path to the a `*.sensors.json` file, located in the submodule `neurotec-sensors`.
  E.g., PIPELINE_SENSORS_FILEPATH="submodules/neurotec-sensors/neurotec.sensors.json"
- DEBUG_LOG_REQUESTS_SENSORS_MODULE: boolean which will toggle detailed log of the incoming/outcoming request of the
  sensors module
- SMS_SMTP_HOST: SMTP host used for sending email (e.g. "cicero.metanet.ch")
- SMS_SMTP_USER: SMTP user used for sending mail
- SMS_SMTP_PASSWORD: password of the user used for sending mail
- NODE_ENV: production/development, used to distinguish production from development (e.g. in prod we don't build the
  open api documentation)

### Install dependencies and run the app.

```shell script
npm install

npm run start:dev
```

### Run using docker

If you want to run using docker, proceed as follow:

```shell script
cp .env.README .env
./run-via-docker.sh
```

## API Documentation

This project generates a documentation for the REST API using Swagger. Once running, the documentation is exposed at:
`http://localhost:4000`

## Websockets

The `SRS-Backend` exposes websockets to get updated with:

- the latest status of the sensors,
- preview of the cameras,
- current status of the running/active experiment.

Websockets for sensors status and cameras preview require authentication.

| Gateway/websocket namespace | Port  | Authentication required |
| --------------------------- | ----- | ----------------------- |
| cameras                     | 4222  | Yes                     |
| sensors                     | 4222  | Yes                     |
| active-experiment-status    | 4111  | No                      |

Access to the cameras websocket is traced in the logbook. In order to distinguish between the sensors and cameras
gateway, the query param "name" is expected to be sent with the name of the socket ("sensors" or "cameras"). See
implementation of
[`AuthenticatedSocketIoAdapter`](/src/authenticated-socket-io.adapter.ts) for details
and [e2e tests for websockets](test/websockets.e2e-spec.ts)
for examples how to connect from clients.

## Validation

Validation of input data (DTOs) is done using the NestJS validation pipe which uses the `class-validator` under the
hood. For the list of available decorators, visit this page:
[https://www.npmjs.com/package/class-validator](https://www.npmjs.com/package/class-validator)

## Tests

Unit tests cover controllers and service tests, where internal dependencies are mocked. E2E tests cover module testing,
including validation of the DTOs.

### Unit tests

Run the following command to run the unit tests:

```shell script
npm run test
```

Or, to reload the tests upon changes (during development):

```shell script
npm run test:watch
```

Hint: use 'p' to narrow the tests to the module you are currently developing.

### E2E tests

E2E tests check:

- validation of the DTOs
- roles and permissions to perform actions via the various endpoints
- security of the endpoints (whether they require authentication via a valid JWT or not)

Make use of the following npm command to run the E2E tests:

```shell script
npm run test:e2e
```

Or this one if you want to reload upon changes:

```shell script
npm run test:e2e:watch
```

Or run this script to run all the E2E tests:

```shell scrips
./run_tests.sh
```

## Backups / Export

```shell script
cd scripts
./mongo_export.sh
```
