#!/bin/bash

source ../.env

backup_dir=$(date +'%Y-%m-%d_%H-%M-%S')

echo "Exporting dump to ../backups/${dirname}"
mongodump --host=localhost --port=27017 --username=${MONGODB_USER} --password=${MONGODB_PASSWORD} --out=../backups/${backup_dir}_compressed
