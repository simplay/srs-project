import enum

import requests
import json

JWT = """YOUR_JWT_TOKEN"""
HOST = 'http://localhost:4000'


class SensorType(enum.Enum):
    CAMERA = 1
    DOOR = 2
    ENVIRONMENT = 3
    LIDAR = 4
    LUMINANCE = 5
    NIGHT_VISION_CAMERA = 6
    SHELLY_EM = 7
    SHELLY_RED = 8
    WATERFLOW = 9


class SensorMonitoringAPIEndpoints(enum.Enum):
    CAMERA = 'sensors/camera'
    DOOR = "sensors/door"
    ENVIRONMENT = 'sensors/environment'
    LIDAR = 'sensors/lidar'
    LUMINANCE = 'sensors/luminance'
    NIGHT_VISION_CAMERA = "sensors/night-vision-camera"
    SHELLY_EM = 'sensors/shelly-em'
    SHELLY_RED = 'sensors/shelly-red'
    WATERFLOW = "sensors/waterflow"


def get_bearer_authorization():
    return "Bearer {}".format(JWT)


def post_sensor_data(dto_list, sensor_type_code):
    url = "{0}/{1}".format(HOST, SensorMonitoringAPIEndpoints[sensor_type_code].value)
    headers = {
        "Content-Type": "application/json",
        # "Authorization": get_bearer_authorization()
    }
    r = requests.post(url, data=json.dumps(dto_list), headers=headers)
    return r
