import json
import time
import base64

from sensor_monitoring_api.restclient import post_sensor_data, SensorType


def read_json_file(file_path):
    with open(file_path)  as json_file:
        data = json.load(json_file)
        return data


def get_current_timestamp():
    # Number of seconds elapsed since epoch
    return time.time()


def adapt_to_current_timestamp(dto_list):
    for dto in dto_list:
        dto['timestamp'] = get_current_timestamp()

    return dto_list


sensor_type_sensor_data = {
    SensorType.CAMERA.name: "example-dtos/camera-dto-list.json",
    SensorType.DOOR.name: "example-dtos/door-dto-list.json",
    SensorType.ENVIRONMENT.name: "example-dtos/environment-dto-list.json",
    SensorType.LIDAR.name: "example-dtos/lidar-dto-list.json",
    SensorType.LUMINANCE.name: "example-dtos/luminance-dto-list.json",
    SensorType.NIGHT_VISION_CAMERA.name: "example-dtos/night-vision-camera-dto-list.json",
    SensorType.SHELLY_EM.name: "example-dtos/shelly-em-dto-list.json",
    SensorType.SHELLY_RED.name: "example-dtos/shelly-red-dto-list.json",
    SensorType.WATERFLOW.name: "example-dtos/waterflow-dto-list.json",
}

cnt = 0
while True:

    for sensor_type in sensor_type_sensor_data.keys():
        cnt = cnt + 1
        json_filename = sensor_type_sensor_data[sensor_type]

        dto_list = adapt_to_current_timestamp(read_json_file(json_filename))

        if sensor_type == SensorType.CAMERA.name:
            image_nbr = 1 if cnt % 20 > 10 else 2
            with open('test-data/kitchen-{}.jpg'.format(image_nbr), 'rb') as img_file:
                image_data = base64.b64encode(img_file.read()).decode('utf-8')
                for dto in dto_list:
                    dto['data'] = image_data

        if sensor_type == SensorType.NIGHT_VISION_CAMERA.name:
            image_nbr = 1 if cnt % 20 > 10 else 2
            with open('test-data/kitchen-{}-night.jpg'.format(image_nbr), 'rb') as img_file:
                image_data = base64.b64encode(img_file.read()).decode('utf-8')
                for dto in dto_list:
                    dto['data'] = image_data

        r = post_sensor_data(dto_list, sensor_type)

        print("{}\tTook {}ms to send \t{} chunk".format(cnt, r.elapsed.microseconds/1000, sensor_type))

        if not r.status_code == 201:
            print("Error creating sample: {}".format(r.status_code))
            print("\t{}".format(r.text))
            exit(1)
        # else:
        #    print(r.text)

        time.sleep(0.05)

    if cnt > 235:
        print("done")
        exit(0)
