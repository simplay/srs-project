import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { WinstonModule, utilities as nestWinstonModuleUtilities } from 'nest-winston';
import * as winston from 'winston';
import 'winston-daily-rotate-file';
import * as bodyParser from 'body-parser';
import { AuthenticatedSocketIoAdapter } from './authenticated-socket-io.adapter';

const requiredEnvVariablesDefined = (): boolean => {
  let missingVariable = false;

  const requiredEnvVariables: string[] = [
    'SMS_JWT_PRIVATE_KEY_FILEPATH',
    'SMS_JWT_PUBLIC_KEY_FILEPATH',
    'SMS_JWT_VALIDITY_LENGTH',
    'MONGODB_USER',
    'MONGODB_PASSWORD',
    'MONGODB_HOST',
    'MONGODB_DB_NAME',
    'DEBUG_LOG_REQUESTS_SENSORS_MODULE',
    'PIPELINE_SENSORS_FILEPATH',
    'SMS_SMTP_HOST',
    'SMS_SMTP_USER',
    'SMS_SMTP_PASSWORD',
    'NODE_ENV',
  ];

  console.log(process.env)
  requiredEnvVariables.forEach((variable) => {
    if (process.env[variable] === undefined) {
      missingVariable = true;
      console.log(`Missing required ENV variable: ${variable}`);
    }
  });

  return !missingVariable;
};

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    // cors: true,
    // Replace the Nest logger with the WinstonLogger
    logger: WinstonModule.createLogger({
      // Options
      format: winston.format.combine(
        winston.format.timestamp({ format: 'MMM-DD-YYYY HH:mm:ss' }),
        winston.format.align(),
        winston.format.printf((info) => `${info.level}: ${[info.timestamp]}: ${info.message}`),
      ),
      transports: [
        new winston.transports.Console({
          // Nest-like special formatter for console transport
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
        // Write all logs with level `warn` and below to `sms-warn-error.log`
        new winston.transports.DailyRotateFile({
          filename: 'sms-warn-error-%DATE%.log',
          datePattern: 'YYYY-MM-DD',
          dirname: 'logs',
          level: 'warn',
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
      ],
    }),
  });

  // TODO: check if we can provide a list of the clients for which we accept connection
  app.enableCors({
    origin: '*',
  });

  // Authenticate websockets
  app.useWebSocketAdapter(new AuthenticatedSocketIoAdapter(app));

  // Increase limit of body parser (default is 100Kb)
  app.use(bodyParser.json({ limit: '10mb' }));

  if (process.env.NODE_ENV === 'development') {
    // Setup Swagger documentation
    const config = new DocumentBuilder()
      .setTitle('SRS Backend REST API')
      .setDescription('API to post sensor data')
      .setVersion('0.0.1')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('/', app, document);
  }

  // Setup validation pipe for the entire app
  app.useGlobalPipes(
    new ValidationPipe({
      // Enable auto transform feature of ValidationPipe (payload being transformed to DTO instances)
      transform: true,
    }),
  );

  await app.listen(4000);
}

if (requiredEnvVariablesDefined()) {
  bootstrap();
} else {
  process.kill(process.pid, 'SIGTERM');
}
