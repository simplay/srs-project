import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isDefined, isEnum } from 'class-validator';

// From: https://stackoverflow.com/questions/59268777/validate-enum-directly-in-controller-function

@Injectable()
export class EnumValidationPipe implements PipeTransform<string, Promise<any>> {
  constructor(private enumEntity: any) {}
  transform(value: string): any {
    if (!isDefined(value)) {
      return value;
    } else {
      if (isEnum(value, this.enumEntity)) {
        return value;
      } else {
        const errorMessage = `The enum value ${value} is not valid. See the acceptable values: ${Object.keys(
          this.enumEntity,
        ).map((key) => this.enumEntity[key])}`;
        throw new BadRequestException(errorMessage);
      }
    }
  }
}
