import { ConsoleLogger, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class RequestLoggerMiddleware implements NestMiddleware {
  constructor(private readonly logger: ConsoleLogger) {
    logger.setContext(RequestLoggerMiddleware.name);
  }

  use(req: Request, res: Response, next: NextFunction) {
    const { ip, method, baseUrl, url } = req;
    const clientIp = req.headers['x-forwarded-for'];

    /*
      For an unknown reason, the callback on 'close' is not called when running in docker ...
      'finish' is called however.
      */
    /*
    res.on('close', () => {
      const { statusCode } = res;
      this.logger.log(`${ip} [${method}] ${baseUrl}${url} ${statusCode}`);
    });
     */
    res.on('finish', () => {
      const { statusCode } = res;
      this.logger.log(`${clientIp} [${method}] ${baseUrl}${url} ${statusCode}`);
    });
    next();
  }
}
