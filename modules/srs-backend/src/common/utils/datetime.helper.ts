export function getCurrentTimestamp() {
  // We return the timestamp in second with ms precision
  // Date.now() returns the nbr of milliseconds since epoch => convert to seconds
  return Date.now() / 1000;
}
