export interface ExportTimestampRange {
  timestampFrom: number;
  timestampTo: number;
}
