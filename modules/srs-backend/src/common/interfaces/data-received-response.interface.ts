import { SensorType } from '../enums/sensor-type.enum';
import { DoorDto } from '../../modules/sensors/dto/door.dto';

export interface SensorLastActivity {
  ip: string;
  lastSampleReceivedAt: number;
  sample: DoorDto | null; // exception for the Doors to display the status (doorOpen) in the dashboard
}

export interface DataReceivedResponse {
  timestamp: number;
  message: string;
  numberSamplesSaved: number;
  numberSamplesSkipped: number;
  sensorType: SensorType;
  ips: string[];
  details: string;
}

export interface DataReceivedWithSensorsLastActivityResponse extends DataReceivedResponse {
  sensorsLastActivity: SensorLastActivity[];
}
