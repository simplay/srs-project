import { CameraDto } from '../../modules/sensors/dto/camera.dto';
import { NightVisionCameraDto } from '../../modules/sensors/dto/night-vision-camera.dto';
import { CameraType } from '../enums/camera-type.enum';

// Holds the last sample (frame) received for a camera
export interface CameraLastSample {
  ip: string;
  receivedAt: number;
  sample: CameraDto | NightVisionCameraDto;
}

export interface LastActivityForCameraType {
  cameraType: CameraType;
  camerasLastSample: CameraLastSample[];
}
