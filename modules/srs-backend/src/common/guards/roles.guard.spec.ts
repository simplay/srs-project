import { createMock } from '@golevelup/ts-jest';
import { ExecutionContext, Type } from '@nestjs/common';
import { RolesGuard } from './roles.guard';
import { Role } from '../enums/role.enum';
import { Reflector } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';

class ReflectorMock {
  handlerUnprotected: boolean | undefined;
  classUnprotected: boolean | undefined;

  getAllAndOverride = jest.fn((metaDataKey: string, targets: any[]) => {
    // return this.handlerUnprotected || this.classUnprotected;
    // Return the required roles to be [Admin]
    return [Role.Admin];
  });
}

describe('RolesGuard', () => {
  let rolesGuard: RolesGuard;
  let mockReflector: ReflectorMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RolesGuard,
        {
          provide: Reflector,
          useClass: ReflectorMock,
        },
      ],
    }).compile();

    rolesGuard = module.get<RolesGuard>(RolesGuard);
  });

  it('should be defined', () => {
    expect(rolesGuard).toBeDefined();
  });
  it('should return true for user with admin role', () => {
    const mockExecutionContext = createMock<ExecutionContext>({
      switchToHttp: () => ({
        getRequest: () => ({
          user: {
            roles: [Role.Admin],
          },
        }),
      }),
    });

    expect(rolesGuard.canActivate(mockExecutionContext)).toBeTruthy();
  });
  it('should return false for user with operator role', () => {
    const mockExecutionContext = createMock<ExecutionContext>({
      switchToHttp: () => ({
        getRequest: () => ({
          user: {
            roles: [Role.Operator],
          },
        }),
      }),
    });

    expect(rolesGuard.canActivate(mockExecutionContext)).toBeFalsy();
  });
});
