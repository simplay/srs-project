// Tags used to filter log book

export enum LogTag {
  ACCESS = 'access', // when accessing dashboard (authentication)
  BOOKINGS = 'bookings', // when managing bookings and rooms
  CAMERA = 'camera', // When logging access to camera
  EXPERIMENTS_CONFIG = 'experiments_config', // when editing experiments config
  EXPORT = 'export', // When exporting data
  NOTE = 'note', // When saving a note for the experiment
  SAMPLES_DELETION = 'delete_samples', // When clearing the samples collection
  USERS_CONFIG = 'users_config', // when editing users config
}
