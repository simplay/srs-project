import { HttpException, HttpStatus } from '@nestjs/common';

export class ExperimentRunningConfigNotEditableException extends HttpException {
  constructor() {
    super(
      'A recording is ongoing for this config. Please stop first the experiment before editing the config.',
      HttpStatus.FORBIDDEN,
    );
  }
}
