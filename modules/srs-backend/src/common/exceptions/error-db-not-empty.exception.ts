import { HttpException, HttpStatus } from '@nestjs/common';

export class ErrorDbNotEmptyException extends HttpException {
  constructor() {
    super('The database of samples is not empty.', HttpStatus.FORBIDDEN);
  }
}
