import { HttpException, HttpStatus } from '@nestjs/common';

export class ImmutableExperimentException extends HttpException {
  constructor(experimentName: string) {
    super(`Experiment ${experimentName} is immutable`, HttpStatus.FORBIDDEN);
  }
}
