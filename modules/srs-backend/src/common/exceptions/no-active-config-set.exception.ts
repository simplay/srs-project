import { HttpException, HttpStatus } from '@nestjs/common';

export class NoActiveConfigSetException extends HttpException {
  constructor() {
    super('No config found that is set as active', HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
