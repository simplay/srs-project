export class Sensor {
  ip: string;
  name: string;
  location: string;
  saveData: boolean;
}
