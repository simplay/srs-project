import { ConsoleLogger, MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { SensorsModule } from './modules/sensors/sensors.module';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { RequestLoggerMiddleware } from './middlewares/request-logger.middleware';
import { SamplesModule } from './modules/samples/samples.module';
import { ConfigsModule } from './modules/configs/configs.module';
import { ParticipantsModule } from './modules/participants/participants.module';
import { ExperimentsModule } from './modules/experiments/experiments.module';
import { LogBookModule } from './modules/log-book/log-book.module';
import { BookingsModule } from './modules/bookings/bookings.module';
import { MailModule } from './modules/mail/mail.module';

@Module({
  imports: [
    SensorsModule,
    SamplesModule,
    UsersModule,
    AuthModule,
    MongooseModule.forRoot(
      // When specifying a database, this database is also taken for authentication by default => specify admin as authSource
      `mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@${process.env.MONGODB_HOST}:27017/${process.env.MONGODB_DB_NAME}?authSource=admin`,
      {
        connectionFactory: (connection) => {
          connection.plugin(require('mongoose-autopopulate'));
          return connection;
        },
      },
    ),
    ConfigsModule,
    ParticipantsModule,
    ExperimentsModule,
    LogBookModule,
    BookingsModule,
    MailModule,
  ],
  controllers: [],
  providers: [ConsoleLogger],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(RequestLoggerMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
