import { Injectable } from '@nestjs/common';
import { User, UserDocument } from './schema/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class UsersRepository {
  constructor(@InjectModel(User.name) private readonly userModel: Model<UserDocument>) {}

  async listUsers(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async findOneWithUsername(username: string): Promise<UserDocument> {
    return await this.userModel.findOne({ username }).exec();
  }

  async findOneWithEmail(email: string): Promise<UserDocument> {
    return await this.userModel.findOne({ email }).exec();
  }

  async createNewUser(user: User): Promise<UserDocument> {
    const userDocumentToSave = new this.userModel(user);
    return await userDocumentToSave.save();
  }

  async updateUser(oldUsername: string, updatedUser: User): Promise<UserDocument> {
    const user = await this.findOneWithUsername(oldUsername);

    // update document
    for (const k in updatedUser) {
      user[k] = updatedUser[k];
    }
    return await user.save();
  }

  async deleteUser(username: string) {
    await this.userModel.deleteOne({ username }).exec();
  }
}
