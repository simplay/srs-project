import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EXPERIMENT_LOG_MODEL_NAME, ExperimentLog, ExperimentLogDocument } from './schema/experiment-log.schema';
import { UserDocument } from './schema/user.schema';
import { ExperimentDocument } from './schema/experiment.schema';
import { getCurrentTimestamp } from '../common/utils/datetime.helper';
import { LogTag } from '../common/enums/log-tag.enum';
import {timestamp} from "rxjs";

@Injectable()
export class LogBookRepository {
  constructor(
    @InjectModel(EXPERIMENT_LOG_MODEL_NAME) private readonly experimentLogModel: Model<ExperimentLogDocument>,
  ) {}

  async saveLog(
    user: UserDocument,
    experiment: ExperimentDocument | null,
    logMessage: string,
    tags: LogTag[],
  ): Promise<ExperimentLogDocument> {
    const experimentLog: ExperimentLog = {
      datetime: getCurrentTimestamp(),
      user,
      experiment,
      log: logMessage,
      tags: [...new Set(tags)], // remove duplicates if any
    };
    const experimentLogDocumentToSave = new this.experimentLogModel(experimentLog);
    return await experimentLogDocumentToSave.save();
  }

  async getLogBook(): Promise<ExperimentLogDocument[]> {
    return await this.experimentLogModel.find().exec();
  }

  // e.g. unix10 = 1646182412.77
  async getLogBookByTimestamp(unix10: number): Promise<ExperimentLogDocument[]> {
    const log_model = await this.experimentLogModel.find(
        { datetime: {
                $gt: unix10
              }
        }).exec();

    return log_model;
  }

  async getLogBookForExperiment(experiment: ExperimentDocument): Promise<ExperimentLogDocument[]> {
    return await this.experimentLogModel.find({ experiment: experiment._id }).exec();
  }
}
