import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Room, RoomDocument } from './schema/room.schema';
import { v4 as uuidv4 } from 'uuid';
import { Booking, BookingDocument } from './schema/booking.schema';

@Injectable()
export class BookingsRepository {
  constructor(
    @InjectModel(Room.name) private readonly roomDocumentModel: Model<RoomDocument>,
    @InjectModel(Booking.name) private readonly bookingDocumentModel: Model<BookingDocument>,
  ) {}

  async saveRoom(name: string): Promise<RoomDocument> {
    const room: Room = {
      uuid: uuidv4(),
      name,
    };
    const roomDocumentToSave = new this.roomDocumentModel(room);
    return await roomDocumentToSave.save();
  }

  async updateRoom(roomDocument: RoomDocument): Promise<RoomDocument> {
    return await roomDocument.save();
  }

  async getRooms(): Promise<RoomDocument[]> {
    return await this.roomDocumentModel.find().exec();
  }

  async findRoom(uuid: string): Promise<RoomDocument> {
    return await this.roomDocumentModel.findOne({ uuid }).exec();
  }

  async deleteRoom(uuid: string) {
    const room = await this.findRoom(uuid);
    if (room) {
      // Delete all the bookings associated with that room
      await this.bookingDocumentModel.deleteMany({ room: room._id });

      // Delete the room
      await this.roomDocumentModel.deleteOne({ uuid }).exec();
    }
  }

  async saveBookingForRoom(booking: Booking): Promise<BookingDocument> {
    const bookingDocumentToSave = new this.bookingDocumentModel(booking);
    return await bookingDocumentToSave.save();
  }

  async findBooking(uuid: string): Promise<BookingDocument> {
    return await this.bookingDocumentModel.findOne({ uuid }).exec();
  }

  async getBookingsForRoom(room: RoomDocument): Promise<BookingDocument[]> {
    return await this.bookingDocumentModel.find({ room: room._id }).exec();
  }

  async deleteBooking(uuid: string) {
    await this.bookingDocumentModel.deleteOne({ uuid }).exec();
  }
}
