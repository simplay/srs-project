import { Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';

import { InjectModel } from '@nestjs/mongoose';
import { Sample, SampleDocument, SampleModel } from './schema/sample.schema';
import { MongoError } from 'mongodb';
import { Model } from 'mongoose';
import { SensorType } from '../common/enums/sensor-type.enum';
import { ErrorSavingSampleException } from '../common/exceptions/error-saving-sample.exception';
import { SamplesCollectionStatsDto } from '../modules/samples/dto/samples-collection-stats.dto';
import { EXPORT_JOB_MODEL_NAME, ExportJob, ExportJobDocument } from './schema/export-job.schema';
import { v4 as uuidv4 } from 'uuid';
import { getCurrentTimestamp } from '../common/utils/datetime.helper';
import { User } from './schema/user.schema';
import { Experiment } from './schema/experiment.schema';
import {
  ACTIVE_EXPORT_JOB_MODEL_NAME,
  ActiveExportJob,
  ActiveExportJobDocument,
} from './schema/active-export-job.schema';
import { ExportJobStatus } from '../common/enums/export-job-status.enum';
import { ActiveExperiment } from './schema/active-experiment.schema';
import { ExportTimestampRange } from '../common/interfaces/export-timestamp-range.interface';

@Injectable()
export class SamplesRepository {
  private readonly logger: Logger = new Logger(SamplesRepository.name);
  private readonly sampleModels: Model<SampleDocument>[];

  constructor(
    @InjectModel(SampleModel.Camera) private readonly sampleCameraModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Door) private readonly sampleDoorModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Emfit) private readonly sampleEmfitModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Environment) private readonly sampleEnvironmentModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Lidar) private readonly sampleLidarModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Luminance) private readonly sampleLuminanceModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Mattress) private readonly sampleMattressModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Microphones) private readonly sampleMicrophoneModel: Model<SampleDocument>,
    @InjectModel(SampleModel.NightVisionCamera) private readonly sampleNightVisionCameraModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Radar) private readonly sampleRadarModel: Model<SampleDocument>,
    @InjectModel(SampleModel.Seismograph) private readonly sampleSeismographModel: Model<SampleDocument>,
    @InjectModel(SampleModel.ShellyEm) private readonly sampleShellyEmModel: Model<SampleDocument>,
    @InjectModel(SampleModel.ShellyRed) private readonly sampleShellyRedModel: Model<SampleDocument>,
    @InjectModel(SampleModel.WaterFlow) private readonly sampleWaterFlowModel: Model<SampleDocument>,
    @InjectModel(EXPORT_JOB_MODEL_NAME) private readonly exportJobModel: Model<ExportJobDocument>,
    @InjectModel(ACTIVE_EXPORT_JOB_MODEL_NAME) private readonly activeExportJobModel: Model<ActiveExportJobDocument>,
  ) {
    // Add the models in the list for easier maintenance actions (e.g. drop collections)
    this.sampleModels = [];
    this.sampleModels.push(this.sampleCameraModel);
    this.sampleModels.push(this.sampleDoorModel);
    this.sampleModels.push(this.sampleEmfitModel);
    this.sampleModels.push(this.sampleEnvironmentModel);
    this.sampleModels.push(this.sampleLidarModel);
    this.sampleModels.push(this.sampleLuminanceModel);
    this.sampleModels.push(this.sampleMattressModel);
    this.sampleModels.push(this.sampleMicrophoneModel);
    this.sampleModels.push(this.sampleNightVisionCameraModel);
    this.sampleModels.push(this.sampleRadarModel);
    this.sampleModels.push(this.sampleSeismographModel);
    this.sampleModels.push(this.sampleShellyEmModel);
    this.sampleModels.push(this.sampleShellyRedModel);
    this.sampleModels.push(this.sampleWaterFlowModel);
  }

  async saveSamples(sensorType: SensorType, samples: Sample[]): Promise<Sample[]> {
    try {
      switch (sensorType) {
        case SensorType.Camera:
          return await this.sampleCameraModel.insertMany(samples);
          break;
        case SensorType.Door:
          return await this.sampleDoorModel.insertMany(samples);
          break;
        case SensorType.Emfit:
          return await this.sampleEmfitModel.insertMany(samples);
          break;
        case SensorType.Environment:
          return await this.sampleEnvironmentModel.insertMany(samples);
          break;
        case SensorType.Lidar:
          return await this.sampleLidarModel.insertMany(samples);
          break;
        case SensorType.Luminance:
          return await this.sampleLuminanceModel.insertMany(samples);
          break;
        case SensorType.Mattress:
          return await this.sampleMattressModel.insertMany(samples);
          break;
        case SensorType.Microphone:
          return await this.sampleMicrophoneModel.insertMany(samples);
          break;
        case SensorType.NightVisionCamera:
          return await this.sampleNightVisionCameraModel.insertMany(samples);
          break;
        case SensorType.Radar:
          return await this.sampleRadarModel.insertMany(samples);
          break;
        case SensorType.Seismograph:
          return await this.sampleSeismographModel.insertMany(samples);
          break;
        case SensorType.ShellyEm:
          return await this.sampleShellyEmModel.insertMany(samples);
          break;
        case SensorType.ShellyRed:
          return await this.sampleShellyRedModel.insertMany(samples);
          break;
        case SensorType.WaterFlow:
          return await this.sampleWaterFlowModel.insertMany(samples);
          break;
        default:
          throw new ErrorSavingSampleException(`No model found to save sample for sensor type ${sensorType}`);
          break;
      }
    } catch (error) {
      this.logger.error(`Error saving ${sensorType} samples in the DB`);

      // TODO: define custom errors for repository
      throw new InternalServerErrorException(error);
    }
  }

  async dropSamplesCollections() {
    this.logger.log('Drop samples collections');
    try {
      for (const sampleModel of this.sampleModels) {
        const collectionName = sampleModel.collection.name;
        if (!(await this.isCollectionEmpty(sampleModel))) {
          this.logger.log(`Collection ${collectionName} not empty => drop`);
          await sampleModel.collection.drop();
        } else {
          this.logger.log(`No document found for collection ${collectionName}`);
        }
      }
    } catch (e) {
      if (e instanceof MongoError) {
        // collection not found error
        if ((e as MongoError).message === 'ns not found') {
          throw new NotFoundException('Collection not found');
        }
      } else {
        throw e;
      }
    }
  }

  private getFilterTimestampInRange(exportTimestampRange: ExportTimestampRange) {
    return {
      timestamp: { $gte: exportTimestampRange.timestampFrom, $lt: exportTimestampRange.timestampTo },
    };
  }
  async getNonEmptySamplesCollectionsNames(exportTimestampRange: ExportTimestampRange): Promise<string[]> {
    const samplesCollectionsNames = [] as string[];
    for (const sampleModel of this.sampleModels) {
      const samples = await sampleModel.find(this.getFilterTimestampInRange(exportTimestampRange)).limit(1).exec();
      if (samples.length > 0) {
        const collectionName = sampleModel.collection.name;
        samplesCollectionsNames.push(collectionName);
      }
    }

    return samplesCollectionsNames;
  }

  async getLastSampleForDoorSensorForActiveExperiment(
    sensorIp: string,
    activeExperiment: ActiveExperiment,
  ): Promise<SampleDocument> {
    return await this.sampleDoorModel
      .findOne({
        ip: sensorIp,
        participantUuid: activeExperiment.participant.uuid,
        taskUuid: activeExperiment.taskUuid,
        trialNbr: activeExperiment.trialNbr,
      })
      .sort({ timestamp: -1 })
      .exec();
  }

  async isCollectionEmpty(model: Model<SampleDocument>): Promise<boolean> {
    return (await model.findOne().exec()) === null ? true : false;
  }

  async getDistinctParticipantsForSamplesCollectionInTimestampRange(
    collectionName: string,
    exportTimestampRange: ExportTimestampRange,
  ): Promise<string[]> {
    let participantsUuids = [];
    const sampleModel = this.sampleModels.find((model) => model.collection.name === collectionName);

    if (sampleModel) {
      participantsUuids = await sampleModel
        .find(this.getFilterTimestampInRange(exportTimestampRange))
        .distinct('participantUuid')
        .exec();
    }

    return participantsUuids;
  }

  async getSamplesCollectionsStats() {
    const res: SamplesCollectionStatsDto[] = [];
    for (const sampleModel of this.sampleModels) {
      if (!(await this.isCollectionEmpty(sampleModel))) {
        const collStats = await sampleModel.collection.stats();
        // const distinctSensorsIps = await sampleModel.find().distinct('ip').exec();

        const sampleCollectionStats: SamplesCollectionStatsDto = {
          collectionName: sampleModel.collection.name,
          numberOfSamples: collStats.count,
          // distinctSensorsIps: distinctSensorsIps,
          distinctSensorsIps: [],
          size: collStats.size,
          avgObjSize: collStats.avgObjSize,
          storageSize: collStats.storageSize,
        };
        res.push(sampleCollectionStats);
      }
    }

    return res;
  }

  async samplesAlreadyCollected(): Promise<boolean> {
    let dataExists = false;
    for (const sampleModel of this.sampleModels) {
      if (!(await this.isCollectionEmpty(sampleModel))) {
        dataExists = true;
        break;
      }
    }
    return dataExists;
  }

  async createNewExportJob(user: User, experiment: Experiment) {
    const exportJob: ExportJob = {
      uuid: uuidv4(),
      createdAt: getCurrentTimestamp(),
      createdBy: user,
      status: ExportJobStatus.ON_GOING,
      completedAt: null,
      experiment,
      mongodumpLog: [],
      duration: 0,
    };

    const exportJobDocumentToSave = new this.exportJobModel(exportJob);
    return await exportJobDocumentToSave.save();
  }

  async getExportJob(uuid: string): Promise<ExportJobDocument> {
    return await this.exportJobModel.findOne({ uuid }).exec();
  }

  async getActiveExportJob(): Promise<ActiveExportJobDocument> {
    return await this.activeExportJobModel.findOne({}).exec();
  }

  async resetActiveExportJob() {
    const documents: ActiveExportJobDocument[] = await this.activeExportJobModel.find().exec();
    if (documents.length > 0) {
      await this.activeExportJobModel.collection.drop();
    }
  }

  async deleteActiveExportJob() {
    await this.resetActiveExportJob();
  }

  async setActiveExportJob(exportJob: ExportJob): Promise<ActiveExportJobDocument> {
    // Remove any previous active export job
    await this.resetActiveExportJob();

    // Save new one
    const activeExportJob: ActiveExportJob = {
      exportJob,
    };
    const activeExportJobDocumentToSave = new this.activeExportJobModel(activeExportJob);
    return await activeExportJobDocumentToSave.save();
  }

  async completeExportJobWithStatusAndLog(
    uuid: string,
    status: ExportJobStatus,
    mongodumpLog: string[],
  ): Promise<ExportJobDocument> {
    const exportJob: ExportJobDocument = await this.exportJobModel.findOne({ uuid }).exec();

    exportJob.status = status;
    exportJob.completedAt = getCurrentTimestamp();
    exportJob.duration = Math.round((exportJob.completedAt - exportJob.createdAt) * 100) / 100; // round 2 decimal
    exportJob.mongodumpLog = mongodumpLog;

    return await exportJob.save();
  }
}
