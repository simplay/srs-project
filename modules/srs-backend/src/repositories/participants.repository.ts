import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Participant, ParticipantDocument } from './schema/participant.schema';
import { getCurrentTimestamp } from '../common/utils/datetime.helper';
import { User, UserDocument } from './schema/user.schema';

@Injectable()
export class ParticipantsRepository {
  constructor(@InjectModel(Participant.name) private readonly participantModel: Model<ParticipantDocument>) {}

  async listParticipants(): Promise<ParticipantDocument[]> {
    return await this.participantModel.find().exec();
  }

  async getParticipant(uuid: string): Promise<ParticipantDocument> {
    return await this.participantModel.findOne({ uuid }).exec();
  }

  async getParticipantWithShortNote(shortNote: string): Promise<ParticipantDocument> {
    return await this.participantModel.findOne({ shortNote }).exec();
  }

  async createNewParticipant(
    participantUuid: string,
    shortNote: string,
    createdBy: User,
  ): Promise<ParticipantDocument> {
    const participant: Participant = {
      uuid: participantUuid,
      shortNote: shortNote,
      createdAt: getCurrentTimestamp(),
      createdBy: createdBy,
    };
    const configDocumentToSave = new this.participantModel(participant);
    return await configDocumentToSave.save();
  }

  async deleteParticipant(uuid: string) {
    await this.participantModel.deleteOne({ uuid }).exec();
  }

  async updateParticipant(updatedParticipant: ParticipantDocument): Promise<ParticipantDocument> {
    return await updatedParticipant.save();
  }
}
