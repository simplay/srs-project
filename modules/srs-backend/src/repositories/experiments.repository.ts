import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Experiment, ExperimentDocument } from './schema/experiment.schema';
import { Participant, ParticipantDocument } from './schema/participant.schema';
import { User, UserDocument } from './schema/user.schema';
import {
  ACTIVE_EXPERIMENT_MODEL_NAME,
  ActiveExperiment,
  ActiveExperimentDocument,
} from './schema/active-experiment.schema';
import { ConfigDocument } from './schema/config.schema';
import {
  PARTICIPANT_PROGRESS_MODEL_NAME,
  ParticipantProgress,
  ParticipantProgressDocument,
} from './schema/participant-progress.schema';
import {
  MESSAGE_SCREEN_LOFT_MODEL_NAME,
  MessageScreenEntrance,
  MessageScreenEntranceDocument,
} from './schema/message-screen-entrance.schema';

@Injectable()
export class ExperimentsRepository {
  constructor(
    @InjectModel(Experiment.name) private readonly experimentModel: Model<ExperimentDocument>,
    @InjectModel(ACTIVE_EXPERIMENT_MODEL_NAME) private readonly activeExperimentModel: Model<ActiveExperimentDocument>,
    @InjectModel(PARTICIPANT_PROGRESS_MODEL_NAME)
    private readonly participantProgressModel: Model<ParticipantProgressDocument>,
    @InjectModel(MESSAGE_SCREEN_LOFT_MODEL_NAME)
    private readonly messageScreenEntranceModel: Model<MessageScreenEntranceDocument>,
  ) {}

  async listExperiments(): Promise<ExperimentDocument[]> {
    return await this.experimentModel.find().exec();
  }

  async getExperiment(uuid: string): Promise<ExperimentDocument> {
    return await this.experimentModel.findOne({ uuid }).exec();
  }

  async getActiveExperiment(): Promise<ActiveExperimentDocument> {
    const activeExperimentDocument = await this.activeExperimentModel.findOne().exec();
    return activeExperimentDocument;
  }

  async resetActiveExperiment() {
    const documents: ActiveExperiment[] = await this.activeExperimentModel.find().exec();
    if (documents.length > 0) {
      await this.activeExperimentModel.collection.drop();
    }
  }

  async replaceActiveExperiment(activeExperiment: ActiveExperiment): Promise<ActiveExperimentDocument> {
    // Remove any previous active experiment
    await this.resetActiveExperiment();

    // Save new one
    const activeExperimentDocumentToSave = new this.activeExperimentModel(activeExperiment);
    return await activeExperimentDocumentToSave.save();
  }

  async updateActiveExperiment(activeExperimentDocument: ActiveExperimentDocument): Promise<ActiveExperimentDocument> {
    return await activeExperimentDocument.save();
  }

  async createNewExperiment(experiment: Experiment): Promise<ExperimentDocument> {
    const experimentDocumentToSave = new this.experimentModel(experiment);
    return await experimentDocumentToSave.save();
  }

  async updateExperiment(experimentDocument: ExperimentDocument): Promise<ExperimentDocument> {
    return await experimentDocument.save();
  }

  async getExperimentsForParticipant(participant: Participant): Promise<ExperimentDocument[]> {
    return await this.experimentModel.find({ participants: { $in: [participant] } }).exec();
  }

  async getExperimentsForUser(user: UserDocument): Promise<ExperimentDocument[]> {
    return await this.experimentModel.find({ $or: [{ operators: { $in: [user] } }, { owner: user._id }] }).exec();
  }

  async getExperimentsHavingConfig(config: ConfigDocument): Promise<ExperimentDocument[]> {
    return await this.experimentModel.find({ config: config._id }).exec();
  }

  async getParticipantProgressForExperimentTask(
    participant: ParticipantDocument,
    experiment: ExperimentDocument,
    taskUuid: string,
  ): Promise<ParticipantProgressDocument> {
    return await this.participantProgressModel
      .findOne({ participant: participant._id, experiment: experiment._id, taskUuid })
      .exec();
  }

  async updateParticipantProgressForExperimentTask(
    participant: ParticipantDocument,
    experiment: ExperimentDocument,
    taskUuid: string,
  ): Promise<ParticipantProgressDocument> {
    const participantProgressDocument = await this.getParticipantProgressForExperimentTask(
      participant,
      experiment,
      taskUuid,
    );

    // If one entry exists already, we increment the trial count
    if (participantProgressDocument) {
      participantProgressDocument.trialCounter++;
      return await participantProgressDocument.save();
    } else {
      const participantProgress: ParticipantProgress = {
        participant,
        experiment,
        taskUuid,
        trialCounter: 1,
      };
      const participantProgressDocumentToSave = new this.participantProgressModel(participantProgress);
      return await participantProgressDocumentToSave.save();
    }
  }

  async getParticipantsProgressForExperiment(experiment: ExperimentDocument): Promise<ParticipantProgressDocument[]> {
    return await this.participantProgressModel.find({ experiment: experiment._id }).exec();
  }

  async getMessageScreenEntrance(): Promise<MessageScreenEntranceDocument> {
    return await this.messageScreenEntranceModel.findOne().exec();
  }

  async saveMessageScreenEntrance(
    messageScreenEntrance: MessageScreenEntrance,
  ): Promise<MessageScreenEntranceDocument> {
    // Drop previous message if any
    const documents: MessageScreenEntranceDocument[] = await this.messageScreenEntranceModel.find().exec();
    if (documents.length > 0) {
      await this.messageScreenEntranceModel.collection.drop();
    }

    // Save new message
    const messageScreenEntranceToSave = new this.messageScreenEntranceModel(messageScreenEntrance);

    return await messageScreenEntranceToSave.save();
  }
}
