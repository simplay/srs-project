import { Sensor } from '../../common/entities/sensor.entity';
import { SensorType } from '../../common/enums/sensor-type.enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.schema';

export type ConfigDocument = Config & Document;

/*
export type SensorsPerType = {
  [key in SensorType]: Sensor[];
};
*/

// type SensorsPerType = Record<SensorType, Sensor[]>;
class SensorsPerType {}

@Schema()
export class Config {
  @Prop({ required: true, unique: true })
  uuid: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop()
  // sensorsPerType: Record<SensorType, Sensor[]>;
  // @Prop({ type: 'SensorsPerType' })
  sensorsPerType: SensorsPerType;

  @Prop()
  createdAt: number;

  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  createdBy: User;
}
export const ConfigSchema = SchemaFactory.createForClass(Config);
