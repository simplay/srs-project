import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.schema';

export type ParticipantDocument = Participant & Document;

@Schema()
export class Participant {
  @Prop({ unique: true, required: true })
  uuid: string;

  @Prop({ unique: true, required: true })
  shortNote: string;

  @Prop()
  createdAt: number;

  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  createdBy: User;
}

export const ParticipantSchema = SchemaFactory.createForClass(Participant);
