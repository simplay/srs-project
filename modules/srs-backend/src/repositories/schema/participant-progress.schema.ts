import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Participant } from './participant.schema';
import { Experiment } from './experiment.schema';

export type ParticipantProgressDocument = ParticipantProgress & Document;

@Schema()
export class ParticipantProgress {
  @Prop({ type: Types.ObjectId, ref: Participant.name, autopopulate: true, unique: false })
  participant: Participant;

  @Prop({ type: Types.ObjectId, ref: Experiment.name, autopopulate: true, unique: false })
  experiment: Experiment;

  @Prop({ unique: false })
  taskUuid: string;

  @Prop()
  trialCounter: number;
}

export const ParticipantProgressSchema = SchemaFactory.createForClass(ParticipantProgress);

// Create a unique constraint for participant <-> task
// https://stackoverflow.com/a/49420511
ParticipantProgressSchema.index({ participant: 1, taskUuid: 1 }, { unique: true });

// Name to be used to build the collection name and to reference object
export const PARTICIPANT_PROGRESS_MODEL_NAME = 'progress_participant';
