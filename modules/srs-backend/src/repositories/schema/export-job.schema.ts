import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.schema';
import { Experiment } from './experiment.schema';
import { ExportJobStatus } from '../../common/enums/export-job-status.enum';

export type ExportJobDocument = ExportJob & Document;

@Schema()
export class ExportJob {
  @Prop({ required: true, unique: true })
  uuid: string;

  @Prop({ type: Types.ObjectId, ref: Experiment.name, autopopulate: true })
  experiment: Experiment;

  @Prop()
  createdAt: number;

  @Prop()
  completedAt: number;

  @Prop()
  status: ExportJobStatus;

  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  createdBy: User;

  @Prop()
  mongodumpLog: string[];

  @Prop()
  duration: number;
}
export const ExportJobSchema = SchemaFactory.createForClass(ExportJob);

/*

  Names of the models from which are derived the names of the collections.

  Name of the collection = "Name of the model".toLowerCase() + "s"
 */

export const EXPORT_JOB_MODEL_NAME = 'export_job';
