import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Experiment } from './experiment.schema';
import { Participant } from './participant.schema';

export type ActiveExperimentDocument = ActiveExperiment & Document;

@Schema()
export class ActiveExperiment {
  @Prop({ type: Types.ObjectId, ref: Experiment.name, autopopulate: true })
  experiment: Experiment;

  @Prop({ required: true })
  isRecording: boolean;

  @Prop({ type: Types.ObjectId, ref: Participant.name, autopopulate: true })
  participant: Participant;

  @Prop()
  taskUuid: string;

  @Prop()
  trialNbr: number;
}
export const ActiveExperimentSchema = SchemaFactory.createForClass(ActiveExperiment);

// Name to be used to build the collection name and to reference object
export const ACTIVE_EXPERIMENT_MODEL_NAME = 'active_experiment';
