import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Participant } from './participant.schema';
import { User } from './user.schema';
import { Config } from './config.schema';

export type ExperimentDocument = Experiment & Document;

// Nested schema
@Schema()
export class Task {
  @Prop({ required: true, unique: true })
  uuid: string;

  @Prop()
  name: string;

  @Prop()
  description: string;
}
export const TaskSchema = SchemaFactory.createForClass(Task);

// Parent schema
@Schema()
export class Experiment {
  @Prop({ required: true, unique: true })
  uuid: string;

  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  owner: User;

  @Prop({ type: [Types.ObjectId], ref: User.name, autopopulate: true })
  operators: User[];

  @Prop({ type: [Types.ObjectId], ref: Participant.name, autopopulate: true })
  participants: Participant[];

  @Prop({ type: Types.ObjectId, ref: Config.name, autopopulate: true })
  config: Config;

  @Prop({ type: [TaskSchema], default: [] })
  tasks: Task[];

  @Prop()
  createdAt: number;

  @Prop()
  startedAt: number;

  @Prop()
  completedAt: number;

  @Prop({ required: true })
  completed: boolean;

  // TODO: add notes
}
export const ExperimentSchema = SchemaFactory.createForClass(Experiment);
