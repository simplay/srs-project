import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

// @Prop() with Buffer do not seem to work => used the standard way to create schema (see below)
/*
@Schema()
export class Sample {
  @Prop()
  ip: string;

  @Prop()
  timestamp: number;

  @Prop()
  sensor_type: string;

  @Prop()
  location: string;

  @Prop()
  public blob?: Buffer;
}
export const SampleSchema = SchemaFactory.createForClass(Sample);
 */

export class Sample {
  ip: string;
  timestamp: number;
  location: string;
  participantUuid: string;
  taskUuid: string;
  trialNbr: number;
  blob: Buffer;
}

// Schema to be saved in MongoDB
export const SampleSchema = new mongoose.Schema({
  // Javascript types (not TS)
  ip: String,
  timestamp: Number,
  location: String,
  participantUuid: String,
  taskUuid: String,
  trialNbr: Number,
  blob: Buffer,
});

export type SampleDocument = Sample & Document;

/*

  Names of the models from which are derived the names of the collections.

  Name of the collection = "Name of the model".toLowerCase() + "s"
 */

export enum SampleModel {
  Camera = 'Camera_Sample',
  Emfit = 'Emfit_Sample',
  Door = 'Door_Sample',
  Environment = 'Environment_Sample',
  Lidar = 'Lidar_Sample',
  Luminance = 'Luminance_Sample',
  Mattress = 'Mattress_Sample',
  Microphones = 'Microphone_Sample',
  NightVisionCamera = 'NightVision_Camera_Sample',
  Radar = 'Radar_Sample',
  Seismograph = 'Seismograph_Sample',
  ShellyEm = 'ShellyEm_Sample',
  ShellyRed = 'ShellyRed_Sample',
  WaterFlow = 'WaterFlow_Sample',
}
