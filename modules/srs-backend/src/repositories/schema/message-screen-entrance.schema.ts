import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.schema';

export type MessageScreenEntranceDocument = MessageScreenEntrance & Document;

@Schema()
export class MessageScreenEntrance {
  @Prop()
  message: string;

  @Prop()
  updatedAt: number;

  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  createdBy: User;
}
export const MessageScreenEntranceSchema = SchemaFactory.createForClass(MessageScreenEntrance);

/*

  Names of the models from which are derived the names of the collections.

  Name of the collection = "Name of the model".toLowerCase() + "s"
 */

export const MESSAGE_SCREEN_LOFT_MODEL_NAME = 'screen_message';
