import { Role } from '../../common/enums/role.enum';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop()
  firstname: string;

  @Prop()
  lastname: string;

  @Prop({ unique: true })
  email: string;

  @Prop({ unique: true, required: true })
  username: string;

  @Prop()
  password: string;

  @Prop()
  createdAt: number;

  @Prop()
  updatedAt: number;

  @Prop()
  roles: Role[];
}

export const UserSchema = SchemaFactory.createForClass(User);
