import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { BookingType } from '../../common/enums/booking-type.enum';
import { Room } from './room.schema';

export type BookingDocument = Booking & Document;

@Schema()
export class Booking {
  @Prop({ required: true, unique: true })
  uuid: string;

  @Prop({ required: true })
  name: string;

  @Prop({ type: Types.ObjectId, ref: Room.name, autopopulate: true })
  room: Room;

  @Prop()
  bookingType: BookingType;

  @Prop({ required: true })
  start: number;

  @Prop({ required: true })
  end: number;

  @Prop({ required: false })
  email: string;
}

export const BookingSchema = SchemaFactory.createForClass(Booking);
