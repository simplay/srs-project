import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { EXPORT_JOB_MODEL_NAME, ExportJob } from './export-job.schema';

export type ActiveExportJobDocument = ActiveExportJob & Document;

@Schema()
export class ActiveExportJob {
  @Prop({ type: Types.ObjectId, ref: EXPORT_JOB_MODEL_NAME, autopopulate: true })
  exportJob: ExportJob;
}
export const ActiveExportJobSchema = SchemaFactory.createForClass(ActiveExportJob);

export const ACTIVE_EXPORT_JOB_MODEL_NAME = 'active_export_job';
