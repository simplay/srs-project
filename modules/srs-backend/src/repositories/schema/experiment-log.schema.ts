import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { User } from './user.schema';
import { Experiment } from './experiment.schema';
import { LogTag } from '../../common/enums/log-tag.enum';

export type ExperimentLogDocument = ExperimentLog & Document;

@Schema()
export class ExperimentLog {
  @Prop()
  datetime: number;

  @Prop()
  tags: LogTag[];

  // The experiment it links to (if applicable)
  @Prop({ type: Types.ObjectId, ref: Experiment.name, autopopulate: true })
  experiment: Experiment;

  // User performing the action
  @Prop({ type: Types.ObjectId, ref: User.name, autopopulate: true })
  user: User;

  @Prop()
  log: string;
}
export const ExperimentLogSchema = SchemaFactory.createForClass(ExperimentLog);

// Name to be used to build the collection name and to reference object
export const EXPERIMENT_LOG_MODEL_NAME = 'experiment_log';
