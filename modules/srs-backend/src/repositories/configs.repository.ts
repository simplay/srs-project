import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { Validator } from 'jsonschema';
import { Sensor } from '../common/entities/sensor.entity';
import { SensorType } from '../common/enums/sensor-type.enum';
import { Config, ConfigDocument } from './schema/config.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema, Types } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { getCurrentTimestamp } from '../common/utils/datetime.helper';
import { User } from './schema/user.schema';

const SENSORS_CONFIG_FORMAT_FILE = './modules/neurotec-sensors/definition/sensors-config-format.json';
const SENSORS_CONFIG_FILE = process.env['PIPELINE_SENSORS_FILEPATH'];

@Injectable()
export class ConfigsRepository {
  sensorsConfigFilePath: string;
  initialSensorsPerTypeConfig: Record<SensorType, Sensor[]>;

  constructor(@InjectModel(Config.name) private readonly configModel: Model<ConfigDocument>) {
    this.sensorsConfigFilePath = SENSORS_CONFIG_FILE;
    this.initInitialSensorsConfigFromJsonConfig();
  }

  private readSensorsConfigFormat = () => {
    let format: any = {};
    const formatString = fs.readFileSync(SENSORS_CONFIG_FORMAT_FILE, 'utf8');
    try {
      format = JSON.parse(formatString);
    } catch (err) {
      console.log('Error parsing JSON string:', err);
    }

    return format;
  };

  private initInitialSensorsConfigFromJsonConfig = () => {
    const validator = new Validator();
    const format = this.readSensorsConfigFormat();
    let jsonSensors: Record<string, any> = {};

    const jsonString = fs.readFileSync(this.sensorsConfigFilePath, 'utf8');
    try {
      jsonSensors = JSON.parse(jsonString);
      const producerValidation = validator.validate(jsonSensors, format);
      if (!producerValidation.valid) {
        throw `File '${this.sensorsConfigFilePath}' has incorrect format: ${producerValidation}`;
      }
    } catch (err) {
      console.log('Error parsing JSON string:', err);
    }

    this.initialSensorsPerTypeConfig = jsonSensors as Record<SensorType, Sensor[]>;
  };

  async createNewConfig(name: string, createdBy: User): Promise<ConfigDocument> {
    const config: Config = {
      uuid: uuidv4(),
      name: name,
      createdBy,
      createdAt: getCurrentTimestamp(),
      sensorsPerType: this.initialSensorsPerTypeConfig,
    };

    const configDocumentToSave = new this.configModel(config);
    return await configDocumentToSave.save();
  }

  async updateConfig(configDocument: ConfigDocument): Promise<ConfigDocument> {
    return await configDocument.save();
  }

  async getConfig(uuid: string) {
    const config = await this.configModel.findOne({ uuid }).exec();
    return config;
  }

  async listConfigs(): Promise<ConfigDocument[]> {
    return await this.configModel.find().exec();
  }

  async deleteConfig(uuid: string) {
    await this.configModel.deleteOne({ uuid }).exec();
  }
}
