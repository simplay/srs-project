import { INestApplicationContext } from '@nestjs/common';
import { IoAdapter } from '@nestjs/platform-socket.io';
import { AuthService } from './modules/auth/auth.service';
import { Role } from './common/enums/role.enum';
import { ExperimentsRepository } from './repositories/experiments.repository';
import { ServerOptions } from 'socket.io';
import { LogBookRepository } from './repositories/log-book.repository';
import { NAMESPACE_WEBSOCKET_CAMERAS, NAMESPACE_WEBSOCKET_SENSORS } from './common/utils/constants';
import { LogTag } from './common/enums/log-tag.enum';

// Inspired from this discussion:
// https://github.com/nestjs/nest/issues/882

export class AuthenticatedSocketIoAdapter extends IoAdapter {
  private authService: AuthService;
  private experimentsRepository: ExperimentsRepository;
  private logBookRepository: LogBookRepository;

  constructor(private app: INestApplicationContext) {
    super(app);
    app.resolve<AuthService>(AuthService).then((authService) => {
      this.authService = authService;
    });
    this.experimentsRepository = app.get(ExperimentsRepository);
    this.logBookRepository = app.get(LogBookRepository);
  }

  createIOServer(port: number, options?: ServerOptions): any {
    options.allowRequest = async (request, allowFunction) => {
      // We do not require authentication for the gateways running on port:
      // - 4111 (active-experiment-status)
      if (port === 4111) {
        return allowFunction(null, true);
      }

      // Didn't find a way here to get namespace of the websocket => used a query param called "name" to
      // distinguish the sensors websocket from the cameras websocket (as we want to log the access to the cameras only)
      if (!('_query' in request && 'name' in (request['_query'] as object))) {
        return allowFunction('Unauthorized', false);
      }

      // Check name of socket we want to access
      // TODO: define an interface for request['_query']
      const websocketName = (request['_query'] as any).name;
      if (!(websocketName === NAMESPACE_WEBSOCKET_SENSORS || websocketName === NAMESPACE_WEBSOCKET_CAMERAS)) {
        return allowFunction('Unauthorized', false);
      }

      let authorized = false;

      // Check the validity of the JWT token, users permissions and log access for the cameras
      while (true) {
        if (!('authorization' in request.headers)) {
          break;
        }

        const token = request.headers.authorization.replace('Bearer ', '');

        const user = await this.authService.verifyTokenAndGetUser(token);

        if (!user) {
          break;
        }

        // Only operators or admins are authorized
        if (!(user.roles.includes(Role.Operator) || user.roles.includes(Role.Admin))) {
          break;
        }

        // If user is operator only, check if he is part of the active experiment
        const activeExperiment = await this.experimentsRepository.getActiveExperiment();
        if (user.roles.includes(Role.Operator) && !user.roles.includes(Role.Admin)) {
          const index = activeExperiment.experiment.operators.findIndex(
            (operator) => operator.username === user.username,
          );
          if (index < 0) {
            break;
          }
        }

        // Log access
        if (websocketName === NAMESPACE_WEBSOCKET_CAMERAS) {
          await this.logBookRepository.saveLog(user, null, `Accessed cameras preview`, [LogTag.CAMERA]);
        }

        // If we reach this point, the user is authorized
        authorized = true;

        break;
      }

      return authorized ? allowFunction(null, true) : allowFunction('Unauthorized', false);
    };

    return super.createIOServer(port, options);
  }
}
