import { Logger, Module } from '@nestjs/common';
import { BookingsController } from './bookings.controller';
import { BookingsService } from './bookings.service';
import { BookingsRepository } from '../../repositories/bookings.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { Room, RoomSchema } from '../../repositories/schema/room.schema';
import { Booking, BookingSchema } from '../../repositories/schema/booking.schema';
import { MailModule } from '../mail/mail.module';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { LogBookModule } from '../log-book/log-book.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Room.name, schema: RoomSchema },
      { name: Booking.name, schema: BookingSchema },
    ]),
    MailModule,
    LogBookModule,
    UsersModule,
  ],
  controllers: [BookingsController],
  providers: [BookingsService, BookingsRepository, Logger],
})
export class BookingsModule {}
