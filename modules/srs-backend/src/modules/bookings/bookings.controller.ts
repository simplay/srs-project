import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { CreateUpdateRoomDto } from './dto/create-update-room.dto';
import { RoomDto } from './dto/room.dto';
import { BookingsService } from './bookings.service';
import { BookingDto } from './dto/booking.dto';
import { CreateUpdateBookingDto } from './dto/create-update-booking.dto';

@ApiTags('bookings')
@Controller('bookings')
export class BookingsController {
  constructor(private bookingsService: BookingsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Create a new room' })
  @ApiCreatedResponse({ type: RoomDto })
  @Post('rooms')
  async createRoom(@Body() createUpdateRoomDto: CreateUpdateRoomDto): Promise<RoomDto> {
    return await this.bookingsService.createRoom(createUpdateRoomDto);
  }

  // This endpoint is public
  @ApiOperation({ description: 'List rooms' })
  @ApiOkResponse({ type: RoomDto, isArray: true })
  @Get('rooms')
  async listRooms(): Promise<RoomDto[]> {
    return await this.bookingsService.listRooms();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Update an room' })
  @ApiCreatedResponse({ type: RoomDto })
  @Put('rooms/:uuid')
  async updateRoom(
    @Body() createUpdateRoomDto: CreateUpdateRoomDto,
    @Param('uuid', new ParseUUIDPipe()) uuid: string,
  ): Promise<RoomDto> {
    return await this.bookingsService.updateRoom(uuid, createUpdateRoomDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Delete a room. This will delete all the bookings associated with that room.' })
  @Delete('rooms/:uuid')
  async deleteRoom(@Param('uuid', new ParseUUIDPipe()) uuid: string) {
    return await this.bookingsService.deleteRoom(uuid);
  }

  // This endpoint is public
  @ApiOperation({ description: 'Get bookings for room' })
  @ApiOkResponse({ type: BookingDto, isArray: true })
  @Get(':roomUuid')
  async getBookingsForRoom(@Param('roomUuid', new ParseUUIDPipe()) roomUuid: string): Promise<BookingDto[]> {
    return await this.bookingsService.getBookingsForRoom(roomUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Secretariat)
  @ApiOperation({ description: 'Create a new booking for a room' })
  @ApiCreatedResponse({ type: BookingDto })
  @Post(':roomUuid')
  async createBooking(
    @Param('roomUuid', new ParseUUIDPipe()) roomUuid: string,
    @Body() createUpdateBookingDto: CreateUpdateBookingDto,
  ): Promise<BookingDto> {
    return await this.bookingsService.createBookingForRoom(roomUuid, createUpdateBookingDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Secretariat)
  @ApiOperation({ description: 'Delete a booking' })
  @Delete(':bookingUuid')
  async deleteBooking(@Param('bookingUuid', new ParseUUIDPipe()) bookingUuid: string) {
    return await this.bookingsService.deleteBooking(bookingUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Secretariat)
  @ApiOperation({ description: 'Update a booking for a room' })
  @ApiCreatedResponse({ type: BookingDto })
  @Put(':roomUuid/:bookingUuid')
  async updateBooking(
    @Param('roomUuid', new ParseUUIDPipe()) roomUuid: string,
    @Param('bookingUuid', new ParseUUIDPipe()) bookingUuid: string,
    @Body() createUpdateBookingDto: CreateUpdateBookingDto,
  ): Promise<RoomDto> {
    return await this.bookingsService.updateBookingForRoom(roomUuid, bookingUuid, createUpdateBookingDto);
  }
}
