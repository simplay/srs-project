import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { BookingType } from '../../../common/enums/booking-type.enum';

export class CreateUpdateBookingDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(BookingType)
  bookingType: BookingType;

  @ApiProperty()
  @IsNumber()
  start: number;

  @ApiProperty()
  @IsNumber()
  end: number;

  @ApiProperty()
  @IsEmail()
  @IsOptional()
  email: string;
}
