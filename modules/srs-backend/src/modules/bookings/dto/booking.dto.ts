import { ApiProperty } from '@nestjs/swagger';
import { BookingType } from '../../../common/enums/booking-type.enum';
import { RoomDto } from './room.dto';

export class BookingDto {
  @ApiProperty()
  readonly uuid: string;

  @ApiProperty()
  readonly room: RoomDto;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly bookingType: BookingType;

  @ApiProperty()
  readonly start: number;

  @ApiProperty()
  readonly end: number;

  @ApiProperty()
  readonly email: string;
}
