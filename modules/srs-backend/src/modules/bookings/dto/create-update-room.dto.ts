import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength } from 'class-validator';

export class CreateUpdateRoomDto {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  name: string;
}
