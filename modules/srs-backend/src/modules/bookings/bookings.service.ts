import { ForbiddenException, Inject, Injectable, Logger, NotFoundException, Scope } from '@nestjs/common';
import { BookingsRepository } from '../../repositories/bookings.repository';
import { CreateUpdateRoomDto } from './dto/create-update-room.dto';
import { Room, RoomDocument } from '../../repositories/schema/room.schema';
import { RoomDto } from './dto/room.dto';
import { BookingDto } from './dto/booking.dto';
import { CreateUpdateBookingDto } from './dto/create-update-booking.dto';
import { v4 as uuidv4 } from 'uuid';
import { Booking, BookingDocument } from '../../repositories/schema/booking.schema';
import { MailService } from '../mail/mail.service';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { UsersService } from '../users/users.service';
import { UsersRepository } from '../../repositories/users.repository';
import { ExperimentDocument } from '../../repositories/schema/experiment.schema';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable({ scope: Scope.REQUEST })
export class BookingsService {
  constructor(
    private readonly bookingsRepository: BookingsRepository,
    private mailService: MailService,
    private readonly logger: Logger = new Logger(BookingsService.name),
    private readonly logBookRepository: LogBookRepository,
    private readonly usersRepository: UsersRepository,
    @Inject(REQUEST) private request: Request,
  ) {}

  async createRoom(createUpdateRoomDto: CreateUpdateRoomDto): Promise<RoomDto> {
    const roomDocument = await this.bookingsRepository.saveRoom(createUpdateRoomDto.name);

    await this.saveLog(`Created new room "${roomDocument.name}" (${roomDocument.uuid})`);

    return this.mapRoomToRoomDto(roomDocument);
  }

  async listRooms(): Promise<RoomDto[]> {
    const roomsDocument = await this.bookingsRepository.getRooms();

    return roomsDocument.map((roomDocument) => this.mapRoomToRoomDto(roomDocument));
  }

  async updateRoom(uuid: string, createUpdateRoomDto: CreateUpdateRoomDto): Promise<RoomDto> {
    const room = await this.getRoomOrThrowError(uuid);

    const roomNameBeforeUpdate = room.name;

    room.name = createUpdateRoomDto.name;

    const updatedDocument = await this.bookingsRepository.updateRoom(room);

    await this.saveLog(`Updated room ${updatedDocument.uuid} (${roomNameBeforeUpdate} -> ${updatedDocument.name})`);

    return this.mapRoomToRoomDto(updatedDocument);
  }

  async deleteRoom(uuid: string) {
    const room = await this.getRoomOrThrowError(uuid);

    await this.bookingsRepository.deleteRoom(uuid);

    await this.saveLog(`Deleted room "${room.name}" (${room.uuid})`);
  }

  async getBookingsForRoom(roomUuid: string): Promise<BookingDto[]> {
    const room = await this.getRoomOrThrowError(roomUuid);

    const bookingsDocuments = await this.bookingsRepository.getBookingsForRoom(room);

    return bookingsDocuments.map((bookingDocument) => this.mapBookingToBookingDto(bookingDocument));
  }

  async createBookingForRoom(roomUuid: string, createUpdateBookingDto: CreateUpdateBookingDto): Promise<BookingDto> {
    this.checkBookingValidityOrThrowError(createUpdateBookingDto);

    const { name, bookingType, start, end, email } = createUpdateBookingDto;

    const room = await this.getRoomOrThrowError(roomUuid);

    const booking: Booking = {
      uuid: uuidv4(), // generate a new uuid
      name,
      room,
      bookingType,
      start,
      end,
      email,
    };

    const bookingDocument = await this.bookingsRepository.saveBookingForRoom(booking);

    await this.saveLog(`Created new booking "${booking.name}" (${booking.uuid}) for room "${booking.room.name}"`);

    if (email) {
      try {
        await this.mailService.sendBookingConfirmation(bookingDocument, false);
      } catch (e) {
        this.logger.error(e);
      }
    }

    return this.mapBookingToBookingDto(bookingDocument);
  }

  async updateBookingForRoom(
    roomUuid: string,
    bookingUuid: string,
    createUpdateBookingDto: CreateUpdateBookingDto,
  ): Promise<BookingDto> {
    this.checkBookingValidityOrThrowError(createUpdateBookingDto);

    const { name, bookingType, start, end, email } = createUpdateBookingDto;

    await this.getRoomOrThrowError(roomUuid);

    const booking = await this.getBookingOrThrowError(bookingUuid);

    // Update booking info
    booking.name = name;
    booking.bookingType = bookingType;
    booking.start = start;
    booking.end = end;
    booking.email = email;

    const bookingDocument = await this.bookingsRepository.saveBookingForRoom(booking);

    await this.saveLog(`Edited booking ${booking.uuid} ("${booking.name}") for room "${booking.room.name}"`);

    if (email) {
      try {
        await this.mailService.sendBookingConfirmation(bookingDocument, true);
      } catch (e) {
        this.logger.error(e);
      }
    }

    return this.mapBookingToBookingDto(bookingDocument);
  }

  async deleteBooking(bookingUuid: string) {
    const bookingDocument = await this.getBookingOrThrowError(bookingUuid);

    if (bookingDocument.email) {
      try {
        await this.mailService.sendBookingDeletionNotification(bookingDocument);
      } catch (e) {
        this.logger.error(e);
      }
    }

    await this.bookingsRepository.deleteBooking(bookingUuid);

    await this.saveLog(
      `Deleted booking "${bookingDocument.name}" (${bookingDocument.uuid}) for room "${bookingDocument.room.name}"`,
    );
  }

  private async getRoomOrThrowError(uuid: string): Promise<RoomDocument> {
    const room = await this.bookingsRepository.findRoom(uuid);
    if (!room) {
      throw new NotFoundException(`Room with uuid '${uuid}' not found`);
    }
    return room;
  }

  private async getBookingOrThrowError(uuid: string): Promise<BookingDocument> {
    const booking = await this.bookingsRepository.findBooking(uuid);
    if (!booking) {
      throw new NotFoundException(`Booking with uuid '${uuid}' not found`);
    }
    return booking;
  }

  private mapRoomToRoomDto(room: Room): RoomDto {
    const dto: RoomDto = {
      uuid: room.uuid,
      name: room.name,
    };
    return dto;
  }

  private mapBookingToBookingDto(booking: BookingDocument): BookingDto {
    const dto: BookingDto = {
      uuid: booking.uuid,
      room: this.mapRoomToRoomDto(booking.room),
      name: booking.name,
      start: booking.start,
      end: booking.end,
      bookingType: booking.bookingType,
      email: booking.email,
    };
    return dto;
  }

  private checkBookingValidityOrThrowError(createUpdateBookingDto: CreateUpdateBookingDto) {
    // Check that end is after start

    if (createUpdateBookingDto.end < createUpdateBookingDto.start) {
      throw new ForbiddenException('The start datetime of the booking must be before the end datetime.');
    }
  }

  async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // This is the express user
    const user = await this.usersRepository.findOneWithUsername(userPerformingRequest.username); // Find back the user document
    return user;
  }

  private async saveLog(logMessage: string) {
    const user = await this.getUserPerformingRequest();

    await this.logBookRepository.saveLog(user, null, logMessage, [LogTag.BOOKINGS]);
  }
}
