import { ConsoleLogger, forwardRef, Module } from '@nestjs/common';
import { SamplesController } from './samples.controller';
import { SamplesService } from './samples.service';
import { SamplesRepository } from '../../repositories/samples.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { SampleModel, SampleSchema } from '../../repositories/schema/sample.schema';
import { EXPORT_JOB_MODEL_NAME, ExportJobSchema } from '../../repositories/schema/export-job.schema';
import {
  ACTIVE_EXPORT_JOB_MODEL_NAME,
  ActiveExportJobSchema,
} from '../../repositories/schema/active-export-job.schema';
import { UsersModule } from '../users/users.module';
import { LogBookModule } from '../log-book/log-book.module';
import { ExperimentsModule } from '../experiments/experiments.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SampleModel.Camera, schema: SampleSchema },
      { name: SampleModel.Door, schema: SampleSchema },
      { name: SampleModel.Emfit, schema: SampleSchema },
      { name: SampleModel.Environment, schema: SampleSchema },
      { name: SampleModel.Lidar, schema: SampleSchema },
      { name: SampleModel.Luminance, schema: SampleSchema },
      { name: SampleModel.Mattress, schema: SampleSchema },
      { name: SampleModel.Microphones, schema: SampleSchema },
      { name: SampleModel.NightVisionCamera, schema: SampleSchema },
      { name: SampleModel.Radar, schema: SampleSchema },
      { name: SampleModel.Seismograph, schema: SampleSchema },
      { name: SampleModel.ShellyEm, schema: SampleSchema },
      { name: SampleModel.ShellyRed, schema: SampleSchema },
      { name: SampleModel.WaterFlow, schema: SampleSchema },

      { name: EXPORT_JOB_MODEL_NAME, schema: ExportJobSchema },
      { name: ACTIVE_EXPORT_JOB_MODEL_NAME, schema: ActiveExportJobSchema },
    ]),
    // Forward reference UsersModule to solve circular dependencies
    // SamplesModule imports UsersModule which imports ExperimentsModule which imports SamplesModule
    forwardRef(() => UsersModule),
    LogBookModule,
    // Forward reference ExperimentsModule to solve circular dependencies
    // SamplesModule imports ExperimentsModule which imports SamplesModule
    forwardRef(() => ExperimentsModule),
  ],
  controllers: [SamplesController],
  providers: [SamplesService, SamplesRepository, ConsoleLogger],
  exports: [SamplesRepository],
})
export class SamplesModule {}
