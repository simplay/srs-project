import {
  ConsoleLogger,
  ForbiddenException,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  Scope,
} from '@nestjs/common';
import { SamplesRepository } from '../../repositories/samples.repository';
import { execSync, spawn } from 'child_process';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import * as fs from 'fs';
import { ExperimentsService } from '../experiments/experiments.service';
import { ExperimentDto } from '../experiments/dto/experiment.dto';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { UsersRepository } from '../../repositories/users.repository';
import { ActiveExportJobDto } from './dto/active-export-job.dto';
import { ExportJob, ExportJobDocument } from '../../repositories/schema/export-job.schema';
import { ExportJobDto } from './dto/export-job.dto';
import { UsersService } from '../users/users.service';
import { ExportJobStatus } from '../../common/enums/export-job-status.enum';
import { ActiveExportJobDocument } from '../../repositories/schema/active-export-job.schema';
import { ExperimentDocument } from '../../repositories/schema/experiment.schema';
import treeKill from 'tree-kill';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { CreateExportJobDto } from './dto/create-export-job.dto';
import { ExportTimestampRange } from '../../common/interfaces/export-timestamp-range.interface';
import { ParticipantProgressDto } from '../experiments/dto/participant-progress.dto';
import { LogBookService } from '../log-book/log-book.service';
import { ExperimentLogDto } from '../experiments/dto/experiment-log.dto';
import { LogTag } from '../../common/enums/log-tag.enum';

const EXPORT_FOLDER = 'exports';

enum ProcessStatus {
  RUNNING,
  FAILED,
  COMPLETED,
}

@Injectable({ scope: Scope.REQUEST })
export class SamplesService {
  private errorDuringExport: boolean;
  private mongodumpOutputs: string[]; // This list will store the outputs of mongodump
  private nbrProcessesToBeSpawned: number;
  private exportJobsProcessIdProcessStatusMap: Record<number, ProcessStatus>; // This record will be used to track progress of spawned processes
  private killRunningExportProcessesAlreadyTriggered: boolean;
  private exportJobsCollectionNameParticipants: Record<string, string[]>; // Will hold the distinct participants for each collection to export for the export, so that we compute only once

  constructor(
    private readonly samplesRepository: SamplesRepository,
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly logBookRepository: LogBookRepository,
    private readonly logBookService: LogBookService,
    private readonly experimentsService: ExperimentsService,
    private readonly usersRepository: UsersRepository,
    private readonly usersService: UsersService,
    private readonly logger: ConsoleLogger,
    @Inject(REQUEST) private request: Request,
  ) {
    logger.setContext(SamplesService.name);
    this.errorDuringExport = false;
    this.mongodumpOutputs = [];
    this.nbrProcessesToBeSpawned = 0;
    this.exportJobsProcessIdProcessStatusMap = {};
    this.killRunningExportProcessesAlreadyTriggered = false;
    this.exportJobsCollectionNameParticipants = {};
  }

  async getSamplesCollectionsStats() {
    return await this.samplesRepository.getSamplesCollectionsStats();
  }

  async dropSamplesCollections() {
    const user = await this.getUserPerformingRequest();

    await this.samplesRepository.dropSamplesCollections();

    const logMessage = 'Deleted samples collections';
    await this.logBookRepository.saveLog(user, null, logMessage, [LogTag.SAMPLES_DELETION]);
  }

  async getActiveExportJob(): Promise<ActiveExportJobDto> {
    const activeExportJob = await this.getActiveExportJobOrThrowError();

    const activeExportJobDto: ActiveExportJobDto = {
      exportJobUuid: activeExportJob.exportJob.uuid,
    };
    return activeExportJobDto;
  }

  async deleteActiveExportJob() {
    await this.getActiveExportJobOrThrowError();
    await this.samplesRepository.deleteActiveExportJob();
  }

  async getExportJob(uuid: string): Promise<ExportJobDto> {
    const exportJob = await this.samplesRepository.getExportJob(uuid);
    if (!exportJob) {
      throw new NotFoundException(`Export job ${uuid} not found`);
    }

    return this.mapExportJobToExportJobDto(exportJob);
  }

  private mapExportJobToExportJobDto(exportJob: ExportJob): ExportJobDto {
    const dto: ExportJobDto = {
      uuid: exportJob.uuid,
      experiment: this.experimentsService.mapExperimentToExperimentDto(exportJob.experiment),
      createdAt: exportJob.createdAt,
      createdBy: this.usersService.mapUserToUserDto(exportJob.createdBy),
      status: exportJob.status,
      completedAt: exportJob.completedAt,
      mongodumpLog: exportJob.mongodumpLog,
      duration: exportJob.duration,
    };
    return dto;
  }

  getDateStringFromDate(date: Date) {
    // Return date with format YYYY-MM-DD

    // Wrap date with new Date() as when calling from tests (e2e), "date" in the CreateExportJobDto is a string and not a date for some reason ...
    return new Date(date).toISOString().slice(0, 10);
  }

  getCurrentDateTimeString(): string {
    // Return with format YYYY-MM-DD_HH-MM-SS
    const today = new Date();
    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    const time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
    return `${date}_${time}`;
  }

  async checkExportFolderDoesNotContainPreexistingDumpForExportDateOrThrowError(
    experimentUuid: string,
    dateExportString: string,
  ) {
    const pathExportExperiment = `${EXPORT_FOLDER}/${experimentUuid}`;
    if (fs.existsSync(pathExportExperiment)) {
      // From the collections to export that have been precomputed, compute the total distinct participants
      const distinctParticipants = [];
      // Loop through the keys of the record (collection names) => for(const ... in ...)
      for (const collection in this.exportJobsCollectionNameParticipants) {
        // Loop through the participants values => for(const ... of ...)
        for (const participant of this.exportJobsCollectionNameParticipants[collection]) {
          if (!distinctParticipants.includes(participant)) {
            distinctParticipants.push(participant);
          }
        }
      }

      // Check that for each participant, a dump for that date does not exist already
      // EXPERIMENT_UUID/PARTICIPANT_UUID/[YYYY-MM-DD]_*.dump
      for (const participantUuid of distinctParticipants) {
        const path = `${pathExportExperiment}/${participantUuid}`;
        if (fs.existsSync(path)) {
          const files = fs.readdirSync(path);
          for (const file of files) {
            if (file.includes(dateExportString)) {
              throw new ForbiddenException(
                `A dump with name ${file} exists already for date ${dateExportString} at path ${path} and might be overwritten by export. Please move any pre-existing dumps having the export date in filename before exporting for that date.`,
              );
            }
          }
        }
      }
    }
  }

  async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // This is the express user
    const user = await this.usersRepository.findOneWithUsername(userPerformingRequest.username); // Find back the user document
    return user;
  }

  async createExportJob(createExportJobDto: CreateExportJobDto): Promise<ExportJobDto> {
    // TODO: make test
    // Check that no export job is currently running
    if (await this.samplesRepository.getActiveExportJob()) {
      throw new ForbiddenException('An active export job already exists');
    }

    const activeExperiment = await this.experimentsRepository.getActiveExperiment();

    // Check that an experiment is set as active
    if (!activeExperiment) {
      throw new ForbiddenException('No active experiment found. Cannot derive experiment uuid');
    }

    const experiment = await this.experimentsRepository.getExperiment(activeExperiment.experiment.uuid);

    // Check that the experiment is not currently recording
    if (activeExperiment.isRecording) {
      throw new ForbiddenException('Please stop recording before exporting the data');
    }

    const user = await this.getUserPerformingRequest();

    // Check that some data exists for date
    const dateExport = createExportJobDto.date;
    const dateExportString = this.getDateStringFromDate(dateExport);
    const exportTimestampRange = this.getExportTimestampRangeForDate(dateExport);

    const samplesCollectionsToExport = await this.samplesRepository.getNonEmptySamplesCollectionsNames(
      exportTimestampRange,
    );

    if (samplesCollectionsToExport.length === 0) {
      throw new ForbiddenException(
        `No sample found in samples collections for date ${dateExportString}. Please record data first.`,
      );
    }

    // Compute once:
    // - the distinct participants for each collection to export
    // - the number of processes to be spawned (so we can check upon process completion if they all completed already)
    // We optimize to only compute this once ahead of the real export job
    for (const collectionName of samplesCollectionsToExport) {
      const distinctParticipantsUuids =
        await this.samplesRepository.getDistinctParticipantsForSamplesCollectionInTimestampRange(
          collectionName,
          exportTimestampRange,
        );
      this.nbrProcessesToBeSpawned += distinctParticipantsUuids.length;
      this.exportJobsCollectionNameParticipants[collectionName] = distinctParticipantsUuids;
    }

    // TODO: make tests
    // Perform sanity check if the export will not overwrite any existing dump
    await this.checkExportFolderDoesNotContainPreexistingDumpForExportDateOrThrowError(
      experiment.uuid,
      dateExportString,
    );

    // Create export job
    const exportJob: ExportJobDocument = await this.samplesRepository.createNewExportJob(
      user,
      activeExperiment.experiment,
    );
    await this.saveLogForExport(
      user,
      experiment,
      `Created export job for date ${dateExportString}. Export job uuid: ${exportJob.uuid}`,
    );

    // Set the export job as active before executing the job
    await this.samplesRepository.setActiveExportJob(exportJob);

    // Execute the job async
    this.executeExportJob(exportJob, experiment, user, samplesCollectionsToExport, dateExport).then(() => {
      this.saveLogForExport(user, experiment, 'Done spawning mongodump jobs.');
    });

    return this.mapExportJobToExportJobDto(exportJob);
  }

  async executeExportJob(
    exportJob: ExportJob,
    experiment: ExperimentDocument,
    user: UserDocument,
    collectionsToExport: string[],
    dateExport: Date,
  ) {
    await this.saveLogForExport(user, experiment, `Execute export job ${exportJob.uuid}`);

    const experimentUuid = exportJob.experiment.uuid;

    // Create the folder for the experiment
    this.createFolderForExperiment(experiment.uuid);

    // Variable used to build filenames later on
    const currentDateTimeString = this.getCurrentDateTimeString();

    // Save experiment information (we convert to DTO to only expose limited user infos)
    const experimentFileName = `experiment_${currentDateTimeString}.json`;
    const experimentDto: ExperimentDto = this.experimentsService.mapExperimentToExperimentDto(exportJob.experiment);
    fs.writeFileSync(`${EXPORT_FOLDER}/${experimentUuid}/${experimentFileName}`, JSON.stringify(experimentDto));

    // Save participants progress
    const participantsProgressFileName = `progress_participants_${currentDateTimeString}.json`;
    const participantsProgress: ParticipantProgressDto[] =
      await this.experimentsService.getParticipantsProgressForExperiment(experiment.uuid);
    fs.writeFileSync(
      `${EXPORT_FOLDER}/${experimentUuid}/${participantsProgressFileName}`,
      JSON.stringify(participantsProgress),
    );

    // Export the data per collection
    for (const collectionName of collectionsToExport) {
      await this.exportDataForCollection(experimentUuid, collectionName, exportJob.uuid, experiment, user, dateExport);
    }
  }

  private getExportTimestampRangeForDate(date: Date): ExportTimestampRange {
    // Timestamp range for export from (0:0 to 23:59)
    // setUTCHours returns in ms => convert to s
    const range: ExportTimestampRange = {
      // Wrap date with new Date() as when calling from tests (e2e), "date" in the CreateExportJobDto is a string and not a date object for some reason ...
      timestampFrom: new Date(date).setUTCHours(0, 0, 0, 0) / 1000,
      timestampTo: new Date(date).setUTCHours(23, 59, 59, 999) / 1000,
    };
    return range;
  }

  async exportDataForCollection(
    experimentUuid: string,
    collectionName: string,
    exportJobUuid: string,
    experiment: ExperimentDocument,
    user: UserDocument,
    dateExport: Date,
  ) {
    const participantsUuids = this.exportJobsCollectionNameParticipants[collectionName];

    const exportDateString = this.getDateStringFromDate(dateExport);

    for (const participantUuid of participantsUuids) {
      await this.saveLogForExport(
        user,
        experiment,
        `Export data for collection ${collectionName} for participant ${participantUuid} for date ${exportDateString}`,
      );

      if (this.errorDuringExport) {
        await this.completeExportJob(exportJobUuid, experiment, user);
        break;
      }

      // Create first the output folder before creating the archive with 'mongodump'
      this.createFolderForParticipant(experimentUuid, participantUuid);

      const timestampRange = this.getExportTimestampRangeForDate(dateExport);
      const args = [
        '--host',
        process.env.MONGODB_HOST,
        '--username',
        process.env.MONGODB_USER,
        '--password',
        process.env.MONGODB_PASSWORD,
        '--authenticationDatabase',
        'admin',
        '--db',
        process.env.MONGODB_DB_NAME,
        '--collection',
        collectionName,
        '--query',
        `{ "participantUuid": "${participantUuid}", "timestamp": { "$gte" : ${timestampRange.timestampFrom}, "$lt": ${timestampRange.timestampTo} } }`,
        `--archive=${EXPORT_FOLDER}/${experimentUuid}/${participantUuid}/${exportDateString}_${collectionName}.dump`,
      ];
      const mongodump = spawn('mongodump', args);

      const pid = mongodump.pid;

      await this.saveLogForExport(
        user,
        experiment,
        `Spawned mongodump job for ${collectionName}, participant ${participantUuid}. PID = ${pid}`,
      );

      this.exportJobsProcessIdProcessStatusMap[pid] = ProcessStatus.RUNNING;

      mongodump.stdout.on('data', (data) => {
        this.mongodumpOutputs.push(data.toString());
        this.saveLogForExport(user, experiment, data.toString());
      });

      mongodump.stderr.on('data', (data) => {
        // Mongo dump output progress on stderr :-(
        this.mongodumpOutputs.push(data.toString());
        this.saveLogForExport(user, experiment, data.toString());
      });

      mongodump.on('error', (code) => {
        this.errorDuringExport = true;
        this.saveLogForExport(user, experiment, `Process with PID ${mongodump.pid} failed. Exit code: ${code}`);

        this.exportJobsProcessIdProcessStatusMap[pid] = ProcessStatus.FAILED;

        // Kill running processes if any
        this.killRunningExportProcesses(experiment, user);

        this.completeExportJob(exportJobUuid, experiment, user);
      });

      mongodump.on('close', (code) => {
        this.exportJobsProcessIdProcessStatusMap[pid] = ProcessStatus.COMPLETED;
        this.saveLogForExport(user, experiment, `Process with PID ${mongodump.pid} completed`);

        if (code !== 0) {
          this.saveLogForExport(user, experiment, `Process with ${pid} exited with a non-zero code: ${code}`);
          this.errorDuringExport = true;
        }

        const nbrOfProcessAlreadySpawned = Object.keys(this.exportJobsProcessIdProcessStatusMap).length;

        if (nbrOfProcessAlreadySpawned === this.nbrProcessesToBeSpawned) {
          if (!this.isRemainingExportProcessRunning()) {
            this.saveLogForExport(user, experiment, 'No pending jobs remaining => flag export as completed.');
            this.completeExportJob(exportJobUuid, experiment, user);
          } else {
            this.logRemainingExportProcessRunning(user, experiment);
          }
        }
      });
    }
  }

  private isRemainingExportProcessRunning() {
    let isRemaining = false;
    for (const key of Object.keys(this.exportJobsProcessIdProcessStatusMap)) {
      if (this.exportJobsProcessIdProcessStatusMap[key] !== ProcessStatus.COMPLETED) {
        isRemaining = true;
        break;
      }
    }
    return isRemaining;
  }

  private async logRemainingExportProcessRunning(user: UserDocument, experiment: ExperimentDocument) {
    for (const key of Object.keys(this.exportJobsProcessIdProcessStatusMap)) {
      if (this.exportJobsProcessIdProcessStatusMap[key] !== ProcessStatus.COMPLETED) {
        await this.saveLogForExport(user, experiment, `Waiting for process with PID ${key} to complete.`);
      }
    }
  }

  private async killRunningExportProcesses(experiment: ExperimentDocument, user: UserDocument) {
    if (this.killRunningExportProcessesAlreadyTriggered) {
      return;
    }
    this.killRunningExportProcessesAlreadyTriggered = true;

    await this.saveLogForExport(null, experiment, 'Kill running export jobs');

    for (const pid of Object.keys(this.exportJobsProcessIdProcessStatusMap)) {
      if (this.exportJobsProcessIdProcessStatusMap[pid] === ProcessStatus.RUNNING) {
        await this.saveLogForExport(null, experiment, `Kill process with PID ${pid}`);
        treeKill(parseInt(pid), 'SIGKILL', function (err) {
          if (err) {
            this.experimentsRepository.saveExperimentLog(
              user,
              experiment,
              `Error killing process with PID ${pid}: ${err.message}`,
            );
          }
        });
      }
    }
  }

  async completeExportJob(exportJobUuid: string, experiment: ExperimentDocument, user: UserDocument) {
    const status: ExportJobStatus = this.errorDuringExport ? ExportJobStatus.FAILED : ExportJobStatus.SUCCEEDED;

    await this.saveLogForExport(user, experiment, `Complete export job with status ${status}`);

    // Save log in for experiment in export folder
    const currentDateTimeString = this.getCurrentDateTimeString();
    const logBookFileName = `experiment_log_${currentDateTimeString}.json`;
    const logBook: ExperimentLogDto[] = await this.logBookService.getLogBookForExperiment(experiment);
    fs.writeFileSync(`${EXPORT_FOLDER}/${experiment.uuid}/${logBookFileName}`, JSON.stringify(logBook));

    await this.samplesRepository.completeExportJobWithStatusAndLog(exportJobUuid, status, this.mongodumpOutputs);

    this.resetLogicForNextExport();
  }

  private resetLogicForNextExport() {
    this.nbrProcessesToBeSpawned = 0;
    this.exportJobsProcessIdProcessStatusMap = {};
    this.errorDuringExport = false;
    this.mongodumpOutputs = [];
    this.killRunningExportProcessesAlreadyTriggered = false;
    this.exportJobsCollectionNameParticipants = {};
  }

  createFolderForExperiment(experimentUuid: string) {
    const folder = `${EXPORT_FOLDER}/${experimentUuid}`;
    try {
      execSync(`mkdir -p ${folder}`);
    } catch (e) {
      const message = 'Error creating export folder for experiment';
      throw new InternalServerErrorException(message);
    }
  }

  createFolderForParticipant(experimentUuid: string, participantUuid: string) {
    const folder = `${EXPORT_FOLDER}/${experimentUuid}/${participantUuid}`;
    try {
      execSync(`mkdir -p ${folder}`);
    } catch (e) {
      const message = 'Error creating export folder for participant';
      throw new InternalServerErrorException(message);
    }
  }

  private async getActiveExportJobOrThrowError(): Promise<ActiveExportJobDocument> {
    const activeExportJob = await this.samplesRepository.getActiveExportJob();
    if (!activeExportJob) {
      throw new NotFoundException('No active export found');
    }
    return activeExportJob;
  }

  private async saveLogForExport(user: UserDocument, experiment: ExperimentDocument, logMessage: string) {
    // Adds the "export" tag to the log
    await this.logBookRepository.saveLog(user, experiment, logMessage, [LogTag.EXPORT]);
  }
}
