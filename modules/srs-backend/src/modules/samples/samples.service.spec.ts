import { Test, TestingModule } from '@nestjs/testing';
import { SamplesService } from './samples.service';
import { SamplesRepository } from '../../repositories/samples.repository';

describe('SamplesService', () => {
  let service: SamplesService;

  const mockSamplesRepository = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SamplesService,
        {
          provide: SamplesRepository,
          useValue: mockSamplesRepository,
        },
      ],
    }).compile();

    service = module.get<SamplesService>(SamplesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
