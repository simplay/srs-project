import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Query, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { SamplesService } from './samples.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { SamplesCollectionStatsDto } from './dto/samples-collection-stats.dto';
import { ActiveExportJobDto } from './dto/active-export-job.dto';
import { ExportJobDto } from './dto/export-job.dto';
import { CreateExportJobDto } from './dto/create-export-job.dto';

@Controller('samples')
@ApiTags('samples')
export class SamplesController {
  constructor(private samplesService: SamplesService) {}

  @ApiOperation({ description: 'Returns the stats of the samples collections at instant of request.' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get('collections-stats')
  async getSamplesCollectionsStats(): Promise<SamplesCollectionStatsDto[]> {
    return await this.samplesService.getSamplesCollectionsStats();
  }

  @ApiOperation({ description: 'Drop the samples collection.' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Delete('drop-samples-collections')
  dropSamplesCollections() {
    return this.samplesService.dropSamplesCollections();
  }

  @ApiOperation({ description: 'Export the samples from the active experiment.' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiCreatedResponse({ type: ExportJobDto })
  @Post('export-job')
  createExportJob(@Body() createExportJobDto: CreateExportJobDto) {
    return this.samplesService.createExportJob(createExportJobDto);
  }

  @ApiOperation({ description: 'Get an export job.' })
  @ApiBearerAuth()
  @ApiOkResponse({ type: ExportJobDto })
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get('export-job/:uuid')
  async getExportJob(@Param('uuid', new ParseUUIDPipe()) exportJobUuid: string): Promise<ExportJobDto> {
    return await this.samplesService.getExportJob(exportJobUuid);
  }

  @ApiOperation({ description: 'Check if an export job is ongoing.' })
  @ApiBearerAuth()
  @ApiOkResponse({ type: ActiveExportJobDto })
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Get('active-export-job')
  async getActiveExportJob(): Promise<ActiveExportJobDto> {
    return await this.samplesService.getActiveExportJob();
  }

  @ApiOperation({ description: 'Delete the currently set active export job.' })
  @ApiBearerAuth()
  @ApiOkResponse()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @Delete('active-export-job')
  async deleteActiveExportJob() {
    return await this.samplesService.deleteActiveExportJob();
  }
}
