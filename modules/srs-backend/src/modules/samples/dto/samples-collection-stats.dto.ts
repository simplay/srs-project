import { ApiProperty } from '@nestjs/swagger';

export class SamplesCollectionStatsDto {
  @ApiProperty()
  collectionName: string;

  @ApiProperty()
  numberOfSamples: number;

  @ApiProperty()
  distinctSensorsIps: string[];

  // Collection size in bytes
  @ApiProperty()
  size: number;

  // Average object size in bytes
  @ApiProperty()
  avgObjSize: number;

  // (Pre)allocated space for the collection in bytes.
  @ApiProperty()
  storageSize: number;
}
