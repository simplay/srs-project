import { IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserDto } from '../../users/dto/user.dto';
import { ExperimentDto } from '../../experiments/dto/experiment.dto';
import { ExportJobStatus } from '../../../common/enums/export-job-status.enum';

export class ExportJobDto {
  @ApiProperty()
  @IsUUID()
  uuid: string;

  @ApiProperty()
  experiment: ExperimentDto;

  @ApiProperty()
  createdAt: number;

  @ApiProperty()
  completedAt: number;

  @ApiProperty()
  status: ExportJobStatus;

  @ApiProperty()
  createdBy: UserDto;

  @ApiProperty()
  mongodumpLog: string[];

  @ApiProperty()
  duration: number;
}
