import { ApiProperty } from '@nestjs/swagger';
import { IsDate } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateExportJobDto {
  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  date: Date;
}
