import { forwardRef, Module } from '@nestjs/common';
import { ConfigsController } from './configs.controller';
import { ConfigsService } from './configs.service';
import { ConfigsRepository } from '../../repositories/configs.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { Config, ConfigSchema } from '../../repositories/schema/config.schema';
import { UsersModule } from '../users/users.module';
import { ExperimentsModule } from '../experiments/experiments.module';
import { LogBookModule } from '../log-book/log-book.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Config.name, schema: ConfigSchema }]),
    // Forward reference ExperimentsModule to solve circular dependencies
    // ConfigsModule imports ExperimentsModule which imports ConfigsModule
    forwardRef(() => ExperimentsModule),
    // Forward reference UsersModule to solve circular dependencies
    // ConfigsModule imports UsersModule which imports ExperimentsModule (which is forwardRef above)
    forwardRef(() => UsersModule),
    LogBookModule,
  ],
  controllers: [ConfigsController],
  providers: [ConfigsService, ConfigsRepository],
  exports: [ConfigsService, ConfigsRepository], // Exports for DI
})
export class ConfigsModule {}
