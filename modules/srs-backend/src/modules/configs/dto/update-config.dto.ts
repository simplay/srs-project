import { Sensor } from '../../../common/entities/sensor.entity';
import { SensorType } from '../../../common/enums/sensor-type.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateConfigDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  sensorsPerType: Record<SensorType, Sensor[]>;
}
