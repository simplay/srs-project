import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class CreateConfigDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;
}
