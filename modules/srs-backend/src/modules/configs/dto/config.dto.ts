import { ApiProperty } from '@nestjs/swagger';
import { SensorType } from '../../../common/enums/sensor-type.enum';
import { Sensor } from '../../../common/entities/sensor.entity';

// Returned
export class ConfigDto {
  @ApiProperty()
  readonly uuid: string;

  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly createdAt: number;

  @ApiProperty()
  readonly createdByUsername: string;

  @ApiProperty()
  readonly sensorsPerType: Record<SensorType, Sensor[]>;
}
