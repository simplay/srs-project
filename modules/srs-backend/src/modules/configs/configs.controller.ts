import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ConfigsService } from './configs.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { UpdateConfigDto } from './dto/update-config.dto';
import { CreateConfigDto } from './dto/create-config.dto';
import { ConfigDto } from './dto/config.dto';

@ApiTags('configs')
@Controller('configs')
export class ConfigsController {
  constructor(private configService: ConfigsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'List configurations' })
  @ApiOkResponse({ type: ConfigDto, isArray: true })
  @Get('')
  async listConfigs(): Promise<ConfigDto[]> {
    return await this.configService.listConfigs();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Get the configuration with name' })
  @ApiOkResponse({ type: ConfigDto })
  @Get(':uuid')
  async getConfig(@Param('uuid', new ParseUUIDPipe()) configUuid: string): Promise<ConfigDto> {
    return await this.configService.getConfig(configUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Update the configuration for the experiment' })
  @ApiCreatedResponse({ type: ConfigDto })
  @Put(':uuid')
  async updateConfig(
    @Param('uuid', new ParseUUIDPipe()) configUuid: string,
    @Body() updateConfigDto: UpdateConfigDto,
  ): Promise<ConfigDto> {
    return await this.configService.updateConfig(configUuid, updateConfigDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Create a new configuration for the experiment' })
  @ApiCreatedResponse({ type: ConfigDto })
  @Post()
  async createConfig(@Body() createExperimentConfigDto: CreateConfigDto): Promise<ConfigDto> {
    return await this.configService.createConfig(createExperimentConfigDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Delete a config' })
  @Delete(':uuid')
  async deleteConfig(@Param('uuid', new ParseUUIDPipe()) configUuid: string) {
    return await this.configService.deleteConfig(configUuid);
  }
}
