import { ConflictException, ForbiddenException, Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { ConfigsRepository } from '../../repositories/configs.repository';
import { UpdateConfigDto } from './dto/update-config.dto';
import { Config, ConfigDocument } from '../../repositories/schema/config.schema';
import { ExperimentRunningConfigNotEditableException } from '../../common/exceptions/experiment-running-config-not-editable.exception';
import { CreateConfigDto } from './dto/create-config.dto';
import { ConfigDto } from './dto/config.dto';
import { Sensor } from '../../common/entities/sensor.entity';
import { SensorType } from '../../common/enums/sensor-type.enum';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { UsersRepository } from '../../repositories/users.repository';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable({ scope: Scope.REQUEST })
export class ConfigsService {
  constructor(
    private readonly configsRepository: ConfigsRepository,
    private readonly usersRepository: UsersRepository,
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly logBookRepository: LogBookRepository,
    @Inject(REQUEST) private request: Request,
  ) {}

  async getConfig(uuid: string): Promise<ConfigDto> {
    const config = await this.getConfigOrThrowError(uuid);
    return this.mapConfigToConfigDto(config);
  }

  async createConfig(createConfigDto: CreateConfigDto): Promise<ConfigDto> {
    const name = createConfigDto.name;
    const config = await this.configsRepository.getConfig(name);
    if (config) {
      throw new ConflictException(`Config with name '${name}' exists already`);
    }

    const user = await this.getUserPerformingRequest();

    const createdConfig = await this.configsRepository.createNewConfig(name, user);

    await this.logBookRepository.saveLog(
      user,
      null,
      `Created new config "${createdConfig.name} (${createdConfig.uuid})"`,
      [LogTag.EXPERIMENTS_CONFIG],
    );

    return this.mapConfigToConfigDto(createdConfig);
  }

  async deleteConfig(uuid: string) {
    const config = await this.getConfigOrThrowError(uuid);

    // Check if config is used in an experiment
    const experiments = await this.experimentsRepository.getExperimentsHavingConfig(config);

    if (experiments.length > 0) {
      const experimentsNames = experiments.map((experiment) => experiment.name).join(',');
      throw new ForbiddenException(
        `Config "${config.name}" is used in experiment(s): ${experimentsNames}. Please remove the config from experiment(s) before deleting the config`,
      );
    }

    await this.configsRepository.deleteConfig(uuid);

    const user = await this.getUserPerformingRequest();
    await this.logBookRepository.saveLog(user, null, `Deleted config "${config.name} (${config.uuid})"`, [
      LogTag.EXPERIMENTS_CONFIG,
    ]);
  }

  async getConfigOrThrowError(uuid: string): Promise<ConfigDocument> {
    const config = await this.configsRepository.getConfig(uuid);
    if (!config) {
      throw new NotFoundException(`Config with uuid '${uuid}' not found`);
    }
    return config;
  }

  async isEditableConfig(config: Config) {
    // We don't allow to edit a config while recording
    // The recording must be stopped in order to update the config
    const activeExperiment = await this.experimentsRepository.getActiveExperiment();
    if (activeExperiment && activeExperiment.isRecording) {
      const configUuidActiveExperiment = activeExperiment.experiment.config.uuid;

      if (configUuidActiveExperiment === config.uuid) {
        return false;
      }
    }
    return true;
  }
  async updateConfig(uuid: string, updateConfigDto: UpdateConfigDto): Promise<ConfigDto> {
    const config = await this.configsRepository.getConfig(uuid);
    if (!config) {
      throw new NotFoundException(`Config '${uuid}' not found`);
    }

    if (!(await this.isEditableConfig(config))) {
      throw new ExperimentRunningConfigNotEditableException();
    }

    // Update config document
    config.name = updateConfigDto.name;
    config.sensorsPerType = updateConfigDto.sensorsPerType;

    const updatedConfig = await this.configsRepository.updateConfig(config);

    const user = await this.getUserPerformingRequest();
    await this.logBookRepository.saveLog(user, null, `Updated config ${config.uuid}"`, [LogTag.EXPERIMENTS_CONFIG]);

    return this.mapConfigToConfigDto(updatedConfig);
  }

  async listConfigs(): Promise<ConfigDto[]> {
    const configs = await this.configsRepository.listConfigs();
    return configs.map((config) => this.mapConfigToConfigDto(config));
  }

  mapConfigToConfigDto(config: Config) {
    const dto: ConfigDto = {
      uuid: config.uuid,
      name: config.name,
      createdAt: config.createdAt,
      createdByUsername: config.createdBy.username,
      sensorsPerType: config.sensorsPerType as Record<SensorType, Sensor[]>,
    };
    return dto;
  }

  async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // This is the express user
    const user = await this.usersRepository.findOneWithUsername(userPerformingRequest.username); // Find back the user document
    return user;
  }
}
