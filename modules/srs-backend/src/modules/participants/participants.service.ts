import {
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  Scope,
} from '@nestjs/common';
import { ParticipantDto } from './dto/participant.dto';
import { ParticipantsRepository } from '../../repositories/participants.repository';
import { Participant, ParticipantDocument } from '../../repositories/schema/participant.schema';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import { UsersRepository } from '../../repositories/users.repository';
import { UsersService } from '../users/users.service';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { UpdateParticipantDto } from './dto/update-participant.dto';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable({ scope: Scope.REQUEST })
export class ParticipantsService {
  constructor(
    // @Inject(forwardRef(() => ExperimentsRepository))
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly participantsRepository: ParticipantsRepository,
    private readonly usersRepository: UsersRepository,
    private readonly usersService: UsersService,
    private readonly logBookRepository: LogBookRepository,
    @Inject(REQUEST) private request: Request,
  ) {}

  async listParticipants(): Promise<ParticipantDto[]> {
    let participants: Participant[] = [];

    const user = await this.getUserPerformingRequest();
    // Return all the participants for the admin
    // For the operator, return only the participants that operator has access to (participants from the experiments the operator is included)
    if (this.usersService.isAdmin(user)) {
      participants = await this.participantsRepository.listParticipants();
    } else if (this.usersService.isOperator(user)) {
      const experiments = await this.experimentsRepository.getExperimentsForUser(user);
      experiments.forEach((experiment) => {
        participants.push(...experiment.participants);
      });
    }

    return participants.map((participant) => this.mapParticipantToParticipantDto(participant));
  }

  async createParticipant(createParticipantDto: CreateParticipantDto): Promise<ParticipantDto> {
    await this.checkUniquenessOfParticipantOrThrowError(createParticipantDto.uuid, createParticipantDto.shortNote);

    const userPerformingRequest = this.request.user as User;
    const user = await this.usersRepository.findOneWithUsername(userPerformingRequest.username);
    const createdParticipant = await this.participantsRepository.createNewParticipant(
      createParticipantDto.uuid,
      createParticipantDto.shortNote,
      user,
    );
    await this.saveLog(`Created new participant: ${createdParticipant.uuid} (${createdParticipant.shortNote})`);
    return this.mapParticipantToParticipantDto(createdParticipant);
  }

  async checkUniquenessOfParticipantOrThrowError(participantUuid: string, shortNote: string) {
    // Check participant uuid
    if (await this.participantsRepository.getParticipant(participantUuid)) {
      throw new ConflictException(`Participant with uuid '${participantUuid}' exists already`);
    }

    await this.checkParticipantWithShortNameDoesNotAlreadyExistOrThrowError(shortNote);
  }

  async checkParticipantWithShortNameDoesNotAlreadyExistOrThrowError(shortNote: string) {
    if (await this.participantsRepository.getParticipantWithShortNote(shortNote)) {
      throw new ConflictException(`Participant with short note '${shortNote}' exists already. Please make it unique.`);
    }
  }

  async deleteParticipant(participantUuid: string) {
    const participant = await this.getParticipantOrThrowError(participantUuid);

    const experiments = await this.experimentsRepository.getExperimentsForParticipant(participant);

    if (experiments.length > 0) {
      const experimentsNames = experiments.map((experiment) => experiment.name).join(',');
      throw new ForbiddenException(
        `The participant is part of experiment(s): ${experimentsNames}. Please remove the participant from experiment(s) before deleting the participant`,
      );
    }
    await this.participantsRepository.deleteParticipant(participant.uuid);

    await this.saveLog(`Deleted participant: ${participant.uuid} (${participant.shortNote})`);
  }

  async updateParticipant(participantUuid: string, updateParticipantDto: UpdateParticipantDto) {
    // TODO: check who can do that. Admin it's OK, but for Operators, probably only the participants linked to the same experiment

    const participant = await this.getParticipantOrThrowError(participantUuid);

    await this.checkParticipantWithShortNameDoesNotAlreadyExistOrThrowError(updateParticipantDto.shortNote);

    const shortNodeBeforeUpdate = participant.shortNote;

    // Update participant
    participant.shortNote = updateParticipantDto.shortNote;

    const updatedParticipant = await this.participantsRepository.updateParticipant(participant);

    await this.saveLog(
      `Updated participant: ${participant.uuid} ("${shortNodeBeforeUpdate}" -> "${updatedParticipant.shortNote}")`,
    );

    return this.mapParticipantToParticipantDto(updatedParticipant);
  }

  private async getParticipantOrThrowError(participantUuid: string): Promise<ParticipantDocument> {
    const participant = await this.participantsRepository.getParticipant(participantUuid);
    if (!participant) {
      throw new NotFoundException(`Participant with uuid '${participantUuid}' not found`);
    }
    return participant;
  }

  mapParticipantToParticipantDto(participant: Participant) {
    const dto: ParticipantDto = {
      uuid: participant.uuid,
      shortNote: participant.shortNote,
      createdAt: participant.createdAt,
      createdBy: this.usersService.mapUserToUserDto(participant.createdBy),
    };
    return dto;
  }

  private async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // this is Express request user

    return await this.getUserOrThrowError(userPerformingRequest.username);
  }

  private async getUserOrThrowError(username: string): Promise<UserDocument> {
    const user = await this.usersRepository.findOneWithUsername(username);
    if (!user) {
      throw new NotFoundException(`User with username '${username}' not found`);
    }
    return user;
  }

  private async saveLog(log: string) {
    const user = await this.getUserPerformingRequest();
    await this.logBookRepository.saveLog(user, null, log, [LogTag.EXPERIMENTS_CONFIG]);
  }
}
