import { Body, Controller, Delete, Get, Param, ParseUUIDPipe, Post, Put, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ParticipantsService } from './participants.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { ParticipantDto } from './dto/participant.dto';
import { CreateParticipantDto } from './dto/create-participant.dto';
import { UpdateParticipantDto } from './dto/update-participant.dto';

@Controller('participants')
@ApiTags('participants')
export class ParticipantsController {
  constructor(private participantsService: ParticipantsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'List participants' })
  @ApiOkResponse({ type: ParticipantDto, isArray: true })
  @Get('')
  async listParticipants(): Promise<ParticipantDto[]> {
    return await this.participantsService.listParticipants();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Create a new participant' })
  @ApiCreatedResponse({ type: ParticipantDto })
  @Post()
  async createParticipant(@Body() createParticipantDto: CreateParticipantDto): Promise<ParticipantDto> {
    return await this.participantsService.createParticipant(createParticipantDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Update an existing participant' })
  @ApiCreatedResponse({ type: ParticipantDto })
  @Put(':uuid')
  async updateParticipant(
    @Body() updateParticipantDto: UpdateParticipantDto,
    @Param('uuid', new ParseUUIDPipe()) participantUuid: string,
  ): Promise<ParticipantDto> {
    return await this.participantsService.updateParticipant(participantUuid, updateParticipantDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Delete participant' })
  @Delete(':uuid')
  async deleteParticipants(@Param('uuid', new ParseUUIDPipe()) participantUuid: string) {
    return await this.participantsService.deleteParticipant(participantUuid);
  }
}
