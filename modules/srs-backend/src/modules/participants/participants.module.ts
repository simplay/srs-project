import { forwardRef, Module } from '@nestjs/common';
import { ParticipantsController } from './participants.controller';
import { ParticipantsService } from './participants.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Participant, ParticipantSchema } from '../../repositories/schema/participant.schema';
import { ParticipantsRepository } from '../../repositories/participants.repository';
import { UsersModule } from '../users/users.module';
import { LogBookModule } from '../log-book/log-book.module';
import { ExperimentsModule } from '../experiments/experiments.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Participant.name, schema: ParticipantSchema }]),
    LogBookModule,
    // Forward reference ExperimentsModule to solve circular dependencies
    // ExperimentsModule imports ParticipantsModule which imports ExperimentsModule
    forwardRef(() => ExperimentsModule),
    // Forward reference UsersModule to solve circular dependencies
    // UsersModule imports ExperimentsModule which imports UsersModule
    forwardRef(() => UsersModule),
  ],
  controllers: [ParticipantsController],
  providers: [ParticipantsService, ParticipantsRepository],
  exports: [ParticipantsService, ParticipantsRepository], // Exports for DI
})
export class ParticipantsModule {}
