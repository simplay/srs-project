import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MinLength } from 'class-validator';

export class UpdateParticipantDto {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(3)
  shortNote: string;
}
