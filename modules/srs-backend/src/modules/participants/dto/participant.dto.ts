import { ApiProperty } from '@nestjs/swagger';
import { IsUUID } from 'class-validator';
import { UserDto } from '../../users/dto/user.dto';

export class ParticipantDto {
  @ApiProperty()
  @IsUUID()
  readonly uuid: string;

  @ApiProperty()
  readonly shortNote: string;

  @ApiProperty()
  readonly createdAt: number;

  @ApiProperty()
  readonly createdBy: UserDto;
}
