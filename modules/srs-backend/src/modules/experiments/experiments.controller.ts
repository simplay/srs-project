import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseArrayPipe,
  ParseUUIDPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiQuery, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { ExperimentDto } from './dto/experiment.dto';
import { ExperimentsService } from './experiments.service';
import { CreateExperimentDto } from './dto/create-experiment.dto';
import { UpdateExperimentDto } from './dto/update-experiment.dto';
import { ActiveExperimentDto } from './dto/active-experiment.dto';
import { StartRecordingExperimentDto } from './dto/start-recording-experiment.dto';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskCurrentRecordingDto } from './dto/update-task-current-recording.dto';
import { ParticipantProgressDto } from './dto/participant-progress.dto';
import { MessageScreenEntranceDto } from './dto/message-screen-entrance.dto';
import { CreateMessageScreenEntranceDto } from './dto/create-message-screen-entrance.dto';
import { ActiveExperimentPublicInfoDto } from './dto/active-experiment-public-info.dto';
import { CreateExperimentNoteDto } from './dto/create-experiment-note.dto';
import { ExperimentLogDto } from './dto/experiment-log.dto';

@ApiTags('experiments')
@Controller('experiments')
export class ExperimentsController {
  constructor(private experimentsService: ExperimentsService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'List all experiments' })
  @ApiOkResponse({ type: ExperimentDto, isArray: true })
  @Get('')
  async listAllExperiments(): Promise<ExperimentDto[]> {
    return await this.experimentsService.listAllExperiments();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Create a new experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Post()
  async createExperiment(@Body() createExperimentDto: CreateExperimentDto): Promise<ExperimentDto> {
    return await this.experimentsService.createExperiment(createExperimentDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Get the active experiment' })
  @ApiOkResponse({ type: ActiveExperimentDto })
  @Get('active')
  async getActiveExperiment(): Promise<ActiveExperimentDto> {
    return await this.experimentsService.getActiveExperiment();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Set the experiment as active' })
  @ApiQuery({ name: 'experimentUuid' })
  @ApiCreatedResponse({ type: ActiveExperimentDto })
  @Post('set-active')
  async setExperimentAsActive(
    @Query('experimentUuid', new ParseUUIDPipe()) experimentUuid: string,
  ): Promise<ActiveExperimentDto> {
    return await this.experimentsService.setExperimentAsActive(experimentUuid);
  }

  // This endpoint is public
  @ApiOperation({
    description: 'Get minimum info about the currently going experiment (aimed for the screen on the entrance door)',
  })
  @ApiOkResponse({ type: ActiveExperimentPublicInfoDto })
  @Get('active-experiment-public-info')
  async getActiveExperimentPublic(): Promise<ActiveExperimentPublicInfoDto> {
    return await this.experimentsService.getActiveExperimentPublic();
  }

  // This endpoint is public
  @ApiOperation({ description: 'Get the message for the screen entrance' })
  @ApiOkResponse({ type: MessageScreenEntranceDto })
  @Get('message-screen-entrance')
  async getMessageScreenEntrance(): Promise<MessageScreenEntranceDto> {
    return await this.experimentsService.getMessageScreenEntrance();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Set the message for the screen entrance' })
  @ApiCreatedResponse({ type: MessageScreenEntranceDto })
  @Post('message-screen-entrance')
  async setMessageScreenEntrance(
    @Body() createMessageScreenEntranceDto: CreateMessageScreenEntranceDto,
  ): Promise<MessageScreenEntranceDto> {
    return await this.experimentsService.setMessageScreenEntrance(createMessageScreenEntranceDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Get experiment with uuid' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Get(':uuid')
  async getExperiment(@Param('uuid', new ParseUUIDPipe()) experimentUuid: string): Promise<ExperimentDto> {
    return await this.experimentsService.getExperiment(experimentUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Update the experiment definition' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Put(':uuid')
  async updateExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Body() updateExperimentDto: UpdateExperimentDto,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.updateExperiment(experimentUuid, updateExperimentDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Add a list of participants to the experiment' })
  @ApiQuery({ name: 'participants', isArray: true })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Post(':uuid/add-participants')
  async addParticipantsToExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Query('participants', new ParseArrayPipe({ items: String, separator: ',' })) participantsUuidList: string[],
  ): Promise<ExperimentDto> {
    return await this.experimentsService.addParticipantsToExperiment(experimentUuid, participantsUuidList);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Remove a participant from the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Post(':uuid/remove-participant')
  async removeParticipantFromExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Query('participant', new ParseUUIDPipe()) participantUuid: string,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.removeParticipantFromExperiment(experimentUuid, participantUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Add a list of operators to the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @ApiQuery({ name: 'operators', isArray: true })
  @Post(':uuid/add-operators')
  async addOperatorsToExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Query('operators', new ParseArrayPipe({ items: String, separator: ',' })) operatorsUsernameList: string[],
  ): Promise<ExperimentDto> {
    return await this.experimentsService.addOperatorsToExperiment(experimentUuid, operatorsUsernameList);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Remove an operator from the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Post(':uuid/remove-operator')
  async removeOperatorFromExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Query('operator') operatorUsername: string,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.removeOperatorFromExperiment(experimentUuid, operatorUsername);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Add a task to the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Post(':uuid/add-task')
  async addTaskToExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Body() createTaskDto: CreateTaskDto,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.addTaskToExperiment(experimentUuid, createTaskDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Update a task of the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Put(':experimentUuid/task/:taskUuid')
  async updateTaskOfExperiment(
    @Param('experimentUuid', new ParseUUIDPipe()) experimentUuid: string,
    @Param('taskUuid', new ParseUUIDPipe()) taskUuid: string,
    @Body() updateTaskDto: UpdateTaskDto,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.updateTaskOfExperiment(experimentUuid, taskUuid, updateTaskDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Delete a task from the experiment' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Delete(':experimentUuid/task/:taskUuid')
  async deleteTaskFromExperiment(
    @Param('experimentUuid', new ParseUUIDPipe()) experimentUuid: string,
    @Param('taskUuid', new ParseUUIDPipe()) taskUuid: string,
  ): Promise<ExperimentDto> {
    return await this.experimentsService.deleteTaskFromExperiment(experimentUuid, taskUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Start recording of the experiment' })
  @ApiCreatedResponse({ type: ActiveExperimentDto })
  @Put(':uuid/start-recording')
  async startRecordingExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Body() startRecordingExperimentDto: StartRecordingExperimentDto,
  ): Promise<ActiveExperimentDto> {
    return await this.experimentsService.startRecordingExperiment(experimentUuid, startRecordingExperimentDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Update the task of the current recording' })
  @ApiCreatedResponse({ type: ActiveExperimentDto })
  @Put(':uuid/update-task')
  async updateTaskCurrentRecording(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
    @Body() updateTaskRecordingDto: UpdateTaskCurrentRecordingDto,
  ): Promise<ActiveExperimentDto> {
    return await this.experimentsService.updateTaskCurrentRecording(experimentUuid, updateTaskRecordingDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({
    description:
      'Increment the trial for the task being currently recorded. A record for the task must exists already to increment trial.',
  })
  @ApiCreatedResponse({ type: ActiveExperimentDto })
  @Put(':uuid/increment-trial-current-recording')
  async incrementTrialCurrentRecording(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
  ): Promise<ActiveExperimentDto> {
    return await this.experimentsService.incrementTrialCurrentRecording(experimentUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Stop recording of the experiment' })
  @ApiCreatedResponse({ type: ActiveExperimentDto })
  @Put(':uuid/stop-recording')
  async stopRecordingExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
  ): Promise<ActiveExperimentDto> {
    return await this.experimentsService.stopRecordingExperiment(experimentUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Complete experiment. Experiment becomes immutable.' })
  @ApiCreatedResponse({ type: ExperimentDto })
  @Put(':uuid/complete-experiment')
  async completeExperiment(@Param('uuid', new ParseUUIDPipe()) experimentUuid: string): Promise<ExperimentDto> {
    return await this.experimentsService.completeExperiment(experimentUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Get participants progress for an experiment' })
  @ApiCreatedResponse({ type: ParticipantProgressDto, isArray: true })
  @Get(':uuid/participants-progress')
  async getParticipantsProgressForExperiment(
    @Param('uuid', new ParseUUIDPipe()) experimentUuid: string,
  ): Promise<ParticipantProgressDto[]> {
    return await this.experimentsService.getParticipantsProgressForExperiment(experimentUuid);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin, Role.Operator)
  @ApiOperation({ description: 'Create a new entry in the logbook for experiment' })
  @ApiCreatedResponse({ type: ExperimentLogDto })
  @Post(':uuid/note')
  async createNoteForExperiment(@Body() note: CreateExperimentNoteDto): Promise<ExperimentLogDto> {
    return await this.experimentsService.saveNoteForExperiment(note);
  }
}
