export class ParticipantProgressDto {
  participantUuid: string;
  experimentUuid: string;
  taskUuid: string;
  trialCounter: number;
}
