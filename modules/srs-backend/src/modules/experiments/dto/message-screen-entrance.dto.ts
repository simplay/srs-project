import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class MessageScreenEntranceDto {
  @ApiProperty()
  message: string;

  @ApiProperty()
  @IsOptional()
  updatedAt: number;

  // We only expose the full name of the user, not the full userDto
  @ApiProperty()
  createdByUserFullName: string;
}
