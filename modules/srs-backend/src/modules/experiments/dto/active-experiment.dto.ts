import { ParticipantDto } from '../../participants/dto/participant.dto';
import { ExperimentDto } from './experiment.dto';
import { ApiProperty } from '@nestjs/swagger';

export class ActiveExperimentDto {
  @ApiProperty()
  participant: ParticipantDto;

  @ApiProperty()
  isRecording: boolean;

  @ApiProperty()
  experiment: ExperimentDto;

  @ApiProperty()
  taskUuid: string;

  @ApiProperty()
  trialNbr: number;
}
