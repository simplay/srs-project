import { ApiProperty } from '@nestjs/swagger';

// Aimed to provide only a limited information about the experiment running in the loft
export class ActiveExperimentPublicInfoDto {
  @ApiProperty()
  isRecording: boolean;

  @ApiProperty()
  experimentName: string;
}
