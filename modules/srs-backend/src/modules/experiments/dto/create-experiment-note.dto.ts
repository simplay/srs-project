import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class CreateExperimentNoteDto {
  @ApiProperty()
  @IsUUID()
  experimentUuid: string;

  @ApiProperty()
  @IsNotEmpty()
  logLine: string;
}
