import { IsNumber, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class StartRecordingExperimentDto {
  @ApiProperty()
  @IsUUID()
  participantUuid: string;

  @ApiProperty()
  @IsUUID()
  taskUuid: string;
}
