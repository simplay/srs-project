import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsUUID } from 'class-validator';

export class UpdateTaskCurrentRecordingDto {
  @ApiProperty()
  @IsOptional() // we allow to reset the taskUuid
  @IsUUID()
  taskUuid: string;
}
