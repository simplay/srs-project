import { ApiProperty } from '@nestjs/swagger';
import { LogTag } from '../../../common/enums/log-tag.enum';

export class ExperimentLogDto {
  @ApiProperty()
  datetime: number;

  @ApiProperty()
  experimentUuid: string;

  @ApiProperty()
  username: string;

  @ApiProperty()
  log: string;

  @ApiProperty()
  tags: LogTag[];
}
