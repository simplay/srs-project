import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateMessageScreenEntranceDto {
  @ApiProperty()
  @IsNotEmpty()
  message: string;
}
