import { ParticipantDto } from '../../participants/dto/participant.dto';
import { ConfigDto } from '../../configs/dto/config.dto';
import { ApiProperty } from '@nestjs/swagger';
import { UserDto } from '../../users/dto/user.dto';
import { TaskDto } from './task.dto';

export class ExperimentDto {
  @ApiProperty()
  uuid: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  participants: ParticipantDto[];

  @ApiProperty()
  owner: UserDto;

  @ApiProperty()
  operators: UserDto[];

  @ApiProperty()
  config: ConfigDto;

  @ApiProperty()
  tasks: TaskDto[];

  @ApiProperty()
  createdAt: number;

  @ApiProperty()
  startedAt: number;

  @ApiProperty()
  completedAt: number;

  @ApiProperty()
  completed: boolean;
}
