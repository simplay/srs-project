import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsUUID } from 'class-validator';

export class UpdateExperimentDto {
  @ApiProperty()
  @IsUUID()
  uuid: string;

  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsUUID()
  configUuid: string;

  @ApiProperty()
  @IsNotEmpty()
  ownerUsername: string;
}
