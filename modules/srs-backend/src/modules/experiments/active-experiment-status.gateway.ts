import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { ActiveExperimentPublicInfoDto } from './dto/active-experiment-public-info.dto';
import { NAMESPACE_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS } from '../../common/utils/constants';

/*
  The ActiveExperimentStatusGateway reports if an experiment is currently under recording
  Gateways on port 4111 doest not require JWT authentication
 */

@WebSocketGateway(4111, {
  namespace: NAMESPACE_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS,
  cors: {
    // TODO: restrict origin once proper dns
    origin: '*',
    methods: ['GET'],
    credentials: true,
  },
})
export class ActiveExperimentStatusGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('ActiveExperimentStatusGateway');

  afterInit(server: Server): any {
    this.logger.log('Initialized');
  }

  handleConnection(socket: Socket, ...args: any[]): any {
    this.logger.log(`Client connected: ${socket.id}`);
  }

  handleDisconnect(socket: Socket): any {
    this.logger.log(`Client disconnected: ${socket.id}`);
  }

  sendToAll(message: ActiveExperimentPublicInfoDto) {
    this.wss.emit('backendToClient', message);
  }
}
