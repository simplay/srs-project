import {
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
  Scope,
} from '@nestjs/common';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import { ExperimentDto } from './dto/experiment.dto';
import { Experiment, ExperimentDocument, Task } from '../../repositories/schema/experiment.schema';
import { CreateExperimentDto } from './dto/create-experiment.dto';
import { ParticipantsRepository } from '../../repositories/participants.repository';
import { ConfigsRepository } from '../../repositories/configs.repository';
import { getCurrentTimestamp } from '../../common/utils/datetime.helper';
import { UpdateExperimentDto } from './dto/update-experiment.dto';
import { ConfigDocument } from '../../repositories/schema/config.schema';
import { ParticipantDocument } from '../../repositories/schema/participant.schema';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { UsersRepository } from '../../repositories/users.repository';
import { UsersService } from '../users/users.service';
import { ParticipantsService } from '../participants/participants.service';
import { Role } from '../../common/enums/role.enum';
import { ActiveExperiment, ActiveExperimentDocument } from '../../repositories/schema/active-experiment.schema';
import { ImmutableExperimentException } from '../../common/exceptions/immutable-experiment.exception';
import { ActiveExperimentDto } from './dto/active-experiment.dto';
import { ConfigsService } from '../configs/configs.service';
import { SamplesRepository } from '../../repositories/samples.repository';
import { ErrorDbNotEmptyException } from '../../common/exceptions/error-db-not-empty.exception';
import { StartRecordingExperimentDto } from './dto/start-recording-experiment.dto';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskDto } from './dto/task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskCurrentRecordingDto } from './dto/update-task-current-recording.dto';
import { ExperimentLog, ExperimentLogDocument } from '../../repositories/schema/experiment-log.schema';
import { ExperimentLogDto } from './dto/experiment-log.dto';
import { ParticipantProgress } from '../../repositories/schema/participant-progress.schema';
import { ParticipantProgressDto } from './dto/participant-progress.dto';
import { MessageScreenEntrance } from '../../repositories/schema/message-screen-entrance.schema';
import { MessageScreenEntranceDto } from './dto/message-screen-entrance.dto';
import { CreateMessageScreenEntranceDto } from './dto/create-message-screen-entrance.dto';
import { ActiveExperimentPublicInfoDto } from './dto/active-experiment-public-info.dto';
import { ActiveExperimentStatusGateway } from './active-experiment-status.gateway';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { CreateExperimentNoteDto } from './dto/create-experiment-note.dto';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable({ scope: Scope.REQUEST })
export class ExperimentsService {
  constructor(
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly logBookRepository: LogBookRepository,
    // @Inject(forwardRef(() => ParticipantsRepository))
    private readonly participantsRepository: ParticipantsRepository,
    // @Inject(forwardRef(() => ParticipantsService))
    private readonly participantsService: ParticipantsService,
    private readonly configsRepository: ConfigsRepository,
    private readonly configsService: ConfigsService,
    private readonly usersRepository: UsersRepository,
    private readonly usersService: UsersService,
    private readonly samplesRepository: SamplesRepository,
    private readonly activeExperimentStatusGateway: ActiveExperimentStatusGateway,
    @Inject(REQUEST) private request: Request,
  ) {}

  async listAllExperiments(): Promise<ExperimentDto[]> {
    const experiments = await this.experimentsRepository.listExperiments();
    return experiments.map((experiment) => this.mapExperimentToExperimentDto(experiment));
  }

  async getExperiment(uuid: string): Promise<ExperimentDto> {
    const experiment = await this.getExperimentOrThrowError(uuid);
    return this.mapExperimentToExperimentDto(experiment);
  }

  async getActiveExperiment(): Promise<ActiveExperimentDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    const userPerformingRequest = this.request.user as User;
    if (this.usersService.isAdmin(userPerformingRequest)) {
      // An admin can see any experiment
    } else if (this.usersService.isOperator(userPerformingRequest)) {
      // Check the operator is in the list of operators for the experiment
      const found = activeExperiment.experiment.operators.find(
        (operator) => operator.username === userPerformingRequest.username,
      );
      if (!found) {
        throw new ForbiddenException(
          `You are not listed as an operator for the active experiment and can't access the running experiment.`,
        );
      }
    } else {
      // TODO: add users to experiments abd make test
    }

    return this.mapActiveExperimentToActiveExperimentDto(activeExperiment);
  }

  async setExperimentAsActive(experimentUuid: string): Promise<ActiveExperimentDto> {
    if (await this.samplesRepository.samplesAlreadyCollected()) {
      throw new ErrorDbNotEmptyException();
    }
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    this.isEditableExperimentOrThrowError(experiment);

    const activeExperiment: ActiveExperiment = {
      experiment,
      isRecording: false,
      participant: null,
      taskUuid: null,
      trialNbr: null,
    };
    const updatedActiveExperimentDocument = await this.experimentsRepository.replaceActiveExperiment(activeExperiment);

    return this.mapActiveExperimentToActiveExperimentDto(updatedActiveExperimentDocument);
  }

  private async saveLog(experiment: ExperimentDocument, log: string): Promise<ExperimentLogDocument> {
    const user = await this.getUserPerformingRequest();
    return await this.logBookRepository.saveLog(user, experiment, log, [LogTag.EXPERIMENTS_CONFIG]);
  }

  async createExperiment(createExperimentDto: CreateExperimentDto): Promise<ExperimentDto> {
    const experimentUuid = createExperimentDto.uuid;
    const experiment = await this.experimentsRepository.getExperiment(experimentUuid);
    if (experiment) {
      throw new ConflictException(`Experiment with uuid '${experimentUuid}' exists already`);
    }

    const configUuid = createExperimentDto.configUuid;
    const config = await this.getConfigOrThrowError(configUuid);

    const ownerUsername = createExperimentDto.ownerUsername;
    const owner = await this.getUserOrThrowError(ownerUsername);

    const experimentToSave: Experiment = {
      uuid: experimentUuid,
      name: createExperimentDto.name,
      participants: [],
      owner,
      operators: [],
      tasks: [],
      config,
      createdAt: getCurrentTimestamp(),
      startedAt: null,
      completedAt: null,
      completed: false,
    };
    const createdExperiment = await this.experimentsRepository.createNewExperiment(experimentToSave);

    await this.saveLog(
      createdExperiment,
      `Created new experiment '${createdExperiment.name}' (${createdExperiment.uuid})`,
    );

    return this.mapExperimentToExperimentDto(createdExperiment);
  }

  async updateExperiment(experimentUuid: string, updateExperimentDto: UpdateExperimentDto): Promise<ExperimentDto> {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // Check the new config exists
    const configUuid = updateExperimentDto.configUuid;
    const config = await this.getConfigOrThrowError(configUuid);

    // Check the new owner exists
    const newOwnerUsername = updateExperimentDto.ownerUsername;
    const newOwner = await this.getUserOrThrowError(newOwnerUsername);

    // TODO: add test
    // Check the new owner is an admin
    if (!newOwner.roles.includes(Role.Admin)) {
      throw new ForbiddenException(`The new owner must be an Admin`);
    }

    // We allow to update the uuid, name, config and owner
    experiment.uuid = updateExperimentDto.uuid;
    experiment.name = updateExperimentDto.name;
    experiment.config = config;
    experiment.owner = newOwner;

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(updatedExperiment, `Updated experiment ${experimentUuid}`);
    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async addParticipantsToExperiment(experimentUuid: string, participantsUuidList: string[]) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // If the user performing request is an operator, we check that it is part of the experiment
    const user = await this.getUserPerformingRequest();
    if (this.usersService.isOperator(user)) {
      const indexFound = experiment.operators.findIndex((operator) => operator.username === user.username);
      if (indexFound < 0) {
        throw new ForbiddenException(
          `You cannot add participants to this experiment as you are not listed as an operator.`,
        );
      }
    }

    // Loop first once through each participants provided and check that they all well exist
    for (let i = 0; i < participantsUuidList.length; i++) {
      const participantUuid = participantsUuidList[i];
      await this.getParticipantOrThrowError(participantUuid);
    }

    // Now, let's make the job to add the participants
    const existingParticipantsUuids = experiment.participants.map((participant) => participant.uuid);
    for (let i = 0; i < participantsUuidList.length; i++) {
      const participantUuidToAdd = participantsUuidList[i];
      const participantToAdd = await this.getParticipantOrThrowError(participantUuidToAdd);

      if (!existingParticipantsUuids.includes(participantUuidToAdd)) {
        experiment.participants.push(participantToAdd);
      }
    }

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Added participants ${participantsUuidList.join(',')} to experiment '${updatedExperiment.name}' (${
        updatedExperiment.uuid
      })`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async removeParticipantFromExperiment(experimentUuid: string, participantUuid: string) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // If the user performing request is an operator, we check that it is part of the experiment
    const user = await this.getUserPerformingRequest();
    if (this.usersService.isOperator(user)) {
      const indexFound = experiment.operators.findIndex((operator) => operator.username === user.username);
      if (indexFound < 0) {
        throw new ForbiddenException(
          `You cannot remove participant from this experiment as you are not listed as an operator.`,
        );
      }
    }

    await this.getParticipantOrThrowError(participantUuid);

    const found = experiment.participants.find((participant) => participant.uuid === participantUuid);

    if (!found) {
      throw new ForbiddenException(`Participant ${participantUuid} is not part of experiment ${experiment.name}`);
    } else {
      experiment.participants = experiment.participants.filter((participant) => participant.uuid !== participantUuid);
    }

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Removed participant ${participantUuid} from experiment '${updatedExperiment.name}' (${updatedExperiment.uuid})`,
    );
    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async addOperatorsToExperiment(experimentUuid: string, operatorsUsernameList: string[]) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // Loop first once through each operators provided and check that they all well exist
    for (let i = 0; i < operatorsUsernameList.length; i++) {
      const operatorUsername = operatorsUsernameList[i];
      await this.getUserOrThrowError(operatorUsername);
    }

    // Now, let's make the job to add the operators
    const existingOperatorsUsernameList = experiment.operators.map((operator) => operator.username);
    for (let i = 0; i < operatorsUsernameList.length; i++) {
      const operatorUsernameToAdd = operatorsUsernameList[i];
      const operatorToAdd = await this.getUserOrThrowError(operatorUsernameToAdd);

      if (!existingOperatorsUsernameList.includes(operatorUsernameToAdd)) {
        experiment.operators.push(operatorToAdd);
      }
    }

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Added operators ${operatorsUsernameList.join(',')} to experiment '${updatedExperiment.name}' (${
        updatedExperiment.uuid
      })`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async removeOperatorFromExperiment(experimentUuid: string, operatorUsername: string) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    await this.getUserOrThrowError(operatorUsername);

    const found = experiment.operators.find((operator) => operator.username === operatorUsername);

    if (!found) {
      throw new ForbiddenException(`Operator ${operatorUsername} is not part of experiment ${experiment.name}`);
    } else {
      experiment.operators = experiment.operators.filter((operator) => operator.username !== operatorUsername);
    }

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Removed operator ${operatorUsername} from experiment '${updatedExperiment.name}' (${updatedExperiment.uuid})`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async addTaskToExperiment(experimentUuid: string, createTaskDto: CreateTaskDto) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // Check the task UUID does not exist already
    if (experiment.tasks.findIndex((task) => task.uuid === createTaskDto.uuid) >= 0) {
      throw new ForbiddenException(`A task with UUID ${createTaskDto.uuid} already exists.`);
    }

    // Check the task name does not exist already (to prevent confusion)
    if (experiment.tasks.findIndex((task) => task.name === createTaskDto.name) >= 0) {
      throw new ForbiddenException(`A task with name ${createTaskDto.name} already exists.`);
    }

    const task: Task = {
      uuid: createTaskDto.uuid,
      name: createTaskDto.name,
      description: createTaskDto.description,
    };
    experiment.tasks.push(task);
    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Added task ${task.name} to experiment '${updatedExperiment.name}' (${updatedExperiment.uuid})`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async updateTaskOfExperiment(experimentUuid: string, taskUuid: string, updateTaskDto: UpdateTaskDto) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // Check that task exists
    const indexTaskToUpdate = experiment.tasks.findIndex((task) => task.uuid === taskUuid);
    if (indexTaskToUpdate < 0) {
      throw new NotFoundException(`Task ${taskUuid} not found`);
    }

    // Check that the new task does not conflict with another one
    const index = experiment.tasks.findIndex((task) => task.uuid !== taskUuid && task.name === updateTaskDto.name);
    if (index >= 0) {
      throw new ForbiddenException(`A task with same name already exist. Please make task names unique.`);
    }

    experiment.tasks[indexTaskToUpdate].name = updateTaskDto.name;
    experiment.tasks[indexTaskToUpdate].description = updateTaskDto.description;

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Updated task ${experiment.tasks[indexTaskToUpdate].uuid} of experiment '${updatedExperiment.name}' (${updatedExperiment.uuid})`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async deleteTaskFromExperiment(experimentUuid: string, taskUuid: string) {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // Check task exists
    const index = experiment.tasks.findIndex((task) => task.uuid === taskUuid);
    if (index < 0) {
      throw new NotFoundException(`Task with uuid ${taskUuid} not found.`);
    }

    experiment.tasks = experiment.tasks.filter((task) => task.uuid !== taskUuid);
    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Deleted task ${taskUuid} from experiment '${updatedExperiment.name}' (${updatedExperiment.uuid})`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  private checkExperimentUuidIsActiveExperimentOrThrowError(experimentUuid: string, activeExperiment) {
    if (activeExperiment.experiment.uuid !== experimentUuid) {
      throw new ForbiddenException(`Experiment with uuid '${experimentUuid}' is not the active configuration`);
    }
  }

  private async checkOperatorIsPartOfExperimentOrThrowError(user: User, activeExperiment: ActiveExperiment) {
    const index = activeExperiment.experiment.operators.findIndex((operator) => operator.username === user.username);

    if (index < 0) {
      throw new ForbiddenException(
        `You are not listed as an operator of the experiment and cannot perform this action.`,
      );
    }
  }

  async startRecordingExperiment(
    experimentUuid: string,
    startRecordingExperimentDto: StartRecordingExperimentDto,
  ): Promise<ActiveExperimentDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    // Only operators/admins arrive here. The controller endpoint does not authorize other roles
    const user = await this.getUserPerformingRequest();

    if (this.usersService.isOperator(user)) {
      await this.checkOperatorIsPartOfExperimentOrThrowError(user, activeExperiment);
    }

    this.checkExperimentUuidIsActiveExperimentOrThrowError(experimentUuid, activeExperiment);

    await this.isEditableExperimentOrThrowError(activeExperiment.experiment);

    const participant = await this.getParticipantOrThrowError(startRecordingExperimentDto.participantUuid);

    const task = this.getTaskOrThrowError(activeExperiment.experiment, startRecordingExperimentDto.taskUuid);

    // Update startedAt date for experiment if null
    const experiment = await this.getExperimentOrThrowError(experimentUuid);
    if (!experiment.startedAt) {
      experiment.startedAt = getCurrentTimestamp();
      await this.experimentsRepository.updateExperiment(experiment);
    }

    // Update participant progress
    const participantProgress = await this.experimentsRepository.updateParticipantProgressForExperimentTask(
      participant,
      experiment,
      task.uuid,
    );

    // Update active experiment
    activeExperiment.isRecording = true;
    activeExperiment.participant = participant;
    activeExperiment.taskUuid = startRecordingExperimentDto.taskUuid;
    activeExperiment.trialNbr = participantProgress.trialCounter;

    const updatedActiveExperiment = await this.experimentsRepository.updateActiveExperiment(activeExperiment);
    const log = `Started recording: participant ${updatedActiveExperiment.participant.uuid}, task: ${task.name} (${updatedActiveExperiment.taskUuid}), trial: ${updatedActiveExperiment.trialNbr}`;
    await this.saveLog(experiment, log);

    this.sendNotificationActiveExperimentStatusGateway(activeExperiment);

    return this.mapActiveExperimentToActiveExperimentDto(updatedActiveExperiment);
  }

  sendNotificationActiveExperimentStatusGateway(activeExperiment: ActiveExperiment) {
    const activeExperimentPublicDto: ActiveExperimentPublicInfoDto = {
      experimentName: activeExperiment.experiment.name,
      isRecording: activeExperiment.isRecording,
    };
    this.activeExperimentStatusGateway.sendToAll(activeExperimentPublicDto);
  }

  async updateTaskCurrentRecording(
    experimentUuid: string,
    updateTaskCurrentRecordingDto: UpdateTaskCurrentRecordingDto,
  ): Promise<ActiveExperimentDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    // Only operators/admins arrive here. The controller endpoint does not authorize other roles
    const user = await this.getUserPerformingRequest();

    if (this.usersService.isOperator(user)) {
      await this.checkOperatorIsPartOfExperimentOrThrowError(user, activeExperiment);
    }

    await this.isEditableExperimentOrThrowError(activeExperiment.experiment);
    this.checkExperimentUuidIsActiveExperimentOrThrowError(experimentUuid, activeExperiment);

    const experiment = await this.getExperimentOrThrowError(experimentUuid);
    const participant = await this.getParticipantOrThrowError(activeExperiment.participant.uuid);
    const task = this.getTaskOrThrowError(activeExperiment.experiment, updateTaskCurrentRecordingDto.taskUuid);

    // Get participant progress for task
    const participantProgress = await this.experimentsRepository.updateParticipantProgressForExperimentTask(
      participant,
      experiment,
      task.uuid,
    );

    // Update active experiment task
    activeExperiment.taskUuid = updateTaskCurrentRecordingDto.taskUuid;
    activeExperiment.trialNbr = participantProgress.trialCounter;

    const updatedActiveExperiment = await this.experimentsRepository.updateActiveExperiment(activeExperiment);
    const log = `Updated task of current recording: participant ${updatedActiveExperiment.participant.uuid}, task: ${task.name} (${updatedActiveExperiment.taskUuid}), trial: ${updatedActiveExperiment.trialNbr}`;
    await this.saveLog(experiment, log);

    return this.mapActiveExperimentToActiveExperimentDto(updatedActiveExperiment);
  }

  async incrementTrialCurrentRecording(experimentUuid: string): Promise<ActiveExperimentDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    // Only operators/admins arrive here. The controller endpoint does not authorize other roles
    const user = await this.getUserPerformingRequest();

    if (this.usersService.isOperator(user)) {
      await this.checkOperatorIsPartOfExperimentOrThrowError(user, activeExperiment);
    }

    await this.isEditableExperimentOrThrowError(activeExperiment.experiment);
    this.checkExperimentUuidIsActiveExperimentOrThrowError(experimentUuid, activeExperiment);

    const experiment = await this.getExperimentOrThrowError(experimentUuid);
    const participant = await this.getParticipantOrThrowError(activeExperiment.participant.uuid);
    const task = this.getTaskOrThrowError(activeExperiment.experiment, activeExperiment.taskUuid);

    await this.checkParticipantAlreadyStartedTaskOfExperimentOrThrowError(participant, experiment, task.uuid);

    // Update participant progress for task
    const participantProgress = await this.experimentsRepository.updateParticipantProgressForExperimentTask(
      participant,
      experiment,
      task.uuid,
    );

    // Update active experiment task
    activeExperiment.trialNbr = participantProgress.trialCounter;

    const updatedActiveExperiment = await this.experimentsRepository.updateActiveExperiment(activeExperiment);

    const log = `Incremented trial of current recording: participant ${updatedActiveExperiment.participant.uuid}, task: ${task.name} (${updatedActiveExperiment.taskUuid}), trial: ${updatedActiveExperiment.trialNbr}`;
    await this.saveLog(experiment, log);

    return this.mapActiveExperimentToActiveExperimentDto(updatedActiveExperiment);
  }

  private async checkParticipantAlreadyStartedTaskOfExperimentOrThrowError(
    participant: ParticipantDocument,
    experiment: ExperimentDocument,
    taskUuid: string,
  ) {
    const participantProgress = await this.experimentsRepository.getParticipantProgressForExperimentTask(
      participant,
      experiment,
      taskUuid,
    );
    if (!participantProgress) {
      throw new ForbiddenException(
        `No record of task ${taskUuid} for participant found. Please start recording task before incrementing trial.`,
      );
    }
  }

  async stopRecordingExperiment(experimentUuid: string): Promise<ActiveExperimentDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    // Only operators/admins arrive here. The controller endpoint does not authorize other roles
    const user = await this.getUserPerformingRequest();

    if (this.usersService.isOperator(user)) {
      await this.checkOperatorIsPartOfExperimentOrThrowError(user, activeExperiment);
    }

    this.checkExperimentUuidIsActiveExperimentOrThrowError(experimentUuid, activeExperiment);

    await this.isEditableExperimentOrThrowError(activeExperiment.experiment);

    activeExperiment.experiment.startedAt = getCurrentTimestamp();
    activeExperiment.isRecording = false;
    activeExperiment.participant = null;
    activeExperiment.taskUuid = null;
    activeExperiment.trialNbr = null;

    const updatedActiveExperiment = await this.experimentsRepository.updateActiveExperiment(activeExperiment);

    const experiment = await this.experimentsRepository.getExperiment(activeExperiment.experiment.uuid);
    await this.saveLog(
      experiment,
      `Stopped recording for experiment '${activeExperiment.experiment.name}' (${activeExperiment.experiment.uuid})`,
    );

    this.sendNotificationActiveExperimentStatusGateway(activeExperiment);

    return this.mapActiveExperimentToActiveExperimentDto(updatedActiveExperiment);
  }

  async completeExperiment(experimentUuid: string): Promise<ExperimentDto> {
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    await this.isEditableExperimentOrThrowError(experiment);

    // Remove the active experiment
    await this.experimentsRepository.resetActiveExperiment();

    // Update the experiment status
    experiment.completed = true;
    experiment.completedAt = getCurrentTimestamp();

    const updatedExperiment = await this.experimentsRepository.updateExperiment(experiment);

    await this.saveLog(
      updatedExperiment,
      `Flagged experiment '${updatedExperiment.name}' (${updatedExperiment.uuid}) as completed`,
    );

    return this.mapExperimentToExperimentDto(updatedExperiment);
  }

  async getParticipantsProgressForExperiment(experimentUuid: string): Promise<ParticipantProgressDto[]> {
    const user = await this.getUserPerformingRequest();
    const experiment = await this.getExperimentOrThrowError(experimentUuid);

    // For not admins, check that they are listed as operators for the experiment
    if (!this.usersService.isAdmin(user)) {
      const index = experiment.operators.findIndex((operator) => operator.username === user.username);
      if (index < 0) {
        throw new ForbiddenException(
          `You cannot get the participants progress as you are not listed as operator for this experiment.`,
        );
      }
    }

    const participantsProgress = await this.experimentsRepository.getParticipantsProgressForExperiment(experiment);

    return participantsProgress.map((participantProgress) =>
      this.mapParticipantProgressToParticipantProgressDto(participantProgress),
    );
  }

  async getActiveExperimentPublic(): Promise<ActiveExperimentPublicInfoDto> {
    const activeExperiment = await this.getActiveExperimentOrThrowError();

    const activeExperimentPublicDto: ActiveExperimentPublicInfoDto = {
      experimentName: activeExperiment.experiment.name,
      isRecording: activeExperiment.isRecording,
    };

    return activeExperimentPublicDto;
  }

  async getMessageScreenEntrance(): Promise<MessageScreenEntranceDto> {
    const messageScreenEntrance = await this.experimentsRepository.getMessageScreenEntrance();
    if (!messageScreenEntrance) {
      throw new NotFoundException(`No message found.`);
    }
    return this.mapMessageScreenEntranceToMessageScreenEntranceDto(messageScreenEntrance);
  }

  async setMessageScreenEntrance(
    createMessageScreenEntranceDto: CreateMessageScreenEntranceDto,
  ): Promise<MessageScreenEntranceDto> {
    const user = await this.getUserPerformingRequest();

    await this.saveLog(null, `User '${user.username}' updated message for screen entrance`);

    const messageScreenEntrance: MessageScreenEntrance = {
      message: createMessageScreenEntranceDto.message,
      updatedAt: getCurrentTimestamp(),
      createdBy: user,
    };
    const createdMessageScreenEntrance = await this.experimentsRepository.saveMessageScreenEntrance(
      messageScreenEntrance,
    );
    return this.mapMessageScreenEntranceToMessageScreenEntranceDto(createdMessageScreenEntrance);
  }

  async saveNoteForExperiment(createExperimentNoteDto: CreateExperimentNoteDto): Promise<ExperimentLogDto> {
    const experiment = await this.getExperimentOrThrowError(createExperimentNoteDto.experimentUuid);

    // Call the saveLog method of the log book repository directly as we want to specify the "note" tag
    const user = await this.getUserPerformingRequest();
    const experimentLog = await this.logBookRepository.saveLog(
      user,
      experiment,
      createExperimentNoteDto.logLine.trim(),
      [LogTag.NOTE],
    );

    return this.mapExperimentLogToExperimentLogDto(experimentLog);
  }

  private async getActiveExperimentOrThrowError(): Promise<ActiveExperimentDocument> {
    const activeExperiment = await this.experimentsRepository.getActiveExperiment();
    if (!activeExperiment) {
      throw new NotFoundException(`No active experiment found`);
    }
    return activeExperiment;
  }

  private async getExperimentOrThrowError(experimentUuid: string): Promise<ExperimentDocument> {
    const experiment = await this.experimentsRepository.getExperiment(experimentUuid);
    if (!experiment) {
      throw new NotFoundException(`Experiment with uuid '${experimentUuid}' not found`);
    }
    return experiment;
  }

  private async getConfigOrThrowError(configUuid: string): Promise<ConfigDocument> {
    const config = await this.configsRepository.getConfig(configUuid);
    if (!config) {
      throw new NotFoundException(`Config with uuid '${configUuid}' not found`);
    }
    return config;
  }

  private async getParticipantOrThrowError(participantUuid: string): Promise<ParticipantDocument> {
    const participant = await this.participantsRepository.getParticipant(participantUuid);
    if (!participant) {
      throw new NotFoundException(`Participant with uuid '${participantUuid}' not found`);
    }
    return participant;
  }

  private async getUserOrThrowError(username: string): Promise<UserDocument> {
    const user = await this.usersRepository.findOneWithUsername(username);
    if (!user) {
      throw new NotFoundException(`User with username '${username}' not found`);
    }
    return user;
  }

  private getTaskOrThrowError(experiment: Experiment, taskUuid: string): Task {
    const task = experiment.tasks.find((task) => task.uuid === taskUuid);
    if (!task) {
      throw new NotFoundException(`Task with uuid '${taskUuid}' not found`);
    }
    return task;
  }

  private isEditableExperimentOrThrowError(experiment: Experiment) {
    if (experiment.completed) {
      throw new ImmutableExperimentException(experiment.name);
    }
  }

  public mapExperimentToExperimentDto(experiment: Experiment) {
    const dto: ExperimentDto = {
      uuid: experiment.uuid,
      name: experiment.name,
      participants: experiment.participants.map((participant) =>
        this.participantsService.mapParticipantToParticipantDto(participant),
      ),
      owner: this.usersService.mapUserToUserDto(experiment.owner),
      operators: experiment.operators.map((operator) => this.usersService.mapUserToUserDto(operator)),
      config: this.configsService.mapConfigToConfigDto(experiment.config),
      tasks: experiment.tasks.map((task) => this.mapTaskToTaskDto(task)),
      createdAt: experiment.createdAt,
      startedAt: experiment.startedAt,
      completedAt: experiment.completedAt,
      completed: experiment.completed,
    };
    return dto;
  }

  private mapActiveExperimentToActiveExperimentDto(activeExperiment: ActiveExperiment) {
    const dto: ActiveExperimentDto = {
      experiment: this.mapExperimentToExperimentDto(activeExperiment.experiment),
      isRecording: activeExperiment.isRecording,
      participant: activeExperiment.participant
        ? this.participantsService.mapParticipantToParticipantDto(activeExperiment.participant)
        : null,
      taskUuid: activeExperiment.taskUuid,
      trialNbr: activeExperiment.trialNbr,
    };
    return dto;
  }

  private mapTaskToTaskDto(task: Task): TaskDto {
    const dto: TaskDto = {
      uuid: task.uuid,
      name: task.name,
      description: task.description,
    };
    return dto;
  }

  private mapParticipantProgressToParticipantProgressDto(
    participantProgress: ParticipantProgress,
  ): ParticipantProgressDto {
    const dto: ParticipantProgressDto = {
      participantUuid: participantProgress.participant.uuid,
      experimentUuid: participantProgress.experiment.uuid,
      trialCounter: participantProgress.trialCounter,
      taskUuid: participantProgress.taskUuid,
    };
    return dto;
  }

  private mapMessageScreenEntranceToMessageScreenEntranceDto(
    messageScreenEntrance: MessageScreenEntrance,
  ): MessageScreenEntranceDto {
    const dto: MessageScreenEntranceDto = {
      message: messageScreenEntrance.message,
      updatedAt: messageScreenEntrance.updatedAt,
      createdByUserFullName: `${messageScreenEntrance.createdBy.firstname} ${messageScreenEntrance.createdBy.lastname}`,
    };
    return dto;
  }

  private mapExperimentLogToExperimentLogDto(experimentLog: ExperimentLog): ExperimentLogDto {
    const dto: ExperimentLogDto = {
      log: experimentLog.log,
      username: experimentLog.user.username,
      datetime: experimentLog.datetime,
      experimentUuid: experimentLog.experiment.uuid,
      tags: experimentLog.tags,
    };
    return dto;
  }

  private async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // this is Express request user

    return await this.getUserOrThrowError(userPerformingRequest.username);
  }
}
