import { forwardRef, Module } from '@nestjs/common';
import { ExperimentsService } from './experiments.service';
import { ExperimentsController } from './experiments.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Experiment, ExperimentSchema } from '../../repositories/schema/experiment.schema';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import {
  ACTIVE_EXPERIMENT_MODEL_NAME,
  ActiveExperimentSchema,
} from '../../repositories/schema/active-experiment.schema';
import {
  PARTICIPANT_PROGRESS_MODEL_NAME,
  ParticipantProgressSchema,
} from '../../repositories/schema/participant-progress.schema';
import {
  MESSAGE_SCREEN_LOFT_MODEL_NAME,
  MessageScreenEntranceSchema,
} from '../../repositories/schema/message-screen-entrance.schema';
import { ActiveExperimentStatusGateway } from './active-experiment-status.gateway';
import { UsersModule } from '../users/users.module';
import { LogBookModule } from '../log-book/log-book.module';
import { ParticipantsModule } from '../participants/participants.module';
import { ConfigsModule } from '../configs/configs.module';
import { SamplesModule } from '../samples/samples.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Experiment.name, schema: ExperimentSchema },
      { name: ACTIVE_EXPERIMENT_MODEL_NAME, schema: ActiveExperimentSchema },
      { name: PARTICIPANT_PROGRESS_MODEL_NAME, schema: ParticipantProgressSchema },
      { name: MESSAGE_SCREEN_LOFT_MODEL_NAME, schema: MessageScreenEntranceSchema },
    ]),
    // Forward reference UsersModule to solve circular dependencies
    // ExperimentsModule imports UsersModule which imports ExperimentsModule
    forwardRef(() => UsersModule),
    LogBookModule,
    // Forward reference ConfigsModule to solve circular dependencies
    // ExperimentsModule imports ConfigsModule which imports ExperimentsModule
    forwardRef(() => ConfigsModule),
    ParticipantsModule,
    // Forward reference SamplesModule to solve circular dependencies
    // ExperimentsModule imports SamplesModule which imports ExperimentsModule
    forwardRef(() => SamplesModule),
  ],
  controllers: [ExperimentsController],
  providers: [ExperimentsService, ExperimentsRepository, ActiveExperimentStatusGateway],
  exports: [ExperimentsRepository, ExperimentsService], // Exports for DI
})
export class ExperimentsModule {}
