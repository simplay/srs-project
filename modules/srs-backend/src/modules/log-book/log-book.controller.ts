import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../../common/guards/roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { Role } from '../../common/enums/role.enum';
import { ExperimentLogDto } from '../experiments/dto/experiment-log.dto';
import { LogBookService } from './log-book.service';
import { LogBookInfoDto } from './dto/log-book-info.dto';

@ApiTags('log-book')
@Controller('log-book')
export class LogBookController {
  constructor(private readonly logBookService: LogBookService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Get log book' })
  @ApiOkResponse({ type: ExperimentLogDto, isArray: true })
  @Get('')
  async getLogBook(): Promise<ExperimentLogDto[]> {
    return await this.logBookService.getLogBook();
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  @ApiOperation({ description: 'Get log book info (tags)' })
  @ApiOkResponse({ type: LogBookInfoDto })
  @Get('info')
  async getLogBookInfo(): Promise<LogBookInfoDto> {
    return await this.logBookService.getLogBookInfo();
  }
}
