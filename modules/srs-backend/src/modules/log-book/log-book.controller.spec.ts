import { Test, TestingModule } from '@nestjs/testing';
import { LogBookController } from './log-book.controller';

describe('LogBookController', () => {
  let controller: LogBookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LogBookController],
    }).compile();

    controller = module.get<LogBookController>(LogBookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
