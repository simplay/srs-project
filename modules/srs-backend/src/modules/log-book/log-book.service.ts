import { Injectable } from '@nestjs/common';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { ExperimentLogDto } from '../experiments/dto/experiment-log.dto';
import { ExperimentLog, ExperimentLogDocument } from '../../repositories/schema/experiment-log.schema';
import { ExperimentDocument } from '../../repositories/schema/experiment.schema';
import { LogBookInfoDto } from './dto/log-book-info.dto';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable()
export class LogBookService {
  constructor(private readonly logBookRepository: LogBookRepository) {}

  async getLogBook(): Promise<ExperimentLogDto[]> {
    // TODO: do not hard-code me
    const timestamp = 1646182312.77
    const experimentLogs: ExperimentLogDocument[] = await this.logBookRepository.getLogBookByTimestamp(timestamp);

    return experimentLogs.map((experimentLog) => this.mapExperimentLogToExperimentLogDto(experimentLog));
  }

  async getLogBookInfo(): Promise<LogBookInfoDto> {
    const logBookInfoDto: LogBookInfoDto = {
      tags: Object.values(LogTag),
    };

    return logBookInfoDto;
  }

  async getLogBookForExperiment(experiment: ExperimentDocument): Promise<ExperimentLogDto[]> {
    const experimentLogs: ExperimentLogDocument[] = await this.logBookRepository.getLogBookForExperiment(experiment);

    return experimentLogs.map((experimentLog) => this.mapExperimentLogToExperimentLogDto(experimentLog));
  }

  private mapExperimentLogToExperimentLogDto(experimentLog: ExperimentLog): ExperimentLogDto {
    const dto: ExperimentLogDto = {
      datetime: experimentLog.datetime,
      experimentUuid: experimentLog.experiment ? experimentLog.experiment.uuid : null,
      username: experimentLog.user ? experimentLog.user.username : null,
      log: experimentLog.log,
      tags: experimentLog.tags,
    };
    return dto;
  }
}
