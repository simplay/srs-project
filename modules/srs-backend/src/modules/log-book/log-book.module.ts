import { Module } from '@nestjs/common';
import { LogBookController } from './log-book.controller';
import { LogBookService } from './log-book.service';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { EXPERIMENT_LOG_MODEL_NAME, ExperimentLogSchema } from '../../repositories/schema/experiment-log.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: EXPERIMENT_LOG_MODEL_NAME, schema: ExperimentLogSchema }, // For the logbook
    ]),
  ],
  controllers: [LogBookController],
  providers: [LogBookService, LogBookRepository],
  exports: [LogBookService, LogBookRepository], // Exports for DI
})
export class LogBookModule {}
