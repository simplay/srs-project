import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as fs from 'fs';
import { UsersRepository } from '../../../repositories/users.repository';
import { User } from '../../../repositories/schema/user.schema';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersRepository: UsersRepository) {
    // Check here for the available options for the strategy:
    // https://github.com/mikenicholson/passport-jwt#configure-strategy
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: fs.readFileSync(process.env.SMS_JWT_PUBLIC_KEY_FILEPATH, 'utf-8'),
      algorithms: ['RS256'], // asymmetric
    });
  }

  async validate(payload: any) {
    const username = payload.username;
    const user: User = await this.usersRepository.findOneWithUsername(username);

    if (!user) {
      throw new UnauthorizedException();
    }

    // Here we could do further validation of the token if wanted, such as
    // looking up the userId in a list of revoked tokens, enabling us to perform token revocation

    return { username: user.username, roles: user.roles };
  }
}
