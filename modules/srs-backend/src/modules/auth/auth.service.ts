import { ConsoleLogger, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as fs from 'fs';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import * as bcrypt from 'bcrypt';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { LogTag } from '../../common/enums/log-tag.enum';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private readonly logger: ConsoleLogger,
    private readonly logBookRepository: LogBookRepository,
  ) {
    logger.setContext(AuthService.name);
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(username);

    if (user && (await bcrypt.compare(password, user.password))) {
      // We only return the following infos of the user (will be contained in the JWT)
      const result = {
        username: user.username,
        roles: user.roles,
      };
      return result;
    } else {
      await this.logBookRepository.saveLog(
        null,
        null,
        `Authentication attempt: failed to authenticate user with username "${username}"`,
        [LogTag.ACCESS],
      );
    }
    return null;
  }

  async login(user: User) {
    const payload = { username: user.username };
    const privateKey = fs.readFileSync(process.env.SMS_JWT_PRIVATE_KEY_FILEPATH, 'utf-8');
    const jwt = this.jwtService.sign(payload, { privateKey: privateKey });

    // Save login in logbook
    const userDocument = await this.usersService.findOne(user.username);
    await this.logBookRepository.saveLog(userDocument, null, `Successfully authenticated => generate JWT`, [
      LogTag.ACCESS,
    ]);

    return {
      access_token: jwt,
    };
  }

  async verifyTokenAndGetUser(token: string): Promise<UserDocument | null> {
    const publicKey = fs.readFileSync(process.env.SMS_JWT_PUBLIC_KEY_FILEPATH, 'utf-8');
    try {
      const { username } = this.jwtService.verify(token, { publicKey: publicKey });
      if (!username) {
        return null;
      }

      const user = await this.usersService.findOne(username);
      return user;
    } catch (e) {
      this.logger.error(`Error verifying JWT token: ${e.name}`, e);
      return null;
    }
  }
}
