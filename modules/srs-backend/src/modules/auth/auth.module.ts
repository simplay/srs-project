import { ConsoleLogger, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies/jwt.strategy';
import { LogBookModule } from '../log-book/log-book.module';

@Module({
  imports: [
    UsersModule,
    LogBookModule,
    PassportModule,
    JwtModule.register({
      signOptions: { expiresIn: process.env.SMS_JWT_VALIDITY_LENGTH, algorithm: 'RS256' },
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, ConsoleLogger],
  controllers: [AuthController],
  exports: [AuthService], // Exports for DI
})
export class AuthModule {}
