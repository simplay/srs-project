import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsArray, IsEmail, IsEnum, IsNotEmpty, Matches, NotContains } from 'class-validator';
import { Role } from '../../../common/enums/role.enum';

export class UserDto {
  @ApiProperty()
  @IsNotEmpty()
  // Do not allow commas or whitespace in the username
  @Matches(/^[^,\s]+$/, { message: 'The username must not contains commas or whitespace' })
  username: string;

  @ApiProperty()
  @IsNotEmpty()
  firstname: string;

  @ApiProperty()
  @IsNotEmpty()
  lastname: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsEnum(Role, { each: true })
  @IsArray()
  @ArrayMinSize(1)
  roles: Role[];

  @ApiProperty()
  createdAt: number;

  @ApiProperty()
  updatedAt: number;
}
