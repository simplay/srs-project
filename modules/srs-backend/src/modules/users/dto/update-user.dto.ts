import { UserDto } from './user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, MinLength } from 'class-validator';

export class UpdateUserDto extends UserDto {
  @ApiProperty()
  @IsOptional()
  @MinLength(6)
  password: string; // optional
}
