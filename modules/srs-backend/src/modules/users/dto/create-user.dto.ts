import { UserDto } from './user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateUserDto extends UserDto {
  @ApiProperty()
  @IsNotEmpty()
  password: string;
}
