import { ForbiddenException, Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { UsersRepository } from '../../repositories/users.repository';
import { User, UserDocument } from '../../repositories/schema/user.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { getCurrentTimestamp } from '../../common/utils/datetime.helper';
import { UserDto } from './dto/user.dto';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import * as bcrypt from 'bcrypt';
import { Role } from '../../common/enums/role.enum';
import { UpdateUserDto } from './dto/update-user.dto';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { LogBookRepository } from '../../repositories/log-book.repository';
import { LogTag } from '../../common/enums/log-tag.enum';

const BCRYPT_ROUNDS = 10;

@Injectable({ scope: Scope.REQUEST })
export class UsersService {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly logBookRepository: LogBookRepository,
    @Inject(REQUEST) private request: Request,
  ) {}

  async listUsers(): Promise<UserDto[]> {
    const users = await this.usersRepository.listUsers();
    return users.map((user) => this.mapUserToUserDto(user));
  }

  async findOne(username: string): Promise<UserDocument> {
    return await this.usersRepository.findOneWithUsername(username);
  }

  async createUser(createUserDto: CreateUserDto): Promise<UserDto> {
    if (await this.usersRepository.findOneWithUsername(createUserDto.username)) {
      throw new ForbiddenException('A user with this username already exists. Please choose a different username.');
    }

    if (await this.usersRepository.findOneWithEmail(createUserDto.email)) {
      throw new ForbiddenException('A user with this email already exists. Please choose a different email.');
    }

    const user: User = {
      username: createUserDto.username,
      firstname: createUserDto.firstname,
      lastname: createUserDto.lastname,
      email: createUserDto.email,
      roles: createUserDto.roles,
      password: bcrypt.hashSync(createUserDto.password, BCRYPT_ROUNDS),
      createdAt: getCurrentTimestamp(),
      updatedAt: getCurrentTimestamp(),
    };

    const createdUser = await this.usersRepository.createNewUser(user);

    await this.saveLog(`Created new user '${createdUser.username}' with roles ${createdUser.roles.join(',')}`);
    return this.mapUserToUserDto(createdUser);
  }

  async updateUser(oldUsername: string, updateUserDto: UpdateUserDto): Promise<UserDto> {
    const user: User = await this.getUserOrThrowError(oldUsername);

    // update fields
    user.username = updateUserDto.username;
    user.firstname = updateUserDto.firstname;
    user.lastname = updateUserDto.lastname;
    user.email = updateUserDto.email;
    user.roles = updateUserDto.roles;
    user.updatedAt = getCurrentTimestamp();

    if (updateUserDto.password) {
      user.password = bcrypt.hashSync(updateUserDto.password, BCRYPT_ROUNDS);
    }

    const updatedUser = await this.usersRepository.updateUser(oldUsername, user);
    await this.saveLog(
      `Updated user '${oldUsername}' to '${updatedUser.username}' with roles ${updatedUser.roles.join(',')}`,
    );
    return this.mapUserToUserDto(updatedUser);
  }

  async deleteUser(username: string) {
    const user = await this.getUserOrThrowError(username);

    const experiments = await this.experimentsRepository.getExperimentsForUser(user);

    if (experiments.length > 0) {
      const experimentsNames = experiments.map((experiment) => experiment.name).join(',');
      throw new ForbiddenException(
        `User "${username}" is part of (owner or operator) of experiment(s): ${experimentsNames}. Please remove the user from experiment(s) before deleting the user`,
      );
    }

    await this.saveLog(`Deleted user '${username}'`);
    await this.usersRepository.deleteUser(username);
  }

  private async getUserOrThrowError(username: string): Promise<UserDocument> {
    const user = await this.usersRepository.findOneWithUsername(username);
    if (!user) {
      throw new NotFoundException(`User with username '${username}' not found`);
    }
    return user;
  }

  isAdmin(user: User): boolean {
    return user.roles.includes(Role.Admin);
  }
  isOperator(user: User): boolean {
    return user.roles.includes(Role.Operator);
  }

  mapUserToUserDto(user: User): UserDto {
    // Do not return password
    const userDto: UserDto = {
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      roles: user.roles,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
      username: user.username,
    };
    return userDto;
  }

  private async saveLog(log: string) {
    const user = await this.getUserPerformingRequest();
    await this.logBookRepository.saveLog(user, null, log, [LogTag.USERS_CONFIG]);
  }

  private async getUserPerformingRequest(): Promise<UserDocument> {
    const userPerformingRequest = this.request.user as User; // this is Express request user

    return await this.getUserOrThrowError(userPerformingRequest.username);
  }
}
