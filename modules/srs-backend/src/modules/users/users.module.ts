import { forwardRef, Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersRepository } from '../../repositories/users.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../../repositories/schema/user.schema';
import { UsersController } from './users.controller';
import { LogBookModule } from '../log-book/log-book.module';
import { ExperimentsModule } from '../experiments/experiments.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    LogBookModule,
    // Import ExperimentsModule to find the experiments of the user
    // Forward reference ExperimentsModule to solve circular dependencies
    // UsersModule imports ExperimentsModule which imports UsersModule
    forwardRef(() => ExperimentsModule),
  ],
  providers: [UsersService, UsersRepository],
  exports: [UsersService, UsersRepository], // Exports for DI
  controllers: [UsersController],
})
export class UsersModule {}
