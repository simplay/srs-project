import { Test, TestingModule } from '@nestjs/testing';
import { SensorsController } from './sensors.controller';
import { SensorsService } from './sensors.service';
import { PostDataSensorsGuard } from './guards/post-data-sensors.guard';
import { SensorsGateway } from './sensors.gateway';

// We test the validation at the controller level

describe('SensorsController', () => {
  let controller: SensorsController;

  const mockSensorsService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SensorsController],
      providers: [
        SensorsService,
        {
          provide: SensorsService,
          useValue: mockSensorsService,
        },
      ],
    })
      .overrideGuard(PostDataSensorsGuard)
      .useValue({ canActivate: () => true })
      .compile();

    controller = module.get<SensorsController>(SensorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
