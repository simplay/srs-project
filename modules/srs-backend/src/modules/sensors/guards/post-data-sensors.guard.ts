import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ConfigsRepository } from '../../../repositories/configs.repository';
import { ExperimentsRepository } from '../../../repositories/experiments.repository';

@Injectable()
export class PostDataSensorsGuard implements CanActivate {
  constructor(private readonly experimentsRepository: ExperimentsRepository) {}
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    // const sensorsPerType = this.configsRepository.getSensorsPerType();

    // We could restrict the access to the endpoints based on the producers in the config file
    // For now, we just authorize the request

    return true;
  }
}
