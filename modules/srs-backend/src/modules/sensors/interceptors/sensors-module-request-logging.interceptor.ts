import {
  CallHandler,
  ConsoleLogger,
  ExecutionContext,
  HttpException,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IncomingHttpHeaders } from 'http';

/**
 * Interceptor that logs input/output requests only for the sensors module
 */
@Injectable()
export class SensorsModuleRequestLoggingInterceptor implements NestInterceptor {
  constructor(private readonly logger: ConsoleLogger) {
    logger.setContext(SensorsModuleRequestLoggingInterceptor.name);
  }

  /**
   * Intercept method, logs before and after the request being processed
   * @param context details about the current request
   * @param call$ implements the handle method that returns an Observable
   */
  public intercept(context: ExecutionContext, call$: CallHandler): Observable<unknown> {
    const req: Request = context.switchToHttp().getRequest();
    const { method, url, body, headers } = req;
    const message = `Incoming request - ${method} - ${url}`;

    // We make sure to only log things for the sensors module
    if (url.includes('sensors') && process.env.DEBUG_LOG_REQUESTS_SENSORS_MODULE === 'true') {
      this.logger.log({ message, method, headers });
    }

    return call$.handle().pipe(
      tap({
        next: (val: unknown): void => {
          this.logNext(val, context);
        },
        error: (err: Error): void => {
          this.logError(err, context);
        },
      }),
    );
  }

  /**
   * Logs the request response in success cases
   * @param body body returned
   * @param context details about the current request
   */
  private logNext(body: unknown, context: ExecutionContext): void {
    const req: Request = context.switchToHttp().getRequest<Request>();
    const res: Response = context.switchToHttp().getResponse<Response>();
    const { method, url } = req;
    const { statusCode } = res;
    const message = `Outgoing response - ${statusCode} - ${method} - ${url}`;

    if (url.includes('sensors') && process.env.DEBUG_LOG_REQUESTS_SENSORS_MODULE === 'true') {
      this.logger.log({
        message,
        body,
      });
    }
  }

  /**
   * Logs the request response in success cases
   * @param error Error object
   * @param context details about the current request
   */
  private logError(error: Error, context: ExecutionContext): void {
    const req: Request = context.switchToHttp().getRequest<Request>();
    const { method, url } = req;

    if (url.includes('sensors') && process.env.DEBUG_LOG_REQUESTS_SENSORS_MODULE === 'true') {
      if (error instanceof HttpException) {
        const statusCode: number = error.getStatus();
        const message = `Outgoing response - ${statusCode} - ${method} - ${url}`;

        this.logger.error({
          method,
          url,
          message,
          error,
        });
      } else {
        this.logger.error(
          {
            message: `Outgoing response - ${method} - ${url}`,
          },
          error.stack,
        );
      }
    }
  }
}
