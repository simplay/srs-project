import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { LastActivityForCameraType } from '../../common/interfaces/camera-last-activity.interface';
import { NAMESPACE_WEBSOCKET_CAMERAS } from '../../common/utils/constants';

/*
  The SensorsGateway sends frames of the cameras to authenticated clients
  Gateways on port 4222 requires JWT authentication
 */

@WebSocketGateway(4222, {
  namespace: NAMESPACE_WEBSOCKET_CAMERAS,

  cors: {
    // TODO: restrict origin once proper dns
    origin: '*',
    methods: ['GET'],
    credentials: true,
  },
})
export class CamerasGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('CamerasGateway');

  afterInit(server: Server): any {
    this.logger.log('Initialized');
  }

  handleConnection(socket: Socket, ...args: any[]): any {
    this.logger.log(`Client connected: ${socket.id}`);
  }

  handleDisconnect(socket: Socket): any {
    this.logger.log(`Client disconnected: ${socket.id}`);
  }

  sendToClients(lastActivityForCameraType: LastActivityForCameraType) {
    this.wss.emit('backendToClient', lastActivityForCameraType);
  }
}
