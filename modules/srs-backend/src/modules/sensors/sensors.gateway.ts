import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { DataReceivedWithSensorsLastActivityResponse } from '../../common/interfaces/data-received-response.interface';
import { NAMESPACE_WEBSOCKET_SENSORS } from '../../common/utils/constants';

/*
  The SensorsGateway sends information about sensors last activity to client
  Gateways on port 4222 requires JWT authentication
 */
@WebSocketGateway(4222, {
  namespace: NAMESPACE_WEBSOCKET_SENSORS,

  cors: {
    // TODO: restrict origin once proper dns
    origin: '*',
    methods: ['GET'],
    credentials: true,
  },
})
export class SensorsGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('SensorsGateway');

  afterInit(server: Server): any {
    this.logger.log('Initialized');
  }

  handleConnection(client: Socket, ...args: any[]): any {
    this.logger.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket): any {
    this.logger.log(`Client disconnected: ${client.id}`);
  }

  sendToAll(message: DataReceivedWithSensorsLastActivityResponse) {
    this.wss.emit('sensorsToClient', message);
  }
}
