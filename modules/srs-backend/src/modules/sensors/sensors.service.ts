import { Inject, Injectable, Logger, Scope, UnprocessableEntityException } from '@nestjs/common';
import { Sample } from '../../repositories/schema/sample.schema';
import { SensorDto } from './dto/sensor.dto';
import { SensorsGateway } from './sensors.gateway';
import {
  DataReceivedResponse,
  DataReceivedWithSensorsLastActivityResponse,
  SensorLastActivity,
} from '../../common/interfaces/data-received-response.interface';
import { SensorType } from '../../common/enums/sensor-type.enum';
import * as zlib from 'zlib';
import { SamplesRepository } from '../../repositories/samples.repository';
import { DoorDto } from './dto/door.dto';
import { NoActiveConfigSetException } from '../../common/exceptions/no-active-config-set.exception';
import { ExperimentsRepository } from '../../repositories/experiments.repository';
import { ActiveExperiment } from '../../repositories/schema/active-experiment.schema';
import { CameraDto } from './dto/camera.dto';
import { NightVisionCameraDto } from './dto/night-vision-camera.dto';
import { CamerasGateway } from './cameras.gateway';
import { CameraLastSample, LastActivityForCameraType } from '../../common/interfaces/camera-last-activity.interface';
import { CameraType } from '../../common/enums/camera-type.enum';

const DELAY_FOR_SENDING_CLIENT_MESSAGES_S = 3;

@Injectable()
export class SensorsService {
  private readonly logger: Logger = new Logger(SensorsService.name);
  // The "sensorsLastActivityPerType" holds the information about the sensors last activity per sensor type
  // The last activity per sensor type (sensor IP, last sample received at) is sent to the client using the SensorsGateway
  // For the door sensors, the last activity holds the state of the door as well
  private readonly sensorsLastActivityPerType: Record<SensorType, SensorLastActivity[]>;

  // The "camerasLastActivityPerType" holds the last frame of each cameras of camera type (normal, night vision cameras)
  // The camera last activity is sent to the client using the CamerasGateway
  private readonly camerasLastActivityPerType: Record<CameraType, CameraLastSample[]>;

  // Those two next members are used to only sent messages to the client at a max rate of "DELAY_FOR_SENDING_CLIENT_MESSAGES_S"
  // We do not want to overload / overwhelm  the websocket
  private readonly timestampLastMessageSentPerSensorType: Record<SensorType, number>;
  private readonly timestampLastMessageSentPerCameraType: Record<CameraType, number>;

  private activeExperiment: ActiveExperiment | null;

  constructor(
    private readonly samplesRepository: SamplesRepository,
    private readonly experimentsRepository: ExperimentsRepository,
    private readonly sensorsGateway: SensorsGateway,
    private readonly camerasGateway: CamerasGateway,
  ) {
    // Prepare a record of sensors activity per sensor type
    this.sensorsLastActivityPerType = {
      [SensorType.Camera]: [],
      [SensorType.Door]: [],
      [SensorType.Dummy]: [],
      [SensorType.Emfit]: [],
      [SensorType.Environment]: [],
      [SensorType.Luminance]: [],
      [SensorType.Lidar]: [],
      [SensorType.Mattress]: [],
      [SensorType.Microphone]: [],
      [SensorType.NightVisionCamera]: [],
      [SensorType.Radar]: [],
      [SensorType.Seismograph]: [],
      [SensorType.ShellyRed]: [],
      [SensorType.ShellyEm]: [],
      [SensorType.WaterFlow]: [],
    };
    this.timestampLastMessageSentPerSensorType = {
      [SensorType.Camera]: 0,
      [SensorType.Door]: 0,
      [SensorType.Dummy]: 0,
      [SensorType.Emfit]: 0,
      [SensorType.Environment]: 0,
      [SensorType.Luminance]: 0,
      [SensorType.Lidar]: 0,
      [SensorType.Mattress]: 0,
      [SensorType.Microphone]: 0,
      [SensorType.NightVisionCamera]: 0,
      [SensorType.Radar]: 0,
      [SensorType.Seismograph]: 0,
      [SensorType.ShellyRed]: 0,
      [SensorType.ShellyEm]: 0,
      [SensorType.WaterFlow]: 0,
    };
    this.camerasLastActivityPerType = {
      [CameraType.DAYLIGHT_CAMERA]: [],
      [CameraType.NIGHT_VISION_CAMERA]: [],
    };
    this.timestampLastMessageSentPerCameraType = {
      [CameraType.DAYLIGHT_CAMERA]: 0,
      [CameraType.NIGHT_VISION_CAMERA]: 0,
    };

    this.activeExperiment = null;
  }

  private getCurrentTimestamp() {
    // We return the timestamp in second with ms precision
    // Date.now() returns the nbr of milliseconds since epoch => convert to seconds
    return Date.now() / 1000;
  }

  private fixTimestampIssueForDoorSensors(sensorDtoList: SensorDto[]): SensorDto[] {
    for (let i = 0; i < sensorDtoList.length; i++) {
      const sensorDto: DoorDto = sensorDtoList[i] as DoorDto;
      const timestampFromSensor = sensorDto.timestamp;
      const now = this.getCurrentTimestamp();
      if (timestampFromSensor > now) {
        this.logger.log(
          `Incorrect timestamp value ${timestampFromSensor} received from door sensor ${sensorDto.ip} => reset to current server local timestamp`,
        );
        sensorDto.timestamp = now;
      }
    }

    return sensorDtoList;
  }

  private filterSampleReturnedLastActivity(sensorType: SensorType, sensorDto: SensorDto): DoorDto | null {
    let sample = null;

    // We only send back the sample for doors (to be displayed in the Dashboard)
    if (sensorType === SensorType.Door) {
      sample = sensorDto as DoorDto;
    }

    return sample;
  }

  updateSensorsLastActivityPerType(sensorType: SensorType, sensorDto: SensorDto) {
    const sensorLastActivity = this.sensorsLastActivityPerType[sensorType].find(
      (sensorsLastActivity) => sensorsLastActivity.ip === sensorDto.ip,
    );

    // Update the last activity per sensor (to be sent to client using websocket / gateway later on)
    if (!sensorLastActivity) {
      this.sensorsLastActivityPerType[sensorType].push({
        ip: sensorDto.ip,
        lastSampleReceivedAt: sensorDto.timestamp,
        sample: this.filterSampleReturnedLastActivity(sensorType, sensorDto),
      });
    } else {
      // Update the timestamp and sample if more recent
      if (sensorDto.timestamp > sensorLastActivity.lastSampleReceivedAt) {
        sensorLastActivity.lastSampleReceivedAt = sensorDto.timestamp;
        sensorLastActivity.sample = this.filterSampleReturnedLastActivity(sensorType, sensorDto);
      }
    }
  }

  updateCamerasLastActivityPerType(cameraType: CameraType, sensorDto: CameraDto | NightVisionCameraDto) {
    const cameraLastActivity = this.camerasLastActivityPerType[cameraType].find(
      (cameraLastActivity) => cameraLastActivity.ip === sensorDto.ip,
    );

    if (!cameraLastActivity) {
      // If no entry yet, we add an entry for that camera
      this.camerasLastActivityPerType[cameraType].push({
        ip: sensorDto.ip,
        receivedAt: sensorDto.timestamp,
        sample: sensorDto,
      });
    } else {
      // Update the last activity information for that camera
      if (sensorDto.timestamp > cameraLastActivity.receivedAt) {
        cameraLastActivity.receivedAt = sensorDto.timestamp;
        cameraLastActivity.sample = sensorDto;
      }
    }
  }

  async createSensorDataFromBatch(sensorDtoList: SensorDto[], sensorType: SensorType) {
    // TODO: find another strategy not to do this call each time
    this.activeExperiment = await this.experimentsRepository.getActiveExperiment();

    if (this.activeExperiment === null) {
      throw new NoActiveConfigSetException();
    }

    const sensorsOfType = this.activeExperiment.experiment.config.sensorsPerType[sensorType];

    if (!sensorsOfType) {
      throw new UnprocessableEntityException(`This sensor type (${sensorType}) is not allowed in the config`);
    }

    // Some door sensors send from time to time an incorrect value for the timestamp (value way in the future).
    // When we receive such samples, we update the timestamp with the server timestamp
    if (sensorType === SensorType.Door) {
      sensorDtoList = this.fixTimestampIssueForDoorSensors(sensorDtoList);
    }

    // Filter the samples that we want to save based on the configuration
    // Let's use a for loop instead of filter as we need async/await to fetch the last door sample from DB
    const warningMessages: string[] = [];
    const sensorDtoListToSave: SensorDto[] = [];
    for (let i = 0; i < sensorDtoList.length; i++) {
      const sensorDto = sensorDtoList[i];

      // Find sensor from config
      const sensor = sensorsOfType.find((sensor) => sensor.ip === sensorDto.ip);

      // Keep track of the last activity per sensor type (so that we can send this info via sensors websocket)
      this.updateSensorsLastActivityPerType(sensorType, sensorDto);

      // If cameras, keep track of the last frame (so that we can send this info via the cameras websocket)
      if (sensorType === SensorType.Camera || sensorType === SensorType.NightVisionCamera) {
        if (sensorType === SensorType.Camera) {
          this.updateCamerasLastActivityPerType(CameraType.DAYLIGHT_CAMERA, sensorDto as CameraDto);
        } else {
          this.updateCamerasLastActivityPerType(CameraType.NIGHT_VISION_CAMERA, sensorDto as NightVisionCameraDto);
        }
      }

      // Build list of samples to save if experiment is recording
      if (this.activeExperiment.isRecording) {
        if (sensor) {
          // Check if the sensor is configured to save data
          if (!sensor.saveData) {
            continue;
          }

          // Only save state change for the door sensors (or if there is no sample yet)
          if (sensorType === SensorType.Door) {
            const lastSample = await this.samplesRepository.getLastSampleForDoorSensorForActiveExperiment(
              sensorDto.ip,
              this.activeExperiment,
            );
            if (lastSample === null) {
              sensorDtoListToSave.push(sensorDto);
              continue;
            }

            const lastSampleDoorDto: DoorDto = JSON.parse(zlib.inflateSync(lastSample.blob).toString());
            const currentSampleDoorDto = sensorDto as DoorDto;

            if (currentSampleDoorDto.doorOpen !== lastSampleDoorDto.doorOpen) {
              sensorDtoListToSave.push(sensorDto);
            }
          } else {
            sensorDtoListToSave.push(sensorDto);
          }
        } else {
          // Prevent logging multiple time the same message (e.g. in case lidar sensor is not defined)
          const warningMessage = `${sensorType} sensor ${sensorDto.ip} is not in the list of sensors => do not save data`;
          if (!warningMessages.includes(warningMessage)) {
            warningMessages.push(warningMessage);
          }
        }
      }
    }

    // Log warning messages
    warningMessages.forEach((warningMessage) => this.logger.warn(warningMessage));

    // Save samples to DB
    const savedSamples = [];
    if (this.activeExperiment.isRecording && sensorDtoListToSave.length > 0) {
      const samplesToSave = this.mapDtoListToSampleList(sensorType, sensorDtoListToSave);

      savedSamples.push(...(await this.samplesRepository.saveSamples(sensorType, samplesToSave)));
    }

    // Prepare return message
    const incomingIps = sensorDtoList.map((sensorDto) => sensorDto.ip);
    const uniqueIncomingIps = [...new Set(incomingIps)]; // filter duplicates, e.g. lidar data
    const numberSamplesSaved = savedSamples.length;
    const numberSamplesSkipped = sensorDtoList.length - numberSamplesSaved;
    const now = this.getCurrentTimestamp();

    let details = '';
    if (sensorType === SensorType.Door) {
      details = sensorDtoList
        .map((sensorDto) => {
          const s = sensorDto as DoorDto;
          return s.doorOpen;
        })
        .join(',doorOpen =');
    }
    const response: DataReceivedResponse = {
      timestamp: now,
      sensorType: sensorType,
      message: `Data received`,
      numberSamplesSaved,
      numberSamplesSkipped,
      ips: uniqueIncomingIps,
      details: details,
    };

    // Send sensors last activity
    this.sendMessageSensorsGateway(response, sensorType);

    // Sens cameras last activity (last frame)
    if (sensorType === SensorType.Camera || sensorType === SensorType.NightVisionCamera) {
      this.sendCamerasLastActivity(
        sensorType === SensorType.Camera ? CameraType.DAYLIGHT_CAMERA : CameraType.NIGHT_VISION_CAMERA,
      );
    }

    return response;
  }

  sendCamerasLastActivity(cameraType: CameraType.DAYLIGHT_CAMERA | CameraType.NIGHT_VISION_CAMERA) {
    // Do not overload client by sending too many messages, we only send one per DELAY_FOR_SENDING_CLIENT_MESSAGES_MS
    const now = this.getCurrentTimestamp();

    if (
      this.timestampLastMessageSentPerCameraType[cameraType] === 0 ||
      now - this.timestampLastMessageSentPerCameraType[cameraType] > DELAY_FOR_SENDING_CLIENT_MESSAGES_S
    ) {
      const lastActivityForCameraType: LastActivityForCameraType = {
        cameraType,
        camerasLastSample: this.camerasLastActivityPerType[cameraType],
      };
      this.camerasGateway.sendToClients(lastActivityForCameraType);

      // Update timestamp
      this.timestampLastMessageSentPerCameraType[cameraType] = now;
    }
  }

  private sendMessageSensorsGateway(message: DataReceivedResponse, sensorType: SensorType) {
    // Do not overload client by sending too many messages, we only send one per DELAY_FOR_SENDING_CLIENT_MESSAGES_MS
    if (
      this.timestampLastMessageSentPerSensorType[sensorType] === 0 ||
      message.timestamp - this.timestampLastMessageSentPerSensorType[sensorType] > DELAY_FOR_SENDING_CLIENT_MESSAGES_S
    ) {
      // Notify all clients connected that samples have been saved
      const messageGateway: DataReceivedWithSensorsLastActivityResponse = {
        ...message,
        sensorsLastActivity: this.sensorsLastActivityPerType[sensorType],
      };
      this.sensorsGateway.sendToAll(messageGateway);

      // Update timestamp last message sent for sensor type
      this.timestampLastMessageSentPerSensorType[sensorType] = message.timestamp;
    }
  }

  private mapDtoListToSampleList(sensorType: SensorType, sensorDtoList: SensorDto[]) {
    const samplesToSave = sensorDtoList.map((dto) => {
      const buffer = Buffer.from(JSON.stringify(dto));
      const compressedBuffer = zlib.deflateSync(buffer);

      /* To decompress buffer
      const decompressedBuffer = zlib.inflateSync(compressedBuffer);
      */

      const sample: Sample = {
        blob: compressedBuffer,
        ip: dto.ip,
        timestamp: dto.timestamp,
        location: this.getLocationOfSensor(sensorType, dto.ip),
        participantUuid: this.activeExperiment.participant.uuid,
        taskUuid: this.activeExperiment.taskUuid,
        trialNbr: this.activeExperiment.trialNbr,
      };
      return sample;
    });
    return samplesToSave;
  }

  getLocationOfSensor(sensorType: SensorType, sensorIp: string) {
    return this.activeExperiment.experiment.config.sensorsPerType[sensorType].find((sensor) => sensor.ip === sensorIp)
      ?.location;
  }
}
