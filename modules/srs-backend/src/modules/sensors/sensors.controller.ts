import {
  BadRequestException,
  Body,
  Controller,
  Get,
  ParseArrayPipe,
  Post,
  Query,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { SensorsService } from './sensors.service';
import { ShellyRedDto } from './dto/shelly-red.dto';
import { ApiBody, ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ShellyEmDto } from './dto/shelly-em.dto';
import { EnvironmentDataDto } from './dto/environment-data.dto';
import { EmfitDto } from './dto/emfit.dto';
import { CameraDto } from './dto/camera.dto';
import { LidarDto } from './dto/lidar.dto';
import { DummyDto } from './dto/dummy.dto';
import { EnumValidationPipe } from '../../pipes/enum-validation.pipe';
import { PostDataSensorsGuard } from './guards/post-data-sensors.guard';
import { DoorDto } from './dto/door.dto';
import { SensorType } from '../../common/enums/sensor-type.enum';
import { LuminanceDto } from './dto/luminance.dto';
import { WaterflowDto } from './dto/waterflow.dto';
import { SeismographDto } from './dto/seismograph.dto';
import { MattressDto } from './dto/mattress.dto';
import { RadarDto } from './dto/radar.dto';
import { SensorsModuleRequestLoggingInterceptor } from './interceptors/sensors-module-request-logging.interceptor';
import { NightVisionCameraDto } from './dto/night-vision-camera.dto';
import { MicrophoneDto } from './dto/microphone.dto';

@ApiTags('sensors')
@Controller('sensors')
@UseInterceptors(SensorsModuleRequestLoggingInterceptor)
export class SensorsController {
  constructor(private sensorsService: SensorsService) {}

  // SensorsGuard restrict endpoints to sensors configured for experiment
  @UseGuards(PostDataSensorsGuard)
  @Post('shelly-red')
  // ApiBody is required for Swagger to generate model definition.
  // See: https://docs.nestjs.com/openapi/types-and-parameters#generics-and-interfaces
  @ApiBody({ type: [ShellyRedDto] })
  createDataShellyRed(@Body(new ParseArrayPipe({ items: ShellyRedDto })) shellyRedDtoList: ShellyRedDto[]) {
    // TODO: Check what is best way to handle empty list
    if (shellyRedDtoList.length == 0) {
      throw new BadRequestException();
    }

    return this.sensorsService.createSensorDataFromBatch(shellyRedDtoList, SensorType.ShellyRed);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('shelly-em')
  @ApiBody({ type: [ShellyEmDto] })
  createDataShellyEm(@Body(new ParseArrayPipe({ items: ShellyEmDto })) shellyEmDtoList: ShellyEmDto[]) {
    if (shellyEmDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(shellyEmDtoList, SensorType.ShellyEm);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('environment')
  @ApiBody({ type: [EnvironmentDataDto] })
  createDataEnvironment(
    @Body(new ParseArrayPipe({ items: EnvironmentDataDto })) environmentDataDtoList: EnvironmentDataDto[],
  ) {
    if (environmentDataDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(environmentDataDtoList, SensorType.Environment);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('emfit-data')
  @ApiBody({ type: [EmfitDto] })
  createDataEmFit(@Body(new ParseArrayPipe({ items: EmfitDto })) emfitDtoList: EmfitDto[]) {
    if (emfitDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(emfitDtoList, SensorType.Emfit);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('camera')
  @ApiBody({ type: [CameraDto] })
  createDataCamera(@Body(new ParseArrayPipe({ items: CameraDto })) cameraDtoList: CameraDto[]) {
    if (cameraDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(cameraDtoList, SensorType.Camera);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('night-vision-camera')
  @ApiBody({ type: [NightVisionCameraDto] })
  createNightVisionDataCamera(
    @Body(new ParseArrayPipe({ items: NightVisionCameraDto })) nightVisionCameraDtoList: NightVisionCameraDto[],
  ) {
    if (nightVisionCameraDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(nightVisionCameraDtoList, SensorType.NightVisionCamera);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('door')
  @ApiBody({ type: [LidarDto] })
  createDataDoor(@Body(new ParseArrayPipe({ items: DoorDto })) doorDtoList: DoorDto[]) {
    if (doorDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(doorDtoList, SensorType.Door);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('lidar')
  @ApiBody({ type: [LidarDto] })
  createDataLidar(@Body(new ParseArrayPipe({ items: LidarDto })) lidarDtoList: LidarDto[]) {
    if (lidarDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(lidarDtoList, SensorType.Lidar);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('waterflow')
  @ApiBody({ type: [WaterflowDto] })
  createDataWaterflow(@Body(new ParseArrayPipe({ items: WaterflowDto })) waterflowDtoList: WaterflowDto[]) {
    if (waterflowDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(waterflowDtoList, SensorType.WaterFlow);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('seismograph')
  @ApiBody({ type: [SeismographDto] })
  createDataSeismograph(@Body(new ParseArrayPipe({ items: SeismographDto })) seismographDtoList: SeismographDto[]) {
    if (seismographDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(seismographDtoList, SensorType.Seismograph);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('radar')
  @ApiBody({ type: [RadarDto] })
  createDataRadar(@Body(new ParseArrayPipe({ items: RadarDto })) radarDtoList: RadarDto[]) {
    if (radarDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(radarDtoList, SensorType.Radar);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('mattress')
  @ApiBody({ type: [MattressDto] })
  createDataMattress(@Body(new ParseArrayPipe({ items: MattressDto })) mattressDtoList: MattressDto[]) {
    if (mattressDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(mattressDtoList, SensorType.Mattress);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('microphone')
  @ApiBody({ type: [MicrophoneDto] })
  createDataMicrophone(@Body(new ParseArrayPipe({ items: MicrophoneDto })) microphoneDtoList: MicrophoneDto[]) {
    if (microphoneDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(microphoneDtoList, SensorType.Microphone);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('luminance')
  @ApiBody({ type: [LuminanceDto] })
  createDataLuminance(@Body(new ParseArrayPipe({ items: LuminanceDto })) luminanceDtoList: LuminanceDto[]) {
    if (luminanceDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(luminanceDtoList, SensorType.Luminance);
  }

  @UseGuards(PostDataSensorsGuard)
  @Post('dummy')
  @ApiBody({ type: [DummyDto] })
  createDataDummy(@Body(new ParseArrayPipe({ items: DummyDto })) dummyDtoList: DummyDto[]) {
    if (dummyDtoList.length == 0) {
      throw new BadRequestException();
    }
    return this.sensorsService.createSensorDataFromBatch(dummyDtoList, SensorType.Dummy);
  }
}
