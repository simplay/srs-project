import { ConsoleLogger, Logger, Module } from '@nestjs/common';
import { SensorsController } from './sensors.controller';
import { SensorsService } from './sensors.service';
import { SensorsGateway } from './sensors.gateway';
import { CamerasGateway } from './cameras.gateway';
import { ExperimentsModule } from '../experiments/experiments.module';
import { SamplesModule } from '../samples/samples.module';

@Module({
  imports: [ExperimentsModule, SamplesModule],
  controllers: [SensorsController],
  providers: [SensorsService, SensorsGateway, ConsoleLogger, Logger, CamerasGateway],
  exports: [],
})
export class SensorsModule {}
