import { Test, TestingModule } from '@nestjs/testing';
import { SensorsService } from './sensors.service';
import { CanActivate } from '@nestjs/common';
import { PostDataSensorsGuard } from './guards/post-data-sensors.guard';
import { ConfigsRepository } from '../../repositories/configs.repository';
import { UsersRepository } from '../../repositories/users.repository';
import { SensorsGateway } from './sensors.gateway';
import { SamplesRepository } from '../../repositories/samples.repository';

describe('SensorsService', () => {
  let service: SensorsService;

  const mockSamplesRepository = {};
  const mockConfigsRepository = {};
  const mockSensorsGateway = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SensorsService,
        {
          provide: SamplesRepository,
          useValue: mockSamplesRepository,
        },
        {
          provide: ConfigsRepository,
          useValue: mockConfigsRepository,
        },
        {
          provide: SensorsGateway,
          useValue: mockSensorsGateway,
        },
      ],
    }).compile();

    service = module.get<SensorsService>(SensorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
