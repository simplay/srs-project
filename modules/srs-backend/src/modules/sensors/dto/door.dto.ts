// Door sensors

import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty } from 'class-validator';

export class DoorDto extends SensorDto {
  @ApiProperty()
  @IsBoolean()
  @IsNotEmpty()
  readonly doorOpen: boolean;
}
