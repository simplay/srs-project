import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { SensorDto } from './sensor.dto';

// Luminance sensors

export class LuminanceDto extends SensorDto {
  @ApiProperty()
  @IsNumber()
  readonly red: number;

  @ApiProperty()
  @IsNumber()
  readonly green: number;

  @ApiProperty()
  @IsNumber()
  readonly blue: number;

  @ApiProperty()
  @IsNumber()
  readonly clear: number;
}
