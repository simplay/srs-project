import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { SensorDto } from './sensor.dto';

// Power sensors attached to devices with high current.

export class ShellyEmDto extends SensorDto {
  @ApiProperty()
  @IsNumber()
  readonly power: number;

  @ApiProperty()
  @IsNumber()
  readonly uptime?: number;
}
