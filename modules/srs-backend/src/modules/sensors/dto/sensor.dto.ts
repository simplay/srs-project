import { ApiProperty } from '@nestjs/swagger';
import { IsIP, IsNumber, Min } from 'class-validator';

// All sensors send these properties

export class SensorDto {
  @ApiProperty()
  @IsIP(4)
  readonly ip: string;

  // Number of seconds elapsed since Epoch. It can have milliseconds precision (float number)
  @ApiProperty()
  @IsNumber()
  // 1622505600 = 2021-06-01 00:00:00
  @Min(1622505600)
  timestamp: number;
}
