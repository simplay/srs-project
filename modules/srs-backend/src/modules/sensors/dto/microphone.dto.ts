import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';

enum DataType {
  NT_USB = 'NT_USB',
  RE_SPEAKER = 'RE_SPEAKER',
  RE_SPEAKER_DOA = 'RE_SPEAKER_DOA',
}

export class MicrophoneDto extends SensorDto {
  @ApiProperty()
  @IsNotEmpty()
  data: string;

  @ApiProperty({ enum: Object.values(DataType) })
  @IsNotEmpty()
  @IsEnum(DataType)
  dataType: DataType;
}
