// Environment sensors

import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class EnvironmentDataDto extends SensorDto {
  @ApiProperty()
  @IsNumber()
  readonly temperature: number;

  @ApiProperty()
  @IsNumber()
  readonly humidity: number;
}
