import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty } from 'class-validator';

export class SeismographDto extends SensorDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  data: string[];
}
