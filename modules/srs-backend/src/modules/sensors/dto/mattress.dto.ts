import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty } from 'class-validator';

export class MattressDto extends SensorDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  data: object[];
}
