import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { SensorDto } from './sensor.dto';

// Power sensors

export class ShellyRedDto extends SensorDto {
  @ApiProperty({ required: false })
  @IsNumber()
  readonly power?: number;

  @ApiProperty()
  @IsNumber()
  readonly temp: number;

  @ApiProperty()
  @IsNumber()
  readonly uptime?: number;
}
