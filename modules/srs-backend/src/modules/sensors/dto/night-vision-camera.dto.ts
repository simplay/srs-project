import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class NightVisionCameraDto extends SensorDto {
  // base64 encoded image
  @ApiProperty()
  @IsNotEmpty()
  data: string;
}
