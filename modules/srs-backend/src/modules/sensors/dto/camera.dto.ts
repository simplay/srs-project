// Camera sensors

import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CameraDto extends SensorDto {
  // base64 encoded image
  @ApiProperty()
  @IsNotEmpty()
  data: string;
}
