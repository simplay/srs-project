import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { SensorDto } from './sensor.dto';

// Light Detection and Ranging sensors

export class LidarDto extends SensorDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly sample: string;
}
