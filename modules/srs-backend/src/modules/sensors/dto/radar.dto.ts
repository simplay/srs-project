import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty } from 'class-validator';

export class RadarDto extends SensorDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  samples: string[];
}
