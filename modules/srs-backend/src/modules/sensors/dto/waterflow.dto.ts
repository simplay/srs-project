import { SensorDto } from './sensor.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class WaterflowDto extends SensorDto {
  @ApiProperty()
  @IsNumber()
  readonly frequency: number;

  @ApiProperty()
  @IsNumber()
  readonly pulses: number;
}
