import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Booking } from '../../repositories/schema/booking.schema';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendBookingConfirmation(booking: Booking, isUpdate: boolean) {
    let subject = isUpdate ? 'Booking updated' : 'Booking confirmation';
    subject += ` for room "${booking.room.name}"`;
    await this.mailerService.sendMail({
      to: booking.email,
      subject,
      template: './booking-confirmation', // `.hbs` extension is appended automatically
      context: {
        // Filling variables in template
        roomName: booking.room.name,
        bookingName: booking.name,
        bookingType: booking.bookingType,
        // timestamp is store in seconds since epoch
        bookingStart: new Date(booking.start * 1000).toLocaleDateString(),
        bookingEnd: new Date(booking.end * 1000).toLocaleDateString(),
      },
    });
  }

  async sendBookingDeletionNotification(booking: Booking) {
    await this.mailerService.sendMail({
      to: booking.email,
      subject: `Booking ${booking.name} for room ${booking.room.name} cancelled`,
      template: './booking-cancellation-notification', // `.hbs` extension is appended automatically
      context: {
        // Filling variables in template
        roomName: booking.room.name,
        bookingName: booking.name,
        bookingType: booking.bookingType,
        // timestamp is store in seconds since epoch
        bookingStart: new Date(booking.start * 1000).toLocaleDateString(),
        bookingEnd: new Date(booking.end * 1000).toLocaleDateString(),
      },
    });
  }
}
