import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { join } from 'path';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: process.env.SMS_SMTP_HOST,
        port: 465,
        secure: true,
        auth: {
          user: process.env.SMS_SMTP_USER,
          pass: process.env.SMS_SMTP_PASSWORD,
        },
      },
      defaults: {
        from: process.env.SMS_SMTP_USER,
      },
      template: {
        dir: join(__dirname, 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  providers: [MailService],
  exports: [MailService], // export for DI
})
export class MailModule {}
