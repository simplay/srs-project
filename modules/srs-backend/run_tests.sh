#!/bin/sh

export $(cat .env | grep -v "#" | xargs)

npm test -- --config ./test/jest-e2e.json --verbose --forceExit --detectOpenHandles
