import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { forwardRef, INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigsModule } from '../src/modules/configs/configs.module';
import { Config, ConfigDocument, ConfigSchema } from '../src/repositories/schema/config.schema';
import { UsersRepository } from '../src/repositories/users.repository';
import { AuthModule } from '../src/modules/auth/auth.module';
import { stopMongoDBInMemoryInstance, rootMongooseTestModule } from './utils/mongoose-test-module';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { CreateConfigDto } from '../src/modules/configs/dto/create-config.dto';
import { Role } from '../src/common/enums/role.enum';
import { ConfigDto } from '../src/modules/configs/dto/config.dto';
import { UpdateConfigDto } from '../src/modules/configs/dto/update-config.dto';
import {
  createTestData,
  configUuid,
  getConfig,
  configName,
  getJWTForUserWithRole,
  experiment2Name,
  experiment1Name,
} from './utils/test-utils';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { Experiment, ExperimentSchema } from '../src/repositories/schema/experiment.schema';
import { SensorType } from '../src/common/enums/sensor-type.enum';
import { Sensor } from '../src/common/entities/sensor.entity';
import {
  ACTIVE_EXPERIMENT_MODEL_NAME,
  ActiveExperiment,
  ActiveExperimentDocument,
  ActiveExperimentSchema,
} from '../src/repositories/schema/active-experiment.schema';
import { Model } from 'mongoose';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('ConfigController (e2e)', () => {
  let app: INestApplication;

  let moduleFixture;
  beforeEach(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [
        ConfigsModule,
        AuthModule, // To authenticate (to get JWT)
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Experiment.name, schema: ExperimentSchema },
          { name: ActiveExperiment.name, schema: ActiveExperimentSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  function getCreateConfigDto(): CreateConfigDto {
    const createExperimentDto: CreateConfigDto = {
      name: 'NEW_CONFIG_NAME',
    };
    return createExperimentDto;
  }

  describe('/configs/:uuid (GET): Get config', () => {
    const endpoint = `/configs/${configUuid}`;
    it('Get config without JWT returns 401 (Unauthorized)', async () => {
      await request(app.getHttpServer()).get(endpoint).expect(401);
    });

    it('Get config with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('Get config with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Get config with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/configs (GET): List configs', () => {
    const endpoint = '/configs';
    it('List configs without JWT returns 401 (Unauthorized)', async () => {
      await request(app.getHttpServer()).get(endpoint).expect(401);
    });

    it('List configs with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('List configs with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('List configs with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/configs (POST): Create config', () => {
    const endpoint = '/configs';

    it('Create config without JWT returns 401 (Unauthorized)', async () => {
      await request(app.getHttpServer()).post(endpoint).send(getCreateConfigDto()).expect(401);
    });

    it('Create config with admin role returns 201 (Created)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);
    });

    it('Create config with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create config with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/configs/:uuid (PUT): Update config', () => {
    const endpoint = `/configs/${configUuid}`;

    async function getUpdateConfigDto(): Promise<UpdateConfigDto> {
      const config: ConfigDocument = await getConfig(configName, moduleFixture);
      const updateConfigDto: UpdateConfigDto = {
        name: 'NEW_CONFIG_NAME',
        sensorsPerType: config.sensorsPerType as Record<SensorType, Sensor[]>,
      };
      return updateConfigDto;
    }

    it('Update config without JWT returns 401 (Unauthorized)', async () => {
      await request(app.getHttpServer()).put(endpoint).send(getUpdateConfigDto()).expect(401);
    });

    it('Update config with admin role returns 200 (OK)', async () => {
      // The active experiment in the in-mem-database is currently set to being recording
      // => update the isRecording flag to false before trying to update the config
      const activeExperimentModel = app.get<Model<ActiveExperimentDocument>>(`${ACTIVE_EXPERIMENT_MODEL_NAME}Model`);
      const activeExperiment = await activeExperimentModel.findOne().exec();
      expect(activeExperiment).not.toBeNull();
      activeExperiment.isRecording = false;
      activeExperiment.save();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(await getUpdateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const configDto: ConfigDto = res.body;
      expect(configDto.name).toEqual('NEW_CONFIG_NAME');
    });

    it('Update config with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(await getUpdateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Update config with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(await getUpdateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Update config of an experiment currently recording with admin role returns 403 (Forbidden)', async () => {
      // The active experiment in the in-mem-database is currently set to being recording
      // => Trying to update the config will trigger the error
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(await getUpdateConfigDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        'A recording is ongoing for this config. Please stop first the experiment before editing the config.',
      );
    });
  });

  describe('/configs/:uuid (DELETE): Delete config', () => {
    it('Delete config being part of an experiment with admin role returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .delete(`/configs/${configUuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `Config "${configName}" is used in experiment(s): ${experiment1Name},${experiment2Name}. Please remove the config from experiment(s) before deleting the config`,
      );
    });

    it('Delete config not being part of an experiment with admin role returns 200 (OK)', async () => {
      // Create a new config
      const createConfigDto = getCreateConfigDto();
      const resCreateConfig = await request(app.getHttpServer())
        .post('/configs')
        .send(createConfigDto)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const createdConfig: ConfigDto = resCreateConfig.body;

      await request(app.getHttpServer())
        .delete(`/configs/${createdConfig.uuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const res = await request(app.getHttpServer())
        .get('/configs')
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const configsDto: ConfigDto[] = res.body;

      const configsUuid = configsDto.map((configDto) => configDto.uuid);
      expect(configsUuid).not.toContain(createdConfig.uuid);
    });

    it('Delete config with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(`/configs/${configUuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete config with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(`/configs/${configUuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });
});
