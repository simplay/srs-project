import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { UsersRepository } from '../src/repositories/users.repository';
import {
  adminUser,
  configUuid,
  createSampleShellyRed,
  createTestData,
  experiment1Name,
  experiment1Uuid,
  experiment2Uuid,
  getJWTForUser,
  getJWTForUserWithRole,
  messageEntranceDoor,
  operatorUser1,
  operatorUser2,
  operatorUser3,
  participant1Uuid,
  participant2Uuid,
  participant3Uuid,
  secretartiatUser,
  task1Name,
  task1Uuid,
  task2Name,
  task2Uuid,
} from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { ExperimentsModule } from '../src/modules/experiments/experiments.module';
import { Role } from '../src/common/enums/role.enum';
import { AuthModule } from '../src/modules/auth/auth.module';
import { CreateExperimentDto } from '../src/modules/experiments/dto/create-experiment.dto';
import { v4 as uuidv4 } from 'uuid';
import { ActiveExperimentDto } from '../src/modules/experiments/dto/active-experiment.dto';
import { ExperimentDto } from '../src/modules/experiments/dto/experiment.dto';
import { UpdateExperimentDto } from '../src/modules/experiments/dto/update-experiment.dto';
import { CreateTaskDto } from '../src/modules/experiments/dto/create-task.dto';
import { UpdateTaskDto } from '../src/modules/experiments/dto/update-task.dto';
import { StartRecordingExperimentDto } from '../src/modules/experiments/dto/start-recording-experiment.dto';
import { UpdateTaskCurrentRecordingDto } from '../src/modules/experiments/dto/update-task-current-recording.dto';
import { ParticipantProgressDto } from '../src/modules/experiments/dto/participant-progress.dto';
import { ActiveExperimentPublicInfoDto } from '../src/modules/experiments/dto/active-experiment-public-info.dto';
import { MessageScreenEntranceDto } from '../src/modules/experiments/dto/message-screen-entrance.dto';
import { CreateMessageScreenEntranceDto } from '../src/modules/experiments/dto/create-message-screen-entrance.dto';
import { CreateExperimentNoteDto } from '../src/modules/experiments/dto/create-experiment-note.dto';
import { ExperimentLogDto } from '../src/modules/experiments/dto/experiment-log.dto';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('ExperimentsController (e2e)', () => {
  let app: INestApplication;
  let moduleFixture = null;

  // We recreate the data before each test
  beforeEach(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [
        ExperimentsModule,
        AuthModule, // For authentication to get JWT
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/experiments (GET): List experiments', () => {
    const endpoint = '/experiments';

    it('List experiments with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('List experiments with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('List experiments with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments (POST): Create experiment', () => {
    const endpoint = '/experiments';

    function getValidCreateExperimentDto(): CreateExperimentDto {
      const createExperimentDto: CreateExperimentDto = {
        uuid: uuidv4(),
        name: 'NEW_EXPERIMENT',
        configUuid: configUuid,
        ownerUsername: adminUser.username,
      };
      return createExperimentDto;
    }

    it('Create experiment with admin role returns 201 (Created)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(getValidCreateExperimentDto())
        .expect(201);

      const body = res.body;
      expect(body).toHaveProperty('name');
      expect(body.name).toEqual('NEW_EXPERIMENT');
    });

    it('Create experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .send(getValidCreateExperimentDto())
        .expect(403);
    });

    it('Create experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(getValidCreateExperimentDto())
        .expect(403);
    });
  });

  describe('/experiments/active (GET): Get active experiment', () => {
    // EXPERIMENT_1 is currently set as the active experiment (see createTestData of test-utils.ts)

    const endpoint = '/experiments/active';

    function checkActiveExperimentIsExperiment1(body: any) {
      expect(body).toHaveProperty('experiment');
      const experiment = body.experiment;
      expect(experiment).toHaveProperty('name');
      expect(experiment.name).toEqual(experiment1Name);
    }

    it('Get active experiment with admin role returns EXPERIMENT_1', async () => {
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      checkActiveExperimentIsExperiment1(res.body);
    });

    it('Get active experiment with OPERATOR_1 being part of active experiment returns EXPERIMENT_1', async () => {
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      checkActiveExperimentIsExperiment1(res.body);
    });

    it('Get active experiment with OPERATOR_2 not being part of active experiment returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        "You are not listed as an operator for the active experiment and can't access the running experiment.",
      );
    });

    it('Get active experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, secretartiatUser)}`)
        .expect(403);
    });
  });

  describe('/experiments/set-active (POST): Change active experiment', () => {
    const url = `/experiments/set-active/?experimentUuid=${experiment2Uuid}`;

    it('Change active experiment with admin role and empty samples collections returns 201 (Created)', async () => {
      const res = await request(app.getHttpServer())
        .post(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.experiment.uuid).toEqual(experiment2Uuid);
    });

    it('Change active experiment with admin role and non-empty samples collections returns 403 (Forbiddem)', async () => {
      // Create a sample of shelly red
      await createSampleShellyRed(moduleFixture);

      const res = await request(app.getHttpServer())
        .post(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual('The database of samples is not empty.');
    });

    it('Change active experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Change active experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid (GET): Get experiment with uuid', () => {
    const url = `/experiments/${experiment1Uuid}`;

    it('Get experiment with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .get(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.uuid).toEqual(experiment1Uuid);
    });

    it('Get experiment with operator roles returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Get experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid (PUT): Update experiment with uuid', () => {
    const url = `/experiments/${experiment1Uuid}`;

    function getValidUpdateExperimentDto(): UpdateExperimentDto {
      const updateExperimentDto: UpdateExperimentDto = {
        uuid: uuidv4(),
        name: 'UPDATED_EXPERIMENT',
        configUuid: configUuid,
        ownerUsername: adminUser.username,
      };
      return updateExperimentDto;
    }

    it('Update experiment with admin role returns 200 (OK)', async () => {
      const updateExperimentDto = getValidUpdateExperimentDto();

      const res = await request(app.getHttpServer())
        .put(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(updateExperimentDto)
        .expect(200);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.uuid).toEqual(updateExperimentDto.uuid);
      expect(experimentDto.name).toEqual(updateExperimentDto.name);
      expect(experimentDto.config.uuid).toEqual(updateExperimentDto.configUuid);
      expect(experimentDto.owner.username).toEqual(updateExperimentDto.ownerUsername);
    });

    it('Update experiment with operator roles returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .send(getValidUpdateExperimentDto())
        .expect(403);
    });

    it('Update experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(getValidUpdateExperimentDto())
        .expect(403);
    });
  });

  describe('/experiments/:uuid/add-participants (POST): Add a list of participants to the experiment', () => {
    // EXPERIMENT_1 has 1 participant (participant1), let's add participant 2 and 3 for these tests

    const participantsUuidList = [participant2Uuid, participant3Uuid];

    const endpoint = `/experiments/${experiment1Uuid}/add-participants?participants=${participantsUuidList.join(',')}`;

    it('Add a list of participants with admin role returns 201 (Created)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.participants.length).toEqual(3);
      const uuids = experimentDto.participants.map((participant) => participant.uuid);
      expect(uuids).toContain(participant1Uuid);
      expect(uuids).toContain(participant2Uuid);
      expect(uuids).toContain(participant3Uuid);
    });

    it('Add a list of participants with OPERATOR_1 being part of experiment returns 201 (Created)', async () => {
      // Operator user 1 is part of EXPERIMENT_1
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.participants.length).toEqual(3);
    });

    it('Add a list of participants with OPERATOR_2 not being part of experiment returns 403 (Forbidden)', async () => {
      // Operator user 2 is not part of EXPERIMENT_1
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        'You cannot add participants to this experiment as you are not listed as an operator.',
      );
    });

    it('Add a list of participants with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    // TODO: add non existing participant, add duplicate, ...
  });

  describe('/experiments/:uuid/remove-participant (POST): Remove a participant from the experiment', () => {
    // EXPERIMENT_1 has 1 participant (participant1), let's remove it

    const endpoint = `/experiments/${experiment1Uuid}/remove-participant?participant=${participant1Uuid}`;

    it('Remove a participant with admin role returns 201 (Created/Updated)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.participants.length).toEqual(0);
    });

    it('Remove a non-existing participant with admin role returns 404 (Not found)', async () => {
      const fakeParticipantUuid = uuidv4();
      const res = await request(app.getHttpServer())
        .post(`/experiments/${experiment1Uuid}/remove-participant?participant=${fakeParticipantUuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(404);

      expect(res.body.message).toEqual(`Participant with uuid '${fakeParticipantUuid}' not found`);
    });

    it('Remove PARTICIPANT_2 not being part of EXPERIMENT_1 returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .post(`/experiments/${experiment1Uuid}/remove-participant?participant=${participant2Uuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(`Participant ${participant2Uuid} is not part of experiment ${experiment1Name}`);
    });

    it('Remove a participant with OPERATOR_1 being part of experiment returns 201 (Created/Updated)', async () => {
      // Operator user 1 is part of EXPERIMENT_1
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.participants.length).toEqual(0);
    });

    it('Remove a participant with OPERATOR_2 not being part of experiment returns 403 (Forbidden)', async () => {
      // Operator user 2 is not part of EXPERIMENT_1
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        'You cannot remove participant from this experiment as you are not listed as an operator.',
      );
    });

    it('Remove a participants with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/add-operators (POST): Add a list of operators to the experiment', () => {
    // EXPERIMENT_1 has 1 operator (operator1), let's add operator2 and operator3

    const operatorsUsernameList = [operatorUser2.username, operatorUser3.username];

    const endpoint = `/experiments/${experiment1Uuid}/add-operators?operators=${operatorsUsernameList.join(',')}`;

    it('Add a list of operators with admin role returns 201 (Created)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.operators.length).toEqual(3);
      const usernameList = experimentDto.operators.map((operator) => operator.username);
      expect(usernameList).toContain(operatorUser1.username);
      expect(usernameList).toContain(operatorUser2.username);
      expect(usernameList).toContain(operatorUser3.username);
    });

    it('Add a list of operators with operators role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Add a list of operators with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    // TODO: add non existing operator, add duplicate, ...
  });

  describe('/experiments/:uuid/remove-operator (POST): Remove an operator from the experiment', () => {
    // EXPERIMENT_1 has 1 operator (operator1), let's remove it

    const endpoint = `/experiments/${experiment1Uuid}/remove-operator?operator=${operatorUser1.username}`;

    it('Remove an operator with admin role returns 201 (Created/Updated)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.operators.length).toEqual(0);
    });

    it('Remove a non-existing operator with admin role returns 404 (Not found)', async () => {
      const username = 'fake';
      const res = await request(app.getHttpServer())
        .post(`/experiments/${experiment1Uuid}/remove-operator?operator=${username}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(404);

      expect(res.body.message).toEqual(`User with username '${username}' not found`);
    });

    it('Remove OPERATOR_2 not being part of EXPERIMENT_1 returns 403 (Forbidden)', async () => {
      const username = operatorUser2.username;
      const res = await request(app.getHttpServer())
        .post(`/experiments/${experiment1Uuid}/remove-operator?operator=${username}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(`Operator ${username} is not part of experiment ${experiment1Name}`);
    });

    it('Remove an operator with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Remove an operator with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/add-task (POST): Add a task to an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/add-task`;

    function getValidCreateTaskDto() {
      const createTaskDto: CreateTaskDto = {
        uuid: uuidv4(),
        name: 'T_9',
        description: 'Task 9 description',
      };

      return createTaskDto;
    }

    it('Add a task to the experiment with admin role returns 201 (Created)', async () => {
      const createNewTaskDto = getValidCreateTaskDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(createNewTaskDto)
        .expect(201);

      const experimentDto: ExperimentDto = res.body;
      const newTask = experimentDto.tasks.find((task) => task.uuid === createNewTaskDto.uuid);
      expect(newTask.name).toEqual(createNewTaskDto.name);
      expect(newTask.description).toEqual(createNewTaskDto.description);
    });

    it('Add a task to the experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .send(getValidCreateTaskDto())
        .expect(403);
    });

    it('Add a task to the experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(getValidCreateTaskDto())
        .expect(403);
    });

    it('Add a task with a task UUID that exists already returns 403 (Forbidden)', async () => {
      const createNewTaskDto: CreateTaskDto = {
        uuid: task1Uuid,
        name: 'T_9',
        description: 'Task description',
      };

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(createNewTaskDto)
        .expect(403);

      expect(res.body.message).toEqual(`A task with UUID ${task1Uuid} already exists.`);
    });

    it('Add a task with a task name that exists already returns 403 (Forbidden)', async () => {
      const createNewTaskDto: CreateTaskDto = {
        uuid: uuidv4(),
        name: task1Name,
        description: 'Task description',
      };

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(createNewTaskDto)
        .expect(403);

      expect(res.body.message).toEqual(`A task with name ${task1Name} already exists.`);
    });
  });

  describe('/experiments/:experimentUuid/task/:taskUuid (PUT): Update a task of an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/task/${task1Uuid}`;

    function getValidUpdateTaskDto(): UpdateTaskDto {
      const updateTaskDto: UpdateTaskDto = {
        name: 'T_1_Updated',
        description: 'New description',
      };
      return updateTaskDto;
    }

    it('Update a task of an experiment with admin role returns 200 (OK)', async () => {
      const updateTaskDto: UpdateTaskDto = getValidUpdateTaskDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(updateTaskDto)
        .expect(200);

      const experimentDto: ExperimentDto = res.body;
      const updatedTask = experimentDto.tasks.find((task) => task.uuid === task1Uuid);
      expect(updatedTask.name).toEqual(updateTaskDto.name);
      expect(updatedTask.description).toEqual(updateTaskDto.description);
    });

    it('Update a task of an experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .send(getValidUpdateTaskDto())
        .expect(403);
    });

    it('Update a task of an experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(getValidUpdateTaskDto())
        .expect(403);
    });

    it(`Update a non-exiting task returns 404 (Not found)`, async () => {
      const fakeTaskUuid = uuidv4();

      const url = `/experiments/${experiment1Uuid}/task/${fakeTaskUuid}`;

      const res = await request(app.getHttpServer())
        .put(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(getValidUpdateTaskDto())
        .expect(404);

      expect(res.body.message).toEqual(`Task ${fakeTaskUuid} not found`);
    });

    it(`Update a task which conflicts with another one returns 403 (Forbidden)`, async () => {
      // try to update T_1 to T_2
      const updateTaskDto: UpdateTaskDto = {
        name: task2Name,
        description: 'New description',
      };

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(updateTaskDto)
        .expect(403);

      expect(res.body.message).toEqual('A task with same name already exist. Please make task names unique.');
    });
  });

  describe('/experiments/:experimentUuid/task/:taskUuid (DELETE): Delete a task from an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/task/${task1Uuid}`;

    it('Delete a task of an experiment with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const experimentDto: ExperimentDto = res.body;
      const updatedTask = experimentDto.tasks.find((task) => task.uuid === task1Uuid);
      expect(updatedTask).toBeUndefined();
    });

    it('Delete a task of an experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete a task of an experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Delete a non existing task returns 404 (Not Found)', async () => {
      const fakeTaskUuid = uuidv4();
      const url = `/experiments/${experiment1Uuid}/task/${fakeTaskUuid}`;

      const res = await request(app.getHttpServer())
        .delete(url)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(404);

      expect(res.body.message).toEqual(`Task with uuid ${fakeTaskUuid} not found.`);
    });
  });

  describe('/experiments/:uuid/start-recording (PUT): Start recording of an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/start-recording`;

    function getStartRecordingExperimentDto(): StartRecordingExperimentDto {
      const startRecordingExperimentDto: StartRecordingExperimentDto = {
        taskUuid: task1Uuid,
        participantUuid: participant1Uuid,
      };
      return startRecordingExperimentDto;
    }

    it('Start recording with admin role returns 200 (OK)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toEqual(startRecordingExperimentDto.taskUuid);
      expect(activeExperimentDto.participant.uuid).toEqual(startRecordingExperimentDto.participantUuid);
      expect(activeExperimentDto.isRecording).toEqual(true);
    });

    it('Start recording with OPERATOR_1 being part of experiment role returns 200 (OK)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toEqual(startRecordingExperimentDto.taskUuid);
      expect(activeExperimentDto.participant.uuid).toEqual(startRecordingExperimentDto.participantUuid);
      expect(activeExperimentDto.isRecording).toEqual(true);
    });

    it('Start recording with OPERATOR_2 not being part of experiment role returns 403 (FORBIDDEN)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .send(startRecordingExperimentDto)
        .expect(403);

      expect(res.body.message).toEqual(
        `You are not listed as an operator of the experiment and cannot perform this action.`,
      );
    });

    it('Start recording with secretariat role returns 403 (FORBIDDEN)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(startRecordingExperimentDto)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/update-task (PUT): Update task of current recording', () => {
    const endpoint = `/experiments/${experiment1Uuid}/update-task`;

    function getUpdateTaskCurrentRecordingDto(): UpdateTaskCurrentRecordingDto {
      const updateTaskCurrentRecordingDto: UpdateTaskCurrentRecordingDto = {
        taskUuid: task2Uuid,
      };
      return updateTaskCurrentRecordingDto;
    }

    it('Update task of current recording with admin role returns 200 (OK)', async () => {
      const updateTaskCurrentRecordingDto = getUpdateTaskCurrentRecordingDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(updateTaskCurrentRecordingDto)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toEqual(updateTaskCurrentRecordingDto.taskUuid);
      expect(activeExperimentDto.isRecording).toEqual(true);
    });

    it('Update task of current recording with OPERATOR_1 being part of experiment role returns 200 (OK)', async () => {
      const updateTaskCurrentRecordingDto = getUpdateTaskCurrentRecordingDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .send(updateTaskCurrentRecordingDto)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toEqual(updateTaskCurrentRecordingDto.taskUuid);
      expect(activeExperimentDto.isRecording).toEqual(true);
    });

    it('Update task of current recording with OPERATOR_2 not being part of experiment role returns 403 (FORBIDDEN)', async () => {
      const updateTaskCurrentRecordingDto = getUpdateTaskCurrentRecordingDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .send(updateTaskCurrentRecordingDto)
        .expect(403);

      expect(res.body.message).toEqual(
        `You are not listed as an operator of the experiment and cannot perform this action.`,
      );
    });

    it('Update task of current recording with secretariat role returns 403 (FORBIDDEN)', async () => {
      const updateTaskCurrentRecordingDto = getUpdateTaskCurrentRecordingDto();

      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(updateTaskCurrentRecordingDto)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/increment-trial-current-recording (PUT): Increment the trial for the task being currently recorded', () => {
    const endpoint = `/experiments/${experiment1Uuid}/increment-trial-current-recording`;

    it('Increment the trial of current recording without previous record for task with admin role throws 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `No record of task ${task1Uuid} for participant found. Please start recording task before incrementing trial.`,
      );
    });

    function getStartRecordingExperimentDto(): StartRecordingExperimentDto {
      const startRecordingExperimentDto: StartRecordingExperimentDto = {
        taskUuid: task1Uuid,
        participantUuid: participant1Uuid,
      };
      return startRecordingExperimentDto;
    }

    it('Increment the trial of current recording with admin role returns 200 (OK)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      // Start recording
      const endpointStartRecording = `/experiments/${experiment1Uuid}/start-recording`;
      await request(app.getHttpServer())
        .put(endpointStartRecording)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      // Increment trial
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.trialNbr).toEqual(2);
      expect(activeExperimentDto.isRecording).toBeTruthy();
    });

    it('Increment the trial of current recording with OPERATOR_1 being part of experiment role returns 200 (OK)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      // Start recording
      const endpointStartRecording = `/experiments/${experiment1Uuid}/start-recording`;
      await request(app.getHttpServer())
        .put(endpointStartRecording)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      // Increment trial
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.trialNbr).toEqual(2);
      expect(activeExperimentDto.isRecording).toBeTruthy();
    });

    it('Increment the trial of current recording with OPERATOR_2 not being part of experiment role returns 403 (FORBIDDEN)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      // Start recording (do this with an admin)
      const endpointStartRecording = `/experiments/${experiment1Uuid}/start-recording`;
      await request(app.getHttpServer())
        .put(endpointStartRecording)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      // Try to increment trial with operator 2
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `You are not listed as an operator of the experiment and cannot perform this action.`,
      );
    });

    it('Increment the trial of current recording with secretariat role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/stop-recording (PUT): Stop recording of an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/stop-recording`;

    it('Stop recording with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toBeNull();
      expect(activeExperimentDto.participant).toBeNull();
      expect(activeExperimentDto.isRecording).toBeFalsy();
    });

    it('Stop recording with OPERATOR_1 being part of experiment role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      const activeExperimentDto: ActiveExperimentDto = res.body;
      expect(activeExperimentDto.taskUuid).toBeNull();
      expect(activeExperimentDto.participant).toBeNull();
      expect(activeExperimentDto.isRecording).toBeFalsy();
    });

    it('Stop recording with OPERATOR_2 not being part of experiment role returns 403 (FORBIDDEN)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `You are not listed as an operator of the experiment and cannot perform this action.`,
      );
    });

    it('Stop recording with secretariat role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/complete.experiment (PUT): Complete an experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/complete-experiment`;

    it('Complete experiment with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const experimentDto: ExperimentDto = res.body;
      expect(experimentDto.completed).toBeTruthy();
    });

    it('Complete experiment with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });
    it('Complete experiment with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/participants-progress (GET): Get participants progress', () => {
    const endpoint = `/experiments/${experiment1Uuid}/participants-progress`;

    function getStartRecordingExperimentDto(): StartRecordingExperimentDto {
      const startRecordingExperimentDto: StartRecordingExperimentDto = {
        taskUuid: task1Uuid,
        participantUuid: participant1Uuid,
      };
      return startRecordingExperimentDto;
    }

    it('Get participants progress with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('Get participants progress with OPERATOR_1 being part of EXPERIMENT_1 returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);
    });

    it('Get participants progress with OPERATOR_2 not being part of EXPERIMENT_1 returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);
    });

    it('Get participants progress with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Get participants progress with OPERATOR_2 not being part of EXPERIMENT_1 returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .get(`/experiments/${experiment1Uuid}/participants-progress`)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser2)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        'You cannot get the participants progress as you are not listed as operator for this experiment.',
      );
    });

    it('Get participants progress with OPERATOR_1 being part of EXPERIMENT_1 returns correct progress (OK)', async () => {
      const startRecordingExperimentDto = getStartRecordingExperimentDto();

      // Start recording
      const endpointStartRecording = `/experiments/${experiment1Uuid}/start-recording`;
      await request(app.getHttpServer())
        .put(endpointStartRecording)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .send(startRecordingExperimentDto)
        .expect(200);

      // Increment trial
      const endpointIncrementTrial = `/experiments/${experiment1Uuid}/increment-trial-current-recording`;
      await request(app.getHttpServer())
        .put(endpointIncrementTrial)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      const participantProgressDtos: ParticipantProgressDto[] = res.body;
      expect(participantProgressDtos.length).toEqual(1);
      const participantProgressDto = participantProgressDtos[0];
      expect(participantProgressDto.participantUuid).toEqual(participant1Uuid);
      expect(participantProgressDto.taskUuid).toEqual(task1Uuid);
      expect(participantProgressDto.trialCounter).toEqual(2);
    });
  });

  describe('/experiments/active-experiment-public-info (GET): Get active experiment public info', () => {
    const endpoint = `/experiments/active-experiment-public-info`;

    it('Get the active experiment public info without JWT returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer()).get(endpoint).expect(200);

      const activeExperimentPublicInfoDto: ActiveExperimentPublicInfoDto = res.body;
      expect(activeExperimentPublicInfoDto.experimentName).toEqual(experiment1Name);
      expect(activeExperimentPublicInfoDto.isRecording).toBeTruthy();
    });
  });

  describe('/experiments/message-screen-entrance (GET): Get the message for the entrance screen', () => {
    const endpoint = `/experiments/message-screen-entrance`;

    it('Get the message for the screen entrance without JWT returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer()).get(endpoint).expect(200);

      const messageScreenEntranceDto: MessageScreenEntranceDto = res.body;
      expect(messageScreenEntranceDto.message).toEqual(messageEntranceDoor);
      expect(messageScreenEntranceDto.createdByUserFullName).toEqual(`${adminUser.firstname} ${adminUser.lastname}`);
    });
  });

  describe('/experiments/message-screen-entrance (POST): Set the message for the entrance screen', () => {
    const endpoint = `/experiments/message-screen-entrance`;

    function getValidCreateMessageScreenEntranceDto() {
      const dto: CreateMessageScreenEntranceDto = {
        message: 'New message',
      };
      return dto;
    }

    it('Set the message for the screen entrance with admin role returns 201 (Created)', async () => {
      const createMessageScreenEntranceDto = getValidCreateMessageScreenEntranceDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .send(createMessageScreenEntranceDto)
        .expect(201);

      const messageScreenEntrance: MessageScreenEntranceDto = res.body;
      expect(messageScreenEntrance.message).toEqual(createMessageScreenEntranceDto.message);
      expect(messageScreenEntrance.createdByUserFullName).toEqual(`${adminUser.firstname} ${adminUser.lastname}`);
    });

    it('Set the message for the screen entrance with operator role returns 403 (Forbidden)', async () => {
      const createMessageScreenEntranceDto = getValidCreateMessageScreenEntranceDto();

      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .send(createMessageScreenEntranceDto)
        .expect(403);
    });

    it('Set the message for the screen entrance with secretariat role returns 403 (Forbidden)', async () => {
      const createMessageScreenEntranceDto = getValidCreateMessageScreenEntranceDto();

      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(createMessageScreenEntranceDto)
        .expect(403);
    });
  });

  describe('/experiments/:uuid/note (POST): Save note for experiment', () => {
    const endpoint = `/experiments/${experiment1Uuid}/note`;

    function getValidCreateExperimentNoteDto(): CreateExperimentNoteDto {
      const createExperimentNoteDto: CreateExperimentNoteDto = {
        logLine: 'My note',
        experimentUuid: experiment1Uuid,
      };
      return createExperimentNoteDto;
    }

    it('Create note with admin role returns 201 (Created)', async () => {
      const createExperimentNoteDto = getValidCreateExperimentNoteDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .send(createExperimentNoteDto)
        .expect(201);

      const body: ExperimentLogDto = res.body;

      expect(body.log).toEqual(createExperimentNoteDto.logLine);
      expect(body.experimentUuid).toEqual(createExperimentNoteDto.experimentUuid);
      expect(body.username).toEqual(adminUser.username);
      expect(body).toHaveProperty('datetime');
    });

    it('Create note with operator role returns 201 (Created)', async () => {
      const createExperimentNoteDto = getValidCreateExperimentNoteDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .send(createExperimentNoteDto)
        .expect(201);

      const body: ExperimentLogDto = res.body;

      expect(body.log).toEqual(createExperimentNoteDto.logLine);
      expect(body.experimentUuid).toEqual(createExperimentNoteDto.experimentUuid);
      expect(body.username).toEqual(operatorUser1.username);
      expect(body).toHaveProperty('datetime');
    });

    it('Create note with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .send(getValidCreateExperimentNoteDto())
        .expect(403);
    });
  });
});
