import { User, UserDocument } from '../../src/repositories/schema/user.schema';
import { Role } from '../../src/common/enums/role.enum';
import { TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { Experiment, ExperimentDocument } from '../../src/repositories/schema/experiment.schema';
import { Config, ConfigDocument } from '../../src/repositories/schema/config.schema';
import { Sample, SampleDocument, SampleModel } from '../../src/repositories/schema/sample.schema';
import { ShellyRedDto } from '../../src/modules/sensors/dto/shelly-red.dto';
import { Sensor } from '../../src/common/entities/sensor.entity';
import { SensorType } from '../../src/common/enums/sensor-type.enum';
import { v4 as uuidv4 } from 'uuid';
import { getCurrentTimestamp } from '../../src/common/utils/datetime.helper';
import { Participant, ParticipantDocument } from '../../src/repositories/schema/participant.schema';
import {
  ACTIVE_EXPERIMENT_MODEL_NAME,
  ActiveExperiment,
  ActiveExperimentDocument,
} from '../../src/repositories/schema/active-experiment.schema';
import * as bcrypt from 'bcrypt';
import {
  MESSAGE_SCREEN_LOFT_MODEL_NAME,
  MessageScreenEntrance,
  MessageScreenEntranceDocument,
} from '../../src/repositories/schema/message-screen-entrance.schema';
import * as request from 'supertest';
import { INestApplication } from '@nestjs/common';
import { SensorDto } from '../../src/modules/sensors/dto/sensor.dto';
import { Room, RoomDocument } from '../../src/repositories/schema/room.schema';
import { Booking, BookingDocument } from '../../src/repositories/schema/booking.schema';
import { BookingType } from '../../src/common/enums/booking-type.enum';

const BCRYPT_ROUNDS = 10;

// User with Role.Admin

export const adminUserPassword = '123456';
export const adminUser: User = {
  firstname: 'Jean',
  lastname: 'Valjean',
  email: 'jean.valjean@email.ch',
  username: 'jean.valjean',
  password: bcrypt.hashSync(adminUserPassword, BCRYPT_ROUNDS),
  roles: [Role.Admin],
  createdAt: getCurrentTimestamp(),
  updatedAt: getCurrentTimestamp(),
};

// Users with Role.Operator
export const operatorUser1Password = '222111';
export const operatorUser2Password = '222222';
export const operatorUser3Password = '222333';
export const operatorUser1: User = {
  firstname: 'Pierre',
  lastname: 'Smith',
  email: 'pierre.smith@email.ch',
  username: 'pierre.smith',
  password: bcrypt.hashSync(operatorUser1Password, BCRYPT_ROUNDS),
  roles: [Role.Operator],
  createdAt: getCurrentTimestamp(),
  updatedAt: getCurrentTimestamp(),
};
export const operatorUser2: User = {
  firstname: 'Sandra',
  lastname: 'Veranda',
  email: 'sandra.veranda@email.ch',
  username: 'sandra.veranda',
  password: bcrypt.hashSync(operatorUser2Password, BCRYPT_ROUNDS),
  roles: [Role.Operator],
  createdAt: getCurrentTimestamp(),
  updatedAt: getCurrentTimestamp(),
};
export const operatorUser3: User = {
  firstname: 'Nicole',
  lastname: 'Malloc',
  email: 'nicole.malloc@email.ch',
  username: 'nicole.malloc',
  password: bcrypt.hashSync(operatorUser3Password, BCRYPT_ROUNDS),
  roles: [Role.Operator],
  createdAt: getCurrentTimestamp(),
  updatedAt: getCurrentTimestamp(),
};

// User with Role.Secretariat
export const secretariatUserPassword = '333333';
export const secretartiatUser: User = {
  firstname: 'Eva',
  lastname: 'Bean',
  email: 'eva.bean@email.ch',
  username: 'eva.bean',
  password: bcrypt.hashSync(secretariatUserPassword, BCRYPT_ROUNDS),
  roles: [Role.Secretariat],
  createdAt: getCurrentTimestamp(),
  updatedAt: getCurrentTimestamp(),
};

export const configUuid = uuidv4();
export const configName = 'SENSORS_CONFIG';
export const doorSensorIp = '192.168.50.1';
export const shellyRedSensorIp = '192.168.50.2';

export const participant1Uuid = uuidv4();
export const participant2Uuid = uuidv4();
export const participant3Uuid = uuidv4();

export const task1Uuid = uuidv4();
export const task1Name = 'T_1';
export const task2Uuid = uuidv4();
export const task2Name = 'T_2';

export const experiment1Name = 'EXPERIMENT_1';
export const experiment1Uuid = uuidv4();
export const experiment2Name = 'EXPERIMENT_2';
export const experiment2Uuid = uuidv4();

export const messageEntranceDoor = 'A recording session is scheduled from Monday Aug 30 to Wednesday Sep 2.';

export const room1Uuid = uuidv4();
export const room1: Room = {
  uuid: room1Uuid,
  name: 'Room 1',
};

export const booking1Name = 'EXP_1';
export const booking1Uuid = uuidv4();

export async function createTestData(app: INestApplication) {
  // The string to get the model is: 'token used as name in Mongoose.forFeature' + 'Model'
  const userModel = app.get<Model<UserDocument>>('UserModel');
  const configModel = app.get<Model<ConfigDocument>>('ConfigModel');
  const experimentModel = app.get<Model<ExperimentDocument>>('ExperimentModel');
  const sampleModel = app.get<Model<SampleDocument>>('SampleModel');
  const participantModel = app.get<Model<ParticipantDocument>>('ParticipantModel');
  const activeExperimentModel = app.get<Model<ActiveExperimentDocument>>(`${ACTIVE_EXPERIMENT_MODEL_NAME}Model`);
  const messageScreenEntranceModel = app.get<Model<MessageScreenEntranceDocument>>(
    `${MESSAGE_SCREEN_LOFT_MODEL_NAME}Model`,
  );
  const roomModel = app.get<Model<RoomDocument>>('RoomModel');
  const bookingModel = app.get<Model<BookingDocument>>('BookingModel');

  // Create users
  const users: User[] = [adminUser, operatorUser1, operatorUser2, operatorUser3, secretartiatUser];
  for (let i = 0; i < users.length; i++) {
    const userToSave = new userModel(users[i]);
    await userToSave.save();
  }

  // Create participants
  const participant1: Participant = {
    uuid: participant1Uuid,
    shortNote: 'P_1',
    createdAt: getCurrentTimestamp(),
    createdBy: await userModel.findOne({ username: operatorUser1.username }).exec(),
  };
  const participant2: Participant = {
    uuid: participant2Uuid,
    shortNote: 'P_2',
    createdAt: getCurrentTimestamp(),
    createdBy: await userModel.findOne({ username: operatorUser2.username }).exec(),
  };
  const participant3: Participant = {
    uuid: participant3Uuid,
    shortNote: 'P_3',
    createdAt: getCurrentTimestamp(),
    createdBy: await userModel.findOne({ username: operatorUser2.username }).exec(),
  };

  const participants: Participant[] = [participant1, participant2, participant3];
  for (let i = 0; i < participants.length; i++) {
    const participantToSave = new participantModel(participants[i]);
    await participantToSave.save();
  }

  // Create sensors config
  const sensorLocation = 'room';
  const shellyRedSensor: Sensor = {
    ip: shellyRedSensorIp,
    name: 'Shelly Red',
    location: sensorLocation,
    saveData: true,
  };
  const doorSensor: Sensor = {
    ip: doorSensorIp,
    name: 'Shelly Red',
    location: sensorLocation,
    saveData: true,
  };
  const sensorsPerTypeConfig: Record<SensorType, Sensor[]> = {
    [SensorType.Camera]: [],
    [SensorType.Door]: [doorSensor],
    [SensorType.Emfit]: [],
    [SensorType.Environment]: [],
    [SensorType.Lidar]: [],
    [SensorType.Luminance]: [],
    [SensorType.Mattress]: [],
    [SensorType.Microphone]: [],
    [SensorType.NightVisionCamera]: [],
    [SensorType.Radar]: [],
    [SensorType.Seismograph]: [],
    [SensorType.ShellyRed]: [shellyRedSensor],
    [SensorType.ShellyEm]: [],
    [SensorType.Dummy]: [],
    [SensorType.WaterFlow]: [],
  };

  const config: Config = {
    uuid: configUuid,
    name: configName,
    sensorsPerType: sensorsPerTypeConfig,
    createdAt: getCurrentTimestamp(),
    createdBy: await userModel.findOne({ username: adminUser.username }).exec(),
  };
  const configDocumentToSave = new configModel(config);
  await configDocumentToSave.save();

  // Create experiments
  /*
   - EXPERIMENT_1: owner: adminUser, operators: [operatorUser1], participants: [participant1]
   - EXPERIMENT_2: owner: adminUser, operators: [operatorUser2], participants: [participant2]
   */
  const experiment1: Experiment = {
    uuid: experiment1Uuid,
    name: experiment1Name,
    owner: await userModel.findOne({ username: adminUser.username }).exec(),
    operators: [await userModel.findOne({ username: operatorUser1.username }).exec()],
    participants: [await participantModel.findOne({ uuid: participant1Uuid }).exec()],
    config: await configModel.findOne({ name: configName }).exec(),
    tasks: [
      {
        uuid: task1Uuid,
        name: task1Name,
        description: 'Task 1 description',
      },
      {
        uuid: task2Uuid,
        name: task2Name,
        description: 'Task 2 description',
      },
    ],
    createdAt: getCurrentTimestamp(),
    startedAt: null,
    completed: false,
    completedAt: null,
  };
  const experiment2: Experiment = {
    uuid: experiment2Uuid,
    name: experiment2Name,
    owner: await userModel.findOne({ username: adminUser.username }).exec(),
    operators: [await userModel.findOne({ username: operatorUser2.username }).exec()],
    participants: [await participantModel.findOne({ uuid: participant2Uuid }).exec()],
    config: await configModel.findOne({ name: configName }).exec(),
    tasks: [
      {
        uuid: uuidv4(),
        name: 'T_1',
        description: 'Task 1 description',
      },
      {
        uuid: uuidv4(),
        name: 'T_2',
        description: 'Task 2 description',
      },
    ],
    createdAt: getCurrentTimestamp(),
    startedAt: null,
    completed: false,
    completedAt: null,
  };
  const experiments: Experiment[] = [experiment1, experiment2];

  for (let i = 0; i < experiments.length; i++) {
    const experimentDocumentToSave = new experimentModel(experiments[i]);
    await experimentDocumentToSave.save();
  }

  // Set first experiment as active for participant1
  const activeExperiment: ActiveExperiment = {
    experiment: await experimentModel.findOne({ uuid: experiment1.uuid }).exec(),
    participant: await participantModel.findOne({ uuid: participant1Uuid }).exec(),
    isRecording: true,
    taskUuid: task1Uuid,
    trialNbr: 1,
  };
  const activeExperimentDocumentToSave = new activeExperimentModel(activeExperiment);
  await activeExperimentDocumentToSave.save();

  // Set the message for the screen entrance
  const messageScreenEntrance: MessageScreenEntrance = {
    message: messageEntranceDoor,
    createdBy: await userModel.findOne({ username: adminUser.username }).exec(),
    updatedAt: getCurrentTimestamp(),
  };
  const messageScreenEntranceToSave = new messageScreenEntranceModel(messageScreenEntrance);
  await messageScreenEntranceToSave.save();

  // Create room
  const roomToSave = new roomModel(room1);
  const roomDocument = await roomToSave.save();

  // Create a booking for the room
  const start = new Date();
  const end = new Date();
  end.setHours(end.getHours() + 2);
  const booking: Booking = {
    uuid: booking1Uuid,
    room: roomDocument,
    name: booking1Name,
    start: start.getTime() / 1000,
    end: end.getTime() / 1000,
    bookingType: BookingType.EXPERIMENT,
    email: 'email-to-notify@test.ch',
  };
  const bookingToSave = new bookingModel(booking);
  await bookingToSave.save();
}

export async function createSampleShellyRed(module: TestingModule) {
  // The string to get the model is: 'token used as name in Mongoose.forFeature' + 'Model'
  const sampleShellyRedModel = module.get<Model<SampleDocument>>(`${SampleModel.ShellyRed}Model`);

  // Create sample
  const shellyRedDto: ShellyRedDto = {
    ip: shellyRedSensorIp,
    timestamp: getCurrentTimestamp(),
    power: 13,
    temp: 25,
    uptime: 100,
  };
  const sensorLocation = 'room';
  const sample: Sample = {
    ip: shellyRedDto.ip,
    timestamp: shellyRedDto.timestamp,
    location: sensorLocation,
    blob: Buffer.from(JSON.stringify(shellyRedDto)),
    participantUuid: participant1Uuid,
    taskUuid: task1Uuid,
    trialNbr: 1,
  };
  const sampleToSave = new sampleShellyRedModel(sample);
  await sampleToSave.save();
}

export async function createSample(app: INestApplication, sensorType: SensorType) {
  // The string to get the model is: 'token used as name in Mongoose.forFeature' + 'Model'
  const sampleCameraModel = app.get<Model<SampleDocument>>(`${SampleModel.Camera}Model`);
  const sampleDoorModel = app.get<Model<SampleDocument>>(`${SampleModel.Door}Model`);
  const sampleEmfitModel = app.get<Model<SampleDocument>>(`${SampleModel.Emfit}Model`);
  const sampleEnvironmentModel = app.get<Model<SampleDocument>>(`${SampleModel.Environment}Model`);
  const sampleLidarModel = app.get<Model<SampleDocument>>(`${SampleModel.Lidar}Model`);
  const sampleLuminanceModel = app.get<Model<SampleDocument>>(`${SampleModel.Luminance}Model`);
  const sampleMattressModel = app.get<Model<SampleDocument>>(`${SampleModel.Mattress}Model`);
  const sampleNightVisionCameraModel = app.get<Model<SampleDocument>>(`${SampleModel.NightVisionCamera}Model`);
  const sampleRadarModel = app.get<Model<SampleDocument>>(`${SampleModel.Radar}Model`);
  const sampleSeismographModel = app.get<Model<SampleDocument>>(`${SampleModel.Seismograph}Model`);
  const sampleShellyEmModel = app.get<Model<SampleDocument>>(`${SampleModel.ShellyEm}Model`);
  const sampleShellyRedModel = app.get<Model<SampleDocument>>(`${SampleModel.ShellyRed}Model`);
  const sampleWaterFlowModel = app.get<Model<SampleDocument>>(`${SampleModel.WaterFlow}Model`);

  let sensorDto: SensorDto = {
    ip: `192.168.47.1`,
    timestamp: getCurrentTimestamp(),
  };

  const sample: Sample = {
    ip: '192.168.1.1',
    timestamp: getCurrentTimestamp(),
    location: 'room',
    blob: Buffer.from(JSON.stringify(sensorDto)),
    participantUuid: participant1Uuid,
    taskUuid: task1Uuid,
    trialNbr: 1,
  };

  let sampleToSave;
  switch (sensorType) {
    case SensorType.Camera:
      sampleToSave = new sampleCameraModel(sample);
      break;
    case SensorType.Door:
      sampleToSave = new sampleDoorModel(sample);
      break;
    case SensorType.Emfit:
      sampleToSave = new sampleEmfitModel(sample);
      break;
    case SensorType.Environment:
      sampleToSave = new sampleEnvironmentModel(sample);
      break;
    case SensorType.Lidar:
      sampleToSave = new sampleLidarModel(sample);
      break;
    case SensorType.Luminance:
      sampleToSave = new sampleLuminanceModel(sample);
      break;
    case SensorType.Mattress:
      sampleToSave = new sampleMattressModel(sample);
      break;
    case SensorType.NightVisionCamera:
      sampleToSave = new sampleNightVisionCameraModel(sample);
      break;
    case SensorType.Radar:
      sampleToSave = new sampleRadarModel(sample);
      break;
    case SensorType.Seismograph:
      sampleToSave = new sampleSeismographModel(sample);
      break;
    case SensorType.ShellyEm:
      sampleToSave = new sampleShellyEmModel(sample);
      break;
    case SensorType.ShellyRed:
      sampleToSave = new sampleShellyRedModel(sample);
      break;
    case SensorType.WaterFlow:
      sampleToSave = new sampleWaterFlowModel(sample);
      break;
  }

  await sampleToSave.save();
}

export function getUserWithRole(role: Role) {
  let user = null;
  switch (role) {
    case Role.Admin:
      user = { username: adminUser.username, password: adminUserPassword };
      break;
    case Role.Operator:
      user = { username: operatorUser1.username, password: operatorUser1Password };
      break;
    case Role.Secretariat:
      user = { username: secretartiatUser.username, password: secretariatUserPassword };
      break;
    default:
      break;
  }
  return user;
}

export function getCredentialsForUser(username: string) {
  const credentials = {
    username,
    password: '',
  };
  switch (username) {
    case adminUser.username:
      credentials.password = adminUserPassword;
      break;
    case operatorUser1.username:
      credentials.password = operatorUser1Password;
      break;
    case operatorUser2.username:
      credentials.password = operatorUser2Password;
      break;
    case operatorUser3.username:
      credentials.password = operatorUser3Password;
      break;
    case secretartiatUser.username:
      credentials.password = secretariatUserPassword;
      break;
    default:
      break;
  }
  return credentials;
}

export async function getConfig(name: string, module: TestingModule): Promise<ConfigDocument> {
  const configModel = module.get<Model<ConfigDocument>>('ConfigModel');
  return await configModel.findOne({ name }).exec();
}

export async function getJWTForUserWithRole(app: INestApplication, role: Role) {
  const resAuth = await request(app.getHttpServer()).post('/auth/login').send(getUserWithRole(role)).expect(201);
  return resAuth.body.access_token;
}

export async function getJWTForUser(app: INestApplication, user: User) {
  const resAuth = await request(app.getHttpServer())
    .post('/auth/login')
    .send(getCredentialsForUser(user.username))
    .expect(201);
  return resAuth.body.access_token;
}
