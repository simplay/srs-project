import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import * as mongoose from 'mongoose';

let mongoDB: MongoMemoryServer;

export const rootMongooseTestModule = () =>
  MongooseModule.forRootAsync({
    useFactory: async () => {
      const mongo = await MongoMemoryServer.create();
      const mongoUri = mongo.getUri();
      // Autopopulate all schema (use mongoose-autopopulate on the connection)
      const options: MongooseModuleOptions = {
        connectionFactory: (connection) => {
          connection.plugin(require('mongoose-autopopulate'));
          return connection;
        },
      };
      return {
        uri: mongoUri,
        ...options,
      };
    },
  });

export const stopMongoDBInMemoryInstance = async () => {
  await mongoose.disconnect();
  if (mongoDB) {
    const res = await mongoDB.stop();
  }
};
