import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { booking1Name, booking1Uuid, createTestData, getJWTForUserWithRole, room1Uuid } from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Role } from '../src/common/enums/role.enum';
import { AuthModule } from '../src/modules/auth/auth.module';

import { BookingsModule } from '../src/modules/bookings/bookings.module';
import { RoomDto } from '../src/modules/bookings/dto/room.dto';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { CreateUpdateRoomDto } from '../src/modules/bookings/dto/create-update-room.dto';
import { Model } from 'mongoose';
import { Room, RoomDocument } from '../src/repositories/schema/room.schema';
import { BookingDto } from '../src/modules/bookings/dto/booking.dto';
import { CreateUpdateBookingDto } from '../src/modules/bookings/dto/create-update-booking.dto';
import { BookingType } from '../src/common/enums/booking-type.enum';
import { Booking, BookingDocument } from '../src/repositories/schema/booking.schema';

describe('BookingsController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        BookingsModule,
        AuthModule, // For authentication to get JWT
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
        ]),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/bookings/rooms (GET): List rooms', () => {
    const endpoint = '/bookings/rooms';

    it('List rooms without JWT returns 200 and all rooms (OK)', async () => {
      const res = await request(app.getHttpServer()).get(endpoint).expect(200);

      const rooms: RoomDto[] = res.body;

      expect(rooms.length).toEqual(1);
      expect(rooms[0].uuid).toEqual(room1Uuid);
    });
  });

  describe('/bookings/rooms (POST): Create room', () => {
    const endpoint = '/bookings/rooms';
    const newRoomName = 'Room 2';

    function getValidCreateUpdateRoomDto() {
      const createUpdateRoomDto: CreateUpdateRoomDto = {
        name: newRoomName,
      };
      return createUpdateRoomDto;
    }

    it('Create room with admin role returns 201 (CREATED)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const room: RoomDto = res.body;

      expect(room.name).toEqual(newRoomName);
    });

    it('Create room with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create room with secretariat role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/bookings/rooms/:uuid (PUT): Update room', () => {
    const endpoint = `/bookings/rooms/${room1Uuid}`;
    const newRoomName = 'Room 2';

    function getValidCreateUpdateRoomDto() {
      const createUpdateRoomDto: CreateUpdateRoomDto = {
        name: newRoomName,
      };
      return createUpdateRoomDto;
    }

    it('Update room with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const room: RoomDto = res.body;

      expect(room.name).toEqual(newRoomName);
    });

    it('Update room with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Update room with secretariat role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateRoomDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/bookings/rooms/:uuid (DELETE): Delete room', () => {
    const endpoint = `/bookings/rooms/${room1Uuid}`;

    it('Delete room with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      // Check that the room is deleted

      const roomModel = await app.get<Model<RoomDocument>>(`${Room.name}Model`);
      const rooms: RoomDocument[] = await roomModel.find();
      expect(rooms.length).toEqual(0);
    });

    it('Delete room with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete room with secretariat returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/bookings/:roomUuid (GET): Get bookings for room', () => {
    const endpoint = `/bookings/${room1Uuid}`;

    it('Get bookings for room without JWT returns 200 and all bookings (OK)', async () => {
      const res = await request(app.getHttpServer()).get(endpoint).expect(200);

      const bookings: BookingDto[] = res.body;

      expect(bookings.length).toEqual(1);
      expect(bookings[0].name).toEqual(booking1Name);
    });
  });

  describe('/bookings/:roomUuid (POST): Create booking for room', () => {
    const endpoint = `/bookings/${room1Uuid}`;
    const newBookingName = 'cleaning';

    function getValidCreateUpdateBookingDto() {
      const start = new Date();
      const end = new Date();
      end.setHours(end.getHours() + 1);

      const createUpdateBookingDto: CreateUpdateBookingDto = {
        name: newBookingName,
        bookingType: BookingType.CLEANING,
        start: start.getTime() / 1000,
        end: end.getTime() / 1000,
        email: 'email-to-notify@test.ch',
      };
      return createUpdateBookingDto;
    }

    it('Create booking for room with admin role returns 201 (CREATED)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const booking: BookingDto = res.body;

      expect(booking.name).toEqual(newBookingName);
    });

    it('Create booking for room with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create booking for room with secretariat role returns 201 (CREATED)', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(201);

      const booking: BookingDto = res.body;

      expect(booking.name).toEqual(newBookingName);
    });
  });

  describe('/bookings/:roomUuid/:bookingUuid (PUT): Update booking for room', () => {
    const endpoint = `/bookings/${room1Uuid}/${booking1Uuid}`;
    const newBookingName = 'EXP_UPDATED';

    function getValidCreateUpdateBookingDto() {
      const start = new Date();
      const end = new Date();
      end.setHours(end.getHours() + 1);

      const createUpdateBookingDto: CreateUpdateBookingDto = {
        name: newBookingName,
        bookingType: BookingType.CLEANING,
        start: start.getTime() / 1000,
        end: end.getTime() / 1000,
        email: 'email-to-notify@test.ch',
      };
      return createUpdateBookingDto;
    }

    it('Update booking for room with admin role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const booking: BookingDto = res.body;

      expect(booking.name).toEqual(newBookingName);
    });

    it('Update booking for room with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Update booking for room with secretariat role returns 200 (OK)', async () => {
      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidCreateUpdateBookingDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(200);

      const booking: BookingDto = res.body;

      expect(booking.name).toEqual(newBookingName);
    });
  });

  describe('/bookings/:bookingUuid (DELETE): Delete booking (for room)', () => {
    const endpoint = `/bookings/${booking1Uuid}`;

    it('Delete booking with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      // Check that the booking is deleted
      const bookingModel = await app.get<Model<BookingDocument>>(`${Booking.name}Model`);
      const bookings: BookingDocument[] = await bookingModel.find();
      expect(bookings.length).toEqual(0);
    });

    it('Delete booking with operator role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete booking with secretariat role returns 403 (FORBIDDEN)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(200);

      // Check that the booking is deleted
      const bookingModel = await app.get<Model<BookingDocument>>(`${Booking.name}Model`);
      const bookings: BookingDocument[] = await bookingModel.find();
      expect(bookings.length).toEqual(0);
    });
  });
});
