import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import {
  adminUser,
  createTestData,
  experiment1Name,
  getJWTForUser,
  getJWTForUserWithRole,
  operatorUser1,
  operatorUser3,
  participant1Uuid,
  participant3Uuid,
} from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { Role } from '../src/common/enums/role.enum';
import { AuthModule } from '../src/modules/auth/auth.module';
import { ParticipantsModule } from '../src/modules/participants/participants.module';
import { ParticipantDto } from '../src/modules/participants/dto/participant.dto';
import { v4 as uuidv4 } from 'uuid';
import { CreateParticipantDto } from '../src/modules/participants/dto/create-participant.dto';
import { UpdateParticipantDto } from '../src/modules/participants/dto/update-participant.dto';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('ParticipantsController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        ParticipantsModule,
        AuthModule, // For authentication to get JWT
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/participants (GET): List participants', () => {
    const endpoint = '/participants';

    it('List participants with admin role returns 200 and all participants (OK)', async () => {
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const participants: ParticipantDto[] = res.body;

      // Should return all the participants
      expect(participants.length).toEqual(3);
    });

    it('List participants of OPERATOR_1 returns 200 but only participants from the experiments he is included (Forbidden)', async () => {
      // OPERATOR_1 is only part of EXPERIMENT_1 for which a single participant is defined (See test-utils.ts for the definition of the experiments)
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser1)}`)
        .expect(200);

      const participants: ParticipantDto[] = res.body;
      expect(participants.length).toEqual(1);
      const participant = participants[0];
      expect(participant.uuid).toEqual(participant1Uuid);
    });

    it('List participants of OPERATOR_3 returns 200 but only participants from the experiments he is included (Forbidden)', async () => {
      // OPERATOR_1 is not part any experiment (See test-utils.ts for the definition of the experiments)
      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUser(app, operatorUser3)}`)
        .expect(200);

      const participants: ParticipantDto[] = res.body;
      expect(participants.length).toEqual(0);
    });

    it('List participants with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/participants (POST): Create new participant', () => {
    const endpoint = '/participants';

    function getValidCreateParticipantDto() {
      const createParticipantDto: CreateParticipantDto = {
        uuid: uuidv4(),
        shortNote: 'P_N',
      };
      return createParticipantDto;
    }
    it('Create new participant with admin role returns 201 (OK)', async () => {
      const createParticipantDto = getValidCreateParticipantDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createParticipantDto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(201);

      const participantDto: ParticipantDto = res.body;

      expect(participantDto.uuid).toEqual(createParticipantDto.uuid);
      expect(participantDto.shortNote).toEqual(createParticipantDto.shortNote);
      expect(participantDto.createdAt).not.toBeNull();
      expect(participantDto.createdBy.username).toEqual(adminUser.username);
    });

    it('Create new participant with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateParticipantDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create new participant with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateParticipantDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Create new participant with admin role with UUID already existing returns 409 (CONFLICT)', async () => {
      const createParticipant1Dto = getValidCreateParticipantDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createParticipant1Dto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(201);

      const createdParticipant: ParticipantDto = res.body;

      const createParticipant2Dto: CreateParticipantDto = {
        uuid: createdParticipant.uuid,
        shortNote: 'P_2',
      };
      const res2 = await request(app.getHttpServer())
        .post(endpoint)
        .send(createParticipant2Dto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(409);

      expect(res2.body.message).toEqual(`Participant with uuid '${createdParticipant.uuid}' exists already`);
    });

    it('Create new participant with admin role with short note already existing returns 409 (CONFLICT)', async () => {
      const createParticipant1Dto = getValidCreateParticipantDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createParticipant1Dto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(201);

      const createdParticipant: ParticipantDto = res.body;

      const createParticipant2Dto: CreateParticipantDto = {
        uuid: uuidv4(),
        shortNote: createdParticipant.shortNote,
      };
      const res2 = await request(app.getHttpServer())
        .post(endpoint)
        .send(createParticipant2Dto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(409);

      expect(res2.body.message).toEqual(
        `Participant with short note '${createdParticipant.shortNote}' exists already. Please make it unique.`,
      );
    });
  });

  describe('/participants/:uuid (PUT): Update participant', () => {
    const endpoint = `/participants/${participant1Uuid}`;

    function getValidUpdateParticipantDto() {
      const updateParticipantDto: UpdateParticipantDto = {
        shortNote: 'New note',
      };
      return updateParticipantDto;
    }
    it('Update participant with admin role returns 200 (OK)', async () => {
      const updateParticipantDto = getValidUpdateParticipantDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(updateParticipantDto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(200);

      const participantDto: ParticipantDto = res.body;

      expect(participantDto.shortNote).toEqual(updateParticipantDto.shortNote);
      expect(participantDto.createdAt).not.toBeNull();
    });

    it('Update participant with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidUpdateParticipantDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Update participant with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidUpdateParticipantDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Update participant with admin role with short note already existing returns 409 (CONFLICT)', async () => {
      const updateParticipantDto = getValidUpdateParticipantDto();

      // P_2 exists already in the DB
      const shortNote = 'P_2';
      updateParticipantDto.shortNote = shortNote;

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(updateParticipantDto)
        .set('Authorization', `Bearer ${await getJWTForUser(app, adminUser)}`)
        .expect(409);

      expect(res.body.message).toEqual(
        `Participant with short note '${shortNote}' exists already. Please make it unique.`,
      );
    });
  });

  describe('/participants/:uuid (DELETE): Delete participant', () => {
    const endpoint = `/participants/${participant1Uuid}`;

    it('Delete participant being part of an EXPERIMENT_2 with admin role returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .delete(`/participants/${participant1Uuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `The participant is part of experiment(s): ${experiment1Name}. Please remove the participant from experiment(s) before deleting the participant`,
      );
    });

    it('Delete participant not being part of an experiment with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .delete(`/participants/${participant3Uuid}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const res = await request(app.getHttpServer())
        .get('/participants')
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const participantsDto: ParticipantDto[] = res.body;

      const uuids = participantsDto.map((participantDto) => participantDto.uuid);
      expect(uuids).not.toContain(participant3Uuid);
    });

    it('Delete participant with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete participant with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });
});
