import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleModel, SampleSchema } from '../src/repositories/schema/sample.schema';
import { SamplesModule } from '../src/modules/samples/samples.module';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { UsersRepository } from '../src/repositories/users.repository';
import { AuthModule } from '../src/modules/auth/auth.module';
import { createSampleShellyRed, createTestData, getJWTForUserWithRole } from './utils/test-utils';
import { Role } from '../src/common/enums/role.enum';
import { SamplesCollectionStatsDto } from '../src/modules/samples/dto/samples-collection-stats.dto';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { Experiment, ExperimentSchema } from '../src/repositories/schema/experiment.schema';
import {
  ACTIVE_EXPERIMENT_MODEL_NAME,
  ActiveExperiment,
  ActiveExperimentDocument,
  ActiveExperimentSchema,
} from '../src/repositories/schema/active-experiment.schema';
import { CreateExportJobDto } from '../src/modules/samples/dto/create-export-job.dto';
import { Model } from 'mongoose';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

const fs = require('fs');

// Validation tests are done here

describe('SamplesController (e2e)', () => {
  let app: INestApplication;

  let moduleFixture: TestingModule;
  beforeEach(async () => {
    moduleFixture = await Test.createTestingModule({
      imports: [
        SamplesModule,
        AuthModule, // For authentication to get JWT
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Experiment.name, schema: ExperimentSchema },
          { name: ActiveExperiment.name, schema: ActiveExperimentSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());

    await createTestData(app);

    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/samples/collections-stats (GET): Get the stats of the samples collections', () => {
    const endpoint = '/samples/collections-stats';

    it('Get samples collection stats without JWT returns 401 (unauthorized)', async () => {
      await request(app.getHttpServer()).get(endpoint).expect(401);
    });

    it('Get samples collection stats with admin role returns 200 (OK)', async () => {
      await createSampleShellyRed(moduleFixture);

      const res = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const statsCollections: SamplesCollectionStatsDto[] = res.body;

      expect(statsCollections.length).toEqual(1);
      const shellyRedSamplesCollectionStats = statsCollections[0];
      expect(shellyRedSamplesCollectionStats.collectionName).toEqual(`${SampleModel.ShellyRed.toLowerCase()}s`);
      expect(shellyRedSamplesCollectionStats.numberOfSamples).toEqual(1);
    });

    it('Get samples collection stats with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Get samples collection stats with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/samples/drop-samples-collections (DELETE): Drop the samples collections', () => {
    const endpoint = '/samples/drop-samples-collections';

    it('Drop samples collections without JWT returns 401 (unauthorized)', async () => {
      await request(app.getHttpServer()).delete(endpoint).expect(401);
    });

    it('Drop samples collections with admin role returns 200', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('Drop samples collections with operator role returns 403', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Drop samples collections with secretariat role returns 403', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/samples/export-job (POST): Create an export job', () => {
    const endpoint = '/samples/export-job';

    function getCreateExportJobDto(): CreateExportJobDto {
      const createExportJobDto: CreateExportJobDto = {
        date: new Date(),
      };
      return createExportJobDto;
    }

    async function stopRecordingActiveExperiment() {
      // The test data created by test-utils defines EXP_1 as active experiment, being currently recording
      // => stop recording for this test
      const activeExperimentModel = await app.get<Model<ActiveExperimentDocument>>(
        `${ACTIVE_EXPERIMENT_MODEL_NAME}Model`,
      );
      const activeExperiment: ActiveExperimentDocument = await activeExperimentModel.findOne().exec();
      expect(activeExperiment).not.toBeNull();
      activeExperiment.isRecording = false;
      await activeExperiment.save();
    }

    it('Create export job without JWT returns 401 (unauthorized)', async () => {
      await request(app.getHttpServer()).post(endpoint).send(getCreateExportJobDto()).expect(401);
    });

    it('Create export job with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create export job with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Create export job with admin role with active experiment being recording returns 403 (Forbidden)', async (done) => {
      // The test data created by test-utils defines EXP_1 as active experiment, being currently recording
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual('Please stop recording before exporting the data');

      done();
    });

    it('Create export job with admin role with active experiment not under recording without any data recorded returns 403 (Forbidden)', async (done) => {
      await stopRecordingActiveExperiment();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      const dateExportString = new Date().toISOString().slice(0, 10);

      expect(res.body.message).toEqual(
        `No sample found in samples collections for date ${dateExportString}. Please record data first.`,
      );

      done();
    });

    /*
    it('Create export job with admin role with active experiment not under recording returns 201 (Created)', async (done) => {
      // Give the test more time to complete (10 s)
      jest.setTimeout(1000 * 10);

      // Save a door sample
      await createSample(app, SensorType.Door);

      await stopRecordingActiveExperiment();

      // Create the export job
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const exportJob: ExportJobDto = res.body;
      const exportJobUuid = exportJob.uuid;
      expect(exportJob.experiment.name).toEqual(experiment1Name);
      expect(exportJob.status).toEqual(ExportJobStatus.ON_GOING);
      expect(exportJob.completedAt).toBeNull();
      expect(exportJob.duration).toEqual(0);

      // Fetch status of export job
      let isJobFinished = false;
      const maxTries = 5;
      let i = 0;

      let exportStatus;
      while (!isJobFinished) {
        i = i + 1;
        if (i >= maxTries) {
          break;
        }

        // Wait 1s before looking after the new export job status
        await new Promise((r) => setTimeout(r, 1000));

        const res2 = await request(app.getHttpServer())
          .get(`/samples/export-job/${exportJobUuid}`)
          .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
          .expect(200);

        const exportJobUpdated: ExportJobDto = res2.body;

        exportStatus = exportJobUpdated.status;

        if (exportStatus !== ExportJobStatus.ON_GOING) {
          isJobFinished = true;
        }
      }
      expect(exportStatus).toEqual(ExportJobStatus.SUCCEEDED);
      done();
    });

     */

    it('Create export job with admin role with no experiment set as active return 403 (Forbidden)', async (done) => {
      // The test data created by test-utils defines EXP_1 as active experiment, being currently recording
      // => remove the entry for the active experiment model
      const activeExperimentModel = await app.get<Model<ActiveExperimentDocument>>(
        `${ACTIVE_EXPERIMENT_MODEL_NAME}Model`,
      );
      await activeExperimentModel.collection.drop();

      // Create the export job
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual('No active experiment found. Cannot derive experiment uuid');

      done();
    });

    /*
    it('Create export job with admin role with active export job already existing returns 403 (Forbidden)', async (done) => {
      await stopRecordingActiveExperiment();

      // Save door sample
      await createSample(app, SensorType.Door);

      // Create the export job
      const res1 = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const exportJob: ExportJobDto = res1.body;
      expect(exportJob.status).toEqual(ExportJobStatus.ON_GOING);

      // Previous call will create a folder, simulate that the folder does not exist
      jest.spyOn(fs, 'existsSync').mockImplementation(() => {
        return false;
      });


      // Create a second export job
      const res2 = await request(app.getHttpServer())
        .post(endpoint)
        .send(getCreateExportJobDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res2.body.message).toEqual('An active export job already exists');

      done();
    });

     */
  });
});
