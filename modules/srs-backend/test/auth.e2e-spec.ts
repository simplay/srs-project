import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AuthModule } from '../src/modules/auth/auth.module';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { UsersRepository } from '../src/repositories/users.repository';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { adminUser, adminUserPassword, createTestData, operatorUser1, operatorUser1Password } from './utils/test-utils';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { Experiment, ExperimentSchema } from '../src/repositories/schema/experiment.schema';
import { ActiveExperiment, ActiveExperimentSchema } from '../src/repositories/schema/active-experiment.schema';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

// Validation tests are done here

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        AuthModule,
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Experiment.name, schema: ExperimentSchema },
          { name: ActiveExperiment.name, schema: ActiveExperimentSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/auth/login (POST): Authenticate user', () => {
    const endpoint = `/auth/login`;
    it('Authenticate with fake user returns 401', () => {
      const res = request(app.getHttpServer())
        .post(endpoint)
        .send({ username: 'not_found', password: 'wrong_password' })
        .expect(401);
    });

    it('Authenticate with unauthorized user returns 401', () => {
      const res = request(app.getHttpServer())
        .post(endpoint)
        .send({ username: adminUser.username, password: 'wrong_password' })
        .expect(401);
    });

    it('Authenticate with authorized user returns JWT and 200', async () => {
      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send({ username: adminUser.username, password: adminUserPassword })
        .expect(201);

      const resData = res.body;
      expect(resData).toHaveProperty('access_token');
    });
  });

  describe('/auth/profile (GET): Get profile', () => {
    const endpoint = '/auth/profile';
    it('Get profile without jwt returns 401', () => {
      request(app.getHttpServer()).get(endpoint).expect(401);
    });

    it('Get profile with invalid token returns 401', () => {
      request(app.getHttpServer()).get(endpoint).set('Authorization', 'Bearer fake_jwt').expect(401);
    });

    it('Get profile with valid token returns 200', async () => {
      const user = { username: operatorUser1.username, password: operatorUser1Password };
      const resAuth = await request(app.getHttpServer()).post('/auth/login').send(user).expect(201);

      const jwt = resAuth.body.access_token;
      const resGetProfile = await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      const userReturned = resGetProfile.body;

      expect(userReturned).toHaveProperty('username');
      expect(userReturned.username).toEqual(operatorUser1.username);
    });
  });
});
