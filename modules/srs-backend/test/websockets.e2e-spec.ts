import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { SensorsModule } from '../src/modules/sensors/sensors.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { UsersRepository } from '../src/repositories/users.repository';
import { createTestData, getJWTForUser, operatorUser1 } from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { io, Socket } from 'socket.io-client';
import {
  NAMESPACE_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS,
  NAMESPACE_WEBSOCKET_CAMERAS,
  NAMESPACE_WEBSOCKET_SENSORS,
} from '../src/common/utils/constants';
import { AuthenticatedSocketIoAdapter } from '../src/authenticated-socket-io.adapter';
import { LogBookRepository } from '../src/repositories/log-book.repository';
import {
  EXPERIMENT_LOG_MODEL_NAME,
  ExperimentLog,
  ExperimentLogDocument,
  ExperimentLogSchema,
} from '../src/repositories/schema/experiment-log.schema';
import { AuthModule } from '../src/modules/auth/auth.module';
import { Model } from 'mongoose';
import { ExperimentsModule } from '../src/modules/experiments/experiments.module';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('Websockets testing (e2e)', () => {
  let app: INestApplication;

  let socket = null as null | Socket;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        SensorsModule, // The sensors and cameras gateways are in the sensors module
        ExperimentsModule, // The active-experiment-status gateway runs in the experiments module
        AuthModule, // Required for JWT authentication
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: EXPERIMENT_LOG_MODEL_NAME, schema: ExperimentLogSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository, LogBookRepository],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    app.useGlobalPipes(new ValidationPipe());
    app.useWebSocketAdapter(new AuthenticatedSocketIoAdapter(app));
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    if (socket !== null) {
      socket.disconnect();
    }
    await app.close();
  });

  describe('/sensors (Websocket): Sensors websocket', () => {
    // Sensors websocket runs on port 4222 (for which authentication is required)
    const socketUrl = `http://localhost:4222/${NAMESPACE_WEBSOCKET_SENSORS}`;

    it('Connect to sensors websocket without JWT fails to connect', async (done) => {
      // Initialize a socket without authorization header
      const socket = io(socketUrl, {
        autoConnect: false,
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to sensors websocket with fake JWT fails to connect', async (done) => {
      // Initialize a socket with a fake token in authorization header
      const socket = io(socketUrl, {
        autoConnect: false,
        extraHeaders: {
          Authorization: `Bearer fake`,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to sensors websocket with a valid JWT but without query param fails to connect', async (done) => {
      // Initialize a token with a valid token in authorization header but without the required query param "name"
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
        extraHeaders: {
          Authorization: `Bearer ${await getJWTForUser(app, operatorUser1)}`,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to sensors websocket with a valid JWT an query param succeeds to connect and access is not logged in LogBook', async (done) => {
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
        extraHeaders: {
          Authorization: `Bearer ${await getJWTForUser(app, operatorUser1)}`,
        },
        query: {
          name: NAMESPACE_WEBSOCKET_SENSORS,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            fail('It should succeed to connect');
            resolve();
          })
          .on('connect', () => {
            resolve();
          }),
      );

      // It should not have logged any message in the logbook
      const experimentLogModel = await app.get<Model<ExperimentLogDocument>>(`${EXPERIMENT_LOG_MODEL_NAME}Model`);
      const logs: ExperimentLog[] = await experimentLogModel.find().exec();
      expect(logs.length).toEqual(1); // it should only contains the message of successful authentication (when we got the JWT)
      expect(logs[0].log).toEqual('Successfully authenticated => generate JWT');

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });
  });

  describe('/cameras (Websocket): Cameras websocket', () => {
    // Cameras websocket runs on port 4222 (for which authentication is required)
    const socketUrl = `http://localhost:4222/${NAMESPACE_WEBSOCKET_CAMERAS}`;

    it('Connect to cameras websocket without JWT fails to connect', async (done) => {
      // Initialize a socket without authorization header
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to cameras websocket with fake JWT fails to connect', async (done) => {
      // Initialize a socket with a fake token in authorization header
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
        extraHeaders: {
          Authorization: `Bearer fake`,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to cameras websocket with a valid JWT but without query param fails to connect', async (done) => {
      // Initialize a token with a valid token in authorization header but without the required query param "name"
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
        extraHeaders: {
          Authorization: `Bearer ${await getJWTForUser(app, operatorUser1)}`,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            resolve();
          })
          .on('connect', () => {
            fail('It should not have connected');
            resolve();
          }),
      );

      // Without explicitly closing the alarm, the test doesn't close properly ...
      await socket.close();
      done();
    });

    it('Connect to cameras websocket with a valid JWT an query param succeeds to connect and access is logged in LogBook', async (done) => {
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
        extraHeaders: {
          Authorization: `Bearer ${await getJWTForUser(app, operatorUser1)}`,
        },
        query: {
          name: NAMESPACE_WEBSOCKET_CAMERAS,
        },
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            fail('It should succeed to connect');
            resolve();
          })
          .on('connect', () => {
            resolve();
          }),
      );

      // It should have logged the access to the cameras websocket
      const experimentLogModel = await app.get<Model<ExperimentLogDocument>>(`${EXPERIMENT_LOG_MODEL_NAME}Model`);
      const logs: ExperimentLog[] = await experimentLogModel.find().exec();
      expect(logs.length).toEqual(2); // it should contains the message of successful authentication (when we got the JWT) and the message of access to the cameras
      expect(logs[0].log).toEqual('Successfully authenticated => generate JWT');
      expect(logs[1].log).toEqual('Accessed cameras preview');

      await socket.close();
      done();
    });
  });

  describe('/active-experiment-status (Websocket): Active experiment status websocket', () => {
    // Active experiment status websocket runs on port 4111 (which does not require authorization)
    const socketUrl = `http://localhost:4111/${NAMESPACE_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS}`;

    it('Connect to active experiment websocket without JWT succeeds to connect', async (done) => {
      // Initialize a socket without authorization header
      const socket = io(socketUrl, {
        autoConnect: false,
        forceNew: true,
      });

      await new Promise<void>((resolve) =>
        socket
          .connect()
          .on('connect_error', (err: any) => {
            fail('It should have succeeded to connect');
            resolve();
          })
          .on('connect', () => {
            resolve();
          }),
      );

      await socket.close();
      done();
    });
  });
});
