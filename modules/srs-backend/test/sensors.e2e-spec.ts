import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { SensorsModule } from '../src/modules/sensors/sensors.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import { JwtStrategy } from '../src/modules/auth/strategies/jwt.strategy';
import { UsersRepository } from '../src/repositories/users.repository';
import { DoorDto } from '../src/modules/sensors/dto/door.dto';
import { createTestData, doorSensorIp, shellyRedSensorIp } from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { getCurrentTimestamp } from '../src/common/utils/datetime.helper';
import { ShellyRedDto } from '../src/modules/sensors/dto/shelly-red.dto';
import { DataReceivedResponse } from '../src/common/interfaces/data-received-response.interface';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('SensorsController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        SensorsModule,
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
      providers: [JwtStrategy, UsersRepository],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/sensors/shelly-red (POST): Shelly red sensors', () => {
    const endpoint = '/sensors/shelly-red';

    function getValidShellyRedDto(): ShellyRedDto {
      const dto: ShellyRedDto = {
        timestamp: getCurrentTimestamp(),
        ip: shellyRedSensorIp,
        temp: 39.1,
        power: 13,
        uptime: 100,
      };
      return dto;
    }

    it('/sensors/shelly-red (POST) Create sample with valid dto returns 201', async () => {
      const res = await request(app.getHttpServer()).post(endpoint).send([getValidShellyRedDto()]).expect(201);

      const resData = res.body;
      expect(resData).toHaveProperty('message');
      expect(resData.message).toContain('Data received');
      expect(resData).toHaveProperty('numberSamplesSaved');
      expect(resData.numberSamplesSaved).toEqual(1);
    });

    it('/sensors/shelly-red (POST) Create sample with invalid dto returns 400', async () => {
      // Simulate dto with invalid IP
      const { ip, ...otherProperties } = getValidShellyRedDto();
      const invalidShellyRedDTO = {
        // Make ip invalid
        ip: '',
        ...otherProperties,
      };
      const res = await request(app.getHttpServer()).post(endpoint).send([invalidShellyRedDTO]).expect(400);

      const resData = res.body;
      expect(resData).toHaveProperty('message');
      expect(resData.message).toContain('ip must be an ip address');
    });

    it('/sensors/shelly-red (POST) Create sample with valid sample with sensor IP not in the list returns 201 but does not save sample', async () => {
      // Simulate dto with sensor IP not in the sensors list
      const { ip, ...otherProperties } = getValidShellyRedDto();
      const shellyRedDTO = {
        ip: '192.168.99.99',
        ...otherProperties,
      };
      const res = await request(app.getHttpServer()).post(endpoint).send([shellyRedDTO]).expect(201);

      const resData: DataReceivedResponse = res.body;
      expect(resData).toHaveProperty('message');
      expect(resData.numberSamplesSaved).toEqual(0);
    });
  });

  describe('/sensors/door (POST): Door sensors', () => {
    const endpoint = '/sensors/door';

    function getValidDoorDto(doorOpen: boolean): DoorDto {
      const dto: DoorDto = {
        timestamp: getCurrentTimestamp(),
        ip: doorSensorIp,
        doorOpen,
      };
      return dto;
    }

    it('/sensors/door (POST) with not recent timestamp returns 400', async () => {
      const dto: DoorDto = {
        timestamp: 100,
        ip: doorSensorIp,
        doorOpen: true,
      };

      const res = await request(app.getHttpServer()).post(endpoint).send([dto]).expect(400);

      const resBody = res.body;
      expect(resBody).toHaveProperty('message');
      expect(resBody.message).toContain('timestamp must not be less than 1622505600');
    });

    it('/sensors/door (POST) only saves samples for state changes (door open/door close)', async () => {
      // Send a first sample with status door closed
      const res1 = await request(app.getHttpServer())
        .post(endpoint)
        .send([getValidDoorDto(false)])
        .expect(201);

      // Sample should have been saved
      const res1Body: DataReceivedResponse = res1.body;
      expect(res1Body.numberSamplesSaved).toEqual(1);

      // Send a second sample with status door closed
      const res2 = await request(app.getHttpServer())
        .post(endpoint)
        .send([getValidDoorDto(false)])
        .expect(201);

      // Sample should not have been saved (as same state for the door)
      const res2Body: DataReceivedResponse = res2.body;
      expect(res2Body.numberSamplesSaved).toEqual(0);

      // Send a third sample with status door open
      const res3 = await request(app.getHttpServer())
        .post(endpoint)
        .send([getValidDoorDto(true)])
        .expect(201);

      // Sample should not have been saved (as same state for the door)
      const res3Body: DataReceivedResponse = res3.body;
      expect(res3Body.numberSamplesSaved).toEqual(1);
    });
  });

  it('/sensors/shelly-red (POST) with an empty list returns 400', () => {
    return request(app.getHttpServer()).post('/sensors/shelly-red').send([]).expect(400);
  });
});
