import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Sample, SampleSchema } from '../src/repositories/schema/sample.schema';
import { rootMongooseTestModule, stopMongoDBInMemoryInstance } from './utils/mongoose-test-module';
import {
  createTestData,
  experiment2Name,
  getJWTForUserWithRole,
  operatorUser2,
  operatorUser3,
} from './utils/test-utils';
import { User, UserSchema } from '../src/repositories/schema/user.schema';
import { Config, ConfigSchema } from '../src/repositories/schema/config.schema';
import { Participant, ParticipantSchema } from '../src/repositories/schema/participant.schema';
import { UsersModule } from '../src/modules/users/users.module';
import { Role } from '../src/common/enums/role.enum';
import { AuthModule } from '../src/modules/auth/auth.module';
import { CreateUserDto } from '../src/modules/users/dto/create-user.dto';
import { UserDto } from '../src/modules/users/dto/user.dto';
import { UpdateUserDto } from '../src/modules/users/dto/update-user.dto';
import { Room, RoomSchema } from '../src/repositories/schema/room.schema';
import { Booking, BookingSchema } from '../src/repositories/schema/booking.schema';

describe('UsersController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        UsersModule,
        AuthModule, // For authentication to get JWT
        rootMongooseTestModule(),

        // Documents needed by test-utils to create test data
        MongooseModule.forFeature([
          { name: User.name, schema: UserSchema },
          { name: Config.name, schema: ConfigSchema },
          { name: Sample.name, schema: SampleSchema },
          { name: Participant.name, schema: ParticipantSchema },
          { name: Room.name, schema: RoomSchema },
          { name: Booking.name, schema: BookingSchema },
        ]),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    await createTestData(app);

    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  afterEach(async () => {
    await stopMongoDBInMemoryInstance();

    await app.close();
  });

  describe('/users (GET): List users', () => {
    const endpoint = '/users';

    it('List users with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);
    });

    it('List users with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('List users with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .get(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/users (POST): Create new user', () => {
    const endpoint = '/users';

    function getValidCreateUserDto() {
      const createUserDto: CreateUserDto = {
        firstname: 'John',
        lastname: 'Carlos',
        username: 'john.carlos',
        password: '123456',
        email: 'john.carlos@email.ch',
        roles: [Role.Operator],
        createdAt: null,
        updatedAt: null,
      };
      return createUserDto;
    }
    it('Create new user with admin role returns 200 (OK)', async () => {
      const createUserDto = getValidCreateUserDto();

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createUserDto)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(201);

      const createdUser: UserDto = res.body;
      // Hashed password must not be returned
      expect(res.body).not.toHaveProperty('password');
      expect(createdUser.firstname).toEqual(createUserDto.firstname);
      expect(createdUser.lastname).toEqual(createUserDto.lastname);
      expect(createdUser.username).toEqual(createUserDto.username);
      expect(createdUser.email).toEqual(createUserDto.email);
      expect(createdUser.roles).toContain(Role.Operator);
      expect(createdUser.roles).not.toContain(Role.Admin);
      expect(createdUser.createdAt).not.toBeNull();
      expect(createdUser.updatedAt).not.toBeNull();
    });

    it('Create new user with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUserDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Create new user with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .post(endpoint)
        .send(getValidCreateUserDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });

    it('Create new user with comma in username returns 400 (Bad request)', async () => {
      const createUserDto = getValidCreateUserDto();

      createUserDto.username = 'john,carlos';

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createUserDto)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(400);

      expect(res.body.message[0]).toEqual('The username must not contains commas or whitespace');
    });

    it('Create new user with whitespace in username returns 400 (Bad request)', async () => {
      const createUserDto = getValidCreateUserDto();

      createUserDto.username = 'john carlos';

      const res = await request(app.getHttpServer())
        .post(endpoint)
        .send(createUserDto)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(400);

      expect(res.body.message[0]).toEqual('The username must not contains commas or whitespace');
    });
  });

  describe('/users/:username (PUT): Update user', () => {
    const endpoint = `/users/${operatorUser2.username}`;

    function getValidUpdateUserDto() {
      const updateUserDto: UpdateUserDto = {
        firstname: 'Sabine',
        lastname: 'Verolla',
        username: 'Sabine.Verolla',
        password: '123456',
        email: 'sabine.verolla@email.ch',
        roles: [Role.Operator],
        createdAt: null,
        updatedAt: null,
      };
      return updateUserDto;
    }
    it('Update user with admin role returns 200 (OK)', async () => {
      const updateUserDto = getValidUpdateUserDto();

      const res = await request(app.getHttpServer())
        .put(endpoint)
        .send(updateUserDto)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const updatedUser: UserDto = res.body;

      // Hashed password must not be returned
      expect(res.body).not.toHaveProperty('password');
      expect(updatedUser.firstname).toEqual(updateUserDto.firstname);
      expect(updatedUser.lastname).toEqual(updateUserDto.lastname);
      expect(updatedUser.username).toEqual(updateUserDto.username);
      expect(updatedUser.email).toEqual(updateUserDto.email);
      expect(updatedUser.roles).toContain(Role.Operator);
      expect(updatedUser.roles).not.toContain(Role.Admin);
      expect(updatedUser.createdAt).not.toBeNull();
      expect(updatedUser.updatedAt).not.toBeNull();
    });

    it('Update user with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidUpdateUserDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Update user with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .put(endpoint)
        .send(getValidUpdateUserDto())
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });

  describe('/users/:username (DELETE): Delete user', () => {
    const endpoint = `/users/${operatorUser2.username}`;

    it('Delete OPERATOR_2 being part of an EXPERIMENT_2 with admin role returns 403 (Forbidden)', async () => {
      const res = await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(403);

      expect(res.body.message).toEqual(
        `User "${operatorUser2.username}" is part of (owner or operator) of experiment(s): ${experiment2Name}. Please remove the user from experiment(s) before deleting the user`,
      );
    });

    it('Delete OPERATOR_3 not being part of an experiment with admin role returns 200 (OK)', async () => {
      await request(app.getHttpServer())
        .delete(`/users/${operatorUser3.username}`)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const res = await request(app.getHttpServer())
        .get('/users')
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Admin)}`)
        .expect(200);

      const usersDto: UserDto[] = res.body;

      const usernames = usersDto.map((userDto) => userDto.username);
      expect(usernames).not.toContain(operatorUser3.username);
    });

    it('Delete user with operator role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Operator)}`)
        .expect(403);
    });

    it('Delete user with secretariat role returns 403 (Forbidden)', async () => {
      await request(app.getHttpServer())
        .delete(endpoint)
        .set('Authorization', `Bearer ${await getJWTForUserWithRole(app, Role.Secretariat)}`)
        .expect(403);
    });
  });
});
