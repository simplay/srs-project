#!/bin/sh

# Stop on failure
set -e

# Load env variables
source .env

image_name="sensor_monitoring_service"
container_name=$image_name

docker rm -f $container_name

docker build -t $image_name .

docker run \
--env SMS_JWT_PRIVATE_KEY_FILEPATH=$SMS_JWT_PRIVATE_KEY_FILEPATH \
--env SMS_JWT_PUBLIC_KEY_FILEPATH=$SMS_JWT_PUBLIC_KEY_FILEPATH \
--env SMS_JWT_VALIDITY_LENGTH=$SMS_JWT_VALIDITY_LENGTH \
--env MONGODB_USER=$MONGODB_USER \
--env MONGODB_PASSWORD=$MONGODB_PASSWORD \
--env MONGODB_HOST=$MONGODB_HOST \
--env MONGODB_HOST=$MONGODB_HOST \
--env PIPELINE_SENSORS_FILEPATH=$PIPELINE_SENSORS_FILEPATH \
--env SMS_SMTP_HOST=$SMS_SMTP_HOST \
--env SMS_SMTP_USER=$SMS_SMTP_USER \
--env SMS_SMTP_PASSWORD=$SMS_SMTP_PASSWORD \
--env NODE_ENV=$NODE_ENV \
-v certs:/certs \
--network host \
-p 4000:4000 -d --name $container_name $image_name
