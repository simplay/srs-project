import { ActionTree, GetterTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { AxiosError } from 'axios'
import { ApiError } from '~/store/interfaces/api-error'

import { Booking } from '~/store/interfaces/bookings/bookings'
import { Room } from '~/store/interfaces/bookings/room'
import { CreateUpdateRoomDto } from '~/store/interfaces/bookings/create-update-room.dto'
import { CreateUpdateBookingDto } from '~/store/interfaces/bookings/create-update-booking.dto'
import {
  ManageBookingResultDto,
  ManageBookingStatus,
} from '~/store/interfaces/bookings/manage-booking-result.dto'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    rooms: [] as Room[],
    roomsFetched: false,
    bookings: [] as Booking[],
    bookingsFetched: false,
    manageBookingResult: null as null | ManageBookingResultDto,
  }
}

export const state = () => getDefaultState()

export type BookingsModuleState = ReturnType<typeof state>

export const getters: GetterTree<BookingsModuleState, RootState> = {}

export const mutations: MutationTree<BookingsModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_BOOKINGS(state, bookings: Booking[]) {
    state.bookings = bookings
  },
  RESET_BOOKINGS(state) {
    state.bookings = []
  },
  SET_BOOKINGS_FETCHED(state, fetched: boolean) {
    state.bookingsFetched = fetched
  },
  SET_MANAGE_BOOKING_RESULT(state, res: ManageBookingResultDto) {
    state.manageBookingResult = res
  },
  RESET_MANAGE_BOOKING_RESULT(state) {
    state.manageBookingResult = null
  },
  SET_ROOMS(state, rooms: Room[]) {
    state.rooms = rooms
  },
  RESET_ROOMS(state) {
    state.rooms = []
  },
  SET_ROOMS_FETCHED(state, fetched: boolean) {
    state.roomsFetched = fetched
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<BookingsModuleState, RootState> = {
  async FETCH_ROOMS({ commit }) {
    commit('SET_ROOMS_FETCHED', false)
    try {
      const rooms: Room[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.BOOKINGS_ROOMS_ENDPOINT
      )
      commit('SET_ROOMS', rooms)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching rooms'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_ROOMS')
    }
    commit('SET_ROOMS_FETCHED', true)
  },

  async CREATE_ROOM(
    { commit },
    { createUpdateRoomDto }: { createUpdateRoomDto: CreateUpdateRoomDto }
  ) {
    try {
      await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.BOOKINGS_ROOMS_ENDPOINT,
        createUpdateRoomDto
      )
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating room'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async UPDATE_ROOM(
    { commit },
    {
      roomUuid,
      createUpdateRoomDto,
    }: { roomUuid: string; createUpdateRoomDto: CreateUpdateRoomDto }
  ) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.BOOKINGS_ROOMS_ENDPOINT}/${roomUuid}`
    try {
      await this.$axios.$put(url, createUpdateRoomDto)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating room'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async DELETE_ROOM({ commit }, { roomUuid }: { roomUuid: string }) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.BOOKINGS_ROOMS_ENDPOINT}/${roomUuid}`
    try {
      await this.$axios.$delete(url)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error deleting room'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async FETCH_BOOKINGS_FOR_ROOM(
    { commit },
    { roomUuid }: { roomUuid: string }
  ) {
    commit('SET_BOOKINGS_FETCHED', false)
    commit('RESET_BOOKINGS')
    const url =
      ApiConfig.API_SENSOR_MONITORING.BOOKINGS_FOR_ROOM_ENDPOINT.replace(
        '[ROOM_UUID]',
        roomUuid
      )
    try {
      const bookings: Booking[] = await this.$axios.$get(url)
      commit('SET_BOOKINGS', bookings)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching bookings'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_BOOKINGS')
    }
    commit('SET_BOOKINGS_FETCHED', true)
  },

  RESET_BOOKINGS({ commit }) {
    commit('RESET_BOOKINGS')
  },

  async CREATE_BOOKING_FOR_ROOM(
    { commit },
    {
      roomUuid,
      createUpdateBookingDto,
    }: { roomUuid: string; createUpdateBookingDto: CreateUpdateBookingDto }
  ) {
    try {
      const url =
        ApiConfig.API_SENSOR_MONITORING.BOOKINGS_FOR_ROOM_ENDPOINT.replace(
          '[ROOM_UUID]',
          roomUuid
        )

      const res: ManageBookingResultDto = await this.$axios.$post(
        url,
        createUpdateBookingDto
      )
      commit('SET_MANAGE_BOOKING_RESULT', res)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating booking'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async UPDATE_BOOKING_FOR_ROOM(
    { commit },
    {
      roomUuid,
      bookingUuid,
      createUpdateBookingDto,
    }: {
      roomUuid: string
      bookingUuid: string
      createUpdateBookingDto: CreateUpdateBookingDto
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.BOOKINGS_FOR_ROOM_ENDPOINT.replace(
        '[ROOM_UUID]',
        roomUuid
      )

    try {
      const res: ManageBookingResultDto = await this.$axios.$put(
        `${url}/${bookingUuid}`,
        createUpdateBookingDto
      )

      commit('SET_MANAGE_BOOKING_RESULT', res)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating room'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async DELETE_BOOKING({ commit }, { bookingUuid }: { bookingUuid: string }) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.BOOKINGS_ENDPOINT}/${bookingUuid}`
    try {
      const res: ManageBookingResultDto = await this.$axios.$delete(url)

      commit('SET_MANAGE_BOOKING_RESULT', res)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error deleting booking'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
