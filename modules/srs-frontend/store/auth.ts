import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

import ApiConfig from '@/plugins/api-config.js'
import * as ApiHelper from '@/plugins/api-helper'
import { AxiosError } from 'axios'
import { Role, User } from '~/store/interfaces/user'
import { Credentials } from '~/store/interfaces/credentials'
import { ApiError } from '~/store/interfaces/api-error'

const getDefaultState = () => {
  return {
    user: null as null | User,
    isFetchingUser: false,
    error: null as null | ApiError,
    jwt: '',
    jwtExpired: false,
    isRequestingAuth: false, // flag used to display circular progress indicator while waiting for answer from server
  }
}

export const state = () => getDefaultState()

export type AuthModuleState = ReturnType<typeof state>

export const getters: GetterTree<AuthModuleState, RootState> = {
  isAdmin: (state) => {
    if (!state.user) {
      return false
    }
    return state.user.roles.includes(Role.Admin)
  },
  isOperator: (state) => {
    if (!state.user) {
      return false
    }
    return state.user.roles.includes(Role.Operator)
  },
  isSecretariat: (state) => {
    if (!state.user) {
      return false
    }
    return state.user.roles.includes(Role.Secretariat)
  },
}

export const mutations: MutationTree<AuthModuleState> = {
  SET_USER(state, user) {
    state.user = user
  },
  SET_IS_FETCHING_USER(state, isFetchingUser) {
    state.isFetchingUser = isFetchingUser
  },
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_JWT(state, jwt) {
    state.jwt = jwt
  },
  SET_JWT_EXPIRED(state, value) {
    state.jwtExpired = value
  },
  SET_IS_REQUESTING_AUTH(state, value) {
    state.isRequestingAuth = value
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<AuthModuleState, RootState> = {
  async AUTHENTICATE({ commit }, credentials: Credentials) {
    commit('SET_IS_REQUESTING_AUTH', true)

    try {
      const res = await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.AUTH_ENDPOINT,
        {
          username: credentials.username,
          password: credentials.password,
        }
      )

      if ('access_token' in res) {
        const token = res.access_token
        commit('SET_JWT', token)

        // Get details of user from API
        const user = await this.$axios.$get(
          ApiConfig.API_SENSOR_MONITORING.PROFILE_ENDPOINT
        )

        if (user) {
          commit('SET_USER', user)
          commit('SET_NO_ERROR')
        }
      }
    } catch (axiosError) {
      const topic = 'Authentication error'
      commit('SET_ERROR', { axiosError, topic })
    }

    commit('SET_IS_REQUESTING_AUTH', false)
  },

  async FETCH_PROFILE({ commit }, username: string) {
    commit('SET_IS_FETCHING_USER', true)

    const url = `${ApiConfig.API_SENSOR_MONITORING.PROFILE_ENDPOINT}/?username=${username}`
    try {
      // Get details of user from API
      const user = await this.$axios.get(url)

      commit('SET_USER', user)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching profile'
      commit('SET_ERROR', { axiosError, topic })
    }

    commit('SET_IS_FETCHING_USER', false)
  },

  JWT_EXPIRED({ commit }) {
    commit('SET_JWT_EXPIRED', true)
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },

  LOGOUT({ commit, dispatch }) {
    // Reset authentication state and reset state of all store modules

    commit('RESET_STATE')

    this.$router.replace({ name: 'login' })

    dispatch('sensors/RESET_STATE', null, { root: true })
    dispatch('samples/RESET_STATE', null, { root: true })
    dispatch('configs/RESET_STATE', null, { root: true })
    dispatch('experiments/RESET_STATE', null, { root: true })
    dispatch('logbook/RESET_STATE', null, { root: true })
    dispatch('users/RESET_STATE', null, { root: true })
    dispatch('participants/RESET_STATE', null, { root: true })
    dispatch('bookings/RESET_STATE', null, { root: true })
  },
}
