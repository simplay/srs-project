import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'
import { AxiosError } from 'axios'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { ApiError } from '~/store/interfaces/api-error'
import { Experiment } from '~/store/interfaces/experiment'
import { CreateExperimentDto } from '~/store/interfaces/dtos/create-experiment.dto'
import { UpdateExperimentDto } from '~/store/interfaces/dtos/update-experiment.dto'
import { ActiveExperiment } from '~/store/interfaces/active-experiment'
import { StartRecordingExperimentDto } from '~/store/interfaces/dtos/start-recording-experiment.dto'
import { CreateTaskDto } from '~/store/interfaces/dtos/create-task.dto'
import { UpdateTaskDto } from '~/store/interfaces/dtos/update-task.dto'
import { UpdateTaskCurrentRecordingDto } from '~/store/interfaces/dtos/update-task-current-recording.dto'
import { ParticipantProgress } from '~/store/interfaces/participant-progress'
import { CreateMessageScreenEntranceDto } from '~/store/interfaces/dtos/create-message-screen-entrance.dto'
import { MessageScreenEntrance } from '~/store/interfaces/message-screen-entrance'
import { ActiveExperimentPublicInfo } from '~/store/interfaces/active-experiment-public-info'
import { CreateExperimentNoteDto } from '~/store/interfaces/dtos/create-experiment-note.dto'
import { ExperimentLog } from '~/store/interfaces/experiment-log'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    experiment: null as Experiment | null,
    experimentFetched: false,
    experiments: [] as Experiment[],
    experimentsFetched: false,
    activeExperiment: null as ActiveExperiment | null,
    activeExperimentFetched: false,
    participantsProgress: [] as ParticipantProgress[],
    participantsProgressFetched: false,
    messageScreenEntrance: null as MessageScreenEntrance | null,
    messageScreenEntranceFetched: false,
    activeExperimentPublicInfo: null as ActiveExperimentPublicInfo | null,
    activeExperimentPublicInfoFetched: false,
    experimentLog: null as null | ExperimentLog, // Log line returned when saving a note for the experiment
  }
}

export const state = () => getDefaultState()

export type ExperimentsModuleState = ReturnType<typeof state>

export const getters: GetterTree<ExperimentsModuleState, RootState> = {}

export const mutations: MutationTree<ExperimentsModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_EXPERIMENT(state, experiment: Experiment) {
    state.experiment = experiment
  },
  RESET_EXPERIMENT(state) {
    state.experiment = null
  },
  SET_EXPERIMENT_FETCHED(state, fetched: boolean) {
    state.experimentFetched = fetched
  },
  SET_EXPERIMENTS(state, experiments: Experiment[]) {
    state.experiments = experiments
  },
  RESET_EXPERIMENTS(state) {
    state.experiments = []
  },
  SET_EXPERIMENTS_FETCHED(state, fetched: boolean) {
    state.experimentsFetched = fetched
  },
  SET_ACTIVE_EXPERIMENT(state, activeExperiment: ActiveExperiment) {
    state.activeExperiment = activeExperiment
  },
  RESET_ACTIVE_EXPERIMENT(state) {
    state.activeExperiment = null
  },
  SET_ACTIVE_EXPERIMENT_FETCHED(state, fetched: boolean) {
    state.activeExperimentFetched = fetched
  },
  SET_PARTICIPANTS_PROGRESS(
    state,
    participantsProgress: ParticipantProgress[]
  ) {
    state.participantsProgress = participantsProgress
  },
  RESET_PARTICIPANTS_PROGRESS(state) {
    state.participantsProgress = []
  },
  SET_PARTICIPANTS_PROGRESS_FETCHED(state, fetched: boolean) {
    state.participantsProgressFetched = fetched
  },
  SET_MESSAGE_SCREEN_ENTRANCE(state, message: MessageScreenEntrance) {
    state.messageScreenEntrance = message
  },
  RESET_MESSAGE_SCREEN_ENTRANCE(state) {
    state.messageScreenEntrance = null
  },
  SET_MESSAGE_SCREEN_ENTRANCE_FETCHED(state, fetched: boolean) {
    state.messageScreenEntranceFetched = fetched
  },
  SET_ACTIVE_EXPERIMENT_PUBLIC_INFO(
    state,
    activeExperimentPublicInfo: ActiveExperimentPublicInfo
  ) {
    state.activeExperimentPublicInfo = activeExperimentPublicInfo
  },
  RESET_ACTIVE_EXPERIMENT_PUBLIC_INFO(state) {
    state.activeExperimentPublicInfo = null
  },
  SET_ACTIVE_EXPERIMENT_PUBLIC_INFO_FETCHED(state, fetched: boolean) {
    state.activeExperimentPublicInfoFetched = fetched
  },
  SET_EXPERIMENT_LOG(state, log: ExperimentLog) {
    state.experimentLog = log
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<ExperimentsModuleState, RootState> = {
  async CREATE_EXPERIMENT(
    { commit },
    { createExperimentDto }: { createExperimentDto: CreateExperimentDto }
  ) {
    try {
      const createdExperiment: Experiment = await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ENDPOINT,
        createExperimentDto
      )
      commit('SET_EXPERIMENT', createdExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating experiment'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async UPDATE_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      updateExperimentDto,
    }: {
      experimentUuid: string
      updateExperimentDto: UpdateExperimentDto
    }
  ) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ENDPOINT}/${experimentUuid}`
    try {
      const updatedExperiment: Experiment = await this.$axios.$put(
        url,
        updateExperimentDto
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating experiment'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async ADD_PARTICIPANTS_TO_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      participantsUuidList,
    }: {
      experimentUuid: string
      participantsUuidList: string[]
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ADD_PARTICIPANTS_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    const params = new URLSearchParams()
    params.append('participants', participantsUuidList.join(','))

    const config = {
      params,
    }
    try {
      const updatedExperiment: Experiment = await this.$axios.$post(
        url,
        null,
        config
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error adding participants to experiment`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async REMOVE_PARTICIPANTS_FROM_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      participantUuid,
    }: {
      experimentUuid: string
      participantUuid: string
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_REMOVE_PARTICIPANT_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    const params = new URLSearchParams()
    params.append('participant', participantUuid)

    const config = {
      params,
    }
    try {
      const updatedExperiment: Experiment = await this.$axios.$post(
        url,
        null,
        config
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error removing participant ${participantUuid} from experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async ADD_OPERATORS_TO_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      operatorsUsernameList,
    }: {
      experimentUuid: string
      operatorsUsernameList: string[]
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ADD_OPERATORS_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    const params = new URLSearchParams()
    params.append('operators', operatorsUsernameList.join(','))

    const config = {
      params,
    }
    try {
      const updatedExperiment: Experiment = await this.$axios.$post(
        url,
        null,
        config
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error adding operators to experiment`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async REMOVE_OPERATOR_FROM_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      operatorUsername,
    }: {
      experimentUuid: string
      operatorUsername: string
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_REMOVE_OPERATOR_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    const params = new URLSearchParams()
    params.append('operator', operatorUsername)

    const config = {
      params,
    }
    try {
      const updatedExperiment: Experiment = await this.$axios.$post(
        url,
        null,
        config
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error removing operator ${operatorUsername} from experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async ADD_TASK_TO_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      createTaskDto,
    }: {
      experimentUuid: string
      createTaskDto: CreateTaskDto
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ADD_TASK_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const updatedExperiment: Experiment = await this.$axios.$post(
        url,
        createTaskDto
      )

      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error adding task to experiment`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async UPDATE_TASK_OF_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      taskUuid,
      updateTaskDto,
    }: {
      experimentUuid: string
      taskUuid: string
      updateTaskDto: UpdateTaskDto
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_TASK_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      ).replace('[TASK_UUID]', taskUuid)

    try {
      const updatedExperiment: Experiment = await this.$axios.$put(
        url,
        updateTaskDto
      )
      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error updating task ${taskUuid} of experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async DELETE_TASK_FROM_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      taskUuid,
    }: {
      experimentUuid: string
      taskUuid: string
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_TASK_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      ).replace('[TASK_UUID]', taskUuid)

    try {
      const updatedExperiment: Experiment = await this.$axios.$delete(url)
      commit('SET_EXPERIMENT', updatedExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error deleting task ${taskUuid} from experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
  },

  async FETCH_EXPERIMENTS({ commit }) {
    commit('SET_EXPERIMENTS_FETCHED', false)

    try {
      const experiments: Experiment[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ENDPOINT
      )
      commit('SET_EXPERIMENTS', experiments)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error listing experiments'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENTS')
    }
    commit('SET_EXPERIMENTS_FETCHED', true)
  },

  async FETCH_EXPERIMENT(
    { commit },
    { experimentUuid }: { experimentUuid: string }
  ) {
    commit('SET_EXPERIMENT_FETCHED', false)

    try {
      const experiment: Experiment = await this.$axios.$get(
        `${ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_ENDPOINT}/${experimentUuid}`
      )
      commit('SET_EXPERIMENT', experiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error fetching experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_EXPERIMENT')
    }
    commit('SET_EXPERIMENT_FETCHED', true)
  },

  async FETCH_ACTIVE_EXPERIMENT({ commit }) {
    commit('SET_ACTIVE_EXPERIMENT_FETCHED', false)

    try {
      const activeExperiment: ActiveExperiment = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_GET_ACTIVE_ENDPOINT
      )
      commit('SET_ACTIVE_EXPERIMENT', activeExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      commit('RESET_ACTIVE_EXPERIMENT')
      const err = axiosError as AxiosError
      if (err.response && err.response.status === 404) {
        console.log('No active experiment found')
      } else {
        const topic = 'Error fetching active experiment'
        commit('SET_ERROR', { axiosError, topic })
      }
    }
    commit('SET_ACTIVE_EXPERIMENT_FETCHED', true)
  },

  async SET_ACTIVE_EXPERIMENT(
    { commit },
    { experimentUuid }: { experimentUuid: string }
  ) {
    commit('SET_ACTIVE_EXPERIMENT_FETCHED', false)

    const params = new URLSearchParams()
    params.append('experimentUuid', experimentUuid)

    const config = {
      params,
    }

    try {
      const activeExperiment: ActiveExperiment = await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_SET_ACTIVE_ENDPOINT,
        null,
        config
      )
      commit('SET_ACTIVE_EXPERIMENT', activeExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error setting active experiment'
      commit('SET_ERROR', { axiosError, topic })
    }
    commit('SET_ACTIVE_EXPERIMENT_FETCHED', true)
  },

  async START_RECORDING_EXPERIMENT(
    { commit },
    {
      experimentUuid,
      startRecordingExperimentDto,
    }: {
      experimentUuid: string
      startRecordingExperimentDto: StartRecordingExperimentDto
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_START_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const activeExperiment: ActiveExperiment = await this.$axios.$put(
        url,
        startRecordingExperimentDto
      )
      commit('SET_ACTIVE_EXPERIMENT', activeExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error starting active experiment'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async UPDATE_TASK_CURRENT_RECORDING(
    { commit },
    {
      experimentUuid,
      updateTaskCurrentRecordingDto,
    }: {
      experimentUuid: string
      updateTaskCurrentRecordingDto: UpdateTaskCurrentRecordingDto
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_UPDATE_TASK_CURRENT_RECORDING_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const activeExperiment: ActiveExperiment = await this.$axios.$put(
        url,
        updateTaskCurrentRecordingDto
      )
      commit('SET_ACTIVE_EXPERIMENT', activeExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error starting active experiment'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async INCREMENT_TRIAL_CURRENT_RECORDING(
    { commit },
    {
      experimentUuid,
    }: {
      experimentUuid: string
    }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENT_INCREMENT_TRIAL_CURRENT_RECORDING_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const updatedActiveExperiment: ActiveExperiment = await this.$axios.$put(
        url
      )
      commit('SET_ACTIVE_EXPERIMENT', updatedActiveExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error increment task of experiment ${experimentUuid}`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async STOP_RECORDING_EXPERIMENT(
    { commit },
    { experimentUuid }: { experimentUuid: string }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_STOP_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const activeExperiment: ActiveExperiment = await this.$axios.$put(url)
      commit('SET_ACTIVE_EXPERIMENT', activeExperiment)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error stopping active experiment'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async COMPLETE_EXPERIMENT(
    { commit },
    { experimentUuid }: { experimentUuid: string }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_COMPLETE_EXPERIMENT_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      await this.$axios.$put(url)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error completing experiment'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async FETCH_PARTICIPANTS_PROGRESS_FOR_EXPERIMENT(
    { commit },
    { experimentUuid }: { experimentUuid: string }
  ) {
    commit('SET_PARTICIPANTS_PROGRESS_FETCHED', false)

    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_GET_PARTICIPANTS_PROGRESS_FOR_EXPERIMENT_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        experimentUuid
      )

    try {
      const participantsProgress: ParticipantProgress[] =
        await this.$axios.$get(url)
      commit('SET_PARTICIPANTS_PROGRESS', participantsProgress)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      commit('RESET_PARTICIPANTS_PROGRESS')
      const topic = 'Error fetching participants progress'
      commit('SET_ERROR', { axiosError, topic })
    }
    commit('SET_PARTICIPANTS_PROGRESS_FETCHED', true)
  },

  async FETCH_MESSAGE_SCREEN_ENTRANCE({ commit }) {
    commit('SET_MESSAGE_SCREEN_ENTRANCE_FETCHED', false)
    try {
      const messageScreenEntrance: MessageScreenEntrance =
        await this.$axios.$get(
          ApiConfig.API_SENSOR_MONITORING
            .EXPERIMENTS_MESSAGE_SCREEN_ENTRANCE_ENDPOINT
        )

      commit('SET_MESSAGE_SCREEN_ENTRANCE', messageScreenEntrance)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const err = axiosError as AxiosError
      if (err.response && err.response.status === 404) {
        console.log('No message found')
      } else {
        const topic = `Error fetching message screen entrance`
        commit('SET_ERROR', { axiosError, topic })
      }
      commit('RESET_MESSAGE_SCREEN_ENTRANCE')
    }
    commit('SET_MESSAGE_SCREEN_ENTRANCE_FETCHED', true)
  },

  async SET_MESSAGE_SCREEN_ENTRANCE(
    { commit },
    {
      createMessageScreenEntranceDto,
    }: {
      createMessageScreenEntranceDto: CreateMessageScreenEntranceDto
    }
  ) {
    try {
      console.log(createMessageScreenEntranceDto)
      const messageScreenEntrance: MessageScreenEntrance =
        await this.$axios.$post(
          ApiConfig.API_SENSOR_MONITORING
            .EXPERIMENTS_MESSAGE_SCREEN_ENTRANCE_ENDPOINT,
          createMessageScreenEntranceDto
        )

      commit('SET_MESSAGE_SCREEN_ENTRANCE', messageScreenEntrance)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error setting message screen entrance`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_MESSAGE_SCREEN_ENTRANCE')
    }
  },

  async FETCH_ACTIVE_EXPERIMENT_PUBLIC_INFO({ commit }) {
    commit('SET_ACTIVE_EXPERIMENT_PUBLIC_INFO_FETCHED', false)
    try {
      const activeExperimentPublicInfo: ActiveExperimentPublicInfo =
        await this.$axios.$get(
          ApiConfig.API_SENSOR_MONITORING
            .EXPERIMENTS_ACTIVE_EXPERIMENT_PUBLIC_INFO_ENDPOINT
        )

      commit('SET_ACTIVE_EXPERIMENT_PUBLIC_INFO', activeExperimentPublicInfo)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const err = axiosError as AxiosError
      if (err.response && err.response.status === 404) {
        console.log('No message found')
      } else {
        const topic = `Error fetching active experiment public info`
        commit('SET_ERROR', { axiosError, topic })
      }
      commit('RESET_ACTIVE_EXPERIMENT_PUBLIC_INFO')
    }
    commit('SET_ACTIVE_EXPERIMENT_PUBLIC_INFO_FETCHED', true)
  },

  async SAVE_NOTE_FOR_EXPERIMENT(
    { commit },
    {
      createExperimentNoteDto,
    }: { createExperimentNoteDto: CreateExperimentNoteDto }
  ) {
    const url =
      ApiConfig.API_SENSOR_MONITORING.EXPERIMENTS_SAVE_NOTE_FOR_EXPERIMENT_ENDPOINT.replace(
        '[EXPERIMENT_UUID]',
        createExperimentNoteDto.experimentUuid
      )

    try {
      const experimentLog: ExperimentLog = await this.$axios.$post(
        url,
        createExperimentNoteDto
      )
      commit('SET_EXPERIMENT_LOG', experimentLog)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating log line in log book'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
