import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'
import { AxiosError } from 'axios'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { ApiError } from '~/store/interfaces/api-error'
import { Participant } from '~/store/interfaces/participant'
import { CreateParticipantDto } from '~/store/interfaces/dtos/create-participant.dto'
import { UpdateParticipantDto } from '~/store/interfaces/dtos/update-participant.dto'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    participants: [] as Participant[],
    participantsFetched: false,
  }
}

export const state = () => getDefaultState()

export type ParticipantsModuleState = ReturnType<typeof state>

export const getters: GetterTree<ParticipantsModuleState, RootState> = {}

export const mutations: MutationTree<ParticipantsModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_PARTICIPANTS(state, participants) {
    state.participants = participants
  },
  RESET_PARTICIPANTS(state) {
    state.participants = []
  },
  SET_PARTICIPANTS_FETCHED(state, fetched) {
    state.participantsFetched = fetched
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<ParticipantsModuleState, RootState> = {
  async FETCH_PARTICIPANTS({ commit }) {
    commit('SET_PARTICIPANTS_FETCHED', false)

    try {
      const participants: Participant[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.PARTICIPANTS_ENDPOINT
      )
      commit('SET_PARTICIPANTS', participants)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching participants'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_PARTICIPANTS')
    }
    commit('SET_PARTICIPANTS_FETCHED', true)
  },

  async CREATE_PARTICIPANT(
    { commit },
    { createParticipantDto }: { createParticipantDto: CreateParticipantDto }
  ) {
    try {
      await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.PARTICIPANTS_ENDPOINT,
        createParticipantDto
      )
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating participant'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async UPDATE_PARTICIPANT(
    { commit },
    {
      participantUuid,
      updateParticipantDto,
    }: {
      participantUuid: string
      updateParticipantDto: UpdateParticipantDto
    }
  ) {
    try {
      const url = `${ApiConfig.API_SENSOR_MONITORING.PARTICIPANTS_ENDPOINT}/${participantUuid}`
      await this.$axios.$put(url, updateParticipantDto)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating participant'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async DELETE_PARTICIPANT(
    { commit },
    { participantUuid }: { participantUuid: string }
  ) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.PARTICIPANTS_ENDPOINT}/${participantUuid}`
    try {
      await this.$axios.$delete(url)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error deleting participant ${participantUuid}`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
