import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { AxiosError } from 'axios'
import { ApiError } from '~/store/interfaces/api-error'
import { SamplesCollectionStats } from '~/store/interfaces/samples-collection-stats'
import { ActiveExportJob } from '~/store/interfaces/active-export-job'
import { ExportJob } from '~/store/interfaces/export-job'
import { CreateExportJobDto } from '~/store/interfaces/dtos/create-export-job.dto'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    activeExportJob: null as ActiveExportJob | null,
    activeExportJobFetched: false,
    exportJob: null as ExportJob | null,
    exportJobFetched: false,
    samplesCollectionsStats: [] as SamplesCollectionStats[] | null,
    samplesCollectionsStatsFetched: false,
  }
}

export const state = () => getDefaultState()

export type SamplesModuleState = ReturnType<typeof state>

export const getters: GetterTree<SamplesModuleState, RootState> = {}

export const mutations: MutationTree<SamplesModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_ACTIVE_EXPORT_JOB(state, activeExportJob: ActiveExportJob) {
    state.activeExportJob = activeExportJob
  },
  RESET_ACTIVE_EXPORT_JOB(state) {
    state.activeExportJob = null
  },
  SET_ACTIVE_EXPORT_JOB_FETCHED(state, fetched: boolean) {
    state.activeExportJobFetched = fetched
  },
  SET_EXPORT_JOB(state, exportJob: ExportJob) {
    state.exportJob = exportJob
  },
  RESET_EXPORT_JOB(state) {
    state.exportJob = null
  },
  SET_EXPORT_JOB_FETCHED(state, fetched: boolean) {
    state.exportJobFetched = fetched
  },
  SET_SAMPLES_COLLECTIONS_STATS(state, stats: SamplesCollectionStats[]) {
    state.samplesCollectionsStats = stats
  },
  SET_SAMPLES_COLLECTIONS_STATS_FETCHED(state, fetched: boolean) {
    state.samplesCollectionsStatsFetched = fetched
  },
  RESET_SAMPLES_COLLECTIONS_STATS(state) {
    state.samplesCollectionsStats = []
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<SamplesModuleState, RootState> = {
  async FETCH_SAMPLES_COLLECTIONS_STATS({ commit }) {
    commit('SET_SAMPLES_COLLECTIONS_STATS_FETCHED', false)
    try {
      const stats: SamplesCollectionStats[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.SAMPLES_COLLECTIONS_STATS
      )
      commit('SET_SAMPLES_COLLECTIONS_STATS', stats)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching samples collection stats'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_SAMPLES_COLLECTIONS_STATS')
    }
    commit('SET_SAMPLES_COLLECTIONS_STATS_FETCHED', true)
  },

  async DELETE_SAMPLES_COLLECTIONS({ commit }) {
    try {
      await this.$axios.$delete(
        ApiConfig.API_SENSOR_MONITORING.DELETE_SAMPLES_COLLECTIONS_ENDPOINT
      )
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error deleting samples collection`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async CREATE_EXPORT_JOB(
    { commit },
    { createExportJobDto }: { createExportJobDto: CreateExportJobDto }
  ) {
    console.log(createExportJobDto)
    try {
      await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.CREATE_EXPORT_JOB_ENDPOINT,
        createExportJobDto
      )
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error exporting data`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async FETCH_ACTIVE_EXPORT_JOB({ commit }) {
    commit('SET_ACTIVE_EXPORT_JOB_FETCHED', false)
    try {
      const activeExportJob: ActiveExportJob = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.GET_ACTIVE_EXPORT_JOB_ENDPOINT
      )
      commit('SET_ACTIVE_EXPORT_JOB', activeExportJob)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      commit('RESET_ACTIVE_EXPORT_JOB')
      const err = axiosError as AxiosError
      if (err.response && err.response.status === 404) {
        console.log('No active export found')
      } else {
        const topic = `Error getting active export job`
        commit('SET_ERROR', { axiosError, topic })
      }
    }

    commit('SET_ACTIVE_EXPORT_JOB_FETCHED', true)
  },

  async RESET_ACTIVE_EXPORT_JOB({ commit }) {
    try {
      await this.$axios.$delete(
        ApiConfig.API_SENSOR_MONITORING.GET_ACTIVE_EXPORT_JOB_ENDPOINT
      )
      commit('RESET_ACTIVE_EXPORT_JOB')
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error resetting active export job`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async FETCH_EXPORT_JOB(
    { commit },
    { exportJobUuid }: { exportJobUuid: string }
  ) {
    commit('SET_EXPORT_JOB_FETCHED', false)

    try {
      const exportJob: ExportJob = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.GET_EXPORT_JOB_ENDPOINT.replace(
          '[EXPORT_JOB_UUID]',
          exportJobUuid
        )
      )
      commit('SET_EXPORT_JOB', exportJob)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error getting export job ${exportJobUuid}`
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_ACTIVE_EXPORT_JOB')
    }

    commit('SET_EXPORT_JOB_FETCHED', true)
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
