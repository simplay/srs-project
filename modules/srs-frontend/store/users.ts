import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'
import { AxiosError } from 'axios'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { ApiError } from '~/store/interfaces/api-error'
import { Role, User } from '~/store/interfaces/user'
import { CreateUpdateUserDto } from '~/store/interfaces/dtos/create-update-user.dto'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    users: [] as User[],
    usersFetched: false,
  }
}

export const state = () => getDefaultState()

export type UsersModuleState = ReturnType<typeof state>

export const getters: GetterTree<UsersModuleState, RootState> = {
  adminUsers: (state) => {
    return state.users.filter((user) => user.roles.includes(Role.Admin))
  },
  operatorUsers: (state) => {
    return state.users.filter((user) => user.roles.includes(Role.Operator))
  },
}

export const mutations: MutationTree<UsersModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_USERS(state, users) {
    state.users = users
  },
  RESET_USERS(state) {
    state.users = []
  },
  SET_USERS_FETCHED(state, fetched) {
    state.usersFetched = fetched
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<UsersModuleState, RootState> = {
  async FETCH_USERS({ commit }) {
    commit('SET_USERS_FETCHED', false)

    try {
      const users: User[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.USERS_ENDPOINT
      )
      commit('SET_USERS', users)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching users'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_USERS')
    }
    commit('SET_USERS_FETCHED', true)
  },

  async CREATE_USER({ commit }, { user }: { user: CreateUpdateUserDto }) {
    try {
      await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.USERS_ENDPOINT,
        user
      )
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating user'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async UPDATE_USER(
    { commit },
    { oldUsername, user }: { user: CreateUpdateUserDto; oldUsername: string }
  ) {
    try {
      const url = `${ApiConfig.API_SENSOR_MONITORING.USERS_ENDPOINT}/${oldUsername}`
      await this.$axios.$put(url, user)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating user'
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async DELETE_USER({ commit }, { username }: { username: string }) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.USERS_ENDPOINT}/${username}`
    try {
      await this.$axios.$delete(url)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error deleting user ${username}`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
