import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

import * as ApiHelper from '@/plugins/api-helper'
import Vue from 'vue'
import { AxiosError } from 'axios'
import { SensorType } from '~/store/enums/sensor-type.enum'
import { SensorTypeLastReceivedMessage } from '~/store/interfaces/sensor-data-received-message.interface'
import { ApiError } from '~/store/interfaces/api-error'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    // Will hold one last message per sensor type
    sensorsLastActivityPerSensorType: {} as Record<
      SensorType,
      SensorTypeLastReceivedMessage
    >,
  }
}

export const state = () => getDefaultState()

export type SensorsModuleState = ReturnType<typeof state>

export const getters: GetterTree<SensorsModuleState, RootState> = {
  getSensorLastActivityForSensorType:
    (state) => (sensorTypeCode: SensorType) => {
      if (!(sensorTypeCode in state.sensorsLastActivityPerSensorType)) {
        return null
      }
      return state.sensorsLastActivityPerSensorType[sensorTypeCode]
    },
}

export const mutations: MutationTree<SensorsModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  UPDATE_SENSOR_DATA_RECEIVED_MESSAGE(state, msg) {
    const sensorType = msg.sensorType

    // Vue is unable to react to mutations on state arrays (by index)
    // => use Vue.set to make getters react to the change
    Vue.set(state.sensorsLastActivityPerSensorType, sensorType, msg)
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<SensorsModuleState, RootState> = {
  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
