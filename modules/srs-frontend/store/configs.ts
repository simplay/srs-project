import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'
import { AxiosError } from 'axios'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { ApiError } from '~/store/interfaces/api-error'
import { UpdateConfigDto } from '~/store/interfaces/dtos/update-config.dto'
import { CreateConfigDto } from '~/store/interfaces/dtos/create-config.dto'
import { Config } from '~/store/interfaces/config'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    config: null as Config | null,
    configFetched: false,
    configs: [] as Config[],
    configsFetched: false,
  }
}

export const state = () => getDefaultState()

export type ConfigsModuleState = ReturnType<typeof state>

export const getters: GetterTree<ConfigsModuleState, RootState> = {}

export const mutations: MutationTree<ConfigsModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_CONFIG(state, config) {
    state.config = config
  },
  RESET_CONFIG(state) {
    state.config = null
  },
  SET_CONFIG_FETCHED(state, fetched) {
    state.configFetched = fetched
  },
  SET_CONFIGS(state, configs) {
    state.configs = configs
  },
  RESET_CONFIGS(state) {
    state.configs = []
  },
  SET_CONFIGS_FETCHED(state, fetched) {
    state.configsFetched = fetched
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<ConfigsModuleState, RootState> = {
  async FETCH_CONFIG({ commit }, { configUuid }: { configUuid: string }) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.CONFIGS_ENDPOINT}/${configUuid}`

    commit('SET_CONFIG_FETCHED', false)

    try {
      const config: Config = await this.$axios.$get(url)
      commit('SET_CONFIG', config)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching config'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_CONFIG')
    }
    commit('SET_CONFIG_FETCHED', true)
  },

  async CREATE_CONFIG({ commit }, { configName }: { configName: string }) {
    const createConfigDto: CreateConfigDto = {
      name: configName,
    }

    try {
      const createdConfig: Config = await this.$axios.$post(
        ApiConfig.API_SENSOR_MONITORING.CONFIGS_ENDPOINT,
        createConfigDto
      )
      commit('SET_CONFIG', createdConfig)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error creating config'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_CONFIG')
    }
  },

  async UPDATE_CONFIG(
    { commit },
    {
      configUuid,
      updateConfigDto,
    }: {
      configUuid: string
      updateConfigDto: UpdateConfigDto
    }
  ) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.CONFIGS_ENDPOINT}/${configUuid}`
    try {
      const updatedConfig: Config = await this.$axios.$put(url, updateConfigDto)

      commit('SET_CONFIG', updatedConfig)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error updating config'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_CONFIG')
    }
  },

  async DELETE_CONFIG({ commit }, { configUuid }: { configUuid: string }) {
    const url = `${ApiConfig.API_SENSOR_MONITORING.CONFIGS_ENDPOINT}/${configUuid}`
    try {
      await this.$axios.$delete(url)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = `Error deleting config ${configUuid}`
      commit('SET_ERROR', { axiosError, topic })
    }
  },

  async FETCH_CONFIGS({ commit }) {
    commit('SET_CONFIGS_FETCHED', false)

    try {
      const configs: Config[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.CONFIGS_ENDPOINT
      )
      commit('SET_CONFIGS', configs)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error listing config'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_CONFIGS')
    }
    commit('SET_CONFIGS_FETCHED', true)
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
