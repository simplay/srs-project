import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

import * as ApiHelper from '@/plugins/api-helper'
import ApiConfig from '@/plugins/api-config.js'
import { AxiosError } from 'axios'
import { ApiError } from '~/store/interfaces/api-error'

import { ExperimentLog } from '~/store/interfaces/experiment-log'
import { LogbookInfo } from '~/store/interfaces/logbook-info'

const getDefaultState = () => {
  return {
    error: null as null | ApiError,
    logBook: [] as ExperimentLog[],
    logBookFetched: false,
    logBookInfo: null as null | LogbookInfo,
    logBookInfoFetched: false,
  }
}

export const state = () => getDefaultState()

export type LogBookModuleState = ReturnType<typeof state>

export const getters: GetterTree<LogBookModuleState, RootState> = {}

export const mutations: MutationTree<LogBookModuleState> = {
  SET_ERROR: (
    state,
    { axiosError, topic }: { axiosError: AxiosError; topic: string }
  ) => {
    const error = ApiHelper.analyseErr(axiosError)
    error.topic = topic
    state.error = error
  },
  SET_NO_ERROR(state) {
    state.error = null
  },
  SET_LOG_BOOK(state, logBook: ExperimentLog[]) {
    state.logBook = logBook
  },
  RESET_LOG_BOOK(state) {
    state.logBook = []
  },
  SET_LOG_BOOK_FETCHED(state, fetched: boolean) {
    state.logBookFetched = fetched
  },
  SET_LOG_BOOK_INFO(state, logBookInfo: LogbookInfo) {
    state.logBookInfo = logBookInfo
  },
  RESET_LOG_BOOK_INFO(state) {
    state.logBookInfo = null
  },
  SET_LOG_BOOK_INFO_FETCHED(state, fetched: boolean) {
    state.logBookInfoFetched = fetched
  },
  RESET_STATE(state) {
    // Merge rather than replace so we don't lose observers
    // https://github.com/vuejs/vuex/issues/1118
    Object.assign(state, getDefaultState())
  },
}

export const actions: ActionTree<LogBookModuleState, RootState> = {
  async FETCH_LOG_BOOK({ commit }) {
    commit('SET_LOG_BOOK_FETCHED', false)
    try {
      const logBook: ExperimentLog[] = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.LOG_BOOK_ENDPOINT
      )
      commit('SET_LOG_BOOK', logBook)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching log book'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_LOG_BOOK')
    }
    commit('SET_LOG_BOOK_FETCHED', true)
  },

  async FETCH_LOG_BOOK_INFO({ commit }) {
    commit('SET_LOG_BOOK_INFO_FETCHED', false)
    try {
      const logBookInfo: LogbookInfo = await this.$axios.$get(
        ApiConfig.API_SENSOR_MONITORING.LOG_BOOK_INFO_ENDPOINT
      )
      commit('SET_LOG_BOOK_INFO', logBookInfo)
      commit('SET_NO_ERROR')
    } catch (axiosError) {
      const topic = 'Error fetching log book info'
      commit('SET_ERROR', { axiosError, topic })
      commit('RESET_LOG_BOOK_INFO')
    }
    commit('SET_LOG_BOOK_INFO_FETCHED', true)
  },

  RESET_ERROR({ commit }) {
    commit('SET_NO_ERROR')
  },
  RESET_STATE({ commit }) {
    commit('RESET_STATE')
  },
}
