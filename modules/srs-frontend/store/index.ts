import { GetterTree, ActionTree, MutationTree } from 'vuex'

const getDefaultState = () => {
  return {
    locales: ['en', 'de'],
    locale: 'en',
  }
}

export const state = () => getDefaultState()

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, RootState> = {}

export const mutations: MutationTree<RootState> = {
  SET_LANG: (state, locale: string) => {
    if (state.locales.includes(locale)) {
      state.locale = locale
    }
  },
}

export const actions: ActionTree<RootState, RootState> = {
  UPDATE_LANG({ commit }, locale) {
    commit('SET_LANG', locale)
  },
}
