import { SensorType } from '~/store/enums/sensor-type.enum'
import { SensorDto } from '~/store/interfaces/sensors-dto'

export interface Sensor {
  ip: string
  name: string
  location: string
  saveData: boolean
  sensorType: SensorType
  // timestamp is in seconds since Epoch
  lastSampleReceivedAt?: number
  lastSample?: SensorDto
}
