export interface SensorDto {
  ip: string
  timestamp: number
}

export interface CameraDto extends SensorDto {
  // base64 encoded image
  data: string
}

export interface DoorDto extends SensorDto {
  doorOpen: boolean
}

export interface NightVisionCameraDto extends SensorDto {
  // base64 encoded image
  data: string
}
