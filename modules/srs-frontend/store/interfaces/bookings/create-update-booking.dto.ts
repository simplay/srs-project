import { BookingType } from '~/store/enums/booking-type.enum'

export interface CreateUpdateBookingDto {
  name: string
  bookingType: BookingType
  start: number
  end: number
  email?: string
}
