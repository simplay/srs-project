import { BookingType } from '~/store/enums/booking-type.enum'
import { Room } from '~/store/interfaces/bookings/room'

export interface Booking {
  uuid: string
  room: Room
  name: string
  start: number // unix timestamp in seconds
  end: number // unix timestamp in seconds
  bookingType: BookingType
  email?: string
}
