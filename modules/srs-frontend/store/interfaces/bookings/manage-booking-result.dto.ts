import { Booking } from '~/store/interfaces/bookings/bookings'

export enum ManageBookingStatus {
  SUCCESS = 'SUCCESS',
  PARTIAL = 'PARTIAL',
}

export interface ManageBookingResultDto {
  readonly status: ManageBookingStatus
  readonly booking: Booking
  readonly errorMessage: string
}
