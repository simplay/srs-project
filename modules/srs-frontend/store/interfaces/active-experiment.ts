import { Experiment } from '~/store/interfaces/experiment'
import { Participant } from '~/store/interfaces/participant'

export interface ActiveExperiment {
  isRecording: boolean
  experiment: Experiment
  participant: Participant
  taskUuid: string
  trialNbr: number
}
