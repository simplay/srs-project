export enum Role {
  Admin = 'admin',
  Operator = 'operator',
  Secretariat = 'secretariat',
}

export interface User {
  firstname: string
  lastname: string
  email: string
  username: string
  roles: Role[]
  createdAt: number
  updatedAt: number
}
