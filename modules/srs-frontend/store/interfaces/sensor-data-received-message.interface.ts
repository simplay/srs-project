import { SensorType } from '~/store/enums/sensor-type.enum'
import { CameraDto, DoorDto } from '~/store/interfaces/sensors-dto'

export interface SensorLastActivity {
  ip: string
  lastSampleReceivedAt: number
  sample: CameraDto | DoorDto | null
}

export interface SensorTypeLastReceivedMessage {
  timestamp: number
  message: string
  numberSamplesSaved: number
  numberSamplesSkipped: number
  sensorType: SensorType
  sensorsLastActivity: SensorLastActivity[]
}
