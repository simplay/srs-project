import { Participant } from '~/store/interfaces/participant'
import { User } from '~/store/interfaces/user'
import { Config } from '~/store/interfaces/config'
import { Task } from '~/store/interfaces/task'

export interface Experiment {
  uuid: string
  name: string
  participants: Participant[]
  owner: User
  operators: User[]
  config: Config
  tasks: Task[]
  createdAt: number
  startedAt: number
  completedAt: number
  completed: boolean
}
