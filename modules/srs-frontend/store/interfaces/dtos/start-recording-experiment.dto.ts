export interface StartRecordingExperimentDto {
  participantUuid: string
  taskUuid: string
}
