export interface CreateExperimentDto {
  uuid: string
  name: string
  configUuid: string
  ownerUsername: string
}
