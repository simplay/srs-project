import { SensorType } from '~/store/enums/sensor-type.enum'
import { Sensor } from '~/store/interfaces/sensors'

export interface UpdateConfigDto {
  name: string
  sensorsPerType: Record<SensorType, Sensor[]>
}
