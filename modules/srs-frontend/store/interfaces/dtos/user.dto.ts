import { Role } from '~/store/interfaces/user'

export interface UserDto {
  firstname: string
  lastname: string
  username: string
  email: string
  roles: Role[]
  createdAt: number
  updatedAt: number
}
