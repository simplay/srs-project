export interface CreateTaskDto {
  uuid: string
  name: string
  description: string
}
