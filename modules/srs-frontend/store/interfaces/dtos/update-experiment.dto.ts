export interface UpdateExperimentDto {
  uuid: string
  name: string
  configUuid: string
  ownerUsername: string
}
