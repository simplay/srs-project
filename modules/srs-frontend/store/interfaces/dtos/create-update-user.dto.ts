import { Role } from '~/store/interfaces/user'

export interface CreateUpdateUserDto {
  firstname: string
  lastname: string
  username: string
  email: string
  roles: Role[]
  password: string // password is optional when updating
}
