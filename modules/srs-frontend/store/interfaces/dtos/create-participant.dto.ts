export interface CreateParticipantDto {
  uuid: string
  shortNote: string
}
