export interface CreateExperimentNoteDto {
  logLine: string
  experimentUuid: string
}
