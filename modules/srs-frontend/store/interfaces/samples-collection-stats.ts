export interface SamplesCollectionStats {
  collectionName: string
  numberOfSamples: number
  distinctSensorsIps: string[]
  size: number // Collection size in bytes
  avgObjSize: number // Average object size in bytes
  storageSize: number // (Pre)allocated space for the collection in bytes
}
