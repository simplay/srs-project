export interface Task {
  uuid: string
  name: string
  description: string
}
