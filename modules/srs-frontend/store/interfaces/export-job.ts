import { Experiment } from '~/store/interfaces/experiment'
import { User } from '~/store/interfaces/user'

export enum ExportJobStatus {
  ON_GOING = 'ON_GOING',
  FAILED = 'FAILED',
  SUCCEEDED = 'SUCCEEDED',
}

export interface ExportJob {
  uuid: string
  experiment: Experiment
  createdAt: number
  completedAt: number
  status: ExportJobStatus
  createdBy: User
  mongodumpLog: string[]
  duration: number
}
