import { UserDto } from '~/store/interfaces/dtos/user.dto'

export interface Participant {
  uuid: string
  shortNote: string
  createdAt: number
  createdBy: UserDto
}
