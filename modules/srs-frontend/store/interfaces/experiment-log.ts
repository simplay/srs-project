export interface ExperimentLog {
  datetime: number
  log: string
  username: string | null
  experimentUuid: string | null
  tags: string[]
}
