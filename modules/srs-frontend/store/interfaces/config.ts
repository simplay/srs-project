import { SensorType } from '~/store/enums/sensor-type.enum'
import { Sensor } from '~/store/interfaces/sensors'

export interface Config {
  uuid: string
  name: string
  createdAt: number
  createdByUsername: string
  sensorsPerType: Record<SensorType, Sensor[]>
}
