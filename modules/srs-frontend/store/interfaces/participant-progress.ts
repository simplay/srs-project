export interface ParticipantProgress {
  experimentUuid: string
  participantUuid: string
  taskUuid: string
  trialCounter: number
}
