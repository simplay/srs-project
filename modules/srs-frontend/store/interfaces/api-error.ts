export class ApiError {
  topic: string
  code: number
  codeExplanation: string
  detail: string

  constructor() {
    this.topic = ''
    this.code = -1
    this.codeExplanation = ''
    this.detail = ''
  }
}
