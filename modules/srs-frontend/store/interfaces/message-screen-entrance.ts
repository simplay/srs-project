export interface MessageScreenEntrance {
  message: string
  updatedAt: number
  createdByUserFullName: string
}
