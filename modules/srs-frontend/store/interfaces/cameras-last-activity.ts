import { CameraDto, NightVisionCameraDto } from '~/store/interfaces/sensors-dto'
import { CameraType } from '~/store/enums/camera-type.enum'

export interface CameraLastSample {
  ip: string
  receivedAt: number
  sample: CameraDto | NightVisionCameraDto
}

export interface LastActivityForCameraType {
  cameraType: CameraType
  camerasLastSample: CameraLastSample[]
}
