export interface ActiveExperimentPublicInfo {
  experimentName: string
  isRecording: boolean
}
