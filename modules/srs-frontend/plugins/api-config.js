export default Object.freeze({
  API_SENSOR_MONITORING: {
    // Endpoints definitions

    // Users
    USERS_ENDPOINT: '/users',

    // Authentication
    AUTH_ENDPOINT: '/auth/login',
    PROFILE_ENDPOINT: '/auth/profile',

    // Sensors
    SENSORS_ENDPOINT: '/api/sensors',

    // Samples
    SAMPLES_COLLECTIONS_STATS: '/samples/collections-stats',
    DELETE_SAMPLES_COLLECTIONS_ENDPOINT: '/samples/drop-samples-collections',
    CREATE_EXPORT_JOB_ENDPOINT: '/samples/export-job',
    GET_EXPORT_JOB_ENDPOINT: '/samples/export-job/[EXPORT_JOB_UUID]',
    GET_ACTIVE_EXPORT_JOB_ENDPOINT: '/samples/active-export-job',

    // Configs
    CONFIGS_ENDPOINT: '/configs',

    // Participants
    PARTICIPANTS_ENDPOINT: '/participants',

    // Experiments
    EXPERIMENTS_ENDPOINT: '/experiments',
    EXPERIMENTS_ADD_PARTICIPANTS_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/add-participants',
    EXPERIMENTS_REMOVE_PARTICIPANT_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/remove-participant',
    EXPERIMENTS_ADD_OPERATORS_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/add-operators',
    EXPERIMENTS_REMOVE_OPERATOR_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/remove-operator',
    EXPERIMENTS_ADD_TASK_ENDPOINT: '/experiments/[EXPERIMENT_UUID]/add-task',
    EXPERIMENTS_TASK_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/task/[TASK_UUID]',
    EXPERIMENTS_GET_ACTIVE_ENDPOINT: '/experiments/active',
    EXPERIMENTS_SET_ACTIVE_ENDPOINT: '/experiments/set-active',
    EXPERIMENTS_START_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/start-recording',
    EXPERIMENTS_UPDATE_TASK_CURRENT_RECORDING_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/update-task',
    EXPERIMENT_INCREMENT_TRIAL_CURRENT_RECORDING_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/increment-trial-current-recording',
    EXPERIMENTS_STOP_ENDPOINT: '/experiments/[EXPERIMENT_UUID]/stop-recording',
    EXPERIMENTS_COMPLETE_EXPERIMENT_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/complete-experiment',
    EXPERIMENTS_GET_PARTICIPANTS_PROGRESS_FOR_EXPERIMENT_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/participants-progress',
    EXPERIMENTS_MESSAGE_SCREEN_ENTRANCE_ENDPOINT:
      '/experiments/message-screen-entrance',
    EXPERIMENTS_ACTIVE_EXPERIMENT_PUBLIC_INFO_ENDPOINT:
      '/experiments/active-experiment-public-info',
    EXPERIMENTS_SAVE_NOTE_FOR_EXPERIMENT_ENDPOINT:
      '/experiments/[EXPERIMENT_UUID]/note',

    // LogBook
    LOG_BOOK_ENDPOINT: '/log-book',
    LOG_BOOK_INFO_ENDPOINT: '/log-book/info',

    // Bookings
    BOOKINGS_ENDPOINT: '/bookings',
    BOOKINGS_ROOMS_ENDPOINT: '/bookings/rooms',
    BOOKINGS_FOR_ROOM_ENDPOINT: '/bookings/[ROOM_UUID]',
  },
})
