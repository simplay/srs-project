import { Plugin } from '@nuxt/types'
import Vue from 'vue'

export class ValidationRules {
  translator: any

  constructor(readonly i18n: any) {
    this.translator = i18n
  }

  readonly FIRSTNAME_MIN_LENGTH = 3
  readonly LASTNAME_MIN_LENGTH = 3
  readonly PHONE_MASK = '+## ## ### ## ##'
  readonly PHONE_LENGTH = 16 // includes + and whitespaces
  readonly PASSWORD_MIN_LENGTH = 6
  readonly USERNAME_MIN_LENGTH = 4
  readonly UUID_LENGTH = 36
  readonly PARTICIPANT_SHORT_NOTE_MIN_LENGTH = 3
  readonly CONFIG_NAME_MIN_LENGTH = 4
  readonly EXPERIMENT_NAME_MIN_LENGTH = 4
  readonly ROOM_NAME_MIN_LENGTH = 3

  readonly FIRSTNAME = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.firstname_required'),
    (v: string) =>
      (v && v.length >= this.FIRSTNAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.firstname_requirement', {
        nbrChars: this.FIRSTNAME_MIN_LENGTH,
      }),
  ]

  readonly LASTNAME = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.lastname_required'),
    (v: string) =>
      (v && v.length >= this.LASTNAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.lastname_requirement', {
        nbrChars: this.LASTNAME_MIN_LENGTH,
      }),
  ]

  readonly EMAIL = [
    (v: string) => !!v || this.translator.t('validation_rules.email_required'),
    (v: string) =>
      /.+@.+/.test(v) ||
      this.translator.t('validation_rules.email_requirement'),
  ]

  readonly EMAIL_IF_SET = [
    (v: string) =>
      /.+@.+/.test(v) ||
      this.translator.t('validation_rules.email_requirement_if_set'),
  ]

  PHONE = [
    (v: string) => !!v || this.translator.t('validation_rules.phone_required'),
    (v: string) =>
      (v && v.length === this.PHONE_LENGTH) ||
      this.translator.t('validation_rules.phone_requirement'),
  ]

  PASSWORD = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.password_required'),
    (v: string) =>
      (v && v.length >= this.PASSWORD_MIN_LENGTH) ||
      this.translator.t('validation_rules.password_requirement', {
        nbrChars: this.PASSWORD_MIN_LENGTH,
      }),
  ]

  PASSWORD_IF_SET = [
    (v: string) =>
      (v && v.length >= this.PASSWORD_MIN_LENGTH) ||
      this.translator.t('validation_rules.password_requirement_if_set', {
        nbrChars: this.PASSWORD_MIN_LENGTH,
      }),
  ]

  USERNAME = [
    (v: string) =>
      (v && /^[a-zA-Z]/.test(v)) ||
      this.translator.t(
        'validation_rules.username_requirement_start_with_letter'
      ),
    (v: string) =>
      (v && /^[a-zA-Z0-9._-]*$/.test(v)) ||
      this.translator.t('validation_rules.username_requirement_charachters'),
    (v: string) =>
      !!v || this.translator.t('validation_rules.username_required'),
    (v: string) =>
      (v && v.length >= this.USERNAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.username_requirement_length', {
        nbrChars: this.USERNAME_MIN_LENGTH,
      }),
  ]

  PARTICIPANT_UUID = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.participant_uuid_requirement'),
    (v: string) =>
      (v && v.length === this.UUID_LENGTH) ||
      this.translator.t('validation_rules.participant_uuid_requirement'),
  ]

  PARTICIPANT_SHORT_NOTE = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.participant_short_required'),
    (v: string) =>
      (v && v.length >= this.PARTICIPANT_SHORT_NOTE_MIN_LENGTH) ||
      this.translator.t('validation_rules.participant_short_note_requirement', {
        nbrChars: this.PARTICIPANT_SHORT_NOTE_MIN_LENGTH,
      }),
  ]

  TASK_UUID = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.task_uuid_requirement'),
    (v: string) =>
      (v && v.length === this.UUID_LENGTH) ||
      this.translator.t('validation_rules.task_uuid_requirement'),
  ]

  CONFIG_NAME = [
    (v: string) =>
      !!v ||
      this.translator.t('validation_rules.config_name_requirement', {
        nbrChars: this.CONFIG_NAME_MIN_LENGTH,
      }),
    (v: string) =>
      (v && v.length >= this.CONFIG_NAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.config_name_requirement', {
        nbrChars: this.CONFIG_NAME_MIN_LENGTH,
      }),
  ]

  EXPERIMENT_NAME = [
    (v: string) =>
      !!v ||
      this.translator.t('validation_rules.experiment_name_requirement', {
        nbrChars: this.EXPERIMENT_NAME_MIN_LENGTH,
      }),
    (v: string) =>
      (v && v.length >= this.CONFIG_NAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.experiment_name_requirement', {
        nbrChars: this.EXPERIMENT_NAME_MIN_LENGTH,
      }),
  ]

  EXPERIMENT_UUID = [
    (v: string) =>
      !!v || this.translator.t('validation_rules.experiment_uuid_requirement'),
    (v: string) =>
      (v && v.length === this.UUID_LENGTH) ||
      this.translator.t('validation_rules.experiment_uuid_requirement'),
  ]

  ROOM_NAME = [
    (v: string) =>
      !!v ||
      this.translator.t('validation_rules.room_name_requirement', {
        nbrChars: this.ROOM_NAME_MIN_LENGTH,
      }),
    (v: string) =>
      (v && v.length >= this.ROOM_NAME_MIN_LENGTH) ||
      this.translator.t('validation_rules.room_name_requirement', {
        nbrChars: this.ROOM_NAME_MIN_LENGTH,
      }),
  ]

  isValidEmail(email: string): boolean {
    const re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }

  isValidUsername(s: string): boolean {
    return /^[a-zA-Z0-9._-]*$/.test(s)
  }

  stringStartsWithLetter(s: string): boolean {
    return /^[a-zA-Z]/.test(s)
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $validationRules: ValidationRules
  }
}

const validationRulesPlugin: Plugin = (context) => {
  const i18n = context.app.i18n
  Vue.prototype.$validationRules = new ValidationRules(i18n)
}

export default validationRulesPlugin
