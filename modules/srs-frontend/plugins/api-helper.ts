import { AxiosError } from 'axios'
import { ApiError } from '~/store/interfaces/api-error'

function getExplanationFromStatusCode(status: number) {
  let codeExplanation = ''
  switch (status) {
    case 400:
      codeExplanation = 'Bad request'
      break
    case 401:
      codeExplanation = 'Unauthorized'
      break
    case 403:
      codeExplanation = 'Forbidden'
      break
    case 404:
      codeExplanation = 'Not found'
      break
    case 405:
      codeExplanation = 'Method not allowed'
      break
    case 406:
      codeExplanation = 'Not acceptable'
      break
    case 422:
      codeExplanation = 'Unprocessable Entity'
      break
    case 500:
      codeExplanation = 'Internal server error'
      break
    case 502:
      codeExplanation = 'Bad gateway'
      break

    default:
      codeExplanation = 'Unhandled error'
      break
  }
  return codeExplanation
}

export function analyseErr(err: AxiosError): ApiError {
  const error = new ApiError()

  if (err.response) {
    // The request was made and the server responded with a status code
    error.code = err.response.status
    error.detail = err.response.data
    error.codeExplanation = getExplanationFromStatusCode(error.code)
  } else if (err.request) {
    // The request was made but no response was received

    const status = err.request.status
    error.code = status

    console.log(status)
    if (status === 0) {
      // Network Error
      error.codeExplanation = 'Network Error'
    } else {
      error.codeExplanation = getExplanationFromStatusCode(error.code)
    }

    // Check if more info is given
    // Warning: The err.request.response can potentially be null
    try {
      if (
        'response' in err.request &&
        err.request.response !== null &&
        err.request.response.length > 0
      ) {
        const response = JSON.parse(err.request.response)
        if ('errorMessage' in response) {
          error.detail = response.errorMessage
        } else {
          error.detail = response
        }
      }
    } catch (error) {
      console.log('Error parsing JSON response:')
      console.log(error)
    }
  }
  return error
}
