import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    key: 'sensor-monitoring-dashboard',
  })(store)
}
