import Vue from 'vue'
import { TinyEmitter } from 'tiny-emitter'
const emitter = new TinyEmitter()

declare module 'vue/types/vue' {
  interface Vue {
    $events: TinyEmitter
  }
}

Vue.prototype.$events = emitter
