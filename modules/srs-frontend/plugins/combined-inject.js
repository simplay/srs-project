// Injecting methods allow to make them available in the context, vue instances and store
// The $ sign will be prepended automatically to the function
// https://nuxtjs.org/guide/plugins/#combined-inject

export default ({ app, store }, inject) => {
  inject('isJWTValid', (jwt) => {
    try {
      const jwtData = JSON.parse(atob(jwt.split('.')[1]))

      // We could do some more checks on the JWT here (e.g. rights, roles or whatever we inject in the JWT)

      // Check if jwt has not expired
      const currentTime = Date.now() / 1000
      if (jwtData.exp < currentTime) {
        console.log('JWT has expired')
        return false
      }
      return true
    } catch (error) {
      return false
    }
  })
}
