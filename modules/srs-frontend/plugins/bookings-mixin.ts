import Vue from 'vue'
import { BookingType } from '~/store/enums/booking-type.enum'

// We define here some methods related to the bookings topic that we will use in several components

// Inject into Vue instances
// https://typescript.nuxtjs.org/cookbook/plugins

declare module 'vue/types/vue' {
  interface Vue {
    $getBookingColor(bookingType: BookingType): string
  }
}

Vue.prototype.$getBookingColor = (bookingType: BookingType): string => {
  // Colors: https://vuetifyjs.com/en/styles/colors/#material-colors

  let color = 'grey'
  if (bookingType === BookingType.EXPERIMENT) {
    color = 'blue'
  } else if (bookingType === BookingType.CLEANING) {
    color = 'orange'
  } else if (bookingType === BookingType.MAINTENANCE) {
    color = 'green lighten-3'
  }
  return color
}
