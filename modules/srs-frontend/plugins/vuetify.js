import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import options from '@/vuetify.options.js'
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify, { ...options })

export default (ctx) => {
  const vuetify = new Vuetify(options)

  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
  ctx.$vuetify.theme.dark = false
  ctx.$vuetify.theme.light = true
}
