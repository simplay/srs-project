import Vue from 'vue'

declare module 'vue/types/vue' {
  interface Vue {
    $getFormattedDateTime(timestampInSeconds: number): string
    $getFormattedTimeForCalendar(timestampInMs: number): string
  }
}

Vue.prototype.$getFormattedDateTime = (timestampInSeconds: number): string => {
  return new Date(timestampInSeconds * 1000).toLocaleString()
}

Vue.prototype.$getFormattedTimeForCalendar = (
  timestampInMs: number
): string => {
  return new Date(timestampInMs).toLocaleTimeString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit',
  })
}
