const isAuthenticationExpired = (response, store) => {
  const code = response.status

  return store.state.auth.jwt && code === 401
}

export default function ({ $axios, redirect, store }) {
  $axios.onRequest((config) => {
    // Set JWT token to each request
    if (store.state.auth.jwt !== null) {
      config.headers.common.Authorization = `Bearer ${store.state.auth.jwt}`
    }

    console.log(`[${config.method.toUpperCase()}] ${config.url}`)
  })

  $axios.onResponse((response) => {
    if (isAuthenticationExpired(response, store)) {
      store.dispatch('auth/LOGOUT')
      redirect('/')
    }
  })
}
