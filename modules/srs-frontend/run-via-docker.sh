#!/bin/sh

# Stop on failure
set -e

# Load env variables
source .env

image_name="sensor_monitoring_dashboard"
container_name=$image_name

docker rm -f $container_name

docker build -t $image_name .

# We only use runtime env variables
docker run -it \
--env SENSOR_MONITORING_SERVICE_API_URL=$SENSOR_MONITORING_SERVICE_API_URL \
--env SENSOR_MONITORING_SERVICE_AUTHENTICATED_WEBSOCKETS_URL=$SENSOR_MONITORING_SERVICE_AUTHENTICATED_WEBSOCKETS_URL \
--env SENSOR_MONITORING_SERVICE_OPEN_WEBSOCKETS_URL=$SENSOR_MONITORING_SERVICE_OPEN_WEBSOCKETS_URL \
-p 3000:3000 -d --name $container_name $image_name
