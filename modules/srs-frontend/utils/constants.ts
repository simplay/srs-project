export const NAMESPACE_WEBSOCKET_SENSORS = 'sensors'
export const NAMESPACE_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS =
  'active-experiment-status'
export const NAMESPACE_WEBSOCKET_CAMERAS = 'cameras'
