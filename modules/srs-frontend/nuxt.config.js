import colors from 'vuetify/es5/util/colors'

const {
  NAME_WEBSOCKET_SENSORS,
  NAME_WEBSOCKET_ACTIVE_EXPERIMENT_STATUS,
  NAME_WEBSOCKET_CAMERAS,
  // eslint-disable-next-line nuxt/no-cjs-in-config
} = require('./utils/constants')

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - sensor-monitoring-dashboard',
    title: 'sensor-monitoring-dashboard',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/i18n.js',
    '@/plugins/vuetify.js',
    '@/plugins/validation-rules.ts',
    '@/plugins/combined-inject.js',
    '@/plugins/axios.js',
    '@/plugins/formatting-mixin.ts',
    '@/plugins/bookings-mixin.ts',
    '@/plugins/persisted-state.js',
    '@/plugins/event-bus.ts',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  publicRuntimeConfig: {
    axios: {
      // Defines the base URL which is used and prepended to make client side requests.
      // See: https://go.nuxtjs.dev/config-axios
      browserBaseURL: process.env.SENSOR_MONITORING_SERVICE_API_URL,
    },
    sensorMonitoringServiceAuthenticatedWebsocketsURL:
      process.env.SENSOR_MONITORING_SERVICE_AUTHENTICATED_WEBSOCKETS_URL,
    sensorMonitoringServiceOpenWebsocketsURL:
      process.env.SENSOR_MONITORING_SERVICE_OPEN_WEBSOCKETS_URL,
  },
}
