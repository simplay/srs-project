import { Middleware } from '@nuxt/types'
import { AuthModuleState } from '~/store/auth'
import { Role } from '~/store/interfaces/user'

const secretariatRoleMiddleware: Middleware = async ({
  store,
  redirect,
  app,
}) => {
  const authModuleState: AuthModuleState = store.state.auth as AuthModuleState

  if (!authModuleState.jwt) {
    return redirect('/login')
  }

  // Check if token is still valid (didn't expired)
  const jwt = authModuleState.jwt

  if (jwt === null) {
    return redirect('/login')
  }

  if (
    !app.$isJWTValid(jwt) ||
    !authModuleState.user?.roles.includes(Role.Secretariat)
  ) {
    await store.dispatch('auth/LOGOUT')
    await store.dispatch('auth/JWT_EXPIRED')
    return redirect('/login')
  }
}

export default secretariatRoleMiddleware
