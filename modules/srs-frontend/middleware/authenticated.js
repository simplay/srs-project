// Allow to restrict protected page to users which are logged in only
export default async function ({ store, redirect, app }) {
  if (!store.state.auth.jwt) {
    return redirect('/login')
  }

  // Check if token is still valid (didn't expired)
  const jwt = store.state.auth.jwt

  if (jwt === null) {
    return redirect('/login')
  }

  if (!app.$isJWTValid(jwt)) {
    await store.dispatch('auth/LOGOUT')
    await store.dispatch('auth/JWT_EXPIRED')
    return redirect('/login')
  }
}
