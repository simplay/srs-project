// Allow to restrict login and register pages only to users that are not logged in
export default function ({ store, redirect }) {
  if (store.state.auth.authenticated) {
    return redirect('/')
  }
}
