# Pipeline Dump Processor

## Setup

```
sudo apt-get install zlib1g-dev
```

## Initialize your local DB

Download an exported DB dump from the pipeline. To load a collection, say the seismograph samples, into your local DB,
execute the following mongo utility script:

```
mongorestore --archive=seismograph_samples_2021-07-23.dump --host 192.168.1.13 --username admin --password admin --verbose
```

This example assumes, that the seismograph samples are stored in the file **seismograph_samples_2021-07-23.dump** and
the ip address of your local DB server is **192.168.1.13**.

## Install Dependencies

```
pip3 install -r requirements.txt
```

## Usage

```
`python3 run.py -u mongodb://mongodb_user:123456@localhost:27017 -o ~/playground -s ./example.sensors.json` -w json
```

### Runtime Arguments

| Argument Name  | Description                                                         | Example                                                               |
|----------------|---------------------------------------------------------------------|-----------------------------------------------------------------------|
| --url          | MongoDB connection                                                  | "mongodb://mongodb_user:123456@localhost:27017"                       |
| --out-dir      | Location where the database files should be dumped to.              | Some path on your system, by default the `ROOT_PATH`                  |
| --sensor-file  | Path to sensor file that defines which collections should be dumped | Path to a Neurotec sensor file, this file is required                 |
| --writer-class | Target writer class                                                 | either `json`or `parquet`                                             |
| --db_name      | Value for the MongoDB attribute `db_name`                           | By default this is `pipeline_prod` but sometimes it is `pipeline_dev` |
| --quiet        | Do not print status messages to stdout.                             | By default, we are not quiet.                                         |
| --analyze      | Create quality statistics of parquet files                          | Has to be run stand-alone                                             |

### Dumping data for microphones

1. Dump the chunks from the database. This will export wav files (for RE_SPEAKER and NT_USB mics) and text files
   (for RE_SPEAKER_DOA - direction of arrival) in a folder structure like: `[OUTPUT_DIR]/[PARTICIPANT_UUID]/[TASK_UUID]`

   > Note: make sure to use the correct `sensors.json` file

    ```sh
    python3 run.py -u mongodb://mongodb_user:123456@localhost:27017 -o ~/ -s ./example.sensors.json -w mic -d pipeline_dev
    ```

2. Build the complete files for each task from the chunks using the following script:

    ```sh
    python3 mongodumpprocessor/utils/build_microphones_files_from_chunks.py \
            --audio_dir ~/playground/out/audio_samples/ \
            -p 902d6230-c6a4-4e7e-9950-21738a69c15c \
            -t d1101ca2-a675-424a-9259-e706b94a83bc \
            --devices 192.168.1.100,192.168.1.101
    ```
   See the output of the script for the final files produced.
