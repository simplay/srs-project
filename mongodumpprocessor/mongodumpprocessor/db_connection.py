import pymongo.cursor
from pymongo import MongoClient

from mongodumpprocessor.writers import JsonWriter, ParquetWriter, MicrophoneWriter


class ConnectionSettings:
    def __init__(self, url: str, db_name: str):
        """
        :param url:
        :param db_name:
        """

        self.url = url
        self.db_name = db_name


class DbConnection:
    def __init__(self, settings: ConnectionSettings):
        """
        :param settings:
        """

        self.client = MongoClient(settings.url)
        self.db = self.client[settings.db_name]

    def select_by_ip(self, collection_name: str, ip_address: str) -> pymongo.cursor.Cursor:
        """
        :param collection_name:
        :param ip_address:
        :return:
        """

        return self.db.get_collection(collection_name).find({"ip": ip_address})

    def save_samples_as_json(self,
                             db_collection_name: str,
                             ip_address: str,
                             writer_params: dict):
        documents = self.select_by_ip(
            collection_name=db_collection_name,
            ip_address=ip_address
        )

        filename = f"{db_collection_name}_{ip_address}.json"
        JsonWriter(
            ip_address=ip_address,
            root_path=writer_params["output_base_path"],
            output_filename=filename
        ).dump(documents)

    def save_samples_as_parquet(self,
                                db_collection_name: str,
                                ip_address: str,
                                writer_params: dict):
        documents = self.select_by_ip(
            collection_name=db_collection_name,
            ip_address=ip_address
        )
        ParquetWriter(
            ip_address=ip_address,
            collection_name=db_collection_name,
            arrow_definition=writer_params["arrow_definition"],
        ).dump(documents)

    def save_samples_microphones(self,
                                 db_collection_name: str,
                                 ip_address: str,
                                 writer_params: dict):
        documents = self.select_by_ip(
            collection_name=db_collection_name,
            ip_address=ip_address
        )

        MicrophoneWriter(
            ip_address=ip_address,
            output_path=writer_params["output_base_path"],
        ).dump(documents)
