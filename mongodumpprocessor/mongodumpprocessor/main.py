import logging
import os

from datetime import date
from optparse import OptionParser
from pathlib import Path

from mongodumpprocessor.collection_processor import CollectionProcessor, SampleWriters
from mongodumpprocessor.data_quality_analyzer import DataQualityAnalyzer, SeismoDataQualityAnalyzer
from mongodumpprocessor.db_connection import ConnectionSettings, DbConnection
from mongodumpprocessor.sensor_reader import SensorReader

ROOT_PATH = Path(__file__).parent.parent


def main():
    parser = OptionParser()

    default_db_url = "mongodb://mongodb_user:123456@localhost:27017"
    parser.add_option(
        "-u",
        "--url",
        dest="url",
        help="Mongo database connection",
        default=default_db_url
    )

    parser.add_option(
        "-a",
        "--analyze",
        dest="analyze",
        help="Path of directory that contains parquet files",
        default=None
    )

    parser.add_option(
        "-o",
        "--out-dir",
        dest="output_path",
        default=ROOT_PATH,
        help="path where the database files should be dumped."
    )

    parser.add_option(
        "-s",
        "--sensor-file",
        dest="sensor_filepath",
        help="Path to sensor file that defines which collections should be dumped."
    )

    parser.add_option(
        "-w",
        "--writer",
        dest="writer_class",
        help="Target writer class",
        default="json"
    )

    parser.add_option(
        "-d",
        "--db_name",
        dest="db_name",
        help="value for MongoDB db_name",
        default="pipeline_prod"
    )

    parser.add_option(
        "-q",
        "--quiet",
        action="store_false",
        dest="verbose",
        default=True,
        help="Do not print status messages to stdout"
    )

    (options, args) = parser.parse_args()

    today = date.today()
    current_date = today.strftime("%d_%m_%y")
    log_path = os.path.join(ROOT_PATH, 'logs', f"status_{current_date}.log")
    log_handlers = [
        logging.FileHandler(log_path, mode="a", encoding="utf8"),
    ]

    if options.verbose:
        log_handlers.append(
            logging.StreamHandler()
        )

    logging.basicConfig(
        handlers=log_handlers,
        format="%(asctime)s.%(msecs)03d | %(levelname)s | %(message)s",
        datefmt="%d-%m-%Y %H:%M:%S",
        level=logging.DEBUG
    )

    if options.analyze:
        DataQualityAnalyzer.analyze(base_filepath=options.analyze)
        return

    if not options.sensor_filepath:
        parser.error('Sensor Filepath not provided. Add --sensor-filepath PATH/TO/SENSORS_FILE.json')

    logging.info(f"Using the following parameters:")

    for parameter_name, parameter_value in vars(options).items():
        logging.info(f"  {parameter_name}={parameter_value}")

    settings = ConnectionSettings(options.url, db_name=options.db_name)
    db_connection = DbConnection(settings)

    sensor_list = SensorReader.read(options.sensor_filepath)
    logging.info(f"Reading sensors from {sensor_list}")

    writer_class = SampleWriters(options.writer_class)

    # TODO: rename to process_database
    CollectionProcessor.save_db_to_files(
        db_connection,
        sensor_config=sensor_list,
        sample_writer=writer_class,
        output_path=options.output_path,
    )

    DataQualityAnalyzer.analyze(base_filepath=options.output_path)


if __name__ == '__main__':
    main()
