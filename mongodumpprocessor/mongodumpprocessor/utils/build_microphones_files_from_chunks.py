# This script appens the waf files for each microphone types in a directory
# E.g: python3 mongodumpprocessor/utils/build_microphones_files_from_chunks.py \
# --audio_dir ./playground/out/audio_samples/ \
# -p 902d6230-c6a4-4e7e-9950-21738a69c15c \
# -t d1101ca2-a675-424a-9259-e706b94a83bc \
# --devices 192.168.1.101,192.168.1.102

import wave
import glob
import os

from optparse import OptionParser


def main():

    parser = OptionParser()

    required_params = ['audio_dir', 'participant_uuid', 'task_uuid', 'devices']

    parser.add_option(
        "--audio_dir",
        dest="audio_dir",
        help="Root directory containing the audios",
    )

    parser.add_option(
        "-p",
        "--participant_uuid",
        dest="participant_uuid",
        help="Participant uuid",
    )

    parser.add_option(
        "-t",
        "--task_uuid",
        dest="task_uuid",
        help="Task uuid",
    )

    parser.add_option(
        "--devices",
        dest="devices",
        help="List of microphone devices (comma separated) for which to append the wav files",
    )

    (options, args) = parser.parse_args()

    # Check required params
    for required_param in required_params:
        if options.__dict__[required_param] is None:
            parser.error("parameter %s required" % required_param)
            exit(1)

    audio_dir = options.audio_dir
    participant_uuid = options.participant_uuid
    task_uuid = options.task_uuid
    devices = options.devices.split(',')

    data_types = ['NT_USB', 'RE_SPEAKER', 'RE_SPEAKER_DOA']

    dir_audios = os.path.join(audio_dir, participant_uuid, task_uuid)

    # Loop through the data types and
    for data_type in data_types:
        for device in devices:

            # The mongodump process outputs wave with "." replaced with underscores
            device_formatted = str(device).replace('.', '_')

            if data_type == "RE_SPEAKER_DOA":
                # Append text files with the Direction of Arrival (DOA)
                path = os.path.join(dir_audios, f'device_{device_formatted}_{data_type}_*.txt')
                in_files = sorted(glob.glob(path))

                if len(in_files) > 0:
                    doa_lines = []
                    for in_file in in_files:
                        with open(in_file, "r") as f:
                            doa_lines.append(f.read())

                    output_path = os.path.join(dir_audios, f'device_{device_formatted}_{data_type}.txt')

                    with open(output_path, "w") as f:
                        f.writelines(doa_lines)

                    print(f'Wrote doa file for device {device}')
                else:
                    print(f'No doa files found for device {device}')

            else:
                # Append wave files
                path = os.path.join(dir_audios, f'device_{device_formatted}_{data_type}_*.wav')
                in_files = sorted(glob.glob(path))

                if len(in_files) > 0:
                    data = []
                    for in_file in in_files:
                        w = wave.open(in_file, 'rb')
                        data.append([w.getparams(), w.readframes(w.getnframes())])
                        w.close()

                    output_path = os.path.join(dir_audios, f'device_{device_formatted}_{data_type}.wav')
                    output = wave.open(output_path, 'wb')
                    output.setparams(data[0][0])
                    for y in range(len(data)):
                        output.writeframes(data[y][1])
                    output.close()

                    print(f'Wrote wav file for data type {data_type} for device {device}')
                else:
                    print(f'No wav files found for data type {data_type} for device {device}')


if __name__ == '__main__':
    main()
