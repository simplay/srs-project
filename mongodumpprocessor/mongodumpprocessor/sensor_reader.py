import json
import logging


class SensorReader:
    @classmethod
    def read(cls, filepath: str) -> dict:
        """
        :param filepath:
        :return: read sensor file
        """

        logging.info(filepath)
        with open(filepath, "r") as file:
            sensor_list = json.load(file)

            collection_list = {}
            for sensor_type, data in sensor_list.items():
                collection_name = sensor_type.replace("sensors", "samples")
                if collection_name == "shelly_red_samples":
                    collection_name = "shellyred_samples"

                if collection_name == "shelly_em_samples":
                    collection_name = "shellyem_samples"

                collection_list[collection_name] = [d["ip"] for d in data]

            return collection_list
