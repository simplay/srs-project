import json
import logging
import zlib
import os
import time
import pandas as pd
from datetime import date
import base64

import pyarrow
import pyarrow as pa
import pyarrow.parquet as pq

from os import listdir
from os.path import isfile, join

TABLE_DIR_NAME = "table"
STARTUP_TIMESTAMP = int(round(time.time() * 1000))


class JsonWriter:
    def __init__(self, ip_address: str, root_path: str, output_filename: str):
        self.ip_address = ip_address
        self.root_path = root_path
        self.output_filename = output_filename

    def dump(self, documents):
        sample_count = documents.count()

        samples = []
        for local_document_index, document in enumerate(documents, start=1):
            if local_document_index % 100 == 0:
                progress = "{:.3f}".format(100 * (local_document_index / sample_count))
                print(f"\r{local_document_index} / {sample_count} [{progress}%]", end="")

            blob = document["blob"]
            binary_sample = zlib.decompress(blob)
            sample = binary_sample.decode("utf-8")
            sample = json.loads(sample)
            samples.append(sample)

            # if local_document_index > 1000:
            #     break

        self.write_json(self.root_path, self.output_filename, samples)

    def write_json(self, root_path: str, filename: str, samples: list) -> None:
        today = date.today()
        current_date = today.strftime("%d_%m_%y")

        current_date_directory = os.path.join(
            root_path, "out", f"json_samples_{current_date}_{STARTUP_TIMESTAMP}"
        )

        if not os.path.exists(current_date_directory):
            os.makedirs(current_date_directory)
            logging.info(f"Directory {current_date_directory} created")

        filepath = os.path.join(current_date_directory, filename)
        # samples = self.transform(samples)

        with open(filepath, 'w') as file:
            file.write(json.dumps(samples))


class ParquetWriter:
    ITEMS_PER_FILE = 100_000

    def __init__(self, arrow_definition, ip_address: str, collection_name: str):
        self.arrow_definition = arrow_definition
        self.ip_address = ip_address
        self.collection_name = collection_name

    def _write(self,
               samples: list,
               tmp_table_path: str,
               local_document_index: int):

        logging.info(f"Writing {len(samples)} samples to parquet file...")
        if not os.path.exists(tmp_table_path):
            os.mkdir(tmp_table_path)
            logging.info(f"Directory {tmp_table_path} created")

        table_filename = os.path.join(
            tmp_table_path,
            f"{self.collection_name}_{self.ip_address}_{local_document_index}.parquet"
        )

        arrays = self.arrow_definition.arrays_from(samples)
        table = pa.Table.from_arrays(arrays, schema=self.arrow_definition.schema())
        with pq.ParquetWriter(table_filename, self.arrow_definition.schema()) as writer:
            writer.write_table(table)

    def dump(self, documents):

        today = date.today()
        current_date = today.strftime("%d_%m_%y")

        current_path = os.getcwd()
        current_date_directory = os.path.join(
            current_path, "out", TABLE_DIR_NAME, f"parquet_samples_{current_date}_{STARTUP_TIMESTAMP}"
        )

        if not os.path.exists(current_date_directory):
            os.makedirs(current_date_directory)
            logging.info(f"Directory {current_date_directory} created")

        # TODO: clean this up
        current_path = os.getcwd()
        tmp_table_path = current_date_directory  # os.path.join(current_path, "out", TABLE_DIR_NAME, )

        sample_count = documents.count()

        samples = []
        total_iterations = 0
        for local_document_index, document in enumerate(documents, start=1):
            total_iterations += 1
            if local_document_index % 100 == 0:
                progress = "{:.3f}".format(100 * (local_document_index / sample_count))
                print(f"\r{local_document_index} / {sample_count} [{progress}%]", end="")

            if local_document_index % self.ITEMS_PER_FILE == 0:
                self._write(samples, tmp_table_path, local_document_index)

                samples = []

            blob = document["blob"]
            binary_sample = zlib.decompress(blob)
            sample = binary_sample.decode("utf-8")
            sample = json.loads(sample)

            trial_number = None
            if "trialNbr" in document:
                trial_number = document["trialNbr"]

            # TODO: this attribute might not be present in all of the packets.
            #       Think about some proper default fallback mechanism.
            packet_timestamp = None
            if "timestamp" in document:
                packet_timestamp = document["timestamp"]

            task_uuid = ""
            if "taskUuid" in document:
                task_uuid = document["taskUuid"]

            sample["trial_number"] = trial_number
            sample["packet_timestamp"] = packet_timestamp
            sample["task_uuid"] = task_uuid

            samples.append(sample)

        # perform one last final write for samples
        self._write(samples, tmp_table_path, total_iterations + 2)

        skip_combining_files = True
        if skip_combining_files:
            return

        print("done", end="\n")
        self.combine_parquet_files(tmp_table_path)

        onlyfiles = [
            os.path.join(tmp_table_path, f) for f in listdir(tmp_table_path) if
            isfile(join(tmp_table_path, f))
        ]
        for delete_path in onlyfiles:
            os.remove(delete_path)

    def combine_parquet_files(self, combined_table_path: str):
        current_path = os.getcwd()
        onlyfiles = [
            os.path.join(combined_table_path, f) for f in listdir(combined_table_path) if
            isfile(join(combined_table_path, f))
        ]

        onlyfiles.sort()

        frames = []

        file_count = len(onlyfiles)
        base_merged_path = os.path.join(combined_table_path, "combined")

        try:
            os.makedirs(base_merged_path)
        except Exception as error:
            logging.error(f"Path already exists: {error}")

        for file_number, parquet_filepath in enumerate(onlyfiles, 1):
            try:
                if file_number % 10 == 0:
                    progress = "{:.3f}".format(100 * (file_number / file_count))
                    print(f"\r {file_number} / {file_count} [{progress}%]", end="")

                if file_number % 2000 == 0:
                    merged_file_path = os.path.join(
                        base_merged_path,
                        f"{self.collection_name}_{self.ip_address}_{file_number}.parquet"
                    )
                    df_merged = pd.concat(frames)
                    df_merged.to_parquet(merged_file_path, engine='pyarrow')
                    frames = []

                df = pd.read_parquet(parquet_filepath, engine='pyarrow')
                frames.append(df)
            except pyarrow.ArrowInvalid:
                logging.error(f"Invalid {parquet_filepath}")

        if len(frames) > 0:
            df_merged = pd.concat(frames)
            merged_file_path = os.path.join(base_merged_path,
                                            f"{self.collection_name}_{self.ip_address}_{file_number}.parquet")
            logging.info(f"Writing {merged_file_path}")
            df_merged.to_parquet(merged_file_path, engine='pyarrow')

        print("done", end="\n")


class MicrophoneWriter:
    def __init__(self, ip_address: str, output_path: str):
        self.ip_address = ip_address
        self.output_path = output_path

    def dump(self, documents):

        sample_count = documents.count()

        for local_document_index, document in enumerate(documents, start=1):
            if local_document_index % 100 == 0:
                progress = "{:.3f}".format(100 * (local_document_index / sample_count))
                print(f"\r{local_document_index} / {sample_count} [{progress}%]", end="")

            document_id = document['_id']
            participant_uuid = document['participantUuid']
            device_ip = document['ip']
            task_uuid = document['taskUuid']
            compressed_blob = document["blob"]
            blob = zlib.decompress(compressed_blob)
            # See definition of MicrophoneDto in SensorMonitoringService for the format
            microphone_dto = json.loads(blob)
            data_type = microphone_dto['dataType']
            timestamp = microphone_dto['timestamp']
            # The data is stored as a base64 string representation of the binary
            data_bytes = base64.b64decode(microphone_dto['data'])
            filename = f"device_{str(device_ip).replace('.', '_')}_{data_type}_{str(timestamp).replace('.', '_')}_{document_id}"
            # Write wav or text file depending of data type
            if data_type == "RE_SPEAKER_DOA":
                # The direction of arrival (DOA) is stored in a text file
                filename = f"{filename}.txt"
            else:
                filename = f"{filename}.wav"
            self.write_file(self.output_path, participant_uuid, task_uuid, filename, data_bytes)

    # noinspection PyMethodMayBeStatic
    def write_file(self, output_path: str, participant_uuid: str, task_uuid: str, filename: str,
                   data_bytes: bytes) -> None:
        audio_samples_directory = os.path.join(
            output_path, "out", "audio_samples", participant_uuid, task_uuid
        )

        if not os.path.exists(audio_samples_directory):
            os.makedirs(audio_samples_directory)
            logging.info(f"Directory {audio_samples_directory} created")

        filepath = os.path.join(audio_samples_directory, filename)

        with open(filepath, 'wb') as file:
            file.write(data_bytes)
