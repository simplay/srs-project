import logging
import time
import abc

from enum import Enum
from pathlib import Path

import mongodumpprocessor.arrow_definitions as ad
from mongodumpprocessor.db_connection import DbConnection

ROOT_PATH = Path(__file__).parent.parent


class CollectionNames(Enum):
    Samples = "samples"
    Seismograph = "seismograph_samples"
    Lidar = "lidar_samples"
    Door = "door_samples"
    Environment = "environment_samples"
    Luminance = "luminance_samples"
    ShellyEm = "shellyem_samples"
    ShellyRed = "shellyred_samples"
    Waterflow = "waterflow_samples"
    Radar = "radar_samples"
    Mattress = "mattress_samples"
    Microphone = "microphone_samples"


STARTUP_TIMESTAMP = int(round(time.time() * 1000))


class SampleWriters(Enum):
    JSON = "json"
    PARQUET = "parquet"
    MICROPHONE = "mic"


class CollectionProcessor(abc.ABC):

    # TODO: define schema per collection

    @classmethod
    def save_db_to_files(cls,
                         db_connection: DbConnection,
                         sensor_config: dict,
                         sample_writer: SampleWriters,
                         output_path: str = ROOT_PATH) -> None:
        """
        Reads the DB and dumbs the sensor data according to a given sensor_config to output_path

        :param db_connection:
        :param sensor_config:
        :param sample_writer:
        :param output_path:
        """

        # TODO: Use a logger class
        logging.info(f"Writing data to {output_path}")
        for sensor_type, sensor_ips in sensor_config.items():
            for sensor_ip in sensor_ips:
                try:
                    AVAILABLE_PROCESSORS[CollectionNames(sensor_type)](
                        db_connection,
                        root_path=output_path
                    )._process(ip_address=sensor_ip, sample_writer=sample_writer)
                except ValueError as error:
                    logging.error(error)

                logging.info(f"Processed {sensor_type} data for IP {sensor_ip}")

    def __init__(self,
                 connection: DbConnection,
                 collection_name: CollectionNames,
                 root_path: str = ROOT_PATH):
        """
        @param connection connection to the DB
        @param collection_name one of the available Enum values of CollectionNames
        @param root_path path were the output files will be stored
        """

        self.connection = connection
        self.collection_name = collection_name.value
        self.root_path = root_path
        self.sample_writers = {
            SampleWriters.PARQUET.value: self.connection.save_samples_as_parquet,
            SampleWriters.JSON.value: self.connection.save_samples_as_json,
            SampleWriters.MICROPHONE.value: self.connection.save_samples_microphones,
        }

    def arrow_definition(self) -> dict:
        return ARROW_DEFINITIONS[type(self).__name__]

    def _process(self, ip_address: str, sample_writer: SampleWriters):
        logging.info(f"Reading and transforming samples of ip {ip_address} using {sample_writer.value} writer...")

        writer_params = {
            "output_base_path": self.root_path,
            "arrow_definition": self.arrow_definition(),
        }

        writer_class = self.sample_writers[sample_writer.value]
        writer_class(
            db_collection_name=self.collection_name,
            ip_address=ip_address,
            writer_params=writer_params
        )


class SeismographCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Seismograph, root_path)


class LidarCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Lidar, root_path)


class DoorCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Door, root_path)


class EnvironmentCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Environment, root_path)


class LuminanceCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Luminance, root_path)


class ShellyEmCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.ShellyEm, root_path)


class ShellyRedCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.ShellyRed, root_path)


class WaterflowCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Waterflow, root_path)


class RadarCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Radar, root_path)


class MattressCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Mattress, root_path)


class MicrophoneCollectionProcessor(CollectionProcessor):
    def __init__(self, connection: DbConnection, root_path: str = ROOT_PATH):
        super().__init__(connection, CollectionNames.Microphone, root_path)


AVAILABLE_PROCESSORS = {
    CollectionNames.Seismograph: SeismographCollectionProcessor,
    CollectionNames.Lidar: LidarCollectionProcessor,
    CollectionNames.Door: DoorCollectionProcessor,
    CollectionNames.Environment: EnvironmentCollectionProcessor,
    CollectionNames.Luminance: LuminanceCollectionProcessor,
    CollectionNames.ShellyEm: ShellyEmCollectionProcessor,
    CollectionNames.ShellyRed: ShellyRedCollectionProcessor,
    CollectionNames.Waterflow: WaterflowCollectionProcessor,
    CollectionNames.Radar: RadarCollectionProcessor,
    CollectionNames.Mattress: MattressCollectionProcessor,
    CollectionNames.Microphone: MicrophoneCollectionProcessor
}

ARROW_DEFINITIONS = {
    LidarCollectionProcessor.__name__: ad.LidarArrowDefinition,
    ShellyRedCollectionProcessor.__name__: ad.ShellyRedArrowDefinition,
    ShellyEmCollectionProcessor.__name__: ad.ShellyEmArrowDefinition,
    EnvironmentCollectionProcessor.__name__: ad.EnvironmentArrowDefinition,
    DoorCollectionProcessor.__name__: ad.DoorArrowDefinition,
    LuminanceCollectionProcessor.__name__: ad.LuminanceArrowDefinition,
    WaterflowCollectionProcessor.__name__: ad.WaterflowArrowDefinition,
    SeismographCollectionProcessor.__name__: ad.SeismographArrowDefinition,
    RadarCollectionProcessor.__name__: ad.RadarArrowDefinition,
    MattressCollectionProcessor.__name__: ad.MattressArrowDefinition,
    MicrophoneCollectionProcessor.__name__: ad.MicrophoneArrowDefinition
}
