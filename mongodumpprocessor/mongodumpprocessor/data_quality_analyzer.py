import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.transforms import Affine2D
import re
import os
import json
import glob
import logging


class DataQualityAnalyzer:
    @staticmethod
    def analyze(base_filepath):
        parquet_files = os.path.join(base_filepath, "*.parquet")
        parquet_filepaths = glob.glob(parquet_files)
        for parquet_filepath in parquet_filepaths:
            logging.info(f"Processing {parquet_filepath}")
            analyzer = None
            if parquet_filepath.__contains__("seismograph_samples"):
                analyzer = SeismoDataQualityAnalyzer(parquet_filepath, output_filepath=base_filepath)
            else:
                analyzer = DataQualityAnalyzer(parquet_filepath, output_filepath=base_filepath)

            analyzer.perform()

    def __init__(self, parquet_filepath: str, output_filepath="", filename_prefix: str = ""):
        self.parquet_filepath = parquet_filepath
        self.df = pd.read_parquet(parquet_filepath, engine="fastparquet")
        self.filename_prefix = filename_prefix
        self.output_filepath = output_filepath

        filename_pattern = re.compile(r"(.+)_samples_(192\.168\.\d+\.\d+)_(\d+).parquet")
        filename = os.path.basename(parquet_filepath)
        matches = re.fullmatch(filename_pattern, filename)

        self.sample_type = matches[1]
        self.ip = matches[2]
        self.number_of_packets = matches[3]

    def plot_errorbars(self, ax, mean_values, median_values, std_values, min_values, max_values):
        x = np.array(range(len(mean_values)))

        translation_x = -500
        transformation = Affine2D().translate(translation_x, 0.0) + ax.transData
        ax.errorbar(
            x,
            mean_values,
            yerr=std_values,
            fmt='o',
            color='orange',
            ecolor='lightgreen',
            elinewidth=3,
            capsize=10,
            transform=transformation,
            label="Mean (orange) and Variance (green)"
        )

        ax.plot(x + translation_x, min_values, 'x', color="blue", label="Minimum")
        ax.plot(x + translation_x, max_values, 'x', color="blue", label="Maximum")
        ax.errorbar(
            x,
            median_values,
            xerr=0.05,
            fmt='.',
            color='red',
            ecolor='red',
            elinewidth=1,
            capsize=0,
            transform=transformation,
            label="Median"

        )
        # plt.show(block=True)

    def perform(self):
        row = self.df
        self.perform_for_row(row=row, title=f"{self.filename_prefix}_{self.ip}")

    def perform_for_row(self, row, title: str, run_debug: bool = False):
        if run_debug:
            matplotlib.use('TkAgg')

        logging.info(f"Writing statistics of {title} to {self.output_filepath}...")

        timestamps = row["timestamp"]
        timestamps = timestamps[:1000]

        timestamps = timestamps.to_numpy()
        one_second = np.timedelta64(1_000_000_000, 'ns')

        dts = np.array(timestamps)[1:] - np.array(timestamps)[:-1]
        dts = [dt / one_second for dt in dts]
        dts = np.array(dts)

        total_sample_differences = len(dts)
        first_dt = dts[0]
        mdt = np.mean(dts)
        sdt = np.std(dts)
        dt_deviation = 100.0 * (mdt / first_dt) - 100

        not_expected_indices = np.where(dts != first_dt)
        lost_data_in_seconds = np.sum(dts[not_expected_indices] - first_dt)
        not_expected_indices_count = len(not_expected_indices)
        fraction_of_not_perfect_sampled = not_expected_indices_count / total_sample_differences
        packet_sampling_rate = 1.0 / dts[0]

        start_timestamp_seismo = timestamps[0]
        expected_timestamps = pd.Series(
            range(len(timestamps)),
            index=pd.date_range(
                start_timestamp_seismo,
                periods=len(timestamps),
                freq='{}N'.format(int(1e9 / packet_sampling_rate))
            )
        )
        expected_timestamps = expected_timestamps.index.to_numpy()

        delayed_timestamps = [False for _ in range(len(timestamps))]
        for idx in not_expected_indices[0]:
            delayed_timestamps[idx] = True

        extended_dts = np.append(dts, 0)
        data = {
            'timestamp': timestamps,
            'dt': extended_dts,
            'delayed': delayed_timestamps
        }
        timestamp_df = pd.DataFrame(data)

        if run_debug:
            logging.debug(
                f"Samples with delay: {not_expected_indices_count} / {total_sample_differences} [{fraction_of_not_perfect_sampled * 100.0}%]")
            logging.debug("first delta timestamp: ", first_dt)
            logging.debug("total samples: ", total_sample_differences + 1)
            logging.debug("mean dt [s]: ", mdt)
            logging.debug("std dt [s]: ", sdt)
            logging.debug("max dt [s]: ", np.max(dts))
            logging.debug("min dt [s]: ", np.min(dts))
            logging.debug(f"deviation mean dt from first dt [s]: {dt_deviation}%")
            logging.debug("Time gap in seconds [s]: ", lost_data_in_seconds)
            logging.debug(f"Packet sampling rate: {packet_sampling_rate}")
            logging.debug(f"FIRST Timestamps: given={timestamps[0]} expected={expected_timestamps[0]}")
            logging.debug(f"LAST Timestamps: given={timestamps[-1]} expected={expected_timestamps[-1]}")

        statistics = {
            "ip": self.ip,
            "type": self.sample_type,
            "title": title,
            "not_equal_mean_sample_count": not_expected_indices_count,
            "total_sample_count": total_sample_differences,
            "percentage_samples_deviating": fraction_of_not_perfect_sampled * 100.0,
            "first_delta_timestamp": first_dt,
            "deviation_expected_sampling_rate_from_mean": dt_deviation,
            "mean_dt": mdt,
            "std_dt": sdt,
            "max_dt": np.max(dts),
            "median_dt": np.median(dts),
            "min_dt": np.min(dts),
            "lost_data_in_seconds": lost_data_in_seconds,
            "packet_sampling_rate": packet_sampling_rate,
            "first_timestamp_given": str(timestamps[0]),
            "first_timestamp_expected": str(expected_timestamps[0]),
            "last_timestamp_given": str(timestamps[-1]),
            "last_timestamp_expected": str(expected_timestamps[-1]),
        }

        fig, axs = plt.subplots(2, 2, figsize=[32, 24])
        fig.suptitle(f"Timestamp Statistics {title}")

        # axs[0].set_title("Timestamp Sampling Continuity")
        # axs[0].plot(
        #     range(len(expected_timestamps)),
        #     expected_timestamps,
        #     color="red",
        #     alpha=0.4,
        #     label="expected timestamps"
        # )
        # axs[0].plot(timestamps, color="blue", alpha=0.4, label="given timestamps")
        # plt.setp(axs[0], xlabel="Sample Index")
        # plt.setp(axs[0], ylabel="Time")
        # axs[0].legend()

        # TODO: remove me,
        dts = dts[np.where(dts <= 0.231)]
        dts = dts[np.where(dts >= 0.226)]

        #dts = dts[np.where(dts > np.quantile(dts, q=0.001))]

        axs[0][0].set_title("Delay between Samples")
        axs[0][0].plot(dts, ".", alpha=1.0, label="timestamp difference")
        plt.setp(axs[0][0], xlabel="Sample Index")
        plt.setp(axs[0][0], ylabel="Delay [s]")
        # axs[0].axline((0, np.mean(dts)), (len(dts)-1, np.mean(dts)), color="red")
        # axs[0].boxplot(dts)



        # print(np.max(dts))
        # print(np.min(dts))
        # return

        # self.plot_errorbars(
        #     axs[0],
        #     mean_values=[np.mean(dts)],
        #     median_values=[np.median(dts)],
        #     std_values=[np.std(dts)],
        #     min_values=[np.min(dts)],
        #     max_values=[np.max(dts)],
        # )
        axs[0][0].legend()

        axs[0][1].set_title("Distribution of Sampling Rate Deviation")
        plt.setp(axs[0][1], xlabel="Delay [s]")
        plt.setp(axs[0][1], ylabel="Number of samples")
        #counts, edges, bars = axs[0][1].hist(dts[not_expected_indices], align="mid")
        counts, edges, bars = axs[0][1].hist(dts, align="mid")
        axs[0][1].bar_label(bars)
        axs[0][1].set_xticks(edges)

        # axs[3].set_title("Boxplot Sampling Rate")
        # axs[3].boxplot([dts], showmeans=True)
        # plt.setp(axs[2], ylabel="Sampling Rate [s]")

        data_filepath = os.path.join(self.output_filepath, "statistics")
        logging.info(f"Writing statistics files to {data_filepath}")
        if not os.path.exists(data_filepath):
            os.mkdir(data_filepath)
            logging.info(f"Directory {data_filepath} created")

        plt.savefig(os.path.join(data_filepath, f"{title}.png"))
        plt.savefig(os.path.join(data_filepath, f"{title}.eps"))

        # print("q=0.9: ", np.quantile(dts, q=0.99))
        print("Mean: ", np.mean(dts))
        print("med: ", np.median(dts))
        print("Std: ", np.std(dts))
        print("max: ", np.max(dts))
        print("min: ", np.min(dts))

        if run_debug:
            plt.show(block=True)

        timestamp_df.to_csv(os.path.join(data_filepath, f"{title}.csv"))

        json_filename = f"{title}.json"
        with open(os.path.join(data_filepath, json_filename), "w") as file:
            json.dump(statistics, file, indent=2)


class SeismoDataQualityAnalyzer(DataQualityAnalyzer):
    def perform(self):
        data = {
            "ehz": self.df[self.df["channel_name"] == "EHZ"],
            "ehe": self.df[self.df["channel_name"] == "EHE"],
            "ehn": self.df[self.df["channel_name"] == "EHN"]
        }
        for channel, row in data.items():
            self.perform_for_row(row=row, title=f"{self.filename_prefix}_{self.ip}_{channel}")
