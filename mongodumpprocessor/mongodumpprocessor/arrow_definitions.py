import logging

import pyarrow as pa
from datetime import datetime


class LidarArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        samples = []
        ips = []
        timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            sample = batch['sample'][:-1].split(",")
            ip = batch['ip']
            timestamp = datetime.fromtimestamp(batch["timestamp"])
            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            samples.append(sample)
            ips.append(ip)
            timestamps.append(timestamp)
            packet_timestamps.append(packet_timestamp)
            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(samples),
            pa.array(ips),
            pa.array(timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('sample', pa.list_(pa.int32())),
            ('ip', pa.utf8()),
            ('timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class ShellyRedArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        powers = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            power = batch['power']
            ip = batch['ip']
            powers.append(power)
            ips.append(ip)

            sample_timestamp = datetime.fromtimestamp(batch["timestamp"])
            sample_timestamps.append(sample_timestamp)

            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            packet_timestamps.append(packet_timestamp)

            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(powers),
            pa.array(ips),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('power', pa.float32()),
            ('ip', pa.utf8()),
            ('sample_timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class ShellyEmArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        powers = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            power = batch['power']
            ip = batch['ip']
            powers.append(power)
            ips.append(ip)

            sample_timestamp = datetime.fromtimestamp(batch["timestamp"])
            sample_timestamps.append(sample_timestamp)

            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            packet_timestamps.append(packet_timestamp)

            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(powers),
            pa.array(ips),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('power', pa.float32()),
            ('ip', pa.utf8()),
            ('sample_timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class EnvironmentArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        temperatures = []
        humidities = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            temperature = batch['temperature']
            humidity = batch['humidity']
            ip = batch['ip']
            sample_timestamp = datetime.fromtimestamp(float(batch["timestamp"]))
            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            temperatures.append(temperature)
            humidities.append(humidity)
            ips.append(ip)
            sample_timestamps.append(sample_timestamp)
            packet_timestamps.append(packet_timestamp)
            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(temperatures),
            pa.array(humidities),
            pa.array(ips),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('temperature', pa.float32()),
            ('humidity', pa.float32()),
            ('ip', pa.utf8()),
            ('sample_timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class DoorArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        door_open_events = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            door_open_events.append(batch['doorOpen'])
            ips.append(batch['ip'])

            sample_timestamp = datetime.fromtimestamp(batch["timestamp"])
            sample_timestamps.append(sample_timestamp)

            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            packet_timestamps.append(packet_timestamp)

            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(door_open_events),
            pa.array(ips),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('door_open', pa.bool_()),
            ('ip', pa.utf8()),
            ('sample_timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class LuminanceArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        reds = []
        greens = []
        blues = []
        clears = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            red = batch['red']
            green = batch['green']
            blue = batch['blue']
            clear = batch['clear']
            ip = batch['ip']
            sample_timestamp = datetime.fromtimestamp(batch["timestamp"])
            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])
            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            reds.append(red)
            greens.append(green)
            blues.append(blue)
            clears.append(clear)
            ips.append(ip)
            sample_timestamps.append(sample_timestamp)
            packet_timestamps.append(packet_timestamp)
            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(reds),
            pa.array(blues),
            pa.array(greens),
            pa.array(clears),
            pa.array(ips),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('red', pa.float32()),
            ('blue', pa.float32()),
            ('green', pa.float32()),
            ('clear', pa.float32()),
            ('ip', pa.utf8()),
            ('sample_timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class WaterflowArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        frequencies = []
        pulses = []
        ips = []
        sample_timestamps = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            frequency = batch['frequency']
            pulse = batch['pulses']
            ip = batch['ip']
            sample_timestamp = datetime.fromtimestamp(float(batch['timestamp']))
            packet_timestamp = datetime.fromtimestamp(float(batch['packet_timestamp']))
            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            frequencies.append(frequency)
            pulses.append(pulse)
            ips.append(ip)
            sample_timestamps.append(sample_timestamp)
            packet_timestamps.append(packet_timestamp)
            trial_numbers.append(trial_number)
            task_uuids.append(task_uuid)

        return [
            pa.array(frequencies),
            pa.array(pulses),
            pa.array(ips),
            pa.array(packet_timestamps),
            pa.array(sample_timestamps),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('frequency', pa.float32()),
            ('pulse_count', pa.float32()),
            ('ip', pa.utf8()),
            ('packet_timestamp', pa.timestamp("ns")),
            ('sample_timestamp', pa.timestamp("ns")),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class SeismographArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        sample_measurements = []
        sample_timestamps = []
        channel_names = []
        ips = []
        packet_timestamps = []
        trial_numbers = []
        task_uuids = []

        for batch in data:
            ip = batch["ip"]
            packet_timestamp = datetime.fromtimestamp(batch["packet_timestamp"])

            trial_number = batch["trial_number"]
            task_uuid = batch["task_uuid"]

            for item in batch["data"]:
                data_string = item.replace("{", "").replace("}", "")
                geophone_channel_name, item_timestamp, *measurements = data_string.split(",")

                geophone_channel_name = geophone_channel_name.replace("\'", "")
                item_timestamp = datetime.fromtimestamp(float(item_timestamp))

                measurements = list(map(lambda m: float(m), measurements))

                sample_measurements.append(measurements)
                packet_timestamps.append(packet_timestamp)
                sample_timestamps.append(item_timestamp)
                channel_names.append(geophone_channel_name)
                ips.append(ip)

                trial_numbers.append(trial_number)
                task_uuids.append(task_uuid)

        return [
            pa.array(sample_measurements),
            pa.array(sample_timestamps),
            pa.array(packet_timestamps),
            pa.array(channel_names),
            pa.array(ips),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('measurement', pa.list_(pa.float32())),
            ('timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('channel_name', pa.utf8()),
            ('ip', pa.utf8()),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class RadarArrowDefinition:
    @classmethod
    def arrays_from(cls, data: list):
        """
        @param data: created in ParquetWriter.dump()
        """

        # Acronyms similar to RF-Beam's datasheet:
        # rx1 receiver antenna 1, frequency A, ADC channel I

        rx1_freq_a_channel_i_data = []
        rx1_freq_a_channel_q_data = []
        rx2_freq_a_channel_i_data = []
        rx2_freq_a_channel_q_data = []
        rx1_freq_b_channel_i_data = []
        rx1_freq_b_channel_q_data = []
        fft_data = []
        threshold_data = []
        timestamps = []
        packet_timestamps = []
        sample_counts = []
        packet_counts = []
        producer_ips = []
        trial_numbers = []
        task_uuids = []

        for item in data:
            if item["type"] == "adc":
                packet_timestamp = item["timestamp"]
                packet_timestamp = datetime.fromtimestamp(packet_timestamp)

                samples = item["samples"]
                producer_ip = item["ip"]
                packet_count = item["count"]
                trial_number = item["trial_number"]
                task_uuid = item["task_uuid"]

                for sample in samples:
                    try:
                        rx1_freq_a_channel_i = []
                        rx1_freq_a_channel_q = []
                        rx2_freq_a_channel_i = []
                        rx2_freq_a_channel_q = []
                        rx1_freq_b_channel_i = []
                        rx1_freq_b_channel_q = []
                        fft = []
                        threshold = []

                        if sample.__contains__("adc_a1_i"):
                            rx1_freq_a_channel_i = sample["adc_a1_i"]
                            rx1_freq_a_channel_q = sample["adc_a1_q"]
                            rx2_freq_a_channel_i = sample["adc_a2_i"]
                            rx2_freq_a_channel_q = sample["adc_a2_q"]
                            rx1_freq_b_channel_i = sample["adc_b1_i"]
                            rx1_freq_b_channel_q = sample["adc_b1_q"]
                            sample_count = sample["counter"]

                        if sample.__contains__("fft_data"):
                            fft = sample["fft_data"]
                            threshold = sample["threshold"]

                        rx1_freq_a_channel_i_data.append(rx1_freq_a_channel_i)
                        rx1_freq_a_channel_q_data.append(rx1_freq_a_channel_q)
                        rx2_freq_a_channel_i_data.append(rx2_freq_a_channel_i)
                        rx2_freq_a_channel_q_data.append(rx2_freq_a_channel_q)
                        rx1_freq_b_channel_i_data.append(rx1_freq_b_channel_i)
                        rx1_freq_b_channel_q_data.append(rx1_freq_b_channel_q)
                        sample_counts.append(sample_count)
                        fft_data.append(fft)
                        threshold_data.append(threshold)
                        packet_counts.append(packet_count)
                        trial_numbers.append(trial_number)

                        # is already a number
                        sample_timestamp = datetime.fromtimestamp(sample["timestamp"])
                        timestamps.append(sample_timestamp)

                        # TODO: check is item["packet_timestamp"] the same as item["timestamp"] ?
                        packet_timestamps.append(packet_timestamp)
                        producer_ips.append(producer_ip)
                        task_uuids.append(task_uuid)

                    except Exception as error:
                        logging.error(error)
            else:
                logging.error(f"Not supported radar type: {item['type']}")

        return [
            pa.array(rx1_freq_a_channel_i_data),
            pa.array(rx1_freq_a_channel_q_data),
            pa.array(rx2_freq_a_channel_i_data),
            pa.array(rx2_freq_a_channel_q_data),
            pa.array(rx1_freq_b_channel_i_data),
            pa.array(rx1_freq_b_channel_q_data),
            pa.array(fft_data),
            pa.array(threshold_data),
            pa.array(timestamps),
            pa.array(packet_timestamps),
            pa.array(producer_ips),
            pa.array(sample_counts),
            pa.array(packet_counts),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('rx1_freq_a_channel_i_data', pa.list_(pa.float32())),
            ('rx1_freq_a_channel_q_data', pa.list_(pa.float32())),
            ('rx2_freq_a_channel_i_data', pa.list_(pa.float32())),
            ('rx2_freq_a_channel_q_data', pa.list_(pa.float32())),
            ('rx1_freq_b_channel_i_data', pa.list_(pa.float32())),
            ('rx1_freq_b_channel_q_data', pa.list_(pa.float32())),
            ('fft', pa.list_(pa.float32())),
            ('threshold', pa.list_(pa.float32())),
            ('timestamp', pa.timestamp("ns")),
            ('packet_timestamp', pa.timestamp("ns")),
            ('ip', pa.utf8()),
            ('sample_count', pa.int32()),
            ('packet_count', pa.int32()),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class MattressArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        samples = list(map(lambda item: list(map(lambda s: s["samples"], item['data']))[0], data))

        # those are already
        sample_timestamps = list(
            map(lambda item: list(
                map(lambda s: list(
                    map(lambda k: datetime.fromtimestamp(float(k)), s["timestamps"])),
                    item['data']))[0],
                data))
        ips = list(map(lambda item: item['ip'], data))
        packet_timestamps = list(map(lambda item: datetime.fromtimestamp(float(item['timestamp'])), data))
        packet_counts = list(map(lambda item: item['count'], data))
        trial_numbers = list(map(lambda item: item['trial_number'], data))
        task_uuids = list(map(lambda item: item['task_uuid'], data))

        return [
            pa.array(samples),
            pa.array(sample_timestamps),
            pa.array(ips),
            pa.array(packet_timestamps),
            pa.array(packet_counts),
            pa.array(trial_numbers),
            pa.array(task_uuids)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('samples', pa.list_(pa.utf8())),
            ('sample_timestamps', pa.list_(pa.timestamp("ns"))),
            ('ip', pa.utf8()),
            ('packet_timestamp', pa.timestamp("ns")),
            ('packet_count', pa.int32()),
            ('trial_number', pa.int32()),
            ('task_uuid', pa.utf8()),
        ])


class MicrophoneArrowDefinition:
    @classmethod
    def arrays_from(cls, data):
        ips = list(map(lambda item: item['ip'], data))
        timestamps = list(map(lambda item: float(item['timestamp']), data))
        microphone_data = list(map(lambda item: item['data'], data))
        data_type = list(map(lambda item: item['dataType'], data))

        return [
            pa.array(ips),
            pa.array(timestamps),
            pa.array(microphone_data),
            pa.array(data_type)
        ]

    @classmethod
    def schema(cls):
        return pa.schema([
            ('ip', pa.utf8()),
            ('timestamp', pa.float32()),
            ('data', pa.string()),
            ('data_type', pa.string())
        ])
