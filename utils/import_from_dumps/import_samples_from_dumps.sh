#!/bin/bash

echo "This script assums that a mongo db is running on localhost. Adapt the content of the .env file for the credentials to connect to the DB."

# Abort on error
set -e

source .env

if [ -z "$1" ]; then
  echo "No argument supplied. Please provide the path of a directory containing the dumps. Example usage: ./import_samples_from_dump.sh ../data/exports/EXP_UUID/PARTICIPANT_UUID/"
  exit 1
fi

directory=$1

# List all dump files (extension *.dump)
dump_files=($(ls $directory/*.dump))

for dump_file in "${dump_files[@]}"
do
  echo -e "\nRestoring from archive: $dump_file"

  mongorestore --uri="mongodb://$MONGODB_USER:$MONGODB_PASSWORD@localhost:27017/?authSource=admin" --archive="$dump_file" --nsFrom="$MONGODB_DB_NAME.*" --nsTo="$MONGODB_DB_NAME.*"


done

echo "Done"
