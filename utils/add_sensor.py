import json
import sys

filepath = sys.argv[1]

print(filepath)
sensor_file_data = {}
with open(filepath, "r") as file:
    sensor_file_data = json.load(file)

    while True:
        print("Available sensor types:")
        for idx, sensor_type in enumerate(sensor_file_data.keys()):
            print(f"[{idx}] {sensor_type}")


        sensor_type_idx = input(f"Please select a sensor type by number (from 0 to {len(sensor_file_data.keys())- 1}): ")
        sensor_type = list(sensor_file_data.keys())[int(sensor_type_idx)]

        sensor_ip_address = input("Sensor IP (e.g., 192.168.13.37): ")
        sensor_location = input("Location (e.g., Kitchen): ")
        sensor_name = input("Sensor Name (e.g., radar13): ")

        item = {
            "ip": sensor_ip_address.strip(),
            "name": sensor_name.strip(),
            "location": sensor_location.strip(),
            "save_data": True
        }
        sensor_file_data[sensor_type].append(item)

        should_abort = input("Do you want to add more sensor? (type y to continue): ")
        if not should_abort.strip() == "y":
            break

print(f"Writing new sensors to {filepath}...")
json_object = json.dumps(sensor_file_data, indent=4)
# Writing to sample.json
with open(filepath, "w") as outfile:
    outfile.write(json_object)
