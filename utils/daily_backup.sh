#!/bin/bash

# Source required env variables
source .env.prod

current_date=$(date +"%Y-%m-%d")
echo "Running backup scripts for $current_date"

# Output folder (format : backups/2021/08/2021-08-12_backup)
folder_month=$(date +"backups/%Y/%m")
folder_name="${current_date}_backup"
output_folder="${folder_month}/${folder_name}"

mkdir -p $output_folder

echo "Create daily backup in folder $output_folder"

# Names of the collections to backup daily (we save all collections which are not samples)
collections_names=(
  "active_experiments"
  "active_export_jobs"
  "bookings"
  "configs"
  "experiment_logs"
  "experiments"
  "export_jobs"
  "participants"
  "progress_participants"
  "rooms"
  "screen_messages"
  "users"
)

# Backup each collection using mongodump (we produce an archive *.dump)
for collection_name in "${collections_names[@]}"
do
  echo -e "\nBackup collection \"$collection_name\": \n"
  archive_name="${collection_name}.dump"
  mongodump --forceTableScan \
            --host=127.0.0.1 \
            --port=27017 \
            --authenticationDatabase="admin" \
            --username=${MONGODB_USER} \
            --password=${MONGODB_PASSWORD} \
            --db=${MONGODB_DB_NAME} \
            --collection="$collection_name" \
            --archive="${output_folder}/${archive_name}"
done

echo -e "\nBackup complete"



echo "Compressing backups"

# tar options:
# -C: this changes the directory before adding the following files
# -c: create archive
# -j: compress the resulting archive using bzip2
# -f: with filename
tar -C $folder_month -cjf "${output_folder}.tar.bz2" $folder_name

# Remove folder (use variable expansion in case the variable $folder_name is empty)
# If output_folder is not defined, will abort current script
rm -r "${output_folder:?Missing variable output_folder}"

echo "Backup complete."