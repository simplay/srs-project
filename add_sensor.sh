#!/bin/bash

AAA=`grep -A1 'PIPELINE_SENSORS_FILEPATH' .env.dev`
BBB=`echo $AAA | awk '{split($0,a,"="); print a[2]}'`
CCC=$PWD/modules/sensor-monitoring-service/$BBB

cat $CCC
python3 utils/add_sensor.py $CCC
cat $CCC
