# SRS Project

The Sensor Recording Software (SRS) can be used to set up a flexible recording system that allows a systematic
integrating of sensing devices.

Additional information and supplementary material can be found here: https://simplay.github.io/srs/ 

# Installation

The SRS system is executed in form of docker containers, which are orchestrated via docker-compose. To increase the
stability of the project, restart policies have been defined. The volumes of all containers are mounted in the top-level
directory `data`. Further details can be found in the docker-compose configuration files.

The installation process in this README is only documented for Debian based distribution. However, the SRS is not limited to Debian based distributions.

## Requirements

Make sure that you have installed the following programs:

+ `docker`: https://docs.docker.com/engine/install/ubuntu/
+ `docker-compose`: https://docs.docker.com/compose/install/
+ `python3` and `bcrypt` (required to inject users and their credentials in DB). Installation:

## Setup

1. Create a new environment variable file: `cp .env.example .env`.
2. Specify the environment variables by modifying the content of `.env`.
3. Start the installation process: `sudo bash quick.install.sh`
4. Create a new admin user: `sudo bash init_admin_user.sh`
5. Add new sensors to the pipeline: `sudo bash add_sensor.sh`

## Usage

After executing the script `quick_install.sh` the SRS pipeline will be started. You can restart the pipeline by
executing `sudo bash quick_restart.sh`.

Alternatively, you can directly start and stop all involved docker containers by using `make`: `sudo make up` to start
pre-build container; `sudo make stop` to start all running SRS containers.

# Available Environment Variables

The following variables can be modified (see `.env`):

- `GITLAB_OAUTH_GENERIC_CLIENT_ID`: the ID of the gitlab group application used for gitlab authentication (
  in `chronograf`),
- `GITLAB_OAUTH_GENERIC_CLIENT_SECRET`: the secret of the gitlab group application used for gitlab authentication (
  in `chronograf`),
- `GITLAB_OAUTH_TOKEN_SECRET`: a token secret to define between `chronograf` and Gitlab,
- `ID_RSA`: the private ssh key which is used to ssh and deploy the solution on the production machine (the one used by
  the deployer user),
- `SERVER_IP`: the IP of the server on which to deploy the containers (production machine),
- `SERVER_USER`: the name of the user which is used to deploy the solution on the production machine, i.e. `deployer`
- `INFLUXDB_ADMIN_PASSWORD`: the password for the admin user of the influxdb,
- `INFLUXDB_READ_USER_PASSWORD`: the password for the user with read only access to the influxdb,
- `INFLUXDB_WRITE_USER_PASSWORD`:  the password for the user with write access to the influxdb,
- `KAP_SMTP_FROM`: the email address used to send alerts by `kapacitor`,
- `KAP_SMTP_HOST`: the SMTP host used to send alerts by `kapacitor` (`cicero.metanet.ch`),
- `KAP_SMTP_PASSWORD`: the password of the email account used to send alerts by `kapacitor`,
- `KAP_SMTP_PORT`: the port used for SMTP service,
- `KAP_SMTP_TO`: the email recipient to which are sent the alerts
- `KAP_SMTP_USERNAME`: the username of the SMTP user,
- `MONGODB_USER`: the root user of the mongodb database,
- `MONGODB_PASSWORD`: the password of root user of the mongodb database,
- `MONGODB_DB_NAME`: the name of the mongodb database,
- `NGINX_SSL_CERT_FULLCHAIN`: the cert fullchain of the SSL wildard certificate,
- `NGINX_SSL_CERT_PRIVKEY`: the private key of the SSL wildcard certificate,
- `PIPELINE_SENSORS_FILEPATH`: relative path from the `srs-backend` to a `*.sensors.json` file, located in the
  submodule `neurotec-sensors`.
- `SENSOR_MONITORING_SERVICE_API_URL`: URL of the sensor monitoring service API
- `SENSOR_MONITORING_SERVICE_AUTHENTICATED_WEBSOCKETS_URL`: URL to open the authenticated websockets (sensors and
  cameras websockets)
- `SENSOR_MONITORING_SERVICE_OPEN_WEBSOCKETS_URL`: URL to open the non-authenticated websockets (active experiments
  status websocket)
- `SLACK_CHANNEL`: slack channel to which monitoring alerts can be sent by `kapacitor`,
- `SLACK_URL`: the SLACK url used for sending alerts by `kapacitor`,
- `SLACK_WORKSPACE`: the SLACK workspace used for sending alerts by `kapacitor`,
- `SMS_JWT_PRIVATE_KEY`: private key for JWT management,
- `SMS_JWT_PRIVATE_KEY_FILEPATH`: path to the private key for the JWT management,
- `SMS_JWT_PUBLIC_KEY`: public key for JWT management,
- `SMS_JWT_PUBLIC_KEY_FILEPATH`: path to the public key for the JWT management,
- `SMS_JWT_VALIDITY_LENGTH`: validity period for the JWT,
- `SMS_SMTP_HOST`: the host used to send mails for the `srs-backend` (bookings),
- `SMS_SMTP_PASSWORD`: the password of the account used to send mails for the `srs-backend` (bookings),
- `SMS_SMTP_USER`: the user used to send mails for the `srs-backend` (bookings),
- `SMS_SMTP_FROM`: the from address used to send mails for the `srs-backend` (bookings),

# Project Structuring

The SRS system consists of a composition of projects, located at:

+ Backend Service that Implements the APIs, Database: `modules/srs-backend`
+ Frontend Application to Control Sensor Recordings: `modules/srs-frontend`
+ Sensor Definitions: `modules/srs-backend/modules/neurotec-sensors`
+ Reverse Proxy: `nginx`
+ Monitoring of Sensor Streams: `monitoring/tick-stack`
+ Example Sensor Software in Python (Water Flowmeter): `examples-sensor-software/flowmeter`
+ Database Processor: `mongodumpprocessor`
+ Utility tools, such as a MongoDB administrator tool and a backup system: `util`

# Usage Examples

## Starting / Stop recording

The process to start / stop a recording is done through the `SRS-Frontend`.

1. Login to the `SRS-Frontend`
2. Load or create a configuration
3. Make the configuration active
4. Start the recording
5. Stop the recording

A step-by-step guide how to perform all of those steps can be found [here](https://simplay.github.io/srs/assets/files/101e672a-8bf5-4fc4-ad0d-413d687267f5-67816458adc7c67742972d4e3af60547.pdf).

Notes:
- A recording can only be started if the DB is empty.
- You can delete the DB from the `data management`'s page (admin only).
## Exporting data

Use the `SensorMonitoringDashboard` to export data. The data of the active experiment must be exported before starting another experiment. In other words, a new experiment recording can only be started with an empty database (samples collections). In order to export data, navigate to `Admin -> Experiments -> Data management` and click the `Export data active experiment` button. You will be asked to select the date for which to export the data. It is responsibility of the experiment owner to organize and backup the data exported via the dashboard.


## Monitoring Sensor Streams

We use the TICK stack from `InfluxData` to monitor the production machine and some processes of the SRS system.

The name TICK comes from the intial of those components:

- **T**elegraf: agent for collecting and reporting metrics and events
- **I**nfluxDB: the time series database
- **C**hronograph: the interface to manage the entire InfluxData platform
- **K**apacitor: real-time streaming data processing engine, also used for alerting

The monitoring dashboard (`Chronograf`) can be accessed at the following url: https://localhost:8888

## Working with MongoDB container

```sh
# Enter MongoDB container
docker-compose --env-file .env.dev -f docker-compose.yml -f docker-compose.dev.yml exec mongo /bin/bash

# From inside container

# Open MongoDB connection
mongo -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD"
```

## Working with Database Dumps

Export DB dump manually:

```sh
docker-compose --env-file .env.dev -f docker-compose.yml -f docker-compose.dev.yml exec -T mongo sh -c 'mongodump --username=${MONGO_INITDB_ROOT_USERNAME} --password=${MONGO_INITDB_ROOT_PASSWORD} --archive' > ./backups/the-name-for-the-dbdump.dump
```

Restore DB dump

```sh
docker-compose --env-file .env.dev -f docker-compose.yml -f docker-compose.dev.yml exec -T mongo sh -c 'mongorestore --username=${MONGO_INITDB_ROOT_USERNAME} --password=${MONGO_INITDB_ROOT_PASSWORD} --archive' < ./backups/the-name-for-the-dbdump.dump
```

## Structure of Exported Data

The exported experiment data is available in folder `./data/exports/[EXPERIMENT_UUID]/`. (The `SensorMonitoringService` exports the data in the `./exports` folder which is mounted in the `./data` folder on the host, alongside the other persistent data.)

The structure of the exported data is as follows:
- [EXPERIMENT_UUID]/:
  - experiment_[current_datetime].json: contains the details of the experiment at the time the export is executed (current_datetime). This includes the sensors used, participants, owner, operators, tasks, ...
  - experiment_log_[current_datetime].json: contains the part of the logbook associated to the experiment being exported at the time the export is executed (current_datetime).
  - progress_participant_[current_datetime].json: contains the progress of the participants for the experiment at the time the export is executed (current_datetime).
  - [PARTICIPANT_UUID]:
    - [date_selected_for_export]_[collection_type]_samples.dump: mongo dump of the samples collections for the date being selected in export.

# License

This software is available under the following [MIT](LICENSE) license.
