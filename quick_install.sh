#!/bin/bash

echo "Downloading dependencies..."
pip install bcrypt

echo "Initializing .env.dev file..."
cp .env .env.dev
HOST_IP=`hostname -I | awk {'print $1'}`
SED_CMD=s/a.b.c.d/$HOST_IP/
sed -i $SED_CMD .env.dev

echo "Generating empty sensor file quick_installer.sensors.json ..."
cp $PWD/modules/srs-backend/modules/neurotec-sensors/empty.sensors.json $PWD/modules/srs-backend/modules/neurotec-sensors/quick_installer.sensors.json

echo "Generating web-tokens..."
sudo bash $PWD/modules/srs-backend/certs/generate_jwt_certificates.sh
sleep 2
sudo ln -s $PWD/modules/srs-backend/certs $PWD/certs
mv *.pem certs/

echo "Starting system..."
make build
make up

make ps
