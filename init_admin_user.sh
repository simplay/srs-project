#!/bin/bash


# exit when any command fails
set -e

echo "This script will insert a first user with \"admin\" role so that you can log into the Sensor Monitoring Dashboard and start managing users and experiments."

read -p "Enter username for admin user: " username
read -p "Enter password for admin user: " -s password

echo

# Hash password
# We use bcrypt with 10 rounds. Salt is included in returned hash. We skip "$" to use the hash in the next command (to inject into mongoDB)
hashed_password=$(python3 -c 'import bcrypt; print(bcrypt.hashpw(b"'$password'", bcrypt.gensalt(rounds=10)).decode("utf-8").replace("$","\$"))')

now=$(date +%s)

# Inject the user account in the users collection
docker-compose --env-file .env.dev -f docker-compose.yml -f docker-compose.dev.yml exec -T srs-mongo sh -c 'exec mongo $MONGO_INITDB_DATABASE -u "$MONGO_INITDB_ROOT_USERNAME" -p "$MONGO_INITDB_ROOT_PASSWORD"  --authenticationDatabase admin --eval "db.users.insertOne({firstname: \"'$username'\",lastname: \"'$username'\", username: \"'$username'\", password: \"'$hashed_password'\", createdAt: '$now', roles: [\"admin\", \"operator\", \"secretariat\"]})"'


