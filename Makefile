.PHONY: status

DEV_DC = docker-compose --env-file .env.dev -f docker-compose.yml -f docker-compose.dev.yml
up:
	$(DEV_DC) up -d --scale srs-gitlab-runner=0

stop:
	$(DEV_DC) stop

rm:
	$(DEV_DC) rm -fs

build:
	$(DEV_DC) build

ps:
	$(DEV_DC) ps

run_mongo:
	$(DEV_DC) up -d srs-mongo

rm_mongo:
	$(DEV_DC) rm -fs srs-mongo

rm_tick_stack:
	$(DEV_DC) rm -fs srs-influxdb srs-telegraf srs-kapacitor srs-chronograf

build_tick_stack:
	$(DEV_DC) build --no-cache srs-influxdb srs-telegraf srs-kapacitor srs-chronograf

run_tick_stack:
	$(DEV_DC) up -d srs-influxdb srs-telegraf srs-kapacitor srs-chronograf

rm_frontend:
	$(DEV_DC) rm -fs srs-backend srs-frontend srs-nginx